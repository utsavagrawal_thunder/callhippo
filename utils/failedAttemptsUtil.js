var commonDb = require('../models/resources/commonDb');
var mongoose = require('mongoose');

var modelName = 'failedAttempts';

var userUtils = {
    insertfailedAttempts: function (insertObject) {
        return new Promise(function (resolve, reject) {
            commonDb.createDocument(modelName, insertObject)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },

    updatefailedAttempts: function (queryObject, updateObject) {
        return new Promise(function (resolve, reject) {
            commonDb.updateDocument(modelName, queryObject, updateObject)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },

    getfailedAttemptsByFindCriteria: function (findCriteria) {
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, findCriteria)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },
    getAllfailedAttempts: function () {
        return new Promise(function (resolve, reject) {
            commonDb.findAll(modelName)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },

    getfailedAttemptsById: function (userId) {
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, {_id: mongoose.Types.ObjectId(userId)})
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },
    deleteByParam: function (paramObject) {
        return new Promise(function (resolve, reject) {
            commonDb.deleteByParam(modelName, paramObject)
                    .then(function (responseObject) {
                        console.log(responseObject);
                        resolve(responseObject);
                    })
                    .catch(function (errorObject) {
                        console.log("error" + errorObject);
                        reject(errorObject);
                    })
        });
    }

};

module.exports = userUtils;