var commonDb = require('../models/resources/commonDb');
var mongoose = require('mongoose');
var userDb = require('../models/resources/userDb');

var modelName = 'user';

var userUtils = {
    insertUser: function (insertObject) {
        return new Promise(function (resolve, reject) {
            commonDb.createDocument(modelName, insertObject)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },

    updateUser: function (queryObject, updateObject) {
        return new Promise(function (resolve, reject) {
            commonDb.updateDocument(modelName, queryObject, updateObject)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },

    getUserByFindCriteria: function (findCriteria) {
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, findCriteria)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },
    getAllUsers: function () {
        return new Promise(function (resolve, reject) {
            commonDb.findAll(modelName)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },

    getUserById: function (userId) {
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, {_id: mongoose.Types.ObjectId(userId)})
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },

    checkLogin: function (insertObject) {
        return new Promise(function (resolve, reject) {
            userDb.checkPassword(insertObject)
                    .then(function (respObject) {
                        console.log(respObject);
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        console.log("error" + errObject);
                        reject(errObject);
                    });
        });
    },
    getByCustom: function(dataObj) {
        return new Promise(function (resolve, reject) {
            commonDb.findByCustom(modelName, dataObj)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },
    deleteByParam: function (paramObject) {
        return new Promise(function (resolve, reject) {
            commonDb.deleteByParam(modelName, paramObject)
                    .then(function (responseObject) {
                        console.log(responseObject);
                        resolve(responseObject);
                    })
                    .catch(function (errorObject) {
                        console.log("error" + errorObject);
                        reject(errorObject);
                    })
        });
    }

};

module.exports = userUtils;