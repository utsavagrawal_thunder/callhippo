var commonDb = require('../models/resources/commonDb');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

var modelName = 'ussdRegexpMissing';
var ussdRegexpMissingUtil = {
    insert: function (insertObject) {
        return new Promise(function (resolve, reject) {
            commonDb.createDocument(modelName, insertObject)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },
    update: function (queryObject, updateObject) {
        return new Promise(function (resolve, reject) {
            commonDb.updateDocument(modelName, queryObject, updateObject)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },
    getAll: function () {
        return new Promise(function (resolve, reject) {
            commonDb.findAll(modelName)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },
    getById: function(id){
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, {_id: mongoose.Types.ObjectId(id)})
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },
    getByFindCriteria: function(findCriteria) {
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, findCriteria)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },
    getByCustom: function(dataObj) {
        return new Promise(function (resolve, reject) {
            commonDb.findByCustom(modelName, dataObj)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },
    getByAggregate: function(aggregationPipeline) {
        return new Promise(function (resolve, reject) {
            commonDb.aggregateFetch(modelName, aggregationPipeline)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    }
};

module.exports = ussdRegexpMissingUtil;