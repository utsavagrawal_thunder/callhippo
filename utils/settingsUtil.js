var commonDb = require('../models/resources/commonDb');
var mongoose = require('mongoose');
var userDb = require('../models/resources/userDb');


var modelName = 'settings';
var oldModelName = 'settingsArchive';

var settingsUtils = {
    insertSettings: function (insertObject) {
        var oldSettingsId;
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, {})
                .then(function (respFoundSettingsObject) {
//                    resolve(respObject);
                    console.log("respFoundSettingsObject[0]");
                    console.log(respFoundSettingsObject[0]);
                    oldSettingsId = respFoundSettingsObject[0]._id;
                    var archivedSettingsObject = JSON.parse(JSON.stringify(respFoundSettingsObject[0]));
                    return commonDb.createDocument(oldModelName, archivedSettingsObject)
                    
                })
                .then(function(respArchiveInsertObject){
                        console.log("Response of archiving old settings");
                        console.log(respArchiveInsertObject);
                        return commonDb.deleteByParam(modelName, {_id: oldSettingsId})
                })
                .then(function(responseDeletedObject){
                    console.log("successfully deleted");
                    console.log(responseDeletedObject);
                    return commonDb.createDocument(modelName, insertObject)
                })
                .then(function(newSettingsInsertResponse){
                        console.log(newSettingsInsertResponse);
                        console.log("newSettingsInsertResponse");
                        resolve(newSettingsInsertResponse);
                })
                .catch(function (errObject) {
                    console.log("error in creating new settings"+errObject);
                    reject(errObject);
                });
            
        });
    },

    updateSettings: function (queryObject, updateObject) {
        return new Promise(function (resolve, reject) {
            commonDb.updateDocument(modelName, queryObject, updateObject)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },

    getSettingsByFindCriteria: function(findCriteria) {
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, findCriteria)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },
    getAllSettings: function () {
        return new Promise(function (resolve, reject) {
            commonDb.findAll(modelName)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },
    getArchivedSettingsByFindCriteria: function (findCriteria) {
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(oldModelName, findCriteria)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    }

};

module.exports = settingsUtils;