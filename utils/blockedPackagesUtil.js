var commonDb = require('../models/resources/commonDb');
var mongoose = require('mongoose');
var modelName = 'blockedPackages';

var callLogUtil = {
    insertBlockedPackages: function (insertObject) {
        return new Promise(function (resolve, reject) {
            commonDb.createDocument(modelName, insertObject)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },

    updateBlockedPackages: function (queryObject, updateObject) {
        return new Promise(function (resolve, reject) {
            commonDb.updateDocument(modelName, queryObject, updateObject)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },

    getBlockedPackagesFindCriteria: function(findCriteria) {
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, findCriteria)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },

    getAllBlockedPackages: function () {
        return new Promise(function (resolve, reject) {
            commonDb.findAll(modelName)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },

    getBlockedPackagesById: function(otpId){
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, {_id: mongoose.Types.ObjectId(otpId)})
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },

    getBlockedPackagesByCustom: function(dataObj) {
        return new Promise(function (resolve, reject) {
            commonDb.findByCustom(modelName, dataObj)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    }
};

module.exports = callLogUtil;