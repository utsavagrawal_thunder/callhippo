var commonDb = require('../models/resources/commonDb');
var mongoose = require('mongoose');

var modelName = 'recharges';

var rechargeUtil = {
    insertRechargeAttempts: function (insertObject) {
        return new Promise(function (resolve, reject) {
            commonDb.createDocument(modelName, insertObject)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },

    updateRechargeAttempts: function (queryObject, updateObject) {
        return new Promise(function (resolve, reject) {
            commonDb.updateDocument(modelName, queryObject, updateObject)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },

    getRechargeAttemptsByFindCriteria: function (findCriteria) {
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, findCriteria)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },
    getAllRechargeAttempts: function () {
        return new Promise(function (resolve, reject) {
            commonDb.findAll(modelName)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },

    getRechargeAttemptsById: function (userId) {
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, {_id: mongoose.Types.ObjectId(userId)})
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },
    deleteByParam: function (paramObject) {
        return new Promise(function (resolve, reject) {
            commonDb.deleteByParam(modelName, paramObject)
                    .then(function (responseObject) {
                        console.log(responseObject);
                        resolve(responseObject);
                    })
                    .catch(function (errorObject) {
                        console.log("error" + errorObject);
                        reject(errorObject);
                    })
        });
    },
    getCount: function (countQuery) {
        return new Promise(function (resolve, reject) {
            commonDb.getCount(modelName, countQuery)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },
    remove: function (removeObject) {
        return new Promise(function (resolve, reject) {
            commonDb.deleteByParam(modelName, removeObject)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    },
    findByCustomPopulateParams: function (dataObj) {
        return new Promise(function (resolve, reject) {
            commonDb.findByCustomPopulateParams(modelName, dataObj.findCriteria, dataObj.populateParam, dataObj.fetchFields, dataObj.sortCriteria, dataObj.limit, dataObj.skip)
                    .then(function (respObject) {
                        resolve(respObject);
                    })
                    .catch(function (errObject) {
                        reject(errObject);
                    });
        });
    }

};

module.exports = rechargeUtil;