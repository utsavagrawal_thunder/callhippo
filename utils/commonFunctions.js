var mongoose = require("mongoose");
var getSlug = require('speakingurl');
var emailTemplates = require('email-templates');
var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
var Promise = require('bluebird');
var common = require('../config');
var config = common.config();
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var crypto = require('crypto');
var admin = require("firebase-admin");

admin.initializeApp({
    credential: admin.credential.cert(__dirname + "/notification/smartcall-e424a-firebase-adminsdk-0fqg7-5aaa2767ef.json"),
    databaseURL: "https://smartcall-e424a.firebaseio.com"
});

// List of all basic common function
var commonFunctions = {
    isMongoId: function (str) {
        return this.isHexadecimal(str) && str.length === 24;
    },
    isHexadecimal: function (str) {
        var hexadecimal = /^[0-9A-F]+$/i;
        return hexadecimal.test(str);
    },
    isObjectEmpty: function (obj) {
        return (Object.keys(obj).length === 0 && obj.constructor === Object);
    },
    // convert id to mongo object id
    formatJSONArrOfObjData: function (arrStr) {
        var arr;
        try {
            arr = JSON.parse(arrStr);
        } catch (err) {
            arr = [];
        }

        var arrLen = arr.length;
        for (var i = 0; i < arrLen; i++) {
            arr[i]._id = mongoose.Types.ObjectId(arr[i]._id);
        }
        return arr;
    },
    formatJSONObjData: function (objStr) {
        var obj;
        try {
            obj = JSON.parse(objStr);
            obj._id = mongoose.Types.ObjectId(obj._id);

            // add slug for name
            if (obj.hasOwnProperty('name')) {
                obj.slug = getSlug(obj.name);
            }
        } catch (err) {
            obj = {};
        }

        return obj;
    },
    stringToObjectId: function (strId) {
        return mongoose.Types.ObjectId(strId);
    },
    // send email with nodemailer
    sendMail: function (mailOptions, callback) {
        this.sendSmtpEmail(mailOptions)
                .then(function (respObject) {
                    callback(null, respObject);
                })
                .catch(function (errObject) {
                    callback(errObject, null);
                });
    },
    // @Get UNIX timestamp
    getCurrentTimestamp: function () {
        var date = new Date();
        var time = date.getTime();
        var timestamp = Math.round(time / 1000);
        return timestamp;
    },
    sendSmtpEmail: function (mailOptions) {

        return new Promise(function (resolve, reject) {

            var smtpConfig = {
                host: 'smtp.office365.com',
                port: 587,
                auth: {
                    user: 'tanay.surkund@clinito.com',
                    pass: 'discreete123!'
                }
            };

            var transport = nodemailer.createTransport(smtpTransport(smtpConfig));

            console.log("mailOptions");
            console.log(mailOptions);
            transport.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log('Nodemailer error - ', error);
                    reject("Mail Sending Error");
                }
                console.log('Nodemailer resopnse - ', info);
                resolve("sent mail");
                // console.log('Nodemailer sent: ', info.response);

            });

        });
    },
    // send SMS
    // smstype : 0 = Transactional Template,3 = Transactional Optin,4 = Promotional,13 = Transactional OTP,9 = Transactional Template MTNL
    sendSMS: function (mobileNo, msgText, smsType) {
        return new Promise(function (resolve, reject) {
            var url = "http://bulksmsindia.mobi/sendurlcomma.aspx";
            url += '?user=' + config.smsGatewayUser;
            url += '&pwd=' + config.smsGatewayPassword;
            url += '&senderid=' + config.smsGatewaySenderId;
            url += '&mobileno=' + mobileNo;
            url += '&msgtext=' + msgText;
//            url += '&smstype=' + smsType;

            // make http call
            var request = require('request');
            request(url, function (error, response, body) {
                // success
                if (!error && response.statusCode == 200) {
                    console.log('Msg sent. Msg body - ', body);
                    resolve(body);
                }
                // error
                else {
                    console.log("Error in msg send");
                    reject(error)
                }
            });
        });
    },
    // create hash for algo
    createHash: function (data_param) {

        var algo = (typeof (data_param.algo) !== "undefined")
                ? data_param.algo
                : 'md5';
        var data = data_param.data;
        var encoding = (typeof (data_param.encoding) !== "undefined")
                ? data_param.encoding
                : 'hex';

        // create
        var hash = crypto.createHash(algo).update(data).digest(encoding);
        return hash;
    },
    // get date in YMD
    getDateYMD: function (dateVal) {
        var date = new Date(dateVal);
        var year = date.getFullYear();
        var month = this.addZeroToDate(date.getMonth() + 1);
        var day = this.addZeroToDate(date.getDate());

        var date_str = year + '-' + month + '-' + day;
        return date_str;
    },
    // @Add 0 to dates
    addZeroToDate: function (i) {
        if (i < 10) {
            i = '0' + i;
        }
        return i;
    },
    encryptData: function (dataParam) {
        var password = 'C417FB77B7FFE8BACC54C5B273C7E148';
//        var password = 'C417FB77B7FFE8BA';

        var algo = (typeof (dataParam.algo) !== "undefined")
                ? dataParam.algo
                : 'aes-256-cbc';
        var data = dataParam.data;
        var inputEncoding = (typeof (dataParam.inputEncoding) !== "undefined")
                ? dataParam.inputEncoding
                : 'utf8';
        var outputEncoding = (typeof (dataParam.outputEncoding) !== "undefined")
                ? dataParam.outputEncoding
                : 'hex';
        var iv = "3C9FEA599285FDDA";
        var cipher = crypto.createCipheriv(algo,password , iv);

//        var cipher = crypto.createCipher(algo, password, iv)
        var crypted = cipher.update(data, inputEncoding, outputEncoding)
        crypted += cipher.final(outputEncoding);
        return crypted;
    },
    decryptData: function (dataParam) {
        var password = 'C417FB77B7FFE8BACC54C5B273C7E148';
//        var password = 'C417FB77B7FFE8BA';
//      
        var algo = (typeof (dataParam.algo) !== "undefined")
                ? dataParam.algo
                : 'aes-256-cbc';
        var data = dataParam.data;
        var inputEncoding = (typeof (dataParam.inputEncoding) !== "undefined")
                ? dataParam.inputEncoding
                : 'hex';
        var outputEncoding = (typeof (dataParam.outputEncoding) !== "undefined")
                ? dataParam.outputEncoding
                : 'utf8';

//        var iv = crypto.randomBytes(16);
        var iv = "3C9FEA599285FDDA";
        var decipher = crypto.createDecipheriv(algo, password , iv);

        
//        var decipher = crypto.createDecipher(algo, password)
        var dec = decipher.update(data, inputEncoding, outputEncoding)
        dec += decipher.final(outputEncoding);
        return dec;

//        var decipher = crypto.createDecipher('aes-256-cbc', password);
//        return decipher.update(dataParam.data, 'hex', 'utf8') + decipher.final('utf8');

    },
    // send OTP
    // smstype : 0 = Transactional Template,3 = Transactional Optin,4 = Promotional,13 = Transactional OTP,9 = Transactional Template MTNL
    sendOTP: function (mobileNo, msgText, callback) {
        var url = "http://httpapi.zone:7501/failsafe/HttpLink?signature=SMARTC";
        url += '&aid=' + config.aid;
        url += '&pin=' + config.pin;
        url += '&message=' + msgText;
        url += '&mnumber=' + mobileNo;

        // make http call
        var request = require('request');
        request.post(url, function (error, response, body) {
            // success
            if (!error && response.statusCode == 200) {
                console.log('OTP body - ', body);
                return callback(null, 1);
            }
            // error
            else {
                return callback(error, 0);
            }
        });
    },
    // generate otp with given length
    sendOTPRoute: function (mobileNo, msgText, callback) {
        var url = "http://sms6.routesms.com:8080/bulksms/bulksms?";
        url += 'username=' + config.route_username;
        url += '&password=' + config.route_password;
        url += '&message=' + msgText;
        url += '&destination=91' + mobileNo;
        url += '&source=SMARTC';
        url += '&type=0&dlr=1';
        console.log("OTP router is called");
        // make http call
        var request = require('request');
        request.post(url, function (error, response, body) {
            // success
            console.log('OTP body - ', body);
            if (!error && response.statusCode == 200) {
                return callback(null, 1);
            }
            // error
            else {
                return callback(error, 0);
            }
        });
    },
    // generate otp with given length
    generateOTP: function (length) {
        // for OTP generation
        var speakeasy = require('speakeasy');

        var secret = speakeasy.generateSecret({length: 20});
        var OTP = speakeasy.totp({
            secret: secret.base32,
            encoding: 'base32',
            digits: length
        });

        return OTP;
    },
    generateNewReferralCode: function ()
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 7; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        console.log(text);
        return text;
    },
    getMorganLogFormat: function (type) {
        var morganFormat = 'combined';

        if (type == 'custom') {
            morganFormat = '{"url": ":url", "method": ":method", "status": ":status", "response_time": ":response-time", "date": ":date[clf]", "result_length": ":res[content-length]", "remote_addr": ":remote-addr", "remote_user": ":remote-user", "http_version": ":http-version", "referrer": ":referrer", "user_agent": ":user-agent"}';
        } else if (type == 'dev') {
            morganFormat = 'dev';
        }

        return morganFormat;
    },
    // check Y-m-d h:m:s date format
    validateYMDHMSDateFormat: function (str) {
        var pattern = /^([0-9]{4})\-([0-9]{2})\-([0-9]{2}) ([0-9]{2})\:([0-9]{2})\:([0-9]{2})$/;
        return pattern.test(str);
    },
    // get date in YMD HMS
    getDateYMDHMS: function (dateVal) {
        var date = new Date(dateVal);
        var year = date.getFullYear();
        var month = this.addZeroToDate(date.getMonth() + 1);
        var day = this.addZeroToDate(date.getDate());
        var hours = this.addZeroToDate(date.getHours());
        var minutes = this.addZeroToDate(date.getMinutes());
        var seconds = this.addZeroToDate(date.getSeconds());

        var date_str = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
        return date_str;
    },
    sendNotification: function (fcmTokens, notificationContent, dataContent) {
        // See the "Defining the message payload" section below for details
        // on how to define a message payload.
        var payload = {
            data: dataContent,
            notification: notificationContent
//            {
//                title: "up 1.43% on the day",
//                body: "gained 11.80 points to close at 835.67, up 1.43% on the day.",
//                icon: ''
//            }
        };

        // Send a message to the devices corresponding to the provided
        // registration tokens.
        admin.messaging().sendToDevice(fcmTokens, payload)
                .then(function (response) {
                    // See the MessagingDevicesResponse reference documentation for
                    // the contents of response.
                    console.log("Successfully sent message:", response);
                    console.log(JSON.stringify(response));
                    if (response.successCount == 1) {
                        return;
                    }
                })
                .catch(function (error) {
                    console.log("Error sending message:", error);
                });
    }

};

module.exports = commonFunctions;
