var commonDb = require('../models/resources/commonDb');
var mongoose = require('mongoose');
var modelName = 'creditUsageArchive';

var creditUsageArchiveUtil = {
    insertCreditUsageArchive: function (insertObject) {
        return new Promise(function (resolve, reject) {
            commonDb.createDocument(modelName, insertObject)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },

    updateCreditUsageArchive: function (queryObject, updateObject) {
        return new Promise(function (resolve, reject) {
            commonDb.updateDocument(modelName, queryObject, updateObject)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },

    getCreditUsageArchiveByFindCriteria: function(findCriteria) {
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, findCriteria)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },

    getAllCreditUsageArchive: function () {
        return new Promise(function (resolve, reject) {
            commonDb.findAll(modelName)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },

    getCreditUsageArchiveById: function(docId){
        return new Promise(function (resolve, reject) {
            commonDb.findBySimpleParams(modelName, {_id: mongoose.Types.ObjectId(docId)})
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    },

    getCreditUsageArchiveByCustom: function(dataObj) {
        return new Promise(function (resolve, reject) {
            commonDb.findByCustom(modelName, dataObj)
                .then(function (respObject) {
                    resolve(respObject);
                })
                .catch(function (errObject) {
                    reject(errObject);
                });
        });
    }
};

module.exports = creditUsageArchiveUtil;