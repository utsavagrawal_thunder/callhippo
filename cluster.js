// @Cluster - Setup cluster for app
// @Author - Amar Thakur <amarthakur0@gmail.com>

//
// Start/Listen app in cluster mode
var cluster = require('cluster');

// Master process
if (cluster.isMaster) {
  // Count the machine's CPUs
  var cpuCount = require('os').cpus().length;
  console.log('Total CPU Cores - ', cpuCount);

  // Create a worker for each CPU
  for (var i = 0; i < cpuCount; i++) {
    cluster.fork();
  }

  // Listen for dying workers
  cluster.on('exit', function (worker) {
    console.log('Worker ' + worker.id + ' died :(');
    if(worker.id < 30) {
      cluster.fork();
    }
  });
}
// Worker process
else {
  // Required modules
  require('./app');
}