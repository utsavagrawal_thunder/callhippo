#!/usr/local/bin/node
var Promise = require('bluebird');
var userUtil = require('../utils/userUtil');
var failedAttemptsUtil = require('../utils/failedAttemptsUtil');
var commonFunctions = require('../utils/commonFunctions');

var fs = require('fs');

var uniqueUsers = [];
function notifyUsersForReferral() {
    userUtil.getAllUsers()
            .then(function (responseObjectUserList) {
                console.log("responseObjectUserList");
                console.log(responseObjectUserList);
                //Schedule in batches in future
//                for (var j = 0; j < responseObjectUserList.length; j++) {
                if (responseObjectUserList.length > 0) {
                    var notification = {
                        title: "Referral",
                        body: "Get Rs.10 for every friend who joins! Your friend also gets Rs.10"

                    };
                    var data = {
                        type: "7"
                    };
                    commonFunctions.sendNotification(responseObjectUserList, notification, data)
                }

//                }
            })
            .catch(function (errorObject) {
                console.log("errorObject");
                console.log(errorObject);
            })

}

notifyUsersForReferral();

var promiseWhile = function (condition, action) {
    var resolver = Promise.defer();

    var loop = function () {
        if (!condition())
            return resolver.resolve();
        return Promise.cast(action())
                .then(loop)
                .catch(resolver.reject);
    };

    process.nextTick(loop);

    return resolver.promise;
};