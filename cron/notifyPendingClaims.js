#!/usr/local/bin/node
var Promise = require('bluebird');
var userUtil = require('../utils/userUtil');
var failedAttemptsUtil = require('../utils/failedAttemptsUtil');
var commonFunctions = require('../utils/commonFunctions');

var fs = require('fs');

var uniqueUsers = [];
console.log("akjdjas");
function notifyUsersForPendingClaims() {
    failedAttemptsUtil.getAllfailedAttempts()
            .then(function (responseObject) {
                console.log("1");
                for (var i = 0; i < responseObject.length; i++) {
                    if (uniqueUsers.indexOf(responseObject[i].userId) == -1) {
                        uniqueUsers.push(responseObject[i].userId);
                    }
                }
                console.log("hi");
                var dataObject = {
                    findCriteria: {_id: {$in: uniqueUsers}},
                    projectFields: {"fcmToken": 1}
                };
                return userUtil.getByCustom(dataObject)
//                fs.writeFile("./log.txt", uniqueUsers, function (err) {
//                    if (err) {
//                        return console.log(err);
//                    }
//                    console.log("The file was saved!");
//                });
            })
            .then(function (responseObjectUserList) {
                console.log("responseObjectUserList");
                console.log(responseObjectUserList);
                console.log(uniqueUsers);
                console.log(uniqueUsers[0]);
                console.log(typeof uniqueUsers[0]);
                var notification = {
                    title: "Pending Claims",
                    body: "You may have pending calls to redeem. Tap to claim"

                };
                var data = {
                    type: "5"
                };
                for (var j = 0; j < responseObjectUserList.length; j++) {
                    if (responseObjectUserList[j].fcmToken) {
                        commonFunctions.sendNotification([responseObjectUserList[j].fcmToken], notification, data)
                    }

                }
            })
            .catch(function (errorObject) {
                console.log("errorObject");
                console.log(errorObject);
            })

}

notifyUsersForPendingClaims();

var promiseWhile = function (condition, action) {
    var resolver = Promise.defer();

    var loop = function () {
        if (!condition())
            return resolver.resolve();
        return Promise.cast(action())
                .then(loop)
                .catch(resolver.reject);
    };

    process.nextTick(loop);

    return resolver.promise;
};