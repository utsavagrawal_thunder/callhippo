var util = require('util');
var express = require('express');

/* Multer for handling S3 uploads
 var aws = require('aws-sdk')
 var express = require('express')
 var multer = require('multer')
 var multerS3 = require('multer-s3')
 */

var logger = require('morgan');
var path = require('path');
var cookieParser = require('cookie-parser');
var expressValidator = require('express-validator');
var commonFunctions = require('./utils/commonFunctions');
var config = require('./config');
var bodyParser = require('body-parser');
var session = require('express-session');
var Promise = require('bluebird');
var http = require('http');
var auth = require('./middleware/auth');
var flash = require('express-flash');
var RedisStore = require('connect-redis')(session);
var app = express();

// allow cross domain
var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
        res.send(200);
    } else {
        next();
    }
};

app.use(allowCrossDomain);
//app.use(auth);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//logger
app.use(logger(commonFunctions.getMorganLogFormat('custom')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

//adding custom validators
app.use(expressValidator({
    customValidators: {
        isArray: function (value) {
            return Array.isArray(value);
        },
        nonZero: function (value) {
            return (parseInt(value) != 0);
        }
    }
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    store: new RedisStore({host: config.redis_host, port: config.redis_port, prefix: 'sess:admin:', ttl: 1 * 24 * 60 * 60}),
    secret: 'mOgKDT73lJ',
    proxy: true,
    resave: true,
    saveUninitialized: true,
    cookie: {maxAge: 1 * 24 * 60 * 60 * 1000}
}));

app.use(flash());

// Skip login for some routes
var unless = function (path, middleware) {
    return function (req, res, next) {
        if (path === req.path) {
            return next();
        } else if ((req.path).startsWith(path)) {
            return next();
        } else {
            return middleware(req, res, next);
        }
    };
};
//app.use(unless('/order/courier', auth.checkAuthentication));

// Check login
app.use('/*', auth.checkAuthentication);

app.set('superSecret', config.secret);

// Check redis connection for every request
// If connection not active then make connection or use old connection if exists
app.use(function (req, res, next) {
    // connect redis
    require('./middleware/redisConn')(req, res, next);
});


var server = app.listen(process.env.port || 4000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Call Up listening at http://%s:%s", host, port)
});

// Defined Routes here
var index = require('./routes/index');
var userController = require('./routes/userController');
var ussdController = require('./routes/ussdController');

//dashboard routes
app.use('/', index);

//api routes
app.use('/user', userController);
app.use('/ussd', ussdController);

module.exports = app;
