var models = require('../');
var Promise = require('bluebird');
var getSlug = require('speakingurl');


exports.updateAllDocumentsWithSlug = function () {
    console.log("reached util db");
    return new Promise(function (resolve, reject) {
        models.category.find({}).each(
                function (elem) {
                    console.log(elem);
                    //Add check for duplicate slug
                    models.category.update(
                            {
                                _id: elem._id
                            },
                            {
                                $set: {
                                    slug: getSlug(elem.name)
                                }
                            }
                    ).exec(function (error, response) {
                        if (error) {
                            console.log("Error in utilDb");
                        } else {
                            console.log(response);
                        }
                    });
                });
    });
};