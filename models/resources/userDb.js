
var models = require('../');
var Promise = require('bluebird');

exports.checkPassword = function (userObject) {
    return new Promise(function (resolve, reject) {
        models.user.findOne(userObject, function (error, response) {
            if (error) {
                console.log(error);
                reject(error);
            } else {
                // get data
                
                var returnObject = {
                    userId: response._id,
                    email: response.email,
                    totalWalletCredits: response.totalWalletCredits
                };  
                resolve(returnObject);
            }
        });
    });
};
