//Commonly used Mongo methods defined here

//TODO: Force schema check before inserts. If not valid, return with appropriate error

//var mongoose = require('mongoose');
var models = require('../');
var Promise = require('bluebird');

//get all documents
exports.findAll = function (modelName) {
    return new Promise(function (resolve, reject) {
        models[modelName].find({},{"__v":0}).lean().exec(function (error, response) {
            if (error) {
                console.log("Error in " + modelName + " findAll query.");
                console.log(error);
                reject(error);
            } else {
                console.log(modelName + " fetch all successfull.");
                resolve(response);
            }
        });
    });
};

//fetch by simple params like id, name etc in an AND format
/*
 * paramsObject = {_id: ObjectId("....."), name: "someName"}
 */
exports.findBySimpleParams = function (modelName, paramObject) {
    return new Promise(function (resolve, reject) {
        models[modelName].find(paramObject).lean().exec(function (error, response) {
            if (error) {
                console.log("Error in " + modelName + " findAll query.");
                console.log(error);
                reject(error);
            } else {
                console.log(modelName + " fetch by params successfull.");
                resolve(response);
            }
        });
    });
};

//fetch by simple params like id, name etc in an AND format & add project
exports.findByCustom = function (modelName, dataObj) {
    var findCriteria = (typeof(dataObj.findCriteria) != 'undefined') ? dataObj.findCriteria : {};
    var sortCriteria = (typeof(dataObj.sortCriteria) != 'undefined') ? dataObj.sortCriteria : {};
    var projectFields = (typeof(dataObj.projectFields) != 'undefined') ? dataObj.projectFields : {};
    var limit = (typeof(dataObj.limit) != 'undefined') ? dataObj.limit : 0;
    var skip = (typeof(dataObj.skip) != 'undefined') ? dataObj.skip : 0;

    return new Promise(function (resolve, reject) {
        models[modelName]
            .find(findCriteria)
            .lean()
            .select(projectFields)
            .sort(sortCriteria)
            .limit(limit)
            .skip(skip)
            .exec(function(error, response) {
                if (error) {
                    console.log("Error in " + modelName + " findByCustom query.");
                    console.log(error);
                    reject(error);
                } else {
                    console.log(modelName + " fetch by findByCustom query.");
                    resolve(response);
                }
            });
    });
};

// find document count
exports.getCount = function (modelName, countQuery) {
    return new Promise(function (resolve, reject) {
        models[modelName].count(countQuery, function (error, response) {
            if (error) {
                reject(error);
            } else {
                resolve(response);
            }
        });
    });
};

//Fetch by aggregate
/*
 * aggregationPipeline = [{$match: {
 *          _id: ObjectId("....")
 *      }},
 *      {$skip: 10},
 *      {$limit: 10}
 *      ]
 */

exports.aggregateFetch = function (modelName, aggregationPipeline) {
    return new Promise(function (resolve, reject) {
        models[modelName].aggregate(aggregationPipeline, function (error, response) {
            if (error) {
                console.log("Error in " + modelName + " aggregate query.");
                console.log(error);
                reject(error);
            } else {
                console.log(modelName + " aggregate fetch successfull.");
                resolve(response);
            }
        });
    });

};

//delete by some parameter
/*
 * paramObject = {_id: ObjectId("....."), name: "someName"}
 */

exports.deleteByParam = function (modelName, paramObject) {
    return new Promise(function (resolve, reject) {
        models[modelName].remove(paramObject, function (error, response) {
            if (error) {
                console.log("Error in " + modelName + " remove");
                console.log(error);
                reject(error);
            } else {
                console.log(modelName + " remove successfull.");
                resolve(response);
            }
        });
    });
};

//update query
/*
 * paramObject = {$set : {description : "Lorem Ipsum"}}
 *             = {$addToSet: {tags : ["google", "youtube"]}}
 *             
 * queryObject = {_id: ObjectId("....")}
 */

exports.updateDocument = function (modelName, queryObject, paramObject) {
    return new Promise(function (resolve, reject) {
        models[modelName].update(queryObject, paramObject, function (error, response) {
            if (error) {
                console.log("Error in " + modelName + " document update");
                console.log(error);
                reject(error);
            } else {
                console.log(response);
                console.log(modelName + " update successfull.");
                resolve(response);
            }
        });
    });
};

// Update with options too
exports.updateDocumentCustom = function (modelName, dataObj) {
    var query = (typeof(dataObj.query) != 'undefined') ? dataObj.query : {};
    var update = (typeof(dataObj.update) != 'undefined') ? dataObj.update : {};
    var options = (typeof(dataObj.options) != 'undefined') ? dataObj.options : {};

    return new Promise(function (resolve, reject) {
        models[modelName].update(query, update, options, function (error, response) {
            if (error) {
                console.log("Error in " + modelName + " document update");
                console.log(error);
                reject(error);
            } else {
                console.log(modelName + " update successfull.");
                resolve(response);
            }
        });
    });
};



//creates new documents and inserts into db. Returns _id

/*
 * To be done: Make it suitable for inserting multiple documents together. 
 */

exports.createDocument = function (modelName, docObject) {
    return new Promise(function (resolve, reject) {
        models[modelName].create(docObject, function (error, response) {
            if (error) {
                console.log("Error in " + modelName + " document insert");
                console.log(error);
                reject(error);
            } else {
                console.log(modelName + " insert successfull.");
                resolve(response);
            }
        });
    });
};


/*
 * Saves documents
 */

exports.saveDocument = function(modelName, docObject){
    
};

//find and use populate as well
exports.findBySimplePopulateParams = function (modelName, searchObj, populateParam, fetchFields){
    return new Promise(function (resolve, reject){
        models[modelName]
            .find(searchObj)
            .lean()
            .populate(populateParam, fetchFields)
            .exec(function(error, response) {
                if (error) {
                    console.log("Error in " + modelName + " findBySimplePopulateParams query.");
                    console.log(error);
                    reject(error);
                } else {
                    console.log(modelName + " fetch by findBySimplePopulateParams query.");
                    resolve(response);
                }
            });
    })
}

//find and use populate as well
exports.findByMuliplePopulateParams = function (modelName, searchObj, populateQuery){
    return new Promise(function (resolve, reject){
        models[modelName]
            .find(searchObj)
            .lean()
            .populate(populateQuery)
            .exec(function(error, response) {
                if (error) {
                    console.log("Error in " + modelName + " findByMuliplePopulateParams query.");
                    console.log(error);
                    reject(error);
                } else {
                    console.log(modelName + " fetch by findByMuliplePopulateParams query.");
                    resolve(response);
                }
            });
    })
}