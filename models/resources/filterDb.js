var mongoose = require('mongoose');
var models = require('../');
var Promise = require('bluebird');

exports.updateFilterValue = function(insertObject){
    return new Promise(function(resolve, reject){
        models.filters.update({_id: mongoose.Types.ObjectId(insertObject._id)}, {$addToSet : {"possibleValues": {"value": insertObject.value, "isActive":1}}}, function(error, response){            
            if(error) {
                console.log(error);
                reject(error);
            }
            else {
                console.log(response);
                resolve(response);
            }
        });
    });
};