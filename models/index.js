var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var reviews = require('./schema/reviews.js');
var otp = require('./schema/otp.js');
var settings = require('./schema/settings.js');
var settingsArchive = require('./schema/settingsArchive.js');
var creditUsageArchive = require('./schema/creditUsageArchive.js');

var user = require('./schema/user.js');
var wallet = require('./schema/wallet.js');
var recharges = require('./schema/recharges.js');
var callLog = require('./schema/callLog.js');


var ussdRegexp = require('./schema/ussdRegexp');
var ussdRegexpMissing = require('./schema/ussdRegexpMissing');
var operatorUSSD = require('./schema/operatorUSSD');

var failedAttempts = require('./schema/failedAttempts');

var blockedPackages = require('./schema/blockedPackages');

module.exports =
        {
            reviews: reviews,
            otp: otp,
            settings: settings,
            settingsArchive: settingsArchive,
            creditUsageArchive: creditUsageArchive,

            user: user,
            wallet: wallet,
            recharges: recharges,
            callLog: callLog,

            ussdRegexp: ussdRegexp,
            ussdRegexpMissing: ussdRegexpMissing,
            operatorUSSD: operatorUSSD,
            
            failedAttempts: failedAttempts,
            
            blockedPackages: blockedPackages

        };
