var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//var USER_MODEL = 'user';

var blockedPackagesSchema = new Schema({
    name: {
        type: String,
        trim: true,
    },
    isActive: {
        type: Number,
        default: 1
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
}, {collection: 'blockedPackages'});


var blockedPackages = mongoose.model('blockedPackages', blockedPackagesSchema);
module.exports = blockedPackages;