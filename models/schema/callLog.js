var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var callLogSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
//    fromNumber : {
//        type: Number
//    },
//    toNumber : {
//        type: Number
//    },
    callDuration: {
        type: Number
    },
    callStartTime : {
        type: Date
    },
    callEndTime: {
        type: Date
    },
    callCost: {
        type: Number
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
    
    
},{collection: 'callLog'});

var callLog = mongoose.model('callLog', callLogSchema);
module.exports = callLog;