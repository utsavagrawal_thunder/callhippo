var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// otpType ==> [1 = COD Payment]

var otpSchema = new Schema({
    otp : {
        type: Number
    },
    otpType : {
        type: Number
    },
    mobileNo: {
        type: Number,
        required: true 
    },
    createdDate: {
        type: Date,
        default: Date.now,
        // index : { expires : 60*30 }
    }
},{collection: 'otp'});

// add indexes
otpSchema.index({mobileNo: 1});
otpSchema.index({otp: 1});
otpSchema.index({createdDate: -1}, {expireAfterSeconds: 60*30});

var otp = mongoose.model('otp', otpSchema);
module.exports = otp;