// Use - Store all operator ussd regular expressions

// Required modules
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Collection Schema
var ussdRegexpSchema = new Schema({
    regexp: {
        type: String,
        trim: true
    },
    resultGroup: {
        type: String,
        trim: true
    },
    packType: {
        type: String,
        trim: true
    },
    // mobile number operator name
    operatorName: {
        type: String,
        trim: true
    },
    // operator id in app db
    operatorId: {
        type: Number
    },
    isActive: {
        type: Number,
        default: 1
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    updatedDate: {
        type: Date
    },
    // stores last used date time of regular expression
    lastUsed: {
        type: Date
    }
}, {collection: 'ussdRegexp'});

// add indexes
ussdRegexpSchema.index({operatorName: 1});
ussdRegexpSchema.index({operatorId: 1});
ussdRegexpSchema.index({isActive: 1});
ussdRegexpSchema.index({createdDate: -1});

var ussdRegexp = mongoose.model('ussdRegexp', ussdRegexpSchema);
module.exports = ussdRegexp;