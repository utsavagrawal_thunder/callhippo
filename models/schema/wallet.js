var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var walletSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId
    },
    //sim card(mobile number) which was used to generate this document(credit amt)
    generatorNumber: {
        type: Number
    },
    creditAmount: {
        type: Number
    },
    //if this value is not present in a particular document, then it means it is user registration credited amount document
    percentageApplied: {
        type: Number
    }, 
    isOneTap: {
        type: Boolean, 
        default: false
    },
    //should be the date the amount was credited
    createdDate: {
        type: String
//        type: Date,
//        default: Date.now
    },
    isReferralBonus: {
        type: Boolean
    },
    rechargeRef: {
        type: Schema.Types.ObjectId,
        ref: 'recharges'
    }
    
},{collection: 'wallet'});

var wallet = mongoose.model('wallet', walletSchema);
module.exports = wallet;