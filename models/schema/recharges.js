var mongoose = require('mongoose');
var Schema = mongoose.Schema;


//status
/* 0 => Pending, 1 => Finised successfully, 2=> Rejected */

var rechargeSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    operator: {
        type: String
    },
    circle: {
        type: String
    },
    amount: {
        type: Number
    },
    createdDate: {
        type: Date,
        default: Date.now()
    },
    updatedTime: {
        type: Date,
        default: Date.now()
    },
    status: {
        type: Number,
        default : 0
    }

}, {collection: 'recharges'});

var recharges = mongoose.model('recharges', rechargeSchema);
module.exports = recharges;