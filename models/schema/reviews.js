var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//isActive = 0, 1, 2
//1 is Active, 0 is Inactive and 2 is waiting for approval
var categorySchema = new Schema({
	_id : {
		type: Schema.ObjectId,
		ref : 'category'
	},
	name : {
		type: String,
    	trim: true
	},
	slug : {
		type: String,
    	trim: true	
	}
});

var productSchema = new Schema({
	variantId : {
		type: Schema.ObjectId,
		ref : 'product'
	},
    productId : {
        type: Schema.ObjectId,
        ref : 'product'
    },
	name: {
    	type: String,
    	trim: true
    },
    slug: {
    	type: String,
    	trim: true
    },
    category : {
    	type : categorySchema
    },
    image: {
    	type: String,
    	trim: true
    },
    supplierId: {
        type: Schema.ObjectId,
        ref : 'supplier'
    }
});

var ratingBaseSchema = new Schema({
	productQuality: {
    	type: Number,
    	default: 4
    },
    valueForMoney: {
    	type: Number,
    	default: 4
    },
    shippingTime: {
    	type: Number,
    	default: 4
    },
    shippingCost: {
    	type: Number,
    	default: 4
    }
});

var reviewSchema = new Schema({
	buyerId: {
        type: Schema.ObjectId,
        ref: 'buyer'
    },
    product: {
    	type: productSchema
    },
    orderId: {
    	type: String,
        trim: true
    },
    orderObj: {
        type: Schema.ObjectId,
        ref: 'orders'
    },
    rating: {
    	type: ratingBaseSchema
    },
    reviewText: {
        type: String,
        trim: true
    },
    buyerOverall: {
    	type: Number,
    	default: 4
    },
    supplierOverall: {
    	type: Number,
    	default: 4
    },
    shippingOverall: {
    	type: Number,
    	default: 4
    },
    isActive: {
    	type: Number,
    	default: 2
    },
    inActiveReason: {
        type: String,
        trim: true
    }
}, {collection: 'reviews'})

reviewSchema.index({'buyerId': 1});
reviewSchema.index({'product.variantId': 1});
reviewSchema.index({'orderId': 1});
reviewSchema.index({'isActive': 1});

var reviews = mongoose.model('reviews', reviewSchema);
module.exports = reviews;