// Use - Store all operator ussd codes against mccmnc number

// Required modules
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// isSupported ==> [0 = not supported, 1 = supported] 

/*
Airtel          1
Vodafone        2
BSNL            3
Reliance GSM    4
Aircel          5
Dolphin         6
Idea            7
TATA GSM        8
Telenor         9

------ Not supported operators ------
LOOP            10
Videocon        11
MTS             12
S TEL           13
HFCL INFOTEL    14
Etisalat DB     15
*/

// Collection Schema
var operatorUSSDSchema = new Schema({
    // mcc mnc number of mobile network operator
    mccMnc: {
        type: Number,
        required: true
    },
    // network circle region name
    region: {
        type: String,
        required: true
    },
    // mobile number operator name
    operatorName: {
        type: String,
        trim: true
    },
    // operator id in app db
    operatorId: {
        type: Number
    },
    balanceCode: {
        type: String
    },
    data3GCode: {
        type: String
    },
    data2GCode: {
        type: String
    },
    localCallsCode: {
        type: String
    },
    stdCallsCode: {
        type: String
    },
    localAndStdCallsCode: {
        type: String
    },
    phoneNumberCode: {
        type: String
    },
    rateCutterCode: {
        type: String
    },
    specialOfferCode: {
        type: String
    },
    portalCode: {
        type: String
    },
    vasNumber: {
        type: String
    },
    customerCareNumber: {
        type: String
    },
    isActive: {
        type: Number,
        default: 1
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    updatedDate: {
        type: Date
    },
    isSupported: {
        type: Number,
        default: 0
    }
}, {collection: 'operatorUSSD'});

// add indexes
operatorUSSDSchema.index({mccMnc: 1});
operatorUSSDSchema.index({operatorName: 1});
operatorUSSDSchema.index({operatorId: 1});
operatorUSSDSchema.index({isActive: 1});
operatorUSSDSchema.index({createdDate: -1});

var operatorUSSD = mongoose.model('operatorUSSD', operatorUSSDSchema);
module.exports = operatorUSSD;