var mongoose = require('mongoose');
var Schema = mongoose.Schema;


//all numbers will be percentage of call cost of user

var timeCostsSchema = {
    cost1__30 : {
        type: Number
    },
    cost2__30 : {
        type: Number
    },
    cost3__30 : {
        type: Number
    },
    cost4__30 : {
        type: Number
    },
    cost1__60 : {
        type: Number
    },
    cost2__60 : {
        type: Number
    },
    cost3__60: {
        type: Number
    },
    cost4__60: {
        type: Number
    },
    cost1__90: {
        type: Number
    },
    cost2__90: {
        type: Number
    },
    cost3__90: {
        type: Number
    },
    cost4__90: {
        type: Number
    },
    cost1__120 :{
        type: Number
    },
    cost2__120 :{
        type: Number
    },
    cost3__120 :{
        type: Number
    },
    cost4__120 :{
        type: Number
    },
    cost1__150 : {
        type: Number
    },
    cost2__150 : {
        type: Number
    },
    cost3__150 : {    
        type: Number
    },
    cost4__150 : {
        type: Number
    },
    cost1__180 : {
        type: Number
    },
    cost2__180 : {
        type: Number
    },
    cost3__180 : {
        type: Number
    },
    cost4__180 : {
        type: Number
    },
    cost1__210 : {
        type: Number
    },
    cost2__210 : {
        type: Number
    },
    cost3__210 : {
        type: Number
    },
    cost4__210 : {
        type: Number
    },
    cost1__240 : {
        type: Number
    },
    cost2__240 : {
        type: Number
    },
    cost3__240 : {
        type: Number
    },
    cost4__240 : {
        type: Number
    },
    cost1__270 : {
        type: Number
    },
    cost2__270 : {
        type: Number
    },
    cost3__270 : {
        type: Number
    },
    cost4__270 : {
        type: Number
    },
    cost1__300 : {
        type: Number
    },
    cost2__300 : {
        type: Number
    },
    cost3__300 : {
        type: Number
    },
    cost4__300 : {
        type: Number
    },
    cost1__300p : {
        type: Number
    },
    cost2__300p : {
        type: Number
    },
    cost3__300p : {
        type: Number
    },
    cost4__300p : {
        type: Number
    }
};

var walletCapsSchema = new Schema({
    perDay: {
        type: Number
    },
    perWeek: {
        type: Number
    },
    perMonth: {
        type: Number
    },
    perHour: {
        type: Number
    },
    
    maxEarn: {
        type: Number
    }
}); 
//all numbers will be (in paise) which is the max limit for a particular user

var bonusLimitsSchema = new Schema({
    newSignUp: {
        type: Number
    },
    newReferral: {
        type: Number
    }
}, {_id: false});
//all numbers will be (in paise)


var settingsSchema = new Schema({
    timeCosts: {
        type: timeCostsSchema
    },
    walletCaps : {
        type: walletCapsSchema
    },
    bonusLimits: {
        type: bonusLimitsSchema
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }

}, {collection: 'settings'});

//settingsSchema.index({field: 1});

var settings = mongoose.model('settings', settingsSchema);
module.exports = settings;