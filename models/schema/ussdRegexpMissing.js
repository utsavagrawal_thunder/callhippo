// Use - Store record if there is no match found for any USSD

// Required modules
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// appCode ===> 1 = Android, 2 = iOS, 3 = Windows

// Collection Schema
var ussdRegexpMissingSchema = new Schema({
    userId: {
        type: Schema.ObjectId,
        ref: 'user'
    },
    // ussd code
    ussdCode: {
        type: String,
//        required: true
    },
    // ussd type - whether main balance, 4G/3G/2G
    ussdType: {
        type: String,
        required: true
    },
    ussdReplyMsg: {
        type: String,
        required: true
    },
    // mobile number operator name
    operatorName: {
        type: String
    },
    // operator id in app db
    operatorId: {
        type: Number
    },
    // mcc mnc number of mobile network operator
    mccMnc: {
        type: Number,
//        required: true
    },
    reason: {
        type: Number
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    isActive: {
        type: Number,
        default: 1
    },
    appCode: {
        type: Number,
        default: 1
    },
    appVersion: {
        type: Number
    },
    osVersion: {
        type: Number
    },
    callDuration: {
        type: Number
    },
    callStartTime: {
        type: Date
    }
}, {collection: 'ussdRegexpMissing'});

// add indexes
//ussdRegexpMissingSchema.index({ussdCode: 1});
ussdRegexpMissingSchema.index({ussdType: 1});

ussdRegexpMissingSchema.index({createdDate: -1});
ussdRegexpMissingSchema.index({isActive: 1});

var ussdRegexpMissing = mongoose.model('ussdRegexpMissing', ussdRegexpMissingSchema);
module.exports = ussdRegexpMissing;