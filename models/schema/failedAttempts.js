var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//reason 1=> No Internet, 2 => Late USSD, 3 = > No regex match, 21=> Late USSD + No Internet, 23=> Late USSD + No regexp match, 13=> No internet + No regexp match, 123 => No internet + Late USSD + No regexp match, 1234=> Server Not Reachable, 4=> Late resolution, 5=> No Network, 6=> Card Closed

var failedAttemptsSchema = new Schema({
    userId: {
        type: Schema.ObjectId,
        ref: 'user',
        required: true
    },
    reason : {
        type: Number
    },
    callStartTime : {
        type: Date
    },
    callDuration: {
        type: Number,
        required: true 
    },
    callCost: {
        type: Number
    },
    oldAmount: {
        type: Number
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    isActive: {
        type: Boolean,
        default: true
    }
},{collection: 'failedAttempts'});

// add indexes
failedAttemptsSchema.index({reason: 1});
failedAttemptsSchema.index({createdDate: -1});

var failedAttempts = mongoose.model('failedAttempts', failedAttemptsSchema);
module.exports = failedAttempts;