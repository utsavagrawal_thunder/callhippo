var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var USER_MODEL = 'user';


var NestedReferredContactSchema = new Schema({
    referredRegistrationNumber: {
        type: String,
        trim: true
    },
    primaryDeviceToken: {
        type: String
    },
    //optional
    secondaryDeviceToken: {
        type: String
    },
    user_id: {
        type: Schema.ObjectId,
        ref: USER_MODEL
    }
});

//limits for particular users
var walletCapsSchema = new Schema({
    perHour: {
        type: Number
    },
    perDay: {
        type: Number
    },
    perWeek: {
        type: Number
    },
    perMonth: {
        type: Number
    }
});


var NestedCreditUsageSchema = new Schema({
    lastUpdated: {
        type: String
//        type: Date,
//        default: Date.now()
    },
    hourly: {
        type: Number
    },
    daily: {
        type: Number
    },
    weekly: {
        type: Number
    },
    monthly: {
        type: Number
    }
});


var userSchema = new Schema({
    username: {
        type: String,
        trim: true,
    },
    email: {
        type: String,
        trim: true,
    },
    primaryRegistrationDeviceToken: {
        type: String,
        required: true
    },
    //optional
    secondaryRegistrationDeviceToken: {
        type: String
    },
    registrationNumber: {
        type: String,
        trim: true,
        default: ""
//        required: true
    },
    password: {
        type: String
//        required: true
    },
    operatorId: {
        type: Number
//        required: true
    },
    isActive: {
        type: Number,
        default: 0
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    updatedDate: {
        type: Date
    },
    //must be updated after every entry in 'wallet' collection
    totalWalletCredits: {
        type: Number,
        default: 0
    },
    referralCode: {
        type: String
    },
    referredBy: {
        type: NestedReferredContactSchema
    },
    associatedMailIds: {
        type: [String],
        default: []
    },
    //user specific optional limits as per user group. For future purposes
    walletCaps: {
        type: walletCapsSchema
    },
    creditUsage: {
        type: NestedCreditUsageSchema
    },
    fcmToken: {
        type: String
    }

}, {collection: 'users'});


var user = mongoose.model('user', userSchema);
module.exports = user;