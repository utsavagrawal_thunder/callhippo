var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var adminSchema = new Schema({
    username: {
        type: String,
        trim: true,
    },
    email: {
        type: String,
        trim: true,
    },
    password : {
        type: String,
        required: true
    },
    isActive: {
        type: Number,
        default: 0
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    updatedDate: {
        type: Date
    },
},{collection: 'admin'});


var admin = mongoose.model('admin', adminSchema);
module.exports = admin;