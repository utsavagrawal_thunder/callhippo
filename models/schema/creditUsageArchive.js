var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var USER_MODEL = 'user';

var creditUsageSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: USER_MODEL
    },
    lastUpdated: {
        type: Number
//        type: Date,
//        default: Date.now()
    },
    hourly: {
        type: Number
    },
    daily: {
        type: Number
    },
    weekly: {
        type: Number
    },
    monthly: {
        type: Number
    },
    //stores the time of new last updated.
    archivedTime: {
        type: Number
//        type: Date,
//        default: Date.now()
    },
    isOneTap: {
        type: Boolean,
        default: false
    }
}, {collection: 'creditUsageArchive'});


var creditUsageArchive = mongoose.model('creditUsageArchive', creditUsageSchema);
module.exports = creditUsageArchive;