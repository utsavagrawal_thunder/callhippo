var express = require('express');
var router = express.Router();
var commonFunctions = require('../utils/commonFunctions');
var common = require('../config');
var config = common.config();
var filterUtil = require('../utils/filterUtils');
var getSlug = require('speakingurl');
var multer = require('multer');
var excelUploadPath = 'public/uploads/filter/excel-upload';
// required modules for excel filter upload
var path = require('path');

var Promise = require('bluebird');
// file path
var XLSX = require('xlsx');


// rename image
function renameFile(orgFileName) {
    var ext = orgFileName.substr(orgFileName.lastIndexOf('.')+1);
    var fileNameWithoutExt = orgFileName.substr(0, orgFileName.length - ext.length - 1);
    var updName = fileNameWithoutExt+'-'+Date.now()+'.'+ext;

    return updName;
}

// for filter excel file upload
var excelUploadStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, excelUploadPath);
  },
  filename: function (req, file, cb) {
    var updName = renameFile(file.originalname);
    cb(null, updName);
  }
});
var uploadProductExcel = multer({ storage: excelUploadStorage });


router.post('/insert/', function (req, res, next) {
    var validationErrors = validateInput(req);
    if (validationErrors.length > 0) {
        returnError({error: 1, message: "Validation error", data: validationErrors}, res);
    }
    var requestObject = {
        name: req.body.name,
        description: req.body.description,
        possibleValues: req.body.possibleValues,
        isActive: 0
    };
    switch (req.body.status) {
        case "Active" :
            {
                requestObject.isActive = 1;
            }
            ;
            break;
        case "Inactive" :
            {
                requestObject.isActive = 0;
            }
            ;
            break;
        case "Deleted" :
            {
                requestObject.isActive = 2;
            }
            ;
            break;
    }
    ;
    filterUtil.getFilterByCustom({findCriteria: {"name": {$regex: new RegExp("^" + req.body.name + "$", 'i')}}})
            .then(function (respObject) {
                if (Object.keys(respObject).length > 0) {
                    res.send({error: 1, message: "Filter by same name already exists", data: {}});
                } else {
                    filterUtil.insertFilter(requestObject)
                            .then(function (responseObject) {
                                res.send(responseObject);
                            })
                            .catch(function (errorObject) {
                                res.send(errorObject);
                            });
                }
            })
            .catch(function (errObject) {
                console.log("Error in filter get by name" + errObject);
                res.send(errObject);
            });
});

router.post('/insertValueById/', function (req, res) {
    (!req.body.hasOwnProperty('value') || req.body.value == '') ? returnError({error: 1, message: "Validation error", data: "Value must have some characters"}, res) : {};
    filterUtil.insertFilterValue(req.body)
            .then(function (responseObject) {
                res.send(responseObject);
            })
            .catch(function (errorObject) {
                res.send(errorObject);
            });
});

router.get('/getAll/', function (req, res) {
    filterUtil.getAllFilters()
            .then(function (respObject) {
                res.send(respObject);
            })
            .catch(function (errObject) {
                res.send(errObject);
            });
});

router.get('/getById/', function (req, res) {
    filterUtil.getFilterById(req.query._id)
            .then(function (respObject) {
                res.send(respObject);
            })
            .catch(function (errObject) {
                res.send(errObject);
            });
});

// get all active filters
router.get('/getAllActiveFilters', function (req, res) {
    var findCriteria = {
        isActive: 1
    };
    var projectFields = {};
    var dataObj = {
        findCriteria: findCriteria,
        projectFields: projectFields
    };
    filterUtil.getFilterByCustom(dataObj)
            .then(function (respObject) {
                res.send(respObject);
            })
            .catch(function (errObject) {
                res.send(errObject);
            });
});

router.post('/update', function (req, res) {
    console.log("in controller");
    var validationErrors = validateInput(req);
    if (validationErrors.length > 0) {
        returnError({error: 1, message: "Validation error", data: validationErrors}, res);
    }
    var requestObject = req.body;
    var isSlugAdded = false;
    for (var i = 0; i < req.body.possibleValues.length; i++) {
        if (!req.body.possibleValues[i].slug) {
            for (var j = 0; j < req.body.possibleValues.length; j++) {
                if(i == j){
                    
                }
                else if (req.body.possibleValues[j].slug && (getSlug(req.body.possibleValues[i].value) == req.body.possibleValues[j].slug)) {
                    req.body.possibleValues[i].slug = getSlug(req.body.possibleValues[i].value) + "1";
                    isSlugAdded = true;
                }
            }
            if (isSlugAdded == false) {
                req.body.possibleValues[i].slug = getSlug(req.body.possibleValues[i].value);
            }
        }
    }
    
    switch (req.body.status) {
        case "Active" :
            {
                requestObject.isActive = 1;
            }
            ;
            break;
        case "Inactive" :
            {
                requestObject.isActive = 0;
            }
            ;
            break;
        case "Deleted" :
            {
                requestObject.isActive = 2;
            }
            ;
            break;
    }
    console.log("req.body");
    console.log(req.body);
    console.log(req.body._id);
    filterUtil.getFilterByCustom({findCriteria: {"name": {$regex: new RegExp("^" + req.body.name + "$", 'i')}, "_id": {$ne: commonFunctions.stringToObjectId(req.body._id)}}})
            .then(function (responseObject) {
                console.log("responseObject");
                console.log(responseObject);
                if (responseObject.length > 0) {
                    res.send({error: 1, message: "Another filter by same name already exists", data: {}});
                } else {
                    filterUtil.updateFilterById(requestObject)
                            .then(function (respObject) {
                                res.send(respObject);
                            })
                            .catch(function (errObject) {
                                res.send(errObject);
                            });
                }
            })
            .catch(function (errorObject) {
                console.log("Error in filter get by name" + errorObject);
                res.send(errorObject);
            });

});

//script for update all possible values
router.get('/updateAllFilters', function (req, res, next) {

    filterUtil.getFilterById("57d9447c95c9c727534c8967")
            .then(function (respObject) {
                console.log("respObject in filter script");
                console.log(respObject);
                for (var i = 0; i < respObject.length; i++) {
                    var queryObject = {
                        "_id": commonFunctions.stringToObjectId(respObject[i]._id)
                    };
                    var isSlugSet = false;
                    for (var j = 0; j < respObject[i].possibleValues.length; j++) {
                        if (!respObject[i].possibleValues[j].slug) {
                            for (var k = 0; k < respObject[i].possibleValues.length; k++) {
                                if(k ==  j){

                                }
                                else if (respObject[i].possibleValues[k].slug && (getSlug(respObject[i].possibleValues[j].value) == respObject[i].possibleValues[k].slug)) {
                                    respObject[i].possibleValues[j].slug = getSlug(respObject[i].possibleValues[j].value) + "1";
                                    isSlugSet = true;
                                }
                            }
                            if (isSlugSet == false) {
                                respObject[i].possibleValues[j].slug = getSlug(respObject[i].possibleValues[j].value);
                            }
                        }
                    }
                    var paramObject = {
                        $set: {
                            "possibleValues": respObject[i].possibleValues
                        }
                    };
                    console.log("queryObject");
                    console.log(queryObject);
                    console.log('paramObject');
                    console.log(JSON.stringify(paramObject));
                    filterUtil.updateAllFilters(queryObject, paramObject)
                            .then(function (respObject) {
                                res.send({error: 0, msg: "Updated all filters", data: respObject});
                            })
                            .catch(function (errObject) {
                                res.send({error: 1, msg: "Error in Updating all filters", data: errObject});
                            });
                }

            })
            .catch(function (errObject) {
                console.log("errObject");
                console.log(errObject);
            });
});

//upload filters by excel
router.post('/uploadFiltersExcel', uploadProductExcel.single('file'), function(req, res){
    
    if ( !req.file.mimetype.startsWith('application/vnd.ms-excel') && !req.file.mimetype.startsWith('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') ) {

        return res.status(422).json( {
            error : 'The uploaded file must be an excel'
        } );
    }

    // send file upload response
//    res.status(200).send(req.file);
    var renamedFileName = req.file.filename;
    var fullFilePath = path.join(__dirname, '../'+excelUploadPath+'/'+renamedFileName);

    
    // Remove excel file after 10 mins
    setTimeout(function() {
        var fs = require('fs');
        fs.unlink(fullFilePath, function (err) {});
    }, 10*60*1000);

    // Do upload of all excel data to DB
    var workbook = XLSX.readFile(fullFilePath);
    // first sheet name
    var firstSheetName = workbook.SheetNames[0];    
    // Get worksheet
    var worksheet = workbook.Sheets[firstSheetName];
    // convert to JSON    
    var filterJSON = XLSX.utils.sheet_to_json(worksheet);
    
    var filterJSONCnt = filterJSON.length;
    
    for (var i = 0; i < filterJSONCnt; i++) {
        if(!filterJSON[i]["Name"]){
            // send no name error
            return res.status(500).send({error: 1, msg: "Name is not defined for some filter"  });
        }
        if(!filterJSON[i]["Values"]){
            // send no values error
            return res.status(500).send({error: 1, msg: "Values is not defined for some filter"  });
        }
        else {
            if(filterJSON[i]["Values"].trim().length == 0) {
                // send no values error
                return res.status(500).send({error: 1, msg: "Values is not defined for some filter"});
            }
        }
    }
    // send file upload response
    res.status(200).send(req.file);
    
    var insertFilterOperation = [];
    for (var i = 0; i < filterJSONCnt; i++) {
        console.log("filterJSON[i].Values.split(',')");
        console.log(filterJSON[i].Values.split(','));
        var possibleValuesArray = filterJSON[i].Values.split(',');
        var possibleValues = [];
        for (var j = 0; j < possibleValuesArray.length; j++) {
            if(possibleValuesArray[j].trim().length !== 0){
                possibleValues.push({value : possibleValuesArray[j]});
            }
        }
        insertFilterOperation.push(filterUtil.insertFilter({name: filterJSON[i].Name, description: filterJSON[i].Description ? filterJSON[i].Description: "", possibleValues: possibleValues, isActive: 1}));
    }
    
    
    Promise.all(insertFilterOperation)
            .then(function (respObject) {
                console.log("after all filter insert");
                console.log(respObject);
                sendMailToAdmin(req, []);
            })
            .catch(function (errObject) {
                console.log("after all filter insert + error");
                console.log(errObject);
                sendMailToAdmin(req, [errObject]);
            });
    
    
});

// send email to admin
function sendMailToAdmin(req, errors) {
    var errorsCnt = errors.length;

    // user data
    var userInfo = req.session.userInfo;
    var email = userInfo.email;
    var name = userInfo.firstName+' '+userInfo.lastName;

    // email body
    var emailHTML = '<b>Hi '+name+',</b>';

    // no errors
    if(errorsCnt == 0) {
        emailHTML += '<p>All Filters from excel uploaded successfully.</p>';
    }
    // errors in uploading products from excel
    else {
        emailHTML += '<p>Following filters are not uploaded due to given reasons. Please edit excel & upload.</p>';
        emailHTML += '<ol type="1">';
        for(var i = 0; i < errorsCnt; i++) {
            emailHTML += '<li>'+errors[i]+'</li>';
        }
        emailHTML += '</ol>';
    }

    emailHTML += '<p>Thanks,</p>';
    emailHTML += '<p>Clinito Team</p>';

    var mailOptions = {
        from: config.sender_email,
        to: email,
        subject: 'Clinito - Upload Filter Excel',
        html: emailHTML
    };
    commonFunctions.sendMail(mailOptions, function(errMailer, respMailer){
        if(errMailer){
            console.log("Error in sending filter excel upload email");
            console.log(errMailer);
        }
    });
}


/*
 * validate incoming request
 */
function validateInput(req) {
    var validationErrors = [];
    (!req.body.hasOwnProperty('name') || req.body.name == '') ? validationErrors.push({error: 1, message: 'Name is not set'}) : {};
//    (!req.body.hasOwnProperty('description') && !req.body.description) ? validationErrors.push({error: 1, message: 'Description is not set'}) : {};
    (!req.body.hasOwnProperty('possibleValues') || req.body.possibleValues.length == 0) ? validationErrors.push({error: 1, message: 'Atleast add one value for this filter.'}) : {};
    return validationErrors;
}

/*
 * Common function for returning error
 */

function returnError(errObj, res) {
    res.send(errObj);
}
;
module.exports = router;