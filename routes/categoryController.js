var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var common = require('../config');
var config = common.config();
var categoryUtil = require('../utils/categoryUtils');
var relationsUtil = require('../utils/relationsUtils');
var productUtil = require('../utils/productUtils');
var promotionsUtil = require('../utils/promotionsUtils');
var commonFunctions = require('../utils/commonFunctions');
var getSlug = require('speakingurl');

// for image upload
var multer = require('multer');
var AWS = require('aws-sdk');
var multerS3 = require('multer-s3');

AWS.config.update(config.awsConfig);

var s3 = new AWS.S3();

var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: "clinito",
        acl: "public-read",
        ContentType: 'application/octet-stream',
        key: function (req, file, cb) {
            var updName = renameFile(file.originalname);
            cb(null, 'category/' + updName);
        }
    })
});

// rename image
function renameFile(orgFileName) {
    var ext = orgFileName.substr(orgFileName.lastIndexOf('.') + 1);
    var fileNameWithoutExt = orgFileName.substr(0, orgFileName.length - ext.length - 1);
    var updName = fileNameWithoutExt + '-' + Date.now() + '.' + ext;

    return updName;
}

router.post('/upload', upload.single('file'), function (req, res, next) {
    if (!req.file.mimetype.startsWith('image/')) {
        return res.status(422).json({
            error: 'The uploaded file must be an image'
        });
    }

    // // dimensions check
    // var dimensions = sizeOf( req.file.path );
    // if( ( dimensions.width < 300 ) || ( dimensions.height < 100 ) ) {
    //   return res.status( 422 ).json( {
    //     error : 'The image must be at least 300 x 100px'
    //   } );
    // }


    var pid = '10000' + parseInt(Math.random() * 10000000);
    return res.status(200).send(req.file);
});

// for image delete
router.delete('/delete/:name', function (req, res) {

    var name = req.params.name;
    var params = {
        Bucket: 'clinito', /* required */
        Key: 'category/' + name, /* required */
        RequestPayer: 'requester'
    };
    console.log("params");
    console.log(params);
    s3.deleteObject(params, function (err, data) {
        if (err) {
            console.log(err, err.stack);
            // an error occurred
            res.send({error: 1, msg: "Error while deleting image", data: err});
        } else {
            console.log(name + " file deleted from s3 successfully");
            // successful response 
            res.send({error: 0, msg: "Successfully deleted", data: {}});
        }
    });
});

router.post('/insert', function (req, res) {
    /*
     * Validations for set paramaters and expected values
     */

    var validationErrors = validationsCheckOnInserts(req);
    if (validationErrors.length > 0) {
        returnError({error: 1, message: "Validation error", data: validationErrors}, res);
    }

    /*
     * Insert Call 
     */
    categoryUtil.insertcategory(req.body)
            .then(function (respObject) {
                if (respObject) {
                    var promotionsInsertObject = {
                        category: {
                            _id: respObject._id,
                            name: respObject.name,
                            slug: respObject.slug
                        },
                        isHomePage: false
                    };
                    promotionsUtil.createNew(promotionsInsertObject)
                            .then(function (promotionsRespObject) {
                                console.log("Promotions created for category " + respObject.name);
                                res.send(promotionsRespObject);
                            })
                            .catch(function (promotionsErrObject) {
                                if (promotionsErrObject.errors) {
                                    res.send({error: 1, message: promotionsErrObject.message, data: promotionsErrObject});
                                }
                            });
                }
            })
            .catch(function (errObject) {
                if (errObject.errors) {
                    res.send({error: 1, message: errObject.message, data: errObject});
                }
            });
});

// insert filter value only
router.post('/insertFilterValue', function (req, res, next) {
    var data = req.body;
    var value = data.value;
    var filterId = data.filterId;
    var categoryId = data.categoryId;
    var filterType = data.filterType;

    // validation
    if (typeof (value) == 'undefined' || value == '') {
        return res.send({'error': 1, 'msg': 'Please enter value', 'data': {}});
    }
    if (typeof (categoryId) == 'undefined' || !commonFunctions.isMongoId(categoryId)) {
        return res.send({'error': 1, 'msg': 'Product type id not correct', 'data': {}});
    }
    if (typeof (filterId) == 'undefined' || !commonFunctions.isMongoId(filterId)) {
        return res.send({'error': 1, 'msg': 'Filter id not correct', 'data': {}});
    }
    if (filterType != 'product' && filterType != 'variant') {
        return res.send({'error': 1, 'msg': 'Filter type not given', 'data': {}});
    }

    // Check if given value already exists in db
    var findCriteria = {
        '_id': mongoose.Types.ObjectId(categoryId)
    };
    // filter type is product
    if (filterType == 'product') {
        findCriteria['productFilters.values.name'] = new RegExp('^' + value + '$', 'i');
    } else if (filterType == 'variant') {
        findCriteria['variantFilters.values.name'] = new RegExp('^' + value + '$', 'i');
    }

    var projectFields = {name: 1, slug: 1};
    var dataObj = {
        findCriteria: findCriteria,
        projectFields: projectFields
    };
    categoryUtil.getCategoryByFindCriteria(dataObj)
            .then(function (respObjCheck) {
                if (respObjCheck.length > 0) {
                    return res.send({'error': 1, 'msg': 'Filter value already exists', 'data': {}});
                }

                // build value objext
                var valueObj = {
                    name: value,
                    slug: getSlug(value)
                };

                // now add
                var updateObj;
                var queryObj = {
                    '_id': mongoose.Types.ObjectId(categoryId)
                };
                // filter type is product
                if (filterType == 'product') {
                    queryObj['productFilters._id'] = mongoose.Types.ObjectId(filterId);
                    updateObj = {
                        $push: {'productFilters.$.values': valueObj}
                    };
                } else if (filterType == 'variant') {
                    queryObj['variantFilters._id'] = mongoose.Types.ObjectId(filterId);
                    updateObj = {
                        $push: {'variantFilters.$.values': valueObj}
                    };
                }

                categoryUtil.addFilterValue(queryObj, updateObj)
                        .then(function (respObject) {
                            res.send({'error': 0, 'msg': 'Filter value added successfully', 'data': respObject});
                        })
                        .catch(function (errObject) {
                            res.send({'error': 1, 'msg': 'Unable to add filter value', 'data': errObject});
                        });

            })
            .catch(function (errObjCheck) {
                res.send({'error': 1, 'msg': 'Unable to add filter value', 'data': errObject});
            });
});


/* Get all categories */

router.get('/get', function (req, res) {
    categoryUtil.getAllCategories()
            .then(function (respObject) {
                res.send(respObject);
            })
            .catch(function (errObject) {
                res.send(errObject);
            });
});

/* Category get by id for update etc*/

router.get('/getById', function (req, res) {
    categoryUtil.getCategoryById(req.query._id)
            .then(function (respObject) {
                res.send(respObject);
            })
            .catch(function (errObject) {
                res.send(errObject);
            });
});

// Add child to list // check for default parent check
function addToChildCategoryList(childCategoryList, childId, parentCatId, defaultParentId) {
    // check if already added or not
    if (childCategoryList.indexOf(childId) == -1) {
        // check if it has any deafult parent specified
        if (typeof (defaultParentId) != 'undefined') {
            // default parent same
            if (parentCatId == defaultParentId) {
                childCategoryList.push(childId);
            }
        } else {
            childCategoryList.push(childId);
        }
    }

    return childCategoryList;
}

/* Category update */
router.post('/update', function (req, res) {
    var validationErrors = validationsCheckOnInserts(req);
    if (validationErrors.length > 0) {
        returnError({error: 1, message: "Validation error", data: validationErrors}, res);
    }
    //Bug here. Commission value not getting updated/added if not set in insert.
    else {
        // Get category details before to get old commission value
        var categoryId = (req.body._id).toString();
        categoryUtil.getCategoryById(categoryId)
                .then(function (categoryDetails) {
                    /*
                     * Update call
                     */
                    categoryUtil.updateCategory(req.body)
                            .then(function (respObject) {
                                res.send(respObject);

                                //
                                // If commission changed - Check offer price is crossing catalogue price of product
                                var oldCommission = categoryDetails[0].commissionValues;
                                var newCommission = req.body.commissionValues;
                                if (oldCommission != newCommission) {
                                    // check if category is parent / grand parent / only product types
                                    relationsUtil.getRelations()
                                            .then(function (relationsList) {
                                                var relationsCnt = relationsList.length;
                                                // not found check
                                                if (relationsCnt == 0) {
                                                    return;
                                                }

                                                // If parent get its children
                                                var isParent = false;
                                                var childCategoryList = new Array();
                                                for (var i = 0; i < relationsCnt; i++) {
                                                    var mainCategoryId = (relationsList[i].main_category_id).toString();
                                                    var mainCatMatch = false;

                                                    // Main cat matches
                                                    var child1 = relationsList[i].children;
                                                    var child1Cnt = child1.length;
                                                    if (categoryId == mainCategoryId) {
                                                        isParent = true;
                                                        mainCatMatch = true;

                                                        for (var j = 0; j < child1Cnt; j++) {
                                                            var child1Id = (child1[j]._id).toString();
                                                            var child1DefaultParent = child1[j].defaultParent;

                                                            // check if already added or not
                                                            addToChildCategoryList(childCategoryList, child1Id, categoryId, child1DefaultParent._id);

                                                            // level 2
                                                            var child2 = child1[j].children;
                                                            var child2Cnt = child2.length;
                                                            for (var k = 0; k < child2Cnt; k++) {
                                                                var child2Id = (child2[k]._id).toString();
                                                                var child2DefaultParent = child2[k].defaultParent;

                                                                // check if already added or not
                                                                addToChildCategoryList(childCategoryList, child2Id, categoryId, child2DefaultParent._id);

                                                                // level 3
                                                                var child3 = child2[k].children;
                                                                var child3Cnt = child3.length;
                                                                for (var l = 0; l < child3Cnt; l++) {
                                                                    var child3Id = (child3[l]._id).toString();
                                                                    var child3DefaultParent = child3[l].defaultParent;

                                                                    // check if already added or not
                                                                    addToChildCategoryList(childCategoryList, child3Id, categoryId, child3DefaultParent._id);
                                                                }

                                                            }
                                                        }
                                                    }// EOF main cat match

                                                    // Match start for level 1
                                                    if (!mainCatMatch) {
                                                        for (var j = 0; j < child1Cnt; j++) {
                                                            var child1Id = (child1[j]._id).toString();
                                                            var level1Match = false;

                                                            // check match
                                                            if (categoryId == child1Id) {
                                                                isParent = true;
                                                                level1Match = true;

                                                                // level 2
                                                                var child2 = child1[j].children;
                                                                var child2Cnt = child2.length;
                                                                for (var k = 0; k < child2Cnt; k++) {
                                                                    var child2Id = (child2[k]._id).toString();
                                                                    var child2DefaultParent = child2[k].defaultParent;

                                                                    // check if already added or not
                                                                    addToChildCategoryList(childCategoryList, child2Id, categoryId, child2DefaultParent._id);

                                                                    // level 3
                                                                    var child3 = child2[k].children;
                                                                    var child3Cnt = child3.length;
                                                                    for (var l = 0; l < child3Cnt; l++) {
                                                                        var child3Id = (child3[l]._id).toString();
                                                                        var child3DefaultParent = child3[l].defaultParent;

                                                                        // check if already added or not
                                                                        addToChildCategoryList(childCategoryList, child3Id, categoryId, child3DefaultParent._id);
                                                                    }
                                                                }

                                                                break;// exit current internal loop
                                                            }

                                                            // Match start for level 2
                                                            if (!level1Match) {
                                                                // level 2
                                                                var child2 = child1[j].children;
                                                                var child2Cnt = child2.length;
                                                                for (var k = 0; k < child2Cnt; k++) {
                                                                    var child2Id = (child2[k]._id).toString();

                                                                    // check match
                                                                    if (categoryId == child2Id) {
                                                                        isParent = true;

                                                                        // level 3
                                                                        var child3 = child2[k].children;
                                                                        var child3Cnt = child3.length;
                                                                        for (var l = 0; l < child3Cnt; l++) {
                                                                            var child3Id = (child3[l]._id).toString();
                                                                            var child3DefaultParent = child3[l].defaultParent;

                                                                            // check if already added or not
                                                                            addToChildCategoryList(childCategoryList, child3Id, categoryId, child3DefaultParent._id);
                                                                        }

                                                                        break;// exit current internal loop
                                                                    }
                                                                }
                                                            }// EOF Match start for level 2

                                                        }

                                                    }// EOF Match start for level 1

                                                }// Eof for

                                                // If parent
                                                if (isParent) {
                                                    var childCategoryListCnt = childCategoryList.length;
                                                    if (childCategoryListCnt == 0) {
                                                        checkProductPrice(categoryId, newCommission);
                                                    }
                                                    // get category whose commission is 0
                                                    else {
                                                        // check for current id too
                                                        checkProductPrice(categoryId, newCommission);

                                                        // Get all category list
                                                        categoryUtil.getAllCategories()
                                                                .then(function (allCategoryList) {
                                                                    var allCategoryListCnt = allCategoryList.length;

                                                                    // check which category has commission 0
                                                                    for (var c = 0; c < childCategoryListCnt; c++) {
                                                                        var childCategoryId = childCategoryList[c];

                                                                        for (var d = 0; d < allCategoryListCnt; d++) {

                                                                            if ((childCategoryId == (allCategoryList[d]['_id']).toString()) && (allCategoryList[d]['commissionValues'] == 0)) {

                                                                                checkProductPrice(childCategoryId, newCommission);
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                })
                                                                .catch(function (errObjectAllCat) {

                                                                });// EOF all category get
                                                    }
                                                }
                                                // not parent
                                                else {
                                                    checkProductPrice(categoryId, newCommission);
                                                }
                                            });// EOF Get relations
                                }// End of If

                            })
                            .catch(function (errObject) {
                                if (errObject.errors) {
                                    res.send({error: 1, message: errObject.message, data: errObject});
                                }
                            });
                })
                .catch(function (errObjectCatGet) {
                    if (errObjectCatGet.errors) {
                        res.send({error: 1, message: errObjectCatGet.message, data: errObjectCatGet});
                    }
                });
    }
});

// check product for price change after commission value change
function checkProductPrice(categoryId, newCommission) {
    // Get total count
    var countCriteria = {
        'category._id': mongoose.Types.ObjectId(categoryId),
        'variants.suppliers.isActive': 1
    };
    productUtil.getProductCount(countCriteria)
            .then(function (allCount) {
                var limit = 100;
                var skip = 0;
                var pageNum = 1;
                var suppliersToUpdate = [];

                // get products
                if (allCount > 0) {
                    getProductsForCommissionCheck(categoryId, newCommission, allCount, limit, skip, pageNum, suppliersToUpdate);
                }
            })
            .catch(function (errGetCount) {

            });
}

// get product for commission check
function getProductsForCommissionCheck(categoryId, newCommission, allCount, limit, skip, pageNum, suppliersToUpdate) {
    var findCriteria = {
        'category._id': mongoose.Types.ObjectId(categoryId),
        'variants.suppliers.isActive': 1
    };
    var dataObj = {
        findCriteria: findCriteria,
        limit: limit,
        skip: skip
    };
    productUtil.getProductByCustom(dataObj)
            .then(function (productDetails) {
                var productCnt = productDetails.length;
                if (productCnt > 0) {
                    // to loop main product
                    for (var i = 0; i < productCnt; i++) {
                        var variants = productDetails[i].variants;
                        var variantsCnt = variants.length;

                        // to loop variants
                        for (var j = 0; j < variantsCnt; j++) {
                            var suppliers = variants[j].suppliers;
                            var suppliersCnt = suppliers.length;
                            var tmpCataloguePrice = variants[j].cataloguePrice;

                            // to loop suppliers
                            for (var k = 0; k < suppliersCnt; k++) {
                                var tmpSupplierStatus = suppliers[k].isActive;
                                if (tmpSupplierStatus != 1) {
                                    continue;
                                }

                                var tmpSupplierId = suppliers[k]._id;
                                var tmpDiscountPrice = suppliers[k].discountPrice;
                                var newOfferPrice = tmpDiscountPrice + Math.ceil(tmpDiscountPrice * (newCommission / 100));

                                // check if offer price exceeds catalogue price
                                if (newOfferPrice > tmpCataloguePrice) {
                                    // push to update supplier array
                                    var tmpObj = {
                                        supplierId: tmpSupplierId,
                                        index: k
                                    };
                                    suppliersToUpdate.push(tmpObj);
                                }// EOF check if
                            }// EOF supplier loop
                        }// EOF variants loop
                    }// EOF main product loop

                    // Again do for next batch
                    pageNum += 1;
                    var skip = (pageNum - 1) * limit;

                    // do it if all record not looped through
                    if (skip < allCount) {
                        getProductsForCommissionCheck(categoryId, newCommission, allCount, limit, skip, pageNum, suppliersToUpdate);
                    }
                    // done all record processing
                    else {
                        var suppliersToUpdateCnt = suppliersToUpdate.length;
                        for (var s = 0; s < suppliersToUpdateCnt; s++) {
                            // update
                            var setObj = new Object();
                            var supInit = 'variants.$.suppliers.' + suppliersToUpdate[s].index + '.'
                            setObj[supInit + 'isActive'] = 4;
                            setObj[supInit + 'reasonForStatusChange'] = 'Offer price exceeds Catalogue price of product. Due to Commission change by Admin';
                            var updateObj = {
                                $set: setObj
                            };
                            var queryObj = {
                                'variants.suppliers._id': mongoose.Types.ObjectId(suppliersToUpdate[s].supplierId)
                            };

                            productUtil.updateProduct(queryObj, updateObj)
                                    .then(function (respObject) {
                                        // Send notification to supplier
                                    })
                                    .catch(function (errObject) {

                                    });
                        }
                    }

                }// EOF if
            })
            .catch(function (errProductDetails) {

            });
}


router.get('/addSlugToAll', function (req, res) {
    categoryUtil.addSlugToCategories(req.query)
            .then(function (respObject) {
                res.send(respObject);
            })
            .catch(function (errObject) {
                if (errObject.errors) {
                    res.send({error: 1, message: errObject.message, data: errObject});
                }
            });
});

/* Get main categories */
router.get('/getAllMainCategories', function (req, res) {
    var findCriteria = {
        isMainCategory: true,
        isActive: true
    };
    var projectFields = {name: 1, slug: 1};
    var dataObj = {
        findCriteria: findCriteria,
        projectFields: projectFields
    };
    categoryUtil.getCategoryByFindCriteria(dataObj)
            .then(function (respObject) {
                res.send({'error': 0, 'msg': 'Main category list', 'data': respObject});
            })
            .catch(function (errObject) {
                res.send({'error': 1, 'msg': 'No main category found', 'data': []});
            });
});

/* Get other categories */
router.get('/getAllOtherCategories', function (req, res) {
    var findCriteria = {
        isMainCategory: false,
        isProductType: false,
        isActive: true
    };
    var projectFields = {name: 1, slug: 1};
    var dataObj = {
        findCriteria: findCriteria,
        projectFields: projectFields
    };
    categoryUtil.getCategoryByFindCriteria(dataObj)
            .then(function (respObject) {
                res.send({'error': 0, 'msg': 'Other category list', 'data': respObject});
            })
            .catch(function (errObject) {
                res.send({'error': 1, 'msg': 'No other category found', 'data': []});
            });
});


/* Get product types only */
router.get('/getAllProductTypes', function (req, res) {
    var findCriteria = {
        isProductType: true,
        isActive: true
    };
    var projectFields = {name: 1, productFilters: 1, variantFilters: 1, bulkRanges: 1, commissionValues: 1, slug: 1};
    var dataObj = {
        findCriteria: findCriteria,
        projectFields: projectFields
    };
    categoryUtil.getCategoryByFindCriteria(dataObj)
            .then(function (respObject) {
                res.send({'error': 0, 'msg': 'Product type list', 'data': respObject});
            })
            .catch(function (errObject) {
                res.send({'error': 1, 'msg': 'No product type found', 'data': []});
            });
});

// Get parents commission if given category commission is 0
router.get('/getCommission/:id', function (req, res) {
    var categoryId = req.params.id;

    // validations
    if (typeof (categoryId) == 'undefined' || !commonFunctions.isMongoId(categoryId)) {
        return res.send({'error': 1, 'msg': 'Category Id not correct', 'data': {commission: 0}});
    }

    // get category details
    categoryUtil.getCategoryById(categoryId)
            .then(function (respObject) {
                // not found check
                if (respObject.length == 0) {
                    return res.send({'error': 1, 'msg': 'Category not found', 'data': {commission: 0}});
                }

                // get commission
                var categoryCommission = respObject[0].commissionValues;
                if (categoryCommission > 0) {
                    return res.send({'error': 0, 'msg': 'Category commission', 'data': {commission: categoryCommission}});
                }

                // get relations to find out if parant has any commission
                relationsUtil.getRelations()
                        .then(function (relationsList) {
                            var relationsCnt = relationsList.length;
                            // not found check
                            if (relationsCnt == 0) {
                                return res.send({'error': 1, 'msg': 'Unable to get category commission', 'data': {commission: 0}});
                            }

                            // Get commission from parent
                            var matchParentId = 0;
                            var matchGrandParentId = 0;
                            var matchGrandGrandParentId = 0;
                            loop1:
                                    for (var i = 0; i < relationsCnt; i++) {
                                var mainCategoryId = (relationsList[i].main_category_id).toString();

                                // found in main
                                if (categoryId == mainCategoryId) {
                                    break;
                                }

                                // level 1 matches
                                var child1 = relationsList[i].children;
                                var child1Cnt = child1.length;
                                loop2:
                                        for (var j = 0; j < child1Cnt; j++) {
                                    var child1Id = (child1[j]._id).toString();
                                    var child1DefaultParent = child1[j].defaultParent;

                                    // match found at level 1
                                    if (categoryId == child1Id) {
                                        // check if it has any deafult parent specified
                                        if (typeof (child1DefaultParent._id) != 'undefined') {
                                            matchParentId = (child1DefaultParent._id).toString();
                                        } else {
                                            matchParentId = mainCategoryId;
                                        }

                                        break loop1;
                                    }

                                    // level 2
                                    var child2 = child1[j].children;
                                    var child2Cnt = child2.length;
                                    for (var k = 0; k < child2Cnt; k++) {
                                        var child2Id = (child2[k]._id).toString();
                                        var child2DefaultParent = child2[k].defaultParent;

                                        // match found at level 2
                                        if (categoryId == child2Id) {
                                            // check if it has any deafult parent specified
                                            if (typeof (child2DefaultParent._id) != 'undefined') {
                                                matchParentId = (child2DefaultParent._id).toString();
                                            } else {
                                                matchParentId = child1Id;
                                            }

                                            matchGrandParentId = mainCategoryId;

                                            break loop1;
                                        }

                                        // level 3
                                        var child3 = child2[k].children;
                                        var child3Cnt = child3.length;
                                        for (var l = 0; l < child3Cnt; l++) {
                                            var child3Id = (child3[l]._id).toString();
                                            var child3DefaultParent = child3[l].defaultParent;

                                            // match found at level 3
                                            if (categoryId == child3Id) {
                                                // check if it has any deafult parent specified
                                                if (typeof (child3DefaultParent._id) != 'undefined') {
                                                    matchParentId = (child3DefaultParent._id).toString();
                                                } else {
                                                    matchParentId = child2Id;
                                                }

                                                matchGrandParentId = child1Id;
                                                matchGrandGrandParentId = mainCategoryId;

                                                break loop1;
                                            }
                                        }// EOF level 3 for

                                    }// EOF level 2 for

                                }// EOF level 1 for
                            }// EOF loop 1

                            // 0 then not found
                            var commissionZeroSuccess = {'error': 0, 'msg': 'Category commission', 'data': {commission: 0}};
                            if (matchParentId == 0) {
                                return res.send(commissionZeroSuccess);
                            }

                            // Get match parent category details
                            categoryUtil.getCategoryById(matchParentId)
                                    .then(function (parentCatDetails) {
                                        // not found check
                                        if (parentCatDetails.length == 0) {
                                            return res.send(commissionZeroSuccess);
                                        }

                                        // send commission
                                        var matchParentCommission = parentCatDetails[0].commissionValues;
                                        if (matchParentCommission > 0) {
                                            return res.send({'error': 0, 'msg': 'Category commission', 'data': {commission: matchParentCommission}});
                                        }

                                        // check for grand parent commission
                                        categoryUtil.getCategoryById(matchGrandParentId)
                                                .then(function (grandParentCatDetails) {
                                                    // not found check
                                                    if (grandParentCatDetails.length == 0) {
                                                        return res.send(commissionZeroSuccess);
                                                    }

                                                    // send commission
                                                    var matchGrandParentCommission = grandParentCatDetails[0].commissionValues;
                                                    if (matchGrandParentCommission > 0) {
                                                        return res.send({'error': 0, 'msg': 'Category commission', 'data': {commission: matchGrandParentCommission}});
                                                    }

                                                    // Check for grand grand parent
                                                    categoryUtil.getCategoryById(matchGrandGrandParentId)
                                                            .then(function (grandGrandParentCatDetails) {
                                                                // not found check
                                                                if (grandGrandParentCatDetails.length == 0) {
                                                                    return res.send(commissionZeroSuccess);
                                                                }

                                                                // send commission
                                                                var matchGrandGrandParentCommission = grandGrandParentCatDetails[0].commissionValues;
                                                                return res.send({'error': 0, 'msg': 'Category commission', 'data': {commission: matchGrandGrandParentCommission}});
                                                            })
                                                            .catch(function (errObjectGrandGrandParent) {
                                                                return res.send(commissionZeroSuccess);
                                                            });

                                                })
                                                .catch(function (errObjectGrandParent) {
                                                    return res.send(commissionZeroSuccess);
                                                });

                                    })
                                    .catch(function (errObjectParent) {
                                        return res.send(commissionZeroSuccess);
                                    });
                        })
                        .catch(function (errObjectRel) {
                            return res.send({'error': 1, 'msg': 'Unable to get category commission', 'data': {commission: 0}});
                        });
            })
            .catch(function (errObject) {
                return res.send({'error': 1, 'msg': 'Unable to get category commission', 'data': {commission: 0}});
            });
});

/* Angular routing helper */
//router.get('/category/:name', function (req, res){
//    res.render('/pages/category/'+req.params.name, {data : config});
//});

//router.get('/removeDuplicateFilters', function(req, res, next){
//    categoryUtil.removeDuplicateFilters()
//            .then(function (respObject) {
//                console.log("respObject");
//                console.log(respObject);
//            })
//            .catch(function (errObject) {
//                console.log("errObject");
//                console.log(errObject);
//            });
//});

function returnError(errObj, res) {
    res.send(errObj);
}
;

function validationsCheckOnInserts(req) {
    var validationErrors = [];
    (!req.body.hasOwnProperty('name') || req.body.name == '') ? validationErrors.push({error: 1, message: 'Name is not set'}) : {};
    (!req.body.hasOwnProperty('image') || req.body.image == '') ? validationErrors.push({error: 1, message: 'Image is not set '}) : {};
    (!req.body.hasOwnProperty('icon') || req.body.icon == '') ? validationErrors.push({error: 1, message: 'Icon is not set'}) : {};
    (!req.body.hasOwnProperty('description') && !req.body.description) ? validationErrors.push({error: 1, message: 'Description is not set'}) : {};
//    (!req.body.hasOwnProperty('isMainCategory') && !req.body.isMainCategory) ? validationErrors.push({error: 1, message:'isMainCategory is not set'}) : {} ;
//    !req.body.hasOwnProperty('isProductType') && !req.body.isProductType ? validationErrors.push({error: 1, message:'isProductType is not set'}) : {} ;

    (!req.body.hasOwnProperty('isMainCategory') && !req.body.hasOwnProperty('isProductType') && !req.body.isProductType == "Yes" && req.body.isMainCategory == "Yes") ? validationErrors.push({error: 1, message: 'Category cannot be both main and product type'}) : {};
    ((!req.body.hasOwnProperty('commissionValues') && !req.body.commissionValues && req.body.isSetCommision == 'Yes') || (req.body.commissionValues < 0)) ? validationErrors.push({error: 1, message: 'Commission Value is not set or is not in correct format.'}) : {};
    (!req.body.hasOwnProperty('status') && !req.body.status) ? validationErrors.push({error: 1, message: 'Status is not set'}) : {};
    (Object.keys(req.body.productFilters).length < 0) ? validationErrors.push({error: 1, message: 'Please select atleast one product level filter'}) : {};
    (Object.keys(req.body.variantFilters).length < 0) ? validationErrors.push({error: 1, message: 'Please select atleast one variant level filter'}) : {};
    (req.body.bulkFrom1 < 0 || req.body.bulkFrom2 < 0 || req.body.bulkFrom3 < 0 || req.body.bulkTo1 < 0 || req.body.bulkTo2 < 0 || req.body.bulkTo3 < 0 ) ? validationErrors.push({error: 1, message: 'Negative values are not allowed for bulk values'}) : {};
    (req.body.bulkFrom1 > req.body.bulkTo1) ? validationErrors.push({error: 1, message: 'Bulk from field cannot be grater than to field in Bulk field 1'}) : {};
    (req.body.bulkFrom2 > req.body.bulkTo2) ? validationErrors.push({error: 1, message: 'Bulk from field cannot be grater than to field in Bulk field 2'}) : {};
    (req.body.bulkFrom3 > req.body.bulkTo3) ? validationErrors.push({error: 1, message: 'Bulk from field cannot be grater than to field in Bulk field 3'}) : {};
    //(req.body.bulkFrom4 > req.body.bulkTo4) ? validationErrors.push({error: 1, message: 'Bulk from field cannot be grater than to field in Bulk field 4'}) : {};

    return validationErrors;

}

function isValid(str) {
    return !/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str);
}

module.exports = router;
