// Use - Handles all api related to USSD codes

// Required modules
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var async = require('async');
var moment = require('moment');
var ussdRegexpMissingUtil = require('../utils/ussdRegexpMissingUtil');
var ussdInvalidUtil = require('../utils/ussdInvalidUtil');
var ussdRegexpUtil = require('../utils/ussdRegexpUtil');
var operatorUSSDUtil = require('../utils/operatorUSSDUtil');
var commonFunctions = require('../utils/commonFunctions');

// validate ussd regex missing form data details 
function validateRegexpMissingInsertData(command, res, data, modDataObj) {
//    if (typeof (data.ussdCode) == 'undefined' || data.ussdCode == '') {
//        res.send({'error': 1, 'errorCode': 0, 'msg': 'USSD code empty', 'data': {}});
//        return false;
//    }
    if (typeof (data.ussdType) == 'undefined' || data.ussdType == '') {
        res.send({'error': 1, 'errorCode': 0, 'msg': 'USSD type empty', 'data': {}});
        return false;
    }
    if (typeof (data.ussdReplyMsg) == 'undefined' || data.ussdReplyMsg == '') {
        res.send({'error': 1, 'errorCode': 0, 'msg': 'USSD reply message empty', 'data': {}});
        return false;
    }
//    if (typeof (data.mccMnc) == 'undefined' || data.mccMnc == '') {
//        res.send({'error': 1, 'errorCode': 0, 'msg': 'MCC-MNC number empty', 'data': {}});
//        return false;
//    }
//    if (data.mccMnc.length < 6) {
//        res.send({'error': 1, 'errorCode': 0, 'msg': 'MCC-MNC must be 6 digits long', 'data': {}});
//        return false;
//    }
//    if (typeof (data.operatorName) == 'undefined' || data.operatorName == '') {
//        res.send({'error': 1, 'errorCode': 0, 'msg': 'Operator name empty', 'data': {}});
//        return false;
//    }
//    if (typeof (data.operatorId) == 'undefined' || data.operatorId == '') {
//        res.send({'error': 1, 'errorCode': 0, 'msg': 'Operator id empty', 'data': {}});
//        return false;
//    }

    return true;
}

// validate ussd invalid form data details 
function validateUSSDInvalidInsertData(command, res, data, modDataObj) {
    if (typeof (data.ussdCode) == 'undefined' || data.ussdCode == '') {
        res.send({'error': 1, 'errorCode': 0, 'msg': 'USSD code empty', 'data': {}});
        return false;
    }
    if (typeof (data.ussdType) == 'undefined' || data.ussdType == '') {
        res.send({'error': 1, 'errorCode': 0, 'msg': 'USSD type empty', 'data': {}});
        return false;
    }
    if (typeof (data.mccMnc) == 'undefined' || data.mccMnc == '') {
        res.send({'error': 1, 'errorCode': 0, 'msg': 'MCC-MNC number empty', 'data': {}});
        return false;
    }
    if (data.mccMnc.length < 6) {
        res.send({'error': 1, 'errorCode': 0, 'msg': 'MCC-MNC must be 6 digits long', 'data': {}});
        return false;
    }
    if (typeof (data.operatorName) == 'undefined' || data.operatorName == '') {
        res.send({'error': 1, 'errorCode': 0, 'msg': 'Operator name empty', 'data': {}});
        return false;
    }
    if (typeof (data.operatorId) == 'undefined' || data.operatorId == '') {
        res.send({'error': 1, 'errorCode': 0, 'msg': 'Operator id empty', 'data': {}});
        return false;
    }

    return true;
}

// send regular expression last used date update api response
function sendRegexpLastUsedApiResponse(res, totalCnt, doneCounter, updatedRecords, errorDataArr) {
    if (totalCnt == doneCounter) {
        res.send({'error': 0, 'errorCode': 0, 'msg': 'Regular expression last used date update results', 'data': updatedRecords, 'wrongData': errorDataArr});
    }
}

// send ussd regex missing form add api response
function sendRegexpMissingMultiAddApiResponse(res, totalCnt, doneCounter, addedRecords, errorDataArr) {
    if (totalCnt == doneCounter) {
        res.send({'error': 0, 'errorCode': 0, 'msg': 'Missing USSD RegExp entry add results', 'data': addedRecords, 'wrongData': errorDataArr});
    }
}

// send ussd invalid form add api response
function sendUSSDInvalidMultiAddApiResponse(res, totalCnt, doneCounter, addedRecords, errorDataArr) {
    if (totalCnt == doneCounter) {
        res.send({'error': 0, 'errorCode': 0, 'msg': 'Invalid USSD entry add results', 'data': addedRecords, 'wrongData': errorDataArr});
    }
}

//
// insert record if ussd regexp on app not matches with db on app
// compulsory - ussdCode, ussdType, ussdReplyMsg, operatorName, operatorId, mccMnc
// optional - userId, reason, appCode, appVersion, osVersion
router.post('/regexp-missing/add', function (req, res, next) {
    // data passed
    var data = req.body;

    // Validations
    if (!validateRegexpMissingInsertData('ADD', res, data, {})) {
        return;
    }

    // now add
    var insertObj = new Object();
//    insertObj.ussdCode = data.ussdCode;
    insertObj.userId = req.decoded._id;

    //packType
    insertObj.ussdType = data.ussdType;
    insertObj.ussdReplyMsg = data.ussdReplyMsg;
//    insertObj.mccMnc = data.mccMnc;
    insertObj.operatorName = data.operatorName ? data.operatorName : '';
    insertObj.operatorId = data.operatorId ? data.operatorId : 0;
    insertObj.appCode = data.appCode ? data.appCode : 1;
    insertObj.appVersion = data.appVersion ? data.appVersion : 0;
    insertObj.osVersion = data.osVersion ? data.osVersion : 0;
//    callStartTime => Date Object
    insertObj.callStartTime = data.callStartTime ? new Date(parseInt(data.callStartTime)) : new Date();
    insertObj.callDuration = data.callDuration ? data.callDuration : 0;
    insertObj.reason = data.reason ? data.reason : 0;

    console.log(insertObj.callStartTime);
    ussdRegexpMissingUtil.insert(insertObj)
            .then(function (respObject) {
                res.send({'error': 0, 'errorCode': 0, 'msg': 'Missing USSD RegExp entry added successfully', 'data': {}});
            })
            .catch(function (errObject) {
                console.log("errObject");
                console.log(errObject);
                res.send({'error': 1, 'errorCode': 0, 'msg': 'Unable to add missing USSD RegExp entry', 'data': errObject});
            });
});// EOF api

//
// Multiple - insert record if ussd regexp on app not matches with db on app
// compulsory - ussdCode, ussdType, ussdReplyMsg, operatorName, operatorId, mccMnc
// optional - userId, reason, appCode, appVersion, osVersion
router.post('/regexp-missing/add/multi', function (req, res, next) {
    var JSONDataArray = eval(req.body.objects);
    // APP will send data in Array of objects
    console.log("JSONDataArray");
    console.log(JSONDataArray);
    var dataArr = [];
    try {
        dataArr = JSONDataArray;
    } catch (err) {
        dataArr = [];
    }

    // No data sent by app
    if (dataArr.length == 0) {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'No data passed', 'data': [], 'wrongData': [], 'regexpData': []});
    }


    // Validations on array of objects
    var errorDataArr = new Array();
    for (var i = 0; i < dataArr.length; i++) {
        var errorMsgArr = new Array();
//        if (!dataArr[i]['ussdCode'] || dataArr[i]['ussdCode'] == '') {
//            errorMsgArr.push('USSD code empty');
//        }
        if (!dataArr[i]['ussdType'] || dataArr[i]['ussdType'] == '') {
            errorMsgArr.push('USSD type empty');
        }
        if (!dataArr[i]['ussdReplyMsg'] || dataArr[i]['ussdReplyMsg'] == '') {
            errorMsgArr.push('USSD reply message empty');
        }
//        if (!dataArr[i]['mccMnc'] || dataArr[i]['mccMnc'] == '') {
//            errorMsgArr.push('MCC-MNC number empty');
//        }
//        if (dataArr[i]['mccMnc'] && (dataArr[i]['mccMnc']).length < 6) {
//            errorMsgArr.push('MCC-MNC must be 6 digits long');
//        }
        if (!dataArr[i]['operatorName'] || dataArr[i]['operatorName'] == '') {
            errorMsgArr.push('Operator name empty');
        }
        if (!dataArr[i]['operatorId'] || dataArr[i]['operatorId'] == '') {
            errorMsgArr.push('Operator id empty');
        }

        if (errorMsgArr.length > 0) {
            var tmpDataObj = dataArr[i];
            tmpDataObj['errorMsg'] = errorMsgArr;
            errorDataArr.push(tmpDataObj);

            // delete this data
            dataArr.splice(i, 1);
            i--;
        }
    }

    // nothing left to update
    if (dataArr.length == 0) {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'All passed data having errors', 'data': [], 'wrongData': errorDataArr, 'regexpData': []});
    }

    // Get unique operators
    var uniqueOperatorId = new Array();
    for (var i = 0; i < dataArr.length; i++) {
        if (uniqueOperatorId.indexOf(dataArr[i]['operatorId']) == -1) {
            uniqueOperatorId.push(dataArr[i]['operatorId']);
        }
    }

    // Loop through & update all records one by one
    var doneCounter = 0;
    var addedRecords = new Array();
    dataArr.forEach(function (dataObj, index) {
        // Check if ussd msg entry already present
        // var findCriteria = {ussdReplyMsg: new RegExp('^'+dataObj.ussdReplyMsg+'$', 'i')};
        var findCriteria = {ussdReplyMsg: (dataObj.ussdReplyMsg).trim(), operatorId: dataObj.operatorId}; // working properly
        // var findCriteria = { '$text': { $search: '"'+(dataObj.ussdReplyMsg).trim()+'"' } };

        var projectFields = {_id: 1};
        ussdRegexpMissingUtil.getByCustom({findCriteria: findCriteria, projectFields: projectFields})
                .then(function (findResResp) {
                    // already exists // consider it as added
                    if (findResResp && (findResResp.length > 0)) {
                        doneCounter++;
                        addedRecords.push(dataObj);
                        sendRegexpMissingMultiAddApiResponse(res, dataArr.length, doneCounter, addedRecords, errorDataArr);
                        return;
                    }
                    // now add
                    var insertObj = new Object();
                    insertObj.ussdCode = dataObj.ussdCode;
                    insertObj.ussdType = dataObj.ussdType;
                    insertObj.ussdReplyMsg = dataObj.ussdReplyMsg;
//                    insertObj.mccMnc = dataObj.mccMnc;
                    insertObj.operatorName = dataObj.operatorName ? dataObj.operatorName : '';
                    insertObj.operatorId = dataObj.operatorId ? dataObj.operatorId : 0;
                    insertObj.appCode = dataObj.appCode ? dataObj.appCode : 1;
                    insertObj.appVersion = dataObj.appVersion ? dataObj.appVersion : 0;
                    insertObj.osVersion = dataObj.osVersion ? dataObj.osVersion : 0;

                    insertObj.userId = req.decoded._id;
                    insertObj.callStartTime = dataObj.callStartTime ? dataObj.callStartTime : 0;
                    insertObj.callDuration = dataObj.callDuration ? dataObj.callDuration : 0;
                    insertObj.reason = dataObj.reason ? dataObj.reason : 0;
                    ussdRegexpMissingUtil.insert(insertObj)
                            .then(function (respObject) {
                                doneCounter++;

                                // some insert error
                                if (!respObject) {
                                    sendRegexpMissingMultiAddApiResponse(res, dataArr.length, doneCounter, addedRecords, errorDataArr);
                                    return;
                                }

                                // record added
                                addedRecords.push(dataObj);
                                sendRegexpMissingMultiAddApiResponse(res, dataArr.length, doneCounter, addedRecords, errorDataArr);
                            })
                            .catch(function (errObject) {
                                doneCounter++;
                                sendRegexpMissingMultiAddApiResponse(res, dataArr.length, doneCounter, addedRecords, errorDataArr);
                            });
                })
                .catch(function (errObject) {
                    doneCounter++;
                    console.log("errObject in /regexp-missing/add/multi routes");
                    console.log(errObject);
                    sendRegexpMissingMultiAddApiResponse(res, dataArr.length, doneCounter, addedRecords, errorDataArr);
                });
    });// EOF forEach loop


});// EOF api

//
// insert record if ussd giving response invalid
// compulsory - ussdCode, ussdType, operatorName, operatorId, mccMnc
// optional - ussdReplyMsg, userId, reason, appCode, appVersion, osVersion
router.post('/invalid/add', function (req, res, next) {
    // data passed
    var data = req.body;

    // Validations
    if (!validateUSSDInvalidInsertData('ADD', res, data, {})) {
        return;
    }

    // now add
    var insertObj = new Object();
    insertObj.ussdCode = data.ussdCode;
    insertObj.ussdCodeNew = data.ussdCodeNew ? data.ussdCodeNew : '';
    insertObj.ussdType = data.ussdType;
    insertObj.mccMnc = data.mccMnc;
    insertObj.operatorName = data.operatorName ? data.operatorName : '';
    insertObj.operatorId = data.operatorId ? data.operatorId : 0;
    insertObj.ussdReplyMsg = data.ussdReplyMsg ? data.ussdReplyMsg : '';
    insertObj.userId = data.userId ? data.userId : 0;
    insertObj.reason = data.reason ? data.reason : '';
    insertObj.appCode = data.appCode ? data.appCode : 1;
    insertObj.appVersion = data.appVersion ? data.appVersion : 0;
    insertObj.osVersion = data.osVersion ? data.osVersion : 0;
    insertObj.reqFrom = data.reqFrom ? data.reqFrom : 1;

    ussdInvalidUtil.insert(insertObj)
            .then(function (respObject) {
                res.send({'error': 0, 'errorCode': 0, 'msg': 'Invalid USSD entry added successfully', 'data': {}});
            })
            .catch(function (errObject) {
                res.send({'error': 1, 'errorCode': 0, 'msg': 'Unable to add invalid USSD entry', 'data': {}});
            });
});// EOF api

//
// Multiple - insert record if ussd giving response invalid
// compulsory - ussdCode, ussdType, operatorName, operatorId, mccMnc
// optional - ussdReplyMsg, userId, reason, appCode, appVersion, osVersion
router.post('/invalid/add/multi', function (req, res, next) {
    // APP will send data in Array of objects
    var dataArr;
    try {
        dataArr = JSON.parse(req.body.data);
    } catch (err) {
        dataArr = [];
    }

    // No data sent by app
    if (dataArr.length == 0) {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'No data passed', 'data': [], 'wrongData': []});
    }

    // Validations on array of objects
    var errorDataArr = new Array();
    for (var i = 0; i < dataArr.length; i++) {
        var errorMsgArr = new Array();
        if (!dataArr[i]['ussdCode'] || dataArr[i]['ussdCode'] == '') {
            errorMsgArr.push('USSD code empty');
        }
        if (!dataArr[i]['ussdType'] || dataArr[i]['ussdType'] == '') {
            errorMsgArr.push('USSD type empty');
        }
        if (!dataArr[i]['mccMnc'] || dataArr[i]['mccMnc'] == '') {
            errorMsgArr.push('MCC-MNC number empty');
        }
        if (dataArr[i]['mccMnc'] && (dataArr[i]['mccMnc']).length < 6) {
            errorMsgArr.push('MCC-MNC must be 6 digits long');
        }
        if (!dataArr[i]['operatorName'] || dataArr[i]['operatorName'] == '') {
            errorMsgArr.push('Operator name empty');
        }
        if (!dataArr[i]['operatorId'] || dataArr[i]['operatorId'] == '') {
            errorMsgArr.push('Operator id empty');
        }

        if (errorMsgArr.length > 0) {
            var tmpDataObj = dataArr[i];
            tmpDataObj['errorMsg'] = errorMsgArr;
            errorDataArr.push(tmpDataObj);

            // delete this data
            dataArr.splice(i, 1);
            i--;
        }
    }

    // nothing left to update
    if (dataArr.length == 0) {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'All passed data having errors', 'data': [], 'wrongData': errorDataArr});
    }

    // Loop through & update all records one by one
    var doneCounter = 0;
    var addedRecords = new Array();
    dataArr.forEach(function (dataObj, index) {
        // now add
        var insertObj = new Object();
        insertObj.ussdCode = dataObj.ussdCode;
        insertObj.ussdCodeNew = dataObj.ussdCodeNew ? dataObj.ussdCodeNew : '';
        insertObj.ussdType = dataObj.ussdType;
        insertObj.mccMnc = dataObj.mccMnc;
        insertObj.operatorName = dataObj.operatorName ? dataObj.operatorName : '';
        insertObj.operatorId = dataObj.operatorId ? dataObj.operatorId : 0;
        insertObj.ussdReplyMsg = dataObj.ussdReplyMsg ? dataObj.ussdReplyMsg : '';
        insertObj.userId = dataObj.userId ? dataObj.userId : 0;
        insertObj.reason = dataObj.reason ? dataObj.reason : '';
        insertObj.appCode = dataObj.appCode ? dataObj.appCode : 1;
        insertObj.appVersion = dataObj.appVersion ? dataObj.appVersion : 0;
        insertObj.osVersion = dataObj.osVersion ? dataObj.osVersion : 0;
        insertObj.reqFrom = dataObj.reqFrom ? dataObj.reqFrom : 1;

        ussdInvalidUtil.insert(insertObj)
                .then(function (respObject) {
                    doneCounter++;

                    // some insert error
                    if (!respObject) {
                        sendUSSDInvalidMultiAddApiResponse(res, dataArr.length, doneCounter, addedRecords, errorDataArr);
                        return;
                    }

                    // record added
                    addedRecords.push(dataObj);
                    sendUSSDInvalidMultiAddApiResponse(res, dataArr.length, doneCounter, addedRecords, errorDataArr);
                })
                .catch(function (errObject) {
                    doneCounter++;
                    sendUSSDInvalidMultiAddApiResponse(res, dataArr.length, doneCounter, addedRecords, errorDataArr);
                });
    });// EOF forEach loop
});// EOF api

//
// Get USSD Regular expressions for operator
// operatorId will be array [1, 2]
// optional - operatorId

router.get('/regexp/get', function (req, res, next) {
    if (!req.query.packType) {
        res.send({error: 1, msg: "No packtype specified", data: {}});
    }
    if (req.query.token !== '3.14159') {
        res.send({error: 1, msg: "No proper authentication", data: {}});
    } else {
        // Get data from db
        var packType;
        var findCriteria = {isActive: 1};
        if (req.query.packType.indexOf(',') == -1) {
            packType = req.query.packType;
            findCriteria["packType"] = packType;
        } else {
            packType = req.query.packType.split(',');
            findCriteria["packType"] = {$in: packType};
        }
        if (req.query.operatorId) {
            var operatorIds = req.query.operatorId.split(',');
            console.log("operatorIds");
            console.log(operatorIds);
            findCriteria["operatorId"] = {$in: operatorIds}
        }
        var projectFields = {'__v': 0, isActive: 0, updatedDate: 0, lastUsed: 0};
        ussdRegexpUtil.getByCustom({findCriteria: findCriteria, projectFields: projectFields})
                .then(function (respObject) {
                    // no regexp found
                    if (!respObject || (respObject.length == 0)) {
                        res.send({'error': 0, 'errorCode': 0, 'msg': 'No USSD RegExp found', 'count': 0, 'data': []});
                    } else {
                        // Format date
                        var regexpData = respObject;
                        var regexpDataCnt = regexpData.length;
                        for (var i = 0; i < regexpDataCnt; i++) {
                            regexpData[i]['createdDate'] = commonFunctions.getDateYMDHMS(regexpData[i]['createdDate']);
                        }

                        // send all data
                        res.send({'error': 0, 'errorCode': 0, 'msg': 'USSD RegExp List', 'count': regexpData.length, 'data': regexpData});
                    }
                })
                .catch(function (errObject) {
                    res.send({'error': 1, 'errorCode': 0, 'msg': 'Error in getting USSD RegExp', 'count': 0, 'data': errObject});
                });
    }
});// EOF api

//
// Get all operators USSD codes
router.get('/codes/get', function (req, res, next) {
    // optional operator id
    var operatorId = req.query.operatorId ? req.query.operatorId : 0;
    var findCriteria = {isActive: 1};
    if (operatorId) {
        findCriteria["operatorId"] = operatorId;
    }

    // Get data from db    
    var projectFields = {'__v': 0, createdDate: 0, isActive: 0, updatedDate: 0};
    operatorUSSDUtil.getByCustom({findCriteria: findCriteria, projectFields: projectFields})
            .then(function (respObject) {
                // no ussd codes found
                if (!respObject || (respObject.length == 0)) {
                    res.send({'error': 0, 'errorCode': 0, 'msg': 'Operator USSD Codes not found', 'count': 0, 'data': []});
                } else {
                    // send all data
                    res.send({'error': 0, 'errorCode': 0, 'msg': 'Operator USSD Codes List', 'count': respObject.length, 'data': respObject});
                }
            })
            .catch(function (errObject) {
                return res.send({'error': 1, 'errorCode': 0, 'msg': 'Error in getting Operator USSD Codes', 'count': 0, 'data': []});
            });
});// EOF api

//
// Get operators USSD codes after sync time
router.get('/codes/get/sync', function (req, res, next) {
    // check last sync time
    var lastSyncTime = req.query.lastSyncTime;
    if (!lastSyncTime || lastSyncTime == '') {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'Last sync time not passed', 'count': 0, 'data': []});
    }
    if (!commonFunctions.validateYMDHMSDateFormat(lastSyncTime)) {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'Last sync time not valid date. Send in Y-m-d h:m:s format', 'count': 0, 'data': []});
    }
    var isValidDate = moment(lastSyncTime).isValid();
    if (!isValidDate) {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'Last sync time not valid date.', 'count': 0, 'data': []});
    }

    // optional operator id
    var operatorId = req.query.operatorId ? req.query.operatorId : 0;
    var findCriteria = {isActive: 1};
    // for last sync time
    var lsdDate = new Date(lastSyncTime);
    findCriteria['$or'] = [
        {'createdDate': {$gt: lsdDate}},
        {'updatedDate': {$gt: lsdDate}}
    ];
    if (operatorId) {
        findCriteria = {operatorId: operatorId};
    }

    // Get data from db    
    var projectFields = {'__v': 0, createdDate: 0, isActive: 0, updatedDate: 0};
    operatorUSSDUtil.getByCustom({findCriteria: findCriteria, projectFields: projectFields})
            .then(function (respObject) {
                // no ussd codes found
                if (!respObject || (respObject.length == 0)) {
                    return res.send({'error': 0, 'errorCode': 0, 'msg': 'Operator USSD Codes not found', 'count': 0, 'data': []});
                }

                // send all data
                return res.send({'error': 0, 'errorCode': 0, 'msg': 'Operator USSD Codes List', 'count': respObject.length, 'data': respObject});
            })
            .catch(function (errObject) {
                return res.send({'error': 1, 'errorCode': 0, 'msg': 'Error in getting Operator USSD Codes', 'count': 0, 'data': []});
            });
});// EOF api

//
// update last used date time of regular expression 
// compulsory - regexpId, lastUsedTime(Y-m-d h:m:s)
router.post('/regexp/last-used/update', function (req, res, next) {
    // APP will send data in Array of objects
    var dataArr;
    try {
        dataArr = JSON.parse(req.body.data);
    } catch (err) {
        dataArr = [];
    }

    // No data sent by app
    if (dataArr.length == 0) {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'No data passed', 'data': [], 'wrongData': []});
    }

    // Validations on array of objects
    var errorDataArr = new Array();
    for (var i = 0; i < dataArr.length; i++) {
        var errorMsgArr = new Array();
        if (!dataArr[i]['regexpId'] || !commonFunctions.isMongoId(dataArr[i]['regexpId'])) {
            errorMsgArr.push('RegExp id not correct');
        }
        if (!dataArr[i]['lastUsed'] || dataArr[i]['lastUsed'] == '') {
            errorMsgArr.push('Last used date time empty');
        }
        if (!commonFunctions.validateYMDHMSDateFormat(dataArr[i]['lastUsed'])) {
            errorMsgArr.push('Last used date time not valid date. Send in Y-m-d h:m:s format');
        }
        if (commonFunctions.validateYMDHMSDateFormat(dataArr[i]['lastUsed']) && !moment(dataArr[i]['lastUsed']).isValid()) {
            errorMsgArr.push('Last used date time not valid date.');
        }

        if (errorMsgArr.length > 0) {
            var tmpDataObj = dataArr[i];
            tmpDataObj['errorMsg'] = errorMsgArr;
            errorDataArr.push(tmpDataObj);

            // delete this data
            dataArr.splice(i, 1);
            i--;
        }
    }

    // nothing left to update
    if (dataArr.length == 0) {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'All passed data having errors', 'data': [], 'wrongData': errorDataArr});
    }

    // Loop through & update all records one by one
    var doneCounter = 0;
    var updatedRecords = new Array();
    dataArr.forEach(function (dataObj, index) {
        // now update
        var setObj = new Object();
        setObj.lastUsed = new Date(dataObj.lastUsed);
        var updateObj = {
            $set: setObj
        };
        var queryObj = {
            '_id': commonFunctions.stringToObjectId(dataObj.regexpId)
        };

        ussdRegexpUtil.update(queryObj, updateObj)
                .then(function (respObject) {
                    doneCounter++;

                    // some update error
                    if (!respObject) {
                        sendRegexpLastUsedApiResponse(res, dataArr.length, doneCounter, updatedRecords, errorDataArr);
                        return;
                    }

                    // record updated
                    updatedRecords.push(dataObj);
                    sendRegexpLastUsedApiResponse(res, dataArr.length, doneCounter, updatedRecords, errorDataArr);
                })
                .catch(function (errObject) {
                    doneCounter++;
                    sendRegexpLastUsedApiResponse(res, dataArr.length, doneCounter, updatedRecords, errorDataArr);
                });
    });// EOF forEach loop
});// EOF api

//
// Get updated regular expressions after sync time
router.post('/regexp/get/sync', function (req, res, next) {
    // for operator id
    var operatorId;
    var packTypes;
    try {
        operatorId = JSON.parse(req.body.operatorId);
        packTypes = req.body.packTypes.split(",");
    } catch (err) {
        operatorId = [];
        packTypes = [];
    }

    // Validations
    if (!operatorId || operatorId.length == 0) {
        res.send({'error': 1, 'errorCode': 0, 'msg': 'Operator Id not passed', 'count': 0, 'data': []});
        return false;
    }

    // check last sync time
    var lastSyncTime = req.body.lastSyncTime;
    if (!lastSyncTime || lastSyncTime == '') {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'Last sync time not passed', 'count': 0, 'data': []});
    }
    if (!commonFunctions.validateYMDHMSDateFormat(lastSyncTime)) {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'Last sync time not valid date. Send in Y-m-d h:m:s format', 'count': 0, 'data': []});
    }
    var isValidDate = moment(lastSyncTime).isValid();
    if (!isValidDate) {
        return res.send({'error': 1, 'errorCode': 0, 'msg': 'Last sync time not valid date.', 'count': 0, 'data': []});
    }

    // operator id array
    var findCriteria = {
        operatorId: {$in: operatorId},
        packType: {$in: packTypes},
        isActive: 1
    };

    // for last sync time
    var lsdDate = new Date(lastSyncTime);
    findCriteria['$or'] = [
        {'createdDate': {$gt: lsdDate}},
        {'updatedDate': {$gt: lsdDate}},
        {'lastUsed': {$gt: lsdDate}}
    ];

    // Get data from db
    var projectFields = {'__v': 0, isActive: 0, updatedDate: 0, lastUsed: 0};
    ussdRegexpUtil.getByCustom({findCriteria: findCriteria, projectFields: projectFields})
            .then(function (respObject) {
                // no regexp found
                if (!respObject || (respObject.length == 0)) {
                    return res.send({'error': 0, 'errorCode': 0, 'msg': 'USSD RegExp not found', 'count': 0, 'data': []});
                }

                // Format date
                var regexpData = JSON.parse(JSON.stringify(respObject));
                var regexpDataCnt = regexpData.length;
                for (var i = 0; i < regexpDataCnt; i++) {
                    regexpData[i]['createdDate'] = commonFunctions.getDateYMDHMS(regexpData[i]['createdDate']);
                }

                // send all data
                return res.send({'error': 0, 'errorCode': 0, 'msg': 'USSD RegExp List', 'count': regexpDataCnt, 'data': regexpData});
            })
            .catch(function (errObject) {
                console.log("errObject");
                console.log(errObject);
                return res.send({'error': 1, 'errorCode': 0, 'msg': 'Error in getting USSD RegExp', 'count': 0, 'data': []});
            });
});// EOF api
module.exports = router;