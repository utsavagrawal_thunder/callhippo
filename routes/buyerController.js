var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var common = require('../config');
var config = common.config();
var buyerUtil = require('../utils/buyerUtil');
var buyerPointsUtil = require('../utils/buyerPointsUtil');
var htmlTemplates = require('../utils/htmlTemplates');
var smsTemplates = require('../utils/smsTemplates');
var commonFunctions = require('../utils/commonFunctions');

// for image upload
var multer = require('multer');
var AWS = require('aws-sdk');
var multerS3 = require('multer-s3');

AWS.config.update(config.awsConfig);

var s3 = new AWS.S3();

var sizeOf = require('image-size');
var exphbs = require('express-handlebars');
require('string.prototype.startswith');

/*var storage = multer.diskStorage({
 destination: function (req, file, cb) {
 cb(null, 'public/uploads/buyer/business/');
 },
 filename: function (req, file, cb) {
 var updName = renameFile(file.originalname);
 cb(null, updName);
 }
 });
 var upload = multer({storage: storage});*/
var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: "clinito",
        acl: "public-read",
        ContentType: 'application/octet-stream',
        key: function (req, file, cb) {
            var updName = renameFile(file.originalname);
            cb(null, 'buyer/business/' + updName);
        }
    })
});

// update buyer
router.put('/update/:buyerId', function (req, res, next) {

    //var buyerId = req.params.id;
    var buyerId = req.params.buyerId;
    var data = req.body;
    var name = data.name;
    var buyerCode = data.buyerCode;
    var mobileNo = data.mobileNo;
    var email = data.email;
    var user_type = data.user_type;
    var department = data.department;
    var documentStatus = data.documentStatus;
    var documentRemarks = data.documentRemarks;
    var upgradePointsAdded = data.upgradePointsAdded;
    var new_dep_obj = {};

    if (department != undefined) {
        department.id = commonFunctions.stringToObjectId(department.id);
        new_dep_obj._id = department.id;
        new_dep_obj.name = department.name;
        new_dep_obj.slug = department.slug;
    }
    var status = data.status;

    // validation
    if (typeof (buyerId) == 'undefined' || !commonFunctions.isMongoId(buyerId)) {
        return res.send({'error': 1, 'msg': 'Buyer Id not correct', 'data': {}});
    }
    if (typeof (name) == 'undefined' || name == '') {
        return res.send({'error': 1, 'msg': 'Please enter name', 'data': {}});
    }
    if (typeof (mobileNo) == 'undefined' || mobileNo == '') {
        return res.send({'error': 1, 'msg': 'Please enter mobileNo', 'data': {}});
    }
    if (email == '' || typeof (email) == 'undefined') {
        return res.send({'error': 1, 'msg': 'Please enter Email', 'data': {}});
    }
    if (typeof (status) == 'undefined' || status == '') {
        return res.send({'error': 1, 'msg': 'Please select status', 'data': {}});
    }

    if (user_type == undefined || user_type == "") {
        return res.send({'error': 1, 'msg': 'Please select a User Type : General/Upgraded', 'data': {}});
    }
    if (user_type == 2 || user_type == '2') {
        //upgraded
        if (department == undefined || department.id == undefined || department.id == "") {
            return res.send({'error': 1, 'msg': 'For Upgraded user, please select a department!', 'data': {}});
        }
    }
    if (documentStatus != undefined && documentStatus == "rejected") {
        if (documentRemarks == undefined || documentRemarks == "") {
            return res.send({'error': 1, 'msg': 'If the Document Status is rejected, Please mention a reason!', 'data': {}});
        }
    }
    // Check if User name already exists
    var findCriteria = {
        $or: [
            {email: new RegExp('^' + email + '$', 'i')},
            {mobileNo: mobileNo}
        ]
//        _id: { $ne: mongoose.Types.ObjectId(buyerId) }
    };

    //isStatusChanged
    var isStatusChanged = false;
    var existingDocument = {};
            
    buyerUtil.getBuyerByFindCriteria(findCriteria)
            .then(function (respObject) {
                if (respObject.length > 0)
                {
                    existingDocument = respObject[0];
                    for (var i = 0; i < respObject.length; i++) {
                        if (respObject[i]._id == buyerId) {
                            if (respObject[i].isActive != user_type) {
                                isStatusChanged = true;
                            }
                        } else {
                            res.send({'error': 1, 'msg': 'Email or Mobile already exists', 'data': {}});
                            return true;
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            })
            .then(function (alreadyExists) {
                if (alreadyExists) {
                    return;
                }
                // now update
                var setObj = new Object();
                setObj.name = name;
                setObj.mobileNo = mobileNo;
                setObj.email = email;
                setObj.isActive = status;
                setObj.documentStatus = documentStatus;
                setObj.documentRemarks = documentRemarks;
                if (Object.keys(new_dep_obj).length > 0) {
                    setObj.department = new_dep_obj;
                    setObj.buyerType = user_type;
                }
                setObj.updatedDate = new Date();
                if (upgradePointsAdded == false && user_type == 2 && documentStatus == 'verified') {
                    setObj.upgradePointsAdded = true;
                    var updateObj = {
                        $set: setObj,
                        $inc: {points: config.upgrade_points}
                    };
                } else {
                    var updateObj = {
                        $set: setObj
                    };
                }

                var queryObj = {
                    '_id': mongoose.Types.ObjectId(buyerId)
                };
                buyerUtil.updateBuyer(queryObj, updateObj)
                        .then(function (respObject) {
                            if (isStatusChanged == true) {
                                //send sms
                                /* CONTENT MISSINGGG */
                                commonFunctions.sendSMS(mobileNo, smsTemplates.profileStatusChange(name), 13)
                                        .then(function (respObjectMsg) {
                                            console.log("respObjectMsg");
                                            console.log(respObjectMsg);
                                        })
                                        .catch(function (errSmsObject) {
                                            console.log("errSmsObject in send sms");
                                            console.log(errSmsObject);
                                        });
                                //send email'
                                var password = existingDocument.encryptedPassword ? commonFunctions.decryptData({data: existingDocument.encryptedPassword}): "";
                                var htmlString = htmlTemplates.getProfileStatusTemplate(name, existingDocument.email, password);
                                var mailOptions = {
                                    from: config.sender_email,
                                    to: setObj.email,
                                    subject: 'Your profile has been approved and upgraded.',
                                    html: htmlString
                                };
//                                commonFunctions.sendSmtpMail(mailOptions, function(errMailer, respMailer){});
                                commonFunctions.sendSmtpEmail(mailOptions)
                                        .then(function (respObjectInner) {
                                            console.log("respObject mail for profile status sent successfully");
                                            console.log(respObjectInner);
                                        })
                                        .catch(function (errObjectInner) {
                                            console.log("error in sending email for profile status update");
                                            console.log(errObjectInner);
                                        });
                            }
                        

                            var queryObj = {
                                '_id' : mongoose.Types.ObjectId(buyerId)
                            };
                            buyerUtil.updateBuyer(queryObj, updateObj)
                                .then(function (respObject) {
                                    res.send({'error': 0, 'msg': 'Buyer updated successfully', 'data': respObject});
                                    if(upgradePointsAdded == false && user_type == 2 && documentStatus == 'verified'){
                                        //if we have added points to the buyer collection
                                        //we have to add a entry to the buyerPoints collection as well
                                        var insertObjPoints = new Object();
                                        insertObjPoints.buyerId = mongoose.Types.ObjectId(buyerId);
                                        insertObjPoints.buyerCode = (buyerCode != undefined) ? buyerCode : '';
                                        insertObjPoints.points = config.upgrade_points;
                                        insertObjPoints.type = 1;
                                        insertObjPoints.reason = 'Points added on buyer upgradation';
                                        buyerPointsUtil.insertBuyerPoints(insertObjPoints)
                                            .then(function (respObjPoints) {
                                            })
                                            .catch(function (errObjPoints) {
                                            });
                                    }
                                })
                                .catch(function (errObject) {
                                    res.send({'error': 1, 'msg': 'Unable to update buyer', 'data': errObject});
                                });
                                
                })
                .catch(function (errObject) {
                    res.send({'error': 1, 'msg': 'Unable to update buyer', 'data': errObject});
                });
        });
});
//update buyer end


// Get all buyers
router.get('/get', function (req, res) {
    buyerUtil.getAllBuyers()
            .then(function (respObject) {
                res.send({'error': 0, 'msg': 'Buyer list', 'data': respObject});
            })
            .catch(function (errObject) {
                res.send({'error': 1, 'msg': 'No buyer found', 'data': []});
            });
});


// Get data by id
router.get('/get/:buyerId', function (req, res) {

    var buyerId = req.params.buyerId;

    // validation
    if (typeof (buyerId) == 'undefined' || !commonFunctions.isMongoId(buyerId)) {
        return res.send({'error': 1, 'msg': 'Buyer Id not correct', 'data': []});
    }
    buyerUtil.getBuyerById(buyerId)
            .then(function (respObject) {
                res.send({'error': 0, 'msg': 'Buyer list', 'data': respObject});
            })
            .catch(function (errObject) {
                res.send({'error': 1, 'msg': 'No Buyer found', 'data': []});
            });
});

// rename image
function renameFile(orgFileName) {
    var ext = orgFileName.substr(orgFileName.lastIndexOf('.') + 1);
    var fileNameWithoutExt = orgFileName.substr(0, orgFileName.length - ext.length - 1);
    var updName = fileNameWithoutExt + '-' + Date.now() + '.' + ext;

    return updName;
}

// for image upload
router.post('/uploadDoc', upload.single('file'), function (req, res, next) {
    if (!req.file.mimetype.startsWith('image/') && !req.file.mimetype.startsWith('application/pdf') && !req.file.mimetype.startsWith('application/vnd.openxmlformats-officedocument.wordprocessingml.document') && !req.file.mimetype.startsWith('application/msword')) {
        return res.status(422).json({
            error: 'The uploaded file must be an image or pdf or Doc'
        });
    }
    return res.status(200).send(req.file);
});

// for image delete
router.delete('/delFile/:name', function (req, res) {
    var name = req.params.name;

    /*var fs = require('fs');
     var path = require('path');
     var filePath = path.join(__dirname, '../public/uploads/buyer/business/'+name);
     fs.unlink(filePath, function (err) {
     console.log(err);
     if(err) {
     res.json({'error' : 1, 'msg':'Unable to delete image'});
     }
     else {
     res.json({'error' : 0, 'msg':'Image deleted successfully'});
     }
     });*/
    if (name != undefined && req.params.name.trim() != "") {
        var params = {
            Bucket: 'clinito', /* required */
            Key: 'buyer/business/' + name, /* required */
            RequestPayer: 'requester'
        };
        s3.deleteObject(params, function (err, data) {
            if (err) {
                console.log(err, err.stack);
                // an error occurred
                res.json({'error': 1, 'msg': 'Unable to delete image'});
            } else {
                console.log(name + " file deleted from s3 successfully");
                // successful response 
                res.json({'error': 0, 'msg': 'Image deleted successfully'});
            }
        });
    } else {
        res.json({'error': 1, 'msg': 'No File Name found to delete!'});
    }
});


//handle updation for compnay section
router.post('/UpdateCompany/:buyerId', function (req, res, next) {

    var buyerId = req.params.buyerId;
    var update_data = {};

    if (typeof (buyerId) == 'undefined' || !commonFunctions.isMongoId(buyerId)) {
        return res.send({'error': 1, 'msg': 'Buyer Id not correct', 'data': {}});
    }

    //user is logged in
    //we have to check for all the fields existence

    buyerUtil.fieldValidationCompanyUpdate(req)
            .then(function (req_obj) {
                //validation successfull

                //we will update the status
                //update_data.documentStatus = "Under Review";

                //we will fix the document data as well
                update_data.documents = {
                    jobRole: req.body.job_role.trim(),
                    govtRegProof: {
                        proofName: req.body.gov_reg_prof_doc.trim(),
                        proofFileName: req.body.gov_reg_prof_filename.trim()
                    },
                    vatTinProof: {
                        proofName: req.body.vat_tin_doc.trim(),
                        proofFileName: req.body.vat_tin_prof_filename.trim()
                    },
                    csiProof: {
                        proofFileName: req.body.csi_prof_filename.trim()
                    },
                    addressProof: {
                        proofName: req.body.addr_prof_doc.trim(),
                        proofFileName: req.body.addr_prof_filename.trim()
                    },
                    IdProof: {
                        proofName: req.body.id_prof_doc.trim(),
                        proofFileName: req.body.id_prof_filename.trim(),
                    },
                    certProof: {
                        proofName: req.body.cert_prof_doc.trim(),
                        proofFileName: req.body.cert_prof_filename.trim()
                    },
                    drugProof: {
                        proofFileName: req.body.drug_prof_filename.trim()
                    }
                }

                //we have the update object ready 
                search_obj = {
                    _id: mongoose.Types.ObjectId(buyerId)
                }

                return buyerUtil.updateBuyer(search_obj, {$set: update_data});
            })
            .then(function (update_resp) {
                //successfully updated
                //we have also updated the user's status
                res.json({
                    error: 0,
                    msg: "Documents Updated!"
                })
            })
            .catch(function (err) {
                console.log('/UpdateCompany - error - ' + err);
                res.json({
                    error: 1,
                    msg: err
                })
            })

});

router.post('/updateRelationManager/:buyerId', function (req, res, next) {
    var buyerId = req.params.buyerId;
    var update_data = {};
    if (typeof (buyerId) == 'undefined' || !commonFunctions.isMongoId(buyerId)) {
        return res.send({'error': 1, 'msg': 'Buyer Id not correct', 'data': {}});
    }

    req.checkBody('name', 'Name is compulsary').notEmpty();
    req.checkBody('email', 'Email is compulsary').notEmpty();
    req.checkBody('mobileNo', 'Mobile No should have minimum 10 characters, numeric and compulsary').notEmpty().isInt().len(10, 11);

    var err = req.validationErrors(true);
    if (err) {
        var err_string = "";
        //the format of error is in object type
        for (var key in err) {
            err_string += err[key].msg + "<br/>";
        }
        return res.send({'error': 1, 'msg': err_string, 'data': {}});
    }

    update_data.rmDetails = {
        name: req.body.name.trim(),
        mobileNo: req.body.mobileNo.trim(),
        email: req.body.email.trim()
    }
    //we have the update object ready 
    search_obj = {
        _id: mongoose.Types.ObjectId(buyerId)
    }

    buyerUtil.updateBuyer(search_obj, {$set: update_data})
            .then(function (update_resp) {
                //successfully updated
                //we have also updated the user's status
                res.json({
                    error: 0,
                    msg: "Relation Manager data Updated!"
                });
            })
            .catch(function (err) {
                console.log('/updateRelationManager - error - ' + err);
                res.json({
                    error: 1,
                    msg: err
                });
            });
});

module.exports = router;
