var express = require('express');
var router = express.Router();
var auth = require('../middleware/auth.js');
var Promise = require('bluebird');
var common = require('../config');
var config = common.config();

//check if logged in
//router.get('/*checkout*', function (req, res, next) {
//    //use for checking in if user is logged in
//    auth.checkAuthentication(req, res, next);
//    console.log("in middleware");
//    next();
//});

router.get('/admin', function (req, res, next) {
    req.flash('info', 'Welcome');

    var userInfo = (typeof(req.session.userInfo) != 'undefined') ? req.session.userInfo : {};
    res.render('common/layout.ejs', {data : config, userInfo : userInfo});
});


router.get('/admin/pages/all-templates.ejs', function (req, res, next) {
    var userInfo = (typeof(req.session.userInfo) != 'undefined') ? req.session.userInfo : {};
    res.render('pages/all-templates.ejs', {data : config, userInfo : userInfo});
});

router.get('/login/', function (req, res, next) {
    res.render('login.ejs');
});

router.get('/logout', function(req, res, next) {
    req.session.destroy();
    res.redirect('/login');
});

module.exports = router;