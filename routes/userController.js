var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Promise = require('bluebird');
var common = require('../config');
var async = require('async');
var config = common.config();
var userUtil = require('../utils/userUtil');
var settingsUtil = require('../utils/settingsUtil');
var callLogUtil = require('../utils/callLogUtil');
var failedAttemptsUtil = require('../utils/failedAttemptsUtil');
var blockedPackagesUtil = require('../utils/blockedPackagesUtil');
var rechargeUtil = require('../utils/rechargeUtil');
var otpUtil = require('../utils/otpUtil');
var walletUtil = require('../utils/walletUtil');
var creditUsageArchiveUtil = require('../utils/creditUsageArchiveUtil');
var commonFunctions = require('../utils/commonFunctions');
var md5 = require('md5');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens


/******************************FIX FOR SAME NUMBER DIFFERENT PHONE************************************/


// insert User Register
router.post('/insert', function (req, res, next) {

    var data = req.body;
    var email = data.email;
    var gateway = data.gateway;
    var mobileno = data.mobileno;
    var fcmToken = data.fcmToken;
    var operatorId = data.operatorId ? data.operatorId : 0;
    var primaryRegistrationDeviceToken = data.primaryRegistrationDeviceToken;
    var secondaryRegistrationDeviceToken = data.secondaryRegistrationDeviceToken;
    var associatedMailIds = data.associatedMailIds && data.associatedMailIds.length > 0 ? data.associatedMailIds.split(',') : [];

    //current user's referral code
//    var referralCode = md5(Date.now());
//    console.log("referralCode of newly creted user");
//    console.log(referralCode);

    // validation
    if (typeof (email) == 'undefined' || email == '' || email.indexOf("@") == -1 || email.indexOf(".") == -1) {
        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Please enter valid email', 'data': {}})});
        return res.send(returnObjectToAndroid);
    }
    if (typeof (mobileno) == 'undefined' || mobileno == '' || (!mobileno.startsWith('9') && !mobileno.startsWith('8') && !mobileno.startsWith('7')) || mobileno.length != 10) {
        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Please enter mobileno', 'data': {}})});
        return res.send(returnObjectToAndroid);
    }
    if (typeof (gateway) == 'undefined' || gateway == '') {
        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Please enter gateway', 'data': {}})});
        return res.send(returnObjectToAndroid);
    }
    if (typeof (fcmToken) == 'undefined' || fcmToken == '') {
        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Please send fcmToken', 'data': {}})});
        return res.send(returnObjectToAndroid);
    }
    //referral code validation

//    if(typeof(referralCode) == 'undefined' || referralCode == '') {
//        return res.send({'error': 1, 'msg': 'Please select referralCode', 'data': {}});
//    }


    // Check if user number already exists i.e guest account present or permanent account present
    var findCriteria = {};
    var globalFoundReferralCodeResponse = {};
    findCriteria = {
        registrationNumber: mobileno,
        isActive: 1
    };
    var existingUserDocumentIfUpdate = new Object();
    var sendOtpFalse = false;
    var updateDeviceTokens = false;
    var notExistsReferralCodeGlobal = false;
    var userAlreadyExistSkipAll = false;
    var globalUserObjectIfAlreadyExists = new Object();

    var insertObj = new Object();
    //generate OTP
    var OTP = commonFunctions.generateOTP(6);
    var globalSettingsObject = {};
    settingsUtil.getAllSettings()
            .then(function (responseObjectSettings) {
                globalSettingsObject = JSON.parse(JSON.stringify(responseObjectSettings[0]));
                return userUtil.getUserByFindCriteria(findCriteria);
            })
            .then(function (respObject) {
                if (respObject.length == 1 && respObject[0].primaryRegistrationDeviceToken == primaryRegistrationDeviceToken && respObject[0].secondaryRegistrationDeviceToken == secondaryRegistrationDeviceToken) {
                    //send only data and token if user is already registered and is trying to access app after deleting data or by reinstalling
                    //find out how to send same token or invalidate previous token.. Creating new token temporary..........
//                    var token = jwt.sign({_id: respObject[0]._id, email: respObject[0].email, primaryRegistrationDeviceToken: respObject[0].primaryRegistrationDeviceToken, secondaryRegistrationDeviceToken: respObject[0].secondaryRegistrationDeviceToken}, config['secret']);
                    var newObject = JSON.parse(JSON.stringify(respObject[0]));
//                    newObject["token"] = commonFunctions.encryptData({data: token});
//                    newObject["OTP"] = OTP;
                    sendOtpFalse = true;
                    globalUserObjectIfAlreadyExists = newObject;
                    userAlreadyExistSkipAll = true;
                } else if (respObject.length == 1 && ((respObject[0].primaryRegistrationDeviceToken !== primaryRegistrationDeviceToken) || (respObject[0].secondaryRegistrationDeviceToken !== secondaryRegistrationDeviceToken))) {
                    globalUserObjectIfAlreadyExists = JSON.parse(JSON.stringify(respObject[0]));
                    console.log("Updating device tokens");
                    updateDeviceTokens = true;
                    sendOtpFalse = true;
                    return false;
                } else if (respObject.length > 1) {
                    console.log("********************************************");
                    console.log("*multiple active users with same registered number*");
                    console.log("********************************************");
                    console.log("respObject");
                    console.log(respObject);
                    return true;
                }
                sendOtpFalse = true;
                return false;
            })
            .then(function (alreadyExists) {
                if (alreadyExists || userAlreadyExistSkipAll) {
                    return [];
                }

                // Check if referral code that user got from some other user is valid or not
                if (data.hasOwnProperty('referralCode') && data.referralCode.length > 0) {
                    var referralCodeFindCriteria = {
                        referralCode: data.referralCode
                    };

                    return userUtil.getUserByFindCriteria(referralCodeFindCriteria);
                } else {
                    notExistsReferralCodeGlobal = true;
                    return [];
                }

            })
            .then(function (foundReferralCodeResponse) {
                insertObj.email = email;
                insertObj.registrationNumber = mobileno;
                insertObj.isActive = 0;
//                insertObj.referralCode = referralCode;
                insertObj.associatedMailIds = associatedMailIds;
                insertObj.operatorId = operatorId;
                insertObj.fcmToken = fcmToken;
                //device tokens
                insertObj.primaryRegistrationDeviceToken = primaryRegistrationDeviceToken;
                insertObj.secondaryRegistrationDeviceToken = secondaryRegistrationDeviceToken;
                if (!userAlreadyExistSkipAll) {

                    if (foundReferralCodeResponse.length > 0 && !notExistsReferralCodeGlobal) {
                        // now add
                        // insertObj.username = username;
                        //found one user with this referral code
                        if (foundReferralCodeResponse && foundReferralCodeResponse.length == 1) {
                            if (foundReferralCodeResponse[0].primaryRegistrationDeviceToken == primaryRegistrationDeviceToken || foundReferralCodeResponse[0].secondaryRegistrationDeviceToken == secondaryRegistrationDeviceToken) {
                                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "This device is already registered. Cannot use referral for this device.", data: {}})});
                                res.send(returnObjectToAndroid);
                            }
                            insertObj.referredBy = {
                                //remove compulsion.. add only user id
                                user_id: foundReferralCodeResponse[0]._id,
//                        referredRegistrationNumber: foundReferralCodeResponse[0].registrationNumber,
                                primaryDeviceToken: foundReferralCodeResponse[0].primaryRegistrationDeviceToken,
                                secondaryDeviceToken: foundReferralCodeResponse[0].secondaryRegistrationDeviceToken
                            };
                            globalFoundReferralCodeResponse = JSON.parse(JSON.stringify(foundReferralCodeResponse[0]));
                        } else if (foundReferralCodeResponse && foundReferralCodeResponse.length > 1) {
                            console.log("Multiple users with this referral code found" + data.referralCode);
                            var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Error with Referral Code', 'data': {}})});
                            res.send(returnObjectToAndroid);
                            console.log("line 128 after res.send");
                        } else if (data.hasOwnProperty('referralCode') && foundReferralCodeResponse.length == 0) {
                            console.log("Invalid Referral Code" + data.referralCode);
                            var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Invalid Referral Code', 'data': {}})});
                            res.send(returnObjectToAndroid);
                            console.log("line 132 after res.send");
                        }
                    }
                }

                // insert OTP now   
                if (sendOtpFalse) {

                    var otpInsertObj = {
                        otp: OTP,
                        otpType: 1,
                        mobileNo: mobileno
                    };

                    return otpUtil.insertOtp(otpInsertObj);
                } else {
                    return {};
                }
            })
            .then(function (otpInsertedSuccessObject) {
                if (Object.keys(otpInsertedSuccessObject).length !== 0) {
                    // insert success
                    var otpId = otpInsertedSuccessObject._id;
                    // send OTP
                    if (typeof (otpId) == 'undefined') {
                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Unable to insert OTP', 'data': {}})});
                        res.send(returnObjectToAndroid);
                        console.log("After send otp. Line 144");
                    }

                    if (gateway == "MGAGE") {
                        commonFunctions.sendOTP(mobileno, 'Welcome to SmartCall. Get balance updates and cashback for your calls. Relax, No action required from you, verification happens automatically. Code - ' + OTP, function (errOTP, respOTP) {
                            console.log("errOTP");
                            console.log(errOTP);
                            console.log("respOTP");
                            console.log(respOTP);
                        });
                    } else if (gateway == "ROUTE") {
                        commonFunctions.sendOTPRoute(mobileno, 'Welcome to SmartCall. Get balance updates and cashback for your calls. Relax, No action required from you, verification happens automatically. Code - ' + OTP, function (errOTP, respOTP) {
                            console.log("errOTP");
                            console.log(errOTP);
                            console.log("respOTP");
                            console.log(respOTP);
                        });
                    }
                    //*******************************************
                    //check if this number already has another inactive account associated with it

                    var findCriteriaDevice = {
//                        primaryRegistrationDeviceToken: primaryRegistrationDeviceToken,
//                        secondaryRegistrationDeviceToken: secondaryRegistrationDeviceToken,
                        registrationNumber: mobileno,
                        isActive: 0
                    };
                    return userUtil.getUserByFindCriteria(findCriteriaDevice);
                } else if (userAlreadyExistSkipAll) {
                    console.log("in else");
                    return [];
                }
            })
            .then(function (userInsertRespObject) {
                //convert temporary account   
                if (userInsertRespObject && userInsertRespObject.length == 0 && userAlreadyExistSkipAll) {
                    //new primary email is not same as old primary
                    if (globalUserObjectIfAlreadyExists.email != insertObj.email) {
                        var oldPrimaryMailIsSecondary = false;
                        //check if new secondary has old primary
                        for (var j = 0; j < insertObj.associatedMailIds.length; j++) {
                            if (insertObj.associatedMailIds[i] == globalUserObjectIfAlreadyExists.email) {
                                //new secondary has old primary
                                oldPrimaryMailIsSecondary = true;
//                                globalUserObjectIfAlreadyExists.associatedMailIds.splice(i, 1);

                            }
                        }
                        //if new secondary does not have old primary, then add. 
                        if (oldPrimaryMailIsSecondary == false) {
                            insertObj.associatedMailIds.push(globalUserObjectIfAlreadyExists.email);
                        }
                        //check if old secondary has new primary
                        for (var i = 0; i < globalUserObjectIfAlreadyExists.associatedMailIds.length; i++) {
                            if (globalUserObjectIfAlreadyExists.associatedMailIds[i] == insertObj.email) {
                                //if old secondary has new primary email, then delete from secondary to add to primary
                                globalUserObjectIfAlreadyExists.associatedMailIds.splice(i, 1);
                            }
                            var oldSecondaryInNewSecondary = false;
                            for (var j = 0; j < insertObj.associatedMailIds.length; j++) {
                                if (globalUserObjectIfAlreadyExists.associatedMailIds[i] == insertObj.associatedMailIds[j]) {
                                    oldSecondaryInNewSecondary = true;
                                }
                            }
                            if (oldSecondaryInNewSecondary == false) {
                                insertObj.associatedMailIds.push(globalUserObjectIfAlreadyExists.associatedMailIds[i]);
                            }
                        }
                    }
                    console.log("insertObject to be updated");
                    console.log(insertObj);
                    //update user object with primary and secondary email id
                    return userUtil.updateUser({_id: globalUserObjectIfAlreadyExists._id}, insertObj);

                } 
                else if (userInsertRespObject && userInsertRespObject.length > 0 && updateDeviceTokens == false) {
                    //REMOVED ****guest user register with mobile number
                    existingUserDocumentIfUpdate = JSON.parse(JSON.stringify(userInsertRespObject[0]));
                    return userUtil.updateUser({_id: userInsertRespObject[0]._id}, insertObj);
                } else if (updateDeviceTokens == true) {
                    //existing user on a new device
                    existingUserDocumentIfUpdate = JSON.parse(JSON.stringify(globalUserObjectIfAlreadyExists));
                    if (existingUserDocumentIfUpdate.email != insertObj.email) {
                        insertObj.associatedMailIds.push(existingUserDocumentIfUpdate.email);
                    }
                    return userUtil.updateUser({_id: existingUserDocumentIfUpdate._id}, insertObj);
                } else {
//                    create new account**************
                    //add Rs. x to user wallet on fresh registration
                    insertObj.totalWalletCredits = globalSettingsObject.bonusLimits["newSignUp"];
                    return userUtil.insertUser(insertObj);

                }
            })
            .then(function (userSuccessfullyInserted) {
                if (userAlreadyExistSkipAll) {
                    //relogin again
                    console.log("globalUserObjectIfAlreadyExists");
                    console.log(globalUserObjectIfAlreadyExists);
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: 'Account exists. OTP sent. Please verify.', data: globalUserObjectIfAlreadyExists})});
                    res.send(returnObjectToAndroid);
                } else if (userSuccessfullyInserted.hasOwnProperty('nModified') && Object.keys(existingUserDocumentIfUpdate).length > 0) {
                    //guest user converting to permanent account
                    console.log("existingUserDocumentIfUpdate");
                    console.log(existingUserDocumentIfUpdate);
                    existingUserDocumentIfUpdate["registrationNumber"] = mobileno;
//                    var token = jwt.sign({_id: existingUserDocumentIfUpdate._id, email: existingUserDocumentIfUpdate.email, primaryRegistrationDeviceToken: existingUserDocumentIfUpdate.primaryRegistrationDeviceToken, secondaryRegistrationDeviceToken: existingUserDocumentIfUpdate.secondaryRegistrationDeviceToken}, config['secret']);
                    var newObject = JSON.parse(JSON.stringify(existingUserDocumentIfUpdate));
//                    newObject["token"] = commonFunctions.encryptData({data: token});
//                    newObject["OTP"] = OTP;

                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 0, 'msg': 'Account exists. OTP sent. Please verify.', 'data': newObject})});
                    res.send(returnObjectToAndroid);
                } else if (userSuccessfullyInserted.hasOwnProperty('nModified') && Object.keys(globalUserObjectIfAlreadyExists).length > 0) {
                    //existing user with number registering on a new device
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 0, 'msg': 'Account exists. OTP sent. Please verify.', 'data': globalUserObjectIfAlreadyExists})});
                    res.send(returnObjectToAndroid);
                } else {
                    //new user
                    if (!existingUserDocumentIfUpdate.hasOwnProperty("email")) {
                        existingUserDocumentIfUpdate = JSON.parse(JSON.stringify(userSuccessfullyInserted));
                    }
                    existingUserDocumentIfUpdate["registrationNumber"] = mobileno;
//                    var token = jwt.sign({_id: existingUserDocumentIfUpdate._id, email: existingUserDocumentIfUpdate.email, primaryRegistrationDeviceToken: existingUserDocumentIfUpdate.primaryRegistrationDeviceToken, secondaryRegistrationDeviceToken: existingUserDocumentIfUpdate.secondaryRegistrationDeviceToken}, config['secret']);
                    var newObject = JSON.parse(JSON.stringify(existingUserDocumentIfUpdate));
//                    newObject["token"] = commonFunctions.encryptData({data: token});
                    insertObj.totalWalletCredits = globalSettingsObject.bonusLimits["newSignUp"];
//                    newObject["OTP"] = OTP;
                    console.log("Object sent to Android");
                    console.log(newObject);
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 0, 'msg': 'User added successfully.', 'data': newObject})});
                    res.send(returnObjectToAndroid);
                    var insertWalletObject = {
                        userId: commonFunctions.stringToObjectId(newObject._id),
                        creditAmount: globalSettingsObject.bonusLimits["newSignUp"],
                        createdDate: Date.now()
                    };
                    walletUtil.insertWallet(insertWalletObject)
                            .then(function (responseObjectInsertWallet) {
                                console.log("responseObjectInsertWallet");
                                console.log(responseObjectInsertWallet);
                            });
                    //send referral money
                    var insertWalletObject = {
                        userId: commonFunctions.stringToObjectId(globalFoundReferralCodeResponse._id),
                        creditAmount: globalSettingsObject.bonusLimits["newReferral"],
                        createdDate: Date.now(),
                        isReferralBonus: true
                    };
                    walletUtil.insertWallet(insertWalletObject)
                            .then(function (responseObjectReferralBonusInsertWallet) {
                                console.log("responseObjectReferralBonusInsertWallet");
                                console.log(responseObjectReferralBonusInsertWallet);
                                //send referral notification to the referred user
                                var notificationObject = {
                                    title: "Referral Bonus",
                                    type: 2,
                                    body: "Hurray! Rs.10 has been credited to your wallet as referral bonus"
                                };
                                if (Object.keys(globalFoundReferralCodeResponse).length > 0) {
                                    console.log("globalFoundReferralCodeResponse");
                                    console.log(globalFoundReferralCodeResponse);
                                    commonFunctions.sendNotification([globalFoundReferralCodeResponse.fcmToken], notificationObject);
                                }
                            })
                            .catch(function (errorObjectReferralBonusInsertWallet) {
                                console.log("errorObjectReferralBonusInsertWallet");
                                console.log(errorObjectReferralBonusInsertWallet);
                            });

                }
            })
            .catch(function (errObject) {
                console.log("errObject sent to Android");
                console.log(errObject);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Unable to add User', 'data': errObject})});
                res.send(returnObjectToAndroid);
            });

});

//router.post('/guestInsert', function (req, res, next) {
//    console.log("req.post");
//    console.log(req.post);
//
//    var data = req.body;
//    var email = data.email;
//    var primaryRegistrationDeviceToken = data.primaryRegistrationDeviceToken;
//    var secondaryRegistrationDeviceToken = data.secondaryRegistrationDeviceToken;
//    var associatedMailIds = data.associatedMailIds && data.associatedMailIds.length > 0 ? data.associatedMailIds.split(',') : [];
//
//
//    //current user's referral code
//    var referralCode = md5(Date.now());
//    console.log("referralCode");
//    console.log(referralCode);
//
//    // validation
//
//    if (typeof (email) == 'undefined' || email == '') {
//        return res.send({'error': 1, 'msg': 'Please enter email', 'data': {}});
//    }
//
//
//    // Check if user number already exists i.e guest account present or permanent account present
//    var findCriteria = {
//        primaryRegistrationDeviceToken: primaryRegistrationDeviceToken,
//        secondaryRegistrationDeviceToken: secondaryRegistrationDeviceToken,
//        registrationNumber: {"$exists": true, "$eq": ""},
//        isActive: 0
//    };
//
//    var insertObj = new Object();
//    userUtil.getUserByFindCriteria(findCriteria)
//            .then(function (respObject) {
//                if (respObject.length > 0) {
//                    //send only data and token if user is already registered and is trying to access app after deleting data or by reinstalling
//
//                    //find out how to send same token or invalidate previous token.. Creating new token temporary..........
//                    var token = jwt.sign({_id: respObject[0]._id, email: respObject[0].email, primaryRegistrationDeviceToken: respObject[0].primaryRegistrationDeviceToken, secondaryRegistrationDeviceToken: respObject[0].secondaryRegistrationDeviceToken}, config['secret']);
//                    var newObject = JSON.parse(JSON.stringify(respObject[0]));
//                    newObject["token"] = commonFunctions.encryptData({data: token});
//                    console.log("Object sent to Android");
//                    console.log(newObject);
//                    res.send({'error': 0, 'msg': 'Account exists. Logged in now.', 'data': newObject});
//                    return true;
//
//                }
//                return false;
//            })
//            .then(function (alreadyExists) {
//                if (!alreadyExists) {
//
//                    // now add
//                    insertObj.email = email;
//                    insertObj.isActive = 0;
//
//                    //device tokens
//                    insertObj.primaryRegistrationDeviceToken = primaryRegistrationDeviceToken;
//                    insertObj.secondaryRegistrationDeviceToken = secondaryRegistrationDeviceToken;
//                    
//                    insertObj.associatedMailIds = associatedMailIds;
//                    
//                    return userUtil.insertUser(insertObj);
//                } else
//                    return false;
//
//            })
//            .then(function (userSuccessfullyInserted) {
//                if (userSuccessfullyInserted) {
//                    var token = jwt.sign({_id: userSuccessfullyInserted._id, email: userSuccessfullyInserted.email, primaryRegistrationDeviceToken: userSuccessfullyInserted.primaryRegistrationDeviceToken, secondaryRegistrationDeviceToken: userSuccessfullyInserted.secondaryRegistrationDeviceToken}, config['secret']);
//                    var newObject = JSON.parse(JSON.stringify(userSuccessfullyInserted));
//                    newObject["token"] = commonFunctions.encryptData({data: token});
//                    console.log("Object sent to Android");
//                    console.log(newObject);
//                    res.send({'error': 0, 'msg': 'Guest User added successfully.', 'data': newObject});
//                } else {
//                    res.end();
//                }
//            })
//            .catch(function (errObject) {
//                console.log("errObject");
//                console.log(errObject);
//                res.send({'error': 1, 'msg': 'Unable to add User', 'data': errObject});
//            });
//
//});

// Verify OTP
router.post('/verifyOTP', function (req, res, next) {
    // get data
    var data = req.body;
    var otp = data.otp;
    var mobileNo = data.mobileNo;
    // validation
    if (typeof (otp) == 'undefined' || otp == '') {
        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Please enter OTP', 'data': {}})});
        return res.send(returnObjectToAndroid);
    }
    if (typeof (mobileNo) == 'undefined' || mobileNo == '') {
        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Mobile number not found', 'data': {}})});
        return res.send(returnObjectToAndroid);
    }

    // do async
    async.parallel({
        // check otp exists
        otpDetails: function (callback) {
            var findCriteria = {mobileNo: mobileNo, otpType: 1};
            var sortCriteria = {createdDate: -1};
            var dataObjFind = {
                findCriteria: findCriteria,
                sortCriteria: sortCriteria,
                limit: 1
            };
            otpUtil.getOtpByCustom(dataObjFind)
                    .then(function (respObject) {
                        if (respObject.length > 0) {
                            callback(null, respObject[0]);
                        } else {
                            callback(null, {});
                        }
                    })
                    .catch(function (errObject) {
                        callback(null, {});
                    });
        }
    },
            function (err, asyncResults) {
                if (err) {
                    console.log("err OTP");
                    console.log(err);
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Unable to verify OTP', 'data': err})});
                    return res.send(returnObjectToAndroid);
                }

                // otp details not found
                var otpDetails = asyncResults.otpDetails;
                if (commonFunctions.isObjectEmpty(otpDetails)) {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Unable to verify OTP', 'data': {}})});
                    return res.send(returnObjectToAndroid);
                }

                // check if OTP is correct
                if (otp != otpDetails.otp) {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 1, 'msg': 'Invalid OTP', 'data': {}})});
                    return res.send(returnObjectToAndroid);
                }
                
                var dataObject = {
                    sortCriteria: {createdDate: -1},
                    findCriteria: {registrationNumber: mobileNo}
                }
                userUtil.getByCustom(dataObject)
                        .then(function (responseObject1) {
                            console.log("responseObject1");
                            console.log(responseObject1);
                            if (responseObject1[0].isActive == 0) {
                                userUtil.updateUser({_id: responseObject1[0]._id}, {$set: {isActive: 1}})
                                        .then(function (responseObject2) {

                                            // if user is found and password is right
                                            // create a token
                                            var token = jwt.sign({_id: responseObject1[0]._id, email: responseObject1[0].email, primaryRegistrationDeviceToken: responseObject1[0].primaryRegistrationDeviceToken, secondaryRegistrationDeviceToken: responseObject1[0].secondaryRegistrationDeviceToken}, config['secret']);
                                            var newObject2 = JSON.parse(JSON.stringify(responseObject1[0]));
                                            newObject2["token"] = commonFunctions.encryptData({data: token});
                                            console.log("Object sent to Android");
                                            console.log(newObject2);
                                            var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 0, 'msg': 'User Successfully updated', 'data': newObject2})});
                                            res.send(returnObjectToAndroid);
                                        })
                                        .catch(function (errorObject) {
                                            console.log("errorObject in update user status");
                                            console.log(errorObject);
                                        });
                            } else {
                                // verification successfull
                                var token = jwt.sign({_id: responseObject1[0]._id, email: responseObject1[0].email, primaryRegistrationDeviceToken: responseObject1[0].primaryRegistrationDeviceToken, secondaryRegistrationDeviceToken: responseObject1[0].secondaryRegistrationDeviceToken}, config['secret']);
                                var newObject2 = JSON.parse(JSON.stringify(responseObject1[0]));
                                newObject2["token"] = commonFunctions.encryptData({data: token});
                                console.log("Object sent to Android");
                                console.log(newObject2);
                                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({'error': 0, 'msg': 'User Successfully updated', 'data': newObject2})});
                                res.send(returnObjectToAndroid);
                            }

                        })
                        .catch(function (errorObject) {
                            console.log("errorObject");
                            console.log(errorObject);
                        });

            });// EOF async
});// EOF verify otp



// update user
router.put('/update/:id', function (req, res, next) {
    var userId = req.params.id;
    var data = req.body;
    var username = data.username;
    var email = data.email;
    var firstname = data.firstname;
    var lastname = data.lastname;
    var mobileno = data.mobileno;
    var password = data.password;
    var status = data.status;

    // validation
    if (typeof (userId) == 'undefined' || !commonFunctions.isMongoId(userId)) {
        return res.send({'error': 1, 'msg': 'User Id not correct', 'data': {}});
    }
    if (typeof (username) == 'undefined' || username == '') {
        return res.send({'error': 1, 'msg': 'Please enter username', 'data': {}});
    }
    if (typeof (email) == 'undefined' || email == '') {
        return res.send({'error': 1, 'msg': 'Please enter email', 'data': {}});
    }
    if (typeof (firstname) == 'undefined' || firstname == '') {
        return res.send({'error': 1, 'msg': 'Please enter firstname', 'data': {}});
    }
    if (typeof (lastname) == 'undefined' || lastname == '') {
        return res.send({'error': 1, 'msg': 'Please enter lastname', 'data': {}});
    }
    if (typeof (mobileno) == 'undefined' || mobileno == '') {
        return res.send({'error': 1, 'msg': 'Please enter mobileno', 'data': {}});
    }
    if (typeof (password) == 'undefined' || password == '') {
        return res.send({'error': 1, 'msg': 'Please enter password', 'data': {}});
    }
    if (typeof (status) == 'undefined' || status == '') {
        return res.send({'error': 1, 'msg': 'Please select status', 'data': {}});
    }


    // Check if User name already exists
    var findCriteria = {
        username: new RegExp('^' + username + '$', 'i'),

        _id: {$ne: mongoose.Types.ObjectId(userId)}
    };
    userUtil.getUserByFindCriteria(findCriteria)
            .then(function (respObject) {
                if (respObject.length > 0)
                {
                    res.send({'error': 1, 'msg': 'Username name already exists', 'data': {}});
                    return true;
                }
                // else if(respObject.length > 0)
                // {
                //     res.send({'error': 1, 'msg': 'Email id already exists', 'data': {}});
                //     return true;
                // }
                return false;
            })
            .then(function (alreadyExists) {
                if (alreadyExists) {
                    return;
                }
                // now update
                var setObj = new Object();
                setObj.username = username;
                setObj.email = email;
                setObj.firstname = firstname;
                setObj.lastname = lastname;
                setObj.mobileno = mobileno;
                setObj.password = password;
                setObj.isActive = status;
                setObj.updatedDate = new Date();
                var updateObj = {
                    $set: setObj
                };
                var queryObj = {
                    '_id': mongoose.Types.ObjectId(userId)
                };

                userUtil.updateUser(queryObj, updateObj)
                        .then(function (respObject) {
                            res.send({'error': 0, 'msg': 'User updated successfully', 'data': respObject});
                        })
                        .catch(function (errObject) {
                            res.send({'error': 1, 'msg': 'Unable to update user', 'data': errObject});
                        });
            })
            .catch(function (errObject) {
                res.send({'error': 1, 'msg': 'Unable to update user', 'data': errObject});
            });
});
//update user end



// Get all users
router.get('/get', function (req, res) {
    var userInfo = (typeof (req.session.userInfo) != 'undefined') ? req.session.userInfo : {};
    var role = userInfo.role;

    if (typeof (role) != 'undefined') {
        if (role == 'SA') {
            var findCriteria = {};
        } else {
            var BDHeadRoleAccess = ['MIS-EXEC', 'CLIENT-BU', 'CONTENT-MNGR'];
            var markMngrRoleAccess = ['SALE-SU', 'SALE-BU', 'CLIENT-BU', 'CLIENT-SU'];

            if (role == 'BD-HEAD') {
                var findCriteria = {
                    role: {$in: BDHeadRoleAccess}
                };
            } else if (role == 'MARK-MNGR') {
                var findCriteria = {
                    role: {$in: markMngrRoleAccess}
                };
            }
        }
    }

    userUtil.getUserByFindCriteria(findCriteria)
            // userUtil.getAllUsers()
            .then(function (respObject) {
                res.send({'error': 0, 'msg': 'User list', 'data': respObject});
            })
            .catch(function (errObject) {
                res.send({'error': 1, 'msg': 'No User found', 'data': []});
            });
});


// Get data by id
router.get('/get/:id', function (req, res) {
    var userId = req.params.id;

    // validation
    if (typeof (userId) == 'undefined' || !commonFunctions.isMongoId(userId)) {
        return res.send({'error': 1, 'msg': 'User Id not correct', 'data': []});
    }

    userUtil.getUserById(userId)
            .then(function (respObject) {
                res.send({'error': 0, 'msg': 'User list', 'data': respObject});
            })
            .catch(function (errObject) {
                res.send({'error': 1, 'msg': 'No user found', 'data': []});
            });
});



router.post('/checkLogin/', function (req, res, next)
{
    var userObject = {
        registrationNumber: req.body.registrationNumber,
        primaryRegistrationDeviceToken: req.body.primaryRegistrationDeviceToken,
        secondaryRegistrationDeviceToken: req.body.secondaryRegistrationDeviceToken
    };

    userUtil.getUserByFindCriteria(userObject)
            .then(function (respObject) {
                req.session.userInfo = respObject;
                res.redirect('/admin/');
            })
            .catch(function (errObject) {
                res.send(errObject);
            });
});



router.get('/logout', function (req, res, next) {
    req.session.destroy();
    res.redirect('/login');
});


//get user profile
router.get('/getUserProfile', function (req, res, next) {

    userUtil.getUserById(req.decoded._id)
            .then(function (respObject) {
                if (respObject.length > 0) {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: 'User fetch success', data: respObject[0]})});
                    res.send(returnObjectToAndroid);
                } else {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "No such user", data: {}})});
                    res.send(returnObjectToAndroid);
                }
            })
            .catch(function (errorObject) {
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: 'Error in user fetch', data: {errorObject}})});
                res.send(returnObjectToAndroid);
            });
});

//update user profile
router.get('/updateUserProfile', function (req, res, next) {
    var data = req.query;
    console.log("data.registrationNumber");
    console.log(data.registrationNumber);
    var findCriteria = {
        registrationNumber: data.registrationNumber
    };
    userUtil.getUserByFindCriteria(findCriteria)
            .then(function (respObject) {
                if (respObject.length > 0) {
                    res.send({error: 0, msg: 'User fetch success', data: respObject[0]});
                }
            })
            .catch(function (errorObject) {
                res.send({error: 1, msg: 'Error in user fetch by registered mobile number', data: {errorObject}});
            });
});

//end call api
/*
 * @param {number} callCost 
 * @param {number} callDuration 
 * @param {string} callStart  - timestamp
 * @param {string} token  - access token
 * 
 */
/**********************************ROUND OFF MONEY IN PAISE EARNED TO 2 DIGITS*******************************************/

router.post('/endCall', function (req, res, next) {
    var data = req.body;
    if (data.callDuration < 0) {
        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Call Duration value cannot be less than 0"})});
        res.send(returnObjectToAndroid);
    }
    var callDuration = parseInt(data.callDuration);

    var isValidHourly = false;
    var isValidDaily = false;
    var isValidWeekly = false;
    var isValidMonthly = false;

    var resetHourly = false;
    var resetDaily = false;
    var resetWeekly = false;
    var resetMonthly = false;

    var globalSettingsObject = {};
    var currentUserObject = {};
    var userWalletCaps = {};
    var oldArchivedUsages = {};

    var callStartTime = new Date(parseInt(data.callStart));
    var callEndTime = new Date(callStartTime.getTime() + (data.callDuration * 1000));

    //store call logs on server for future reference. [LASTUPDATEDTIME in wallet and elsewhere is callEndTime]
    var insertCallLogObject = {
        callStartTime: callStartTime,
        callEndTime: callEndTime,
        callCost: parseInt(data.callCost),
        userId: commonFunctions.stringToObjectId(req.decoded._id),
        callDuration: data.callDuration
    };
    callLogUtil.insertCallLog(insertCallLogObject)
            .then(function (responseObject) {
                console.log("Insert call log success");
                console.log(responseObject);
            })
            .catch(function (errorObject) {
                console.log("Insert call log error");
                console.log(errorObject);
            });

    settingsUtil.getAllSettings()
            .then(function (responseObject) {
                if (responseObject.length > 0) {
                    //change scope to use in next _then_
                    globalSettingsObject = JSON.parse(JSON.stringify(responseObject[0]));
                    //                res.send({error: 0, msg: "Reimbursement data", data: {}});
                    var findCriteria = {
                        _id: commonFunctions.stringToObjectId(req.decoded._id)
                    };
                    return userUtil.getUserByFindCriteria(findCriteria);
                } else {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "No global settings", data: {}})});
                    res.send(returnObjectToAndroid);
                    //return [];
                }
            })
            .then(function (foundUser) {
                if (foundUser && foundUser.length > 0) {
                    //specific user level cap limit
                    userWalletCaps = foundUser[0].walletCaps;
                    currentUserObject = JSON.parse(JSON.stringify(foundUser[0]));
                } else {
                    console.log("user id");
                    console.log(req.decoded._id);
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Invalid user token", data: {}})});
                    res.send(returnObjectToAndroid);
                }
                //guest account limits
//                if (currentUserObject.totalWalletCredits >= 1000 && currentUserObject.isActive == 0) {
//                    res.send({error: 1, msg: "Limit of guest account reached. Please register.", data: {}});
//                    return;
//                }


                //calculate current cost
                var key = "";
                switch (true) {
                    case (callDuration > 0 && callDuration <= 30):
                        {
                            key = "30";
                        }
                        break;
                    case (callDuration > 30 && callDuration <= 60):
                        {
                            key = "60";
                        }
                        break;
                    case (callDuration > 60 && callDuration <= 90):
                        {
                            key = "90";
                        }
                        break;
                    case (callDuration > 90 && callDuration <= 120):
                        {
                            key = "120";
                        }
                        break;
                    case (callDuration > 120 && callDuration <= 150):
                        {
                            key = "150";
                        }
                        break;
                    case (callDuration > 150 && callDuration <= 180):
                        {
                            key = "180";
                        }
                        break;
                    case (callDuration > 180 && callDuration <= 210):
                        {
                            key = "210";
                        }
                        break;
                    case (callDuration > 210 && callDuration <= 240):
                        {
                            key = "240";
                        }
                        break;
                    case (callDuration > 240 && callDuration <= 270):
                        {
                            key = "270";
                        }
                        break;
                    case (callDuration > 270 && callDuration <= 300):
                        {
                            key = "300";
                        }
                        break;
                    case (callDuration > 300):
                        {
                            key = "300p";
                        }
                        break;
                }
                //use global options to find percentage value
                var index = Math.floor(Math.random() * 4 + 1);
                var currentValue = Math.round((globalSettingsObject.timeCosts["cost" + index + "__" + key] * data.callCost) / 100);
                if (currentValue + currentUserObject.totalWalletCredits >= globalSettingsObject.walletCaps.maxEarn) {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "User limit reached. Please recharge before you start earning again. ", data: {}})});
                    res.send(returnObjectToAndroid);
                } else if (currentValue == 0) {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue}})});
                    res.send(returnObjectToAndroid);
                } else {
                    //get actual limits from global or user

                    var maxDaily, maxMonthly, maxHourly, maxWeekly;
                    if (typeof userWalletCaps !== "undefined") {
                        //user user specific settings
                        maxHourly = userWalletCaps.perHour;
                        maxDaily = userWalletCaps.perDay;
                        maxWeekly = userWalletCaps.perWeek;
                        maxMonthly = userWalletCaps.perMonth;

                    } else {
                        //use global settings
                        maxHourly = globalSettingsObject.walletCaps.perHour;
                        maxDaily = globalSettingsObject.walletCaps.perDay;
                        maxWeekly = globalSettingsObject.walletCaps.perWeek;
                        maxMonthly = globalSettingsObject.walletCaps.perMonth;
                    }

                    //if he has last used data on profile
                    if (currentUserObject.creditUsage) {
                        if (currentUserObject.creditUsage.lastUpdated) {
                            var lastUpdatedTime = new Date(parseInt(currentUserObject.creditUsage.lastUpdated));

                            //find if the reimbursement amount is within the limits or not
                            checkIfCurrentIsValid(lastUpdatedTime, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);
                            //if within the limits
                            if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {

                                //put current data into new variable to later put it into call log schema for archiving and report generation.
                                var oldData = JSON.parse(JSON.stringify(currentUserObject.creditUsage));
                                oldData["archivedTime"] = callEndTime.getTime();
                                oldData["userId"] = commonFunctions.stringToObjectId(currentUserObject._id);
                                var returnArray = createUpdateObject(callEndTime, currentValue, true);

                                //update user
                                userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                                        .then(function (responseObject) {

                                            if (responseObject && responseObject.ok == "1") {
                                                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue}})});
                                                res.send(returnObjectToAndroid);
                                            }
                                        })
                                        .catch(function (errorObject) {
                                            var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some error on update user", data: errorObject})});
                                            res.send(returnObjectToAndroid);
                                        });
                                creditUsageArchiveUtil.insertCreditUsageArchive(oldData)
                                        .then(function (responseObjectArchivedUsage) {
                                            console.log("responseObjectArchivedUsage");
                                            console.log(responseObjectArchivedUsage);
                                        })
                                        .catch(function (errorObjectArchivedUsage) {
                                            console.log("errorObjectArchivedUsage");
                                            console.log(errorObjectArchivedUsage);
                                        });
                                //*********Update into wallet
                                var walletInsertObject = {
                                    userId: currentUserObject._id,
                                    creditAmount: currentValue,
                                    percentageApplied: globalSettingsObject.timeCosts["cost" + index + "__" + key],
                                    createdDate: returnArray[0].lastUpdated
                                };
                                walletUtil.insertWallet(walletInsertObject)
                                        .then(function (responseObjectWalletInsert) {
                                            console.log("responseObjectWalletInsert");
                                            console.log(responseObjectWalletInsert);
                                        })
                                        .catch(function (errorObjectWalletInsert) {
                                            console.log("errorObjectWalletInsert");
                                            console.log(errorObjectWalletInsert);
                                        });
                            } else if (resetHourly == true || resetDaily == true || resetWeekly == true || resetMonthly == true) {
                                //create object to store 'to be archived' information
                                oldArchivedUsages = JSON.parse(JSON.stringify(currentUserObject));
                                console.log("oldArchivedUsages");
                                console.log(oldArchivedUsages);
                                currentUserObject.creditUsage.hourly = (resetHourly == true ? 0 : currentUserObject.creditUsage.hourly);
                                currentUserObject.creditUsage.daily = (resetDaily == true ? 0 : currentUserObject.creditUsage.daily);
                                currentUserObject.creditUsage.weekly = (resetWeekly == true ? 0 : currentUserObject.creditUsage.weekly);
                                currentUserObject.creditUsage.monthly = (resetMonthly == true ? 0 : currentUserObject.creditUsage.monthly);
                                //find if the reimbursement amount is within the limits or not
                                checkIfCurrentIsValid(lastUpdatedTime, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);
                                //update user and send response
                                //if within the limits
                                if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {

                                    var returnArray = createUpdateObject(callEndTime, currentValue, true);
                                    //put current data into new variable to later put it into call log schema for archiving and report generation.
                                    var oldData = JSON.parse(JSON.stringify(currentUserObject.creditUsage));
                                    oldData["archivedTime"] = callEndTime.getTime();
                                    oldData["userId"] = commonFunctions.stringToObjectId(currentUserObject._id);

                                    //update user
                                    userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                                            .then(function (responseObject) {

                                                if (responseObject && responseObject.ok == "1") {
                                                    //***************************************************************
                                                    //BUG here. change this value since this is when hour, month, week, day has changed and not when some limit is reached
                                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue, resetHourly: resetHourly, resetDaily: resetDaily, resetWeekly: resetWeekly, resetMonthly: resetMonthly}})});
                                                    res.send(returnObjectToAndroid);
                                                }
                                            })
                                            .catch(function (errorObject) {
                                                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some error on update user", data: errorObject})});
                                                res.send(returnObjectToAndroid);
                                            });
                                    creditUsageArchiveUtil.insertCreditUsageArchive(oldData)
                                            .then(function (responseObjectArchivedUsage) {
                                                console.log("responseObjectArchivedUsage");
                                                console.log(responseObjectArchivedUsage);
                                            })
                                            .catch(function (errorObjectArchivedUsage) {
                                                console.log("errorObjectArchivedUsage");
                                                console.log(errorObjectArchivedUsage);
                                            });
                                    //*********Update into wallet
                                    var walletInsertObject = {
                                        userId: currentUserObject._id,
                                        creditAmount: currentValue,
                                        percentageApplied: globalSettingsObject.timeCosts["cost" + index + "__" + key],
                                        createdDate: returnArray[0].lastUpdated
                                    };
                                    walletUtil.insertWallet(walletInsertObject)
                                            .then(function (responseObjectWalletInsert) {
                                                console.log("responseObjectWalletInsert");
                                                console.log(responseObjectWalletInsert);
                                            })
                                            .catch(function (errorObjectWalletInsert) {
                                                console.log("errorObjectWalletInsert");
                                                console.log(errorObjectWalletInsert);
                                            });
                                }

                            } else {
                                //force choose lowest amount
                                currentValue = Math.round((globalSettingsObject.timeCosts["cost" + 1 + "__" + key] * data.callCost) / 100);
                                if (currentValue == 0) {
                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue}})});
                                    res.send(returnObjectToAndroid);
                                } else {
                                    //check if lowest amount is within the limits
                                    checkIfCurrentIsValid(lastUpdatedTime, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);


                                    if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {

                                        var returnArray = createUpdateObject(callEndTime, currentValue, true);
                                        //put current data into new variable to later put it into call log schema for archiving and report generation.
                                        var oldData = JSON.parse(JSON.stringify(currentUserObject.creditUsage));
                                        oldData["archivedTime"] = callEndTime.getTime();
                                        oldData["userId"] = commonFunctions.stringToObjectId(currentUserObject._id);

                                        userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                                                .then(function (responseObject) {
                                                    if (responseObject && responseObject.ok == "1") {
                                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue}})});
                                                        res.send(returnObjectToAndroid);
                                                    }
                                                })
                                                .catch(function (errorObject) {
                                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some error in user update", data: errorObject})});
                                                    res.send(returnObjectToAndroid);
                                                });
                                        creditUsageArchiveUtil.insertCreditUsageArchive(oldData)
                                                .then(function (responseObjectArchivedUsage) {
                                                    console.log("responseObjectArchivedUsage");
                                                    console.log(responseObjectArchivedUsage);
                                                })
                                                .catch(function (errorObjectArchivedUsage) {
                                                    console.log("errorObjectArchivedUsage");
                                                    console.log(errorObjectArchivedUsage);
                                                });
                                        //*********Update into wallet
                                        var walletInsertObject = {
                                            userId: currentUserObject._id,
                                            creditAmount: currentValue,
                                            percentageApplied: globalSettingsObject.timeCosts["cost" + 1 + "__" + key],
                                            createdDate: returnArray[0].lastUpdated
                                        };
                                        walletUtil.insertWallet(walletInsertObject)
                                                .then(function (responseObjectWalletInsert) {
                                                    console.log("responseObjectWalletInsert");
                                                    console.log(responseObjectWalletInsert);
                                                })
                                                .catch(function (errorObjectWalletInsert) {
                                                    console.log("errorObjectWalletInsert");
                                                    console.log(errorObjectWalletInsert);
                                                });

                                    } else if (resetHourly == true || resetDaily == true || resetWeekly == true || resetMonthly == true) {
                                        oldArchivedUsages = JSON.parse(JSON.stringify(currentUserObject));
                                        console.log("oldArchivedUsages");
                                        console.log(oldArchivedUsages);

                                        currentUserObject.creditUsage.hourly = (resetHourly == true ? 0 : currentUserObject.creditUsage.hourly);
                                        currentUserObject.creditUsage.daily = (resetDaily == true ? 0 : currentUserObject.creditUsage.daily);
                                        currentUserObject.creditUsage.weekly = (resetWeekly == true ? 0 : currentUserObject.creditUsage.weekly);
                                        currentUserObject.creditUsage.monthly = (resetMonthly == true ? 0 : currentUserObject.creditUsage.monthly);
                                        checkIfCurrentIsValid(lastUpdatedTime, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);
                                        //update user and send response
                                        if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                                            var returnArray = createUpdateObject(callEndTime, currentValue, true);
                                            //put current data into new variable to later put it into call log schema for archiving and report generation.
                                            var oldData = JSON.parse(JSON.stringify(currentUserObject.creditUsage));
                                            oldData["archivedTime"] = callEndTime.getTime();
                                            oldData["userId"] = commonFunctions.stringToObjectId(currentUserObject._id);

                                            userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                                                    .then(function (responseObject) {
                                                        if (responseObject && responseObject.ok == "1") {
                                                            var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue, resetHourly: resetHourly, resetDaily: resetDaily, resetWeekly: resetWeekly, resetMonthly: resetMonthly}})});
                                                            res.send(returnObjectToAndroid);
                                                        } else {
                                                            console.log("Error in user update.. ResponseObject =>");
                                                            console.log(responseObject);
                                                        }
                                                    })
                                                    .catch(function (errorObject) {
                                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some error in user update", data: errorObject})});
                                                        res.send(returnObjectToAndroid);
                                                    });
                                            creditUsageArchiveUtil.insertCreditUsageArchive(oldData)
                                                    .then(function (responseObjectArchivedUsage) {
                                                        console.log("responseObjectArchivedUsage");
                                                        console.log(responseObjectArchivedUsage);
                                                    })
                                                    .catch(function (errorObjectArchivedUsage) {
                                                        console.log("errorObjectArchivedUsage");
                                                        console.log(errorObjectArchivedUsage);
                                                    });

                                            //*********Update into wallet
                                            var walletInsertObject = {
                                                userId: currentUserObject._id,
                                                creditAmount: currentValue,
                                                percentageApplied: globalSettingsObject.timeCosts["cost" + 1 + "__" + key],
                                                createdDate: returnArray[0].lastUpdated
                                            };
                                            walletUtil.insertWallet(walletInsertObject)
                                                    .then(function (responseObjectWalletInsert) {
                                                        console.log("responseObjectWalletInsert");
                                                        console.log(responseObjectWalletInsert);
                                                    })
                                                    .catch(function (errorObjectWalletInsert) {
                                                        console.log("errorObjectWalletInsert");
                                                        console.log(errorObjectWalletInsert);
                                                    });
                                        }
                                    }
                                    //send no money. optimise later
                                    else {
                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Reimbursement amount", data: {amount: 0.0, resetHourly: resetHourly, resetDaily: resetDaily, resetWeekly: resetWeekly, resetMonthly: resetMonthly}})});
                                        res.send(returnObjectToAndroid);
                                    }
                                }

                            }

                        }
                    } else {
                        checkIfCurrentIsValid(-1, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);
                        if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                            var returnArray = createUpdateObject(callEndTime, currentValue);
                            //put current data into new variable to later put it into call log schema for archiving and report generation.

                            userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                                    .then(function (responseObject) {
                                        if (responseObject && responseObject.ok == "1") {
                                            var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue}})});
                                            res.send(returnObjectToAndroid);
                                        }
                                    })
                                    .catch(function (errorObject) {
                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Reimbursement amount", data: errorObject})});
                                        res.send(returnObjectToAndroid);
                                    });
                            //*********Update into wallet
                            var walletInsertObject = {
                                userId: currentUserObject._id,
                                creditAmount: currentValue,
                                percentageApplied: globalSettingsObject.timeCosts["cost" + 1 + "__" + key],
                                createdDate: returnArray[0].lastUpdated
                            };
                            walletUtil.insertWallet(walletInsertObject)
                                    .then(function (responseObjectWalletInsert) {
                                        console.log("responseObjectWalletInsert");
                                        console.log(responseObjectWalletInsert);
                                    })
                                    .catch(function (errorObjectWalletInsert) {
                                        console.log("errorObjectWalletInsert");
                                        console.log(errorObjectWalletInsert);
                                    });

                        } else {
                            //select minimum value out of options
                            currentValue = Math.round((globalSettingsObject.timeCosts["cost" + 1 + "__" + key] * data.callCost) / 100);
                            if (currentValue == 0) {
                                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue}})});
                                res.send(returnObjectToAndroid);
                            } else {
                                checkIfCurrentIsValid(-1, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);

                                if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                                    var returnArray = createUpdateObject(callEndTime, currentValue);
                                    userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                                            .then(function (responseObject) {
                                                if (responseObject && responseObject.ok == "1") {
                                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue}})});
                                                    res.send(returnObjectToAndroid);
                                                }
                                            })
                                            .catch(function (errorObject) {
                                                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some error on update user", data: errorObject})});
                                                res.send(returnObjectToAndroid);
                                            });
                                    //*********Update into wallet
                                    var walletInsertObject = {
                                        userId: currentUserObject._id,
                                        creditAmount: currentValue,
                                        percentageApplied: globalSettingsObject.timeCosts["cost" + 1 + "__" + key],
                                        createdDate: returnArray[0].lastUpdated
                                    };
                                    walletUtil.insertWallet(walletInsertObject)
                                            .then(function (responseObjectWalletInsert) {
                                                console.log("responseObjectWalletInsert");
                                                console.log(responseObjectWalletInsert);
                                            })
                                            .catch(function (errorObjectWalletInsert) {
                                                console.log("errorObjectWalletInsert");
                                                console.log(errorObjectWalletInsert);
                                            });
                                } else {
                                    //send no money. optimise later
                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Reimbursement amount", data: {amount: 0.0}})});
                                    res.send(returnObjectToAndroid);
                                }
                            }

                        }
                    }
                }

            })
            .catch(function (errorObject) {
                console.log("errorObject");
                console.log(errorObject);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some error", data: errorObject})});
                res.send(returnObjectToAndroid);
            });


//**********************************************************************************
//Function find if current is valid
//**********************************************************************************
    function checkIfCurrentIsValid(lastUpdatedTime, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly) {
        if (lastUpdatedTime == -1) {
//            This only executes for the first time.
            if (currentValue <= maxHourly) {
                isValidHourly = true;
            }
            if (currentValue <= maxDaily) {
                isValidDaily = true;
            }
            if (currentValue <= maxWeekly) {
                isValidWeekly = true;
            }
            if (currentValue <= maxMonthly) {
                isValidMonthly = true;
            }
        } else {
            if (lastUpdatedTime.getMonth() == callEndTime.getMonth()) {
                //check if same week
                if (lastUpdatedTime.getWeek() == callEndTime.getWeek()) {
                    //check if same day. Days start from Monday

                    if (lastUpdatedTime.getDay() == callEndTime.getDay()) {
                        //check if same hour
                        if (lastUpdatedTime.getHours() == callEndTime.getHours()) {
                            if ((currentValue + currentUserObject.creditUsage.hourly <= maxHourly)) {
                                isValidHourly = true;
                            }
                        } else {
                            //reset hourly used because hour has changed after last updated
                            resetHourly = true;
                            if ((currentValue <= maxHourly)) {
                                isValidHourly = true;
                            }
                        }
                        if ((currentValue + currentUserObject.creditUsage.daily <= maxDaily)) {
                            isValidDaily = true;
                        }
                    } else {
                        //reset daily and hourly used
                        resetHourly = true;
                        resetDaily = true;
                        if ((currentValue <= maxHourly)) {
                            isValidHourly = true;
                        }
                        if ((currentValue <= maxDaily)) {
                            isValidDaily = true;
                        }
                    }
                    if ((currentValue + currentUserObject.creditUsage.weekly <= maxWeekly)) {
                        isValidWeekly = true;
                    }
                } else {
                    //reset weekly, daily, hourly used
                    resetHourly = true;
                    resetDaily = true;
                    resetWeekly = true;
                    if ((currentValue <= maxHourly)) {
                        isValidHourly = true;
                    }
                    if ((currentValue <= maxDaily)) {
                        isValidDaily = true;
                    }
                    if ((currentValue <= maxWeekly)) {
                        isValidWeekly = true;
                    }
                }
                if ((currentValue + currentUserObject.creditUsage.monthly <= maxMonthly)) {
                    isValidMonthly = true;
                }
            } else {
                //******************Since month has changed, reset daily, hourly & monthly limit in the end
                //check if same week
                if (lastUpdatedTime.getWeek() == callEndTime.getWeek()) {
                    //check if same day. Days start from Monday
                    if (lastUpdatedTime.getDay() == callEndTime.getDay()) {
                        //check if same hour
                        if (lastUpdatedTime.getHours() == callEndTime.getHours()) {
                            if ((currentValue + currentUserObject.creditUsage.hourly <= maxHourly)) {
                                isValidHourly = true;
                            }
                        }
                        if ((currentValue + currentUserObject.creditUsage.daily <= maxDaily)) {
                            isValidDaily = true;
                        }
                    }

                    if ((currentValue + currentUserObject.creditUsage.weekly <= maxWeekly)) {
                        isValidWeekly = true;
                    }
                } else {
                    //reset weekly limits
                    resetWeekly = true;
                    if ((currentValue <= maxWeekly)) {
                        isValidWeekly = true;
                    }
                    //check if same day. Days start from Monday
                    if (lastUpdatedTime.getDay() == callEndTime.getDay()) {
                        //check if same hour
                        if (lastUpdatedTime.getHours() == callEndTime.getHours()) {
                            if ((currentValue + currentUserObject.creditUsage.hourly <= maxHourly)) {
                                isValidHourly = true;
                            }

                        }
                        if ((currentValue + currentUserObject.creditUsage.daily <= maxDaily)) {
                            isValidDaily = true;
                        }
                    } else {
                        //check if same hour
                        if (lastUpdatedTime.getHours() == callEndTime.getHours()) {
                            if ((currentValue + currentUserObject.creditUsage.hourly <= maxHourly)) {
                                isValidHourly = true;
                            }
                        }
                    }
                }
                //Since month has changed, resetting daily, hourly & monthly limit
                resetHourly = true;
                resetDaily = true;
                resetMonthly = true;
                if ((currentValue <= maxHourly)) {
                    isValidHourly = true;
                }
                if ((currentValue <= maxDaily)) {
                    isValidDaily = true;
                }
                if ((currentValue <= maxMonthly)) {
                    isValidMonthly = true;
                }
            }
        }

    }

//**********************************************************************************
//find if the current is within limits and create update object
//**********************************************************************************

    function createUpdateObject(callEndTime, currentValue, condition = false) {
        if (!condition) {
            var creditUsageUpdateObject = {
                lastUpdated: callEndTime.getTime()
            };
            //since first entry, equate directly
            creditUsageUpdateObject["hourly"] = currentValue;
            creditUsageUpdateObject["daily"] = currentValue;
            creditUsageUpdateObject["weekly"] = currentValue;
            creditUsageUpdateObject["monthly"] = currentValue;

            var wallet = currentUserObject.totalWalletCredits + currentValue;

        } else {
            //create new update object
            var creditUsageUpdateObject = {
                lastUpdated: callEndTime.getTime()
            };

            //reset if necessary
            creditUsageUpdateObject["hourly"] = (resetHourly == true ? currentValue : currentUserObject.creditUsage.hourly + currentValue);
            creditUsageUpdateObject["daily"] = (resetDaily == true ? currentValue : currentUserObject.creditUsage.daily + currentValue);
            creditUsageUpdateObject["weekly"] = (resetWeekly == true ? currentValue : currentUserObject.creditUsage.weekly + currentValue);
            creditUsageUpdateObject["monthly"] = (resetMonthly == true ? currentValue : currentUserObject.creditUsage.monthly + currentValue);
            var wallet = currentUserObject.totalWalletCredits + currentValue;

        }
        return [creditUsageUpdateObject, wallet];
    }

});


router.get('/getWalletHistory', function (req, res, next) {
    console.log("Inside API");
    var findCriteria = {
        userId: commonFunctions.stringToObjectId(req.decoded._id),
        creditAmount: {$gte: 0}
    };
    var projectFields = {
        __v: 0,
        _id: 0,
        percentageApplied: 0
    };
    var sortCriteria = {
        createdDate: -1
    };
    walletUtil.getWalletByCustom({findCriteria: findCriteria, projectFields: projectFields, sortCriteria: sortCriteria})
            .then(function (responseObjectFoundWalletDocuments) {
                console.log("responseObjectFoundWalletDocuments");
                console.log(responseObjectFoundWalletDocuments);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "All related documents", data: responseObjectFoundWalletDocuments})});
                res.send(returnObjectToAndroid);
            })
            .catch(function (errorObjectWalletDocuments) {
                console.log("errorObjectWalletDocuments");
                console.log(errorObjectWalletDocuments);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Error during wallet fetch", data: errorObjectWalletDocuments})});
                res.send(returnObjectToAndroid);
            });
});

//get recharge history
router.get('/getRechargeHistory', function (req, res, next) {
    console.log("Inside API");
    var findCriteria = {
        userId: commonFunctions.stringToObjectId(req.decoded._id),
        creditAmount: {$lt: 0}
    };
    var projectFields = {
        __v: 0,
        _id: 0,
        percentageApplied: 0,
        isOneTap: 0
    };
    walletUtil.getWalletByCustom({findCriteria: findCriteria, projectFields: projectFields})
            .then(function (responseObjectFoundWalletDocuments) {
                console.log("responseObjectFoundWalletDocuments");
                console.log(responseObjectFoundWalletDocuments);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "All related documents", data: responseObjectFoundWalletDocuments})});
                res.send(returnObjectToAndroid);
            })
            .catch(function (errorObjectWalletDocuments) {
                console.log("errorObjectWalletDocuments");
                console.log(errorObjectWalletDocuments);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Error during wallet fetch", data: errorObjectWalletDocuments})});
                res.send(returnObjectToAndroid);
            });
});

Date.prototype.getWeek = function () {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
};


router.get('/getReferralCode', function (req, res, next) {
//function getReferralCode() {
    var referralCode;
    console.log("req.decoded");
    console.log(req.decoded);
    var arrayUser = [0];
    userUtil.getUserById(req.decoded._id)
            .then(function (responseUserFindObject) {
                if (responseUserFindObject[0].referralCode && responseUserFindObject[0].referralCode.length > 0 && responseUserFindObject[0].referralCode.length < 10) {
                    console.log("In here");
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Referral Code", data: {code: responseUserFindObject[0].referralCode}})});
                    res.send(returnObjectToAndroid);
                    return false;
                } else {
                    return promiseWhile(function () {
                        // Condition for stopping
                        return arrayUser.length > 0 && arrayUser.length < 2;
                    }, function () {
                        // Action to run, should return a promise
                        return new Promise(function (resolve, reject) {
                            // Arbitrary 250ms async method to simulate async process
                            // In real usage it could just be a normal async event that 
                            // returns a Promise.
                            if (arrayUser[0] == 0) {
                                arrayUser = [];
                            }
                            referralCode = commonFunctions.generateNewReferralCode()
                            console.log(referralCode);
                            console.log(referralCode);
                            var findCriteria = {
                                referralCode: referralCode
                            };
                            userUtil.getUserByFindCriteria(findCriteria)
                                    .then(function (responseObject) {
                                        if (responseObject.length > 0) {
                                            arrayUser.push(responseObject.length);
                                        } else {
                                            resolve();
                                        }
                                    })
                                    .catch(function (errorObject) {
                                        console.log("errorObject");
                                        console.log(errorObject);
                                        reject(errorObject);
                                    });
                        });
                    });
                }

            })
            .then(function (responseFromWhile) {
                console.log("responseFromWhile");
                console.log(responseFromWhile);
                if (responseFromWhile !== false) {
                    // Notice we can chain it because it's a Promise, 
                    // this will run after completion of the promiseWhile Promise!
                    console.log("Done");
                    var queryObject = {
                        _id: commonFunctions.stringToObjectId(req.decoded._id)
                    };
                    var updateObject = {
                        $set: {
                            referralCode: referralCode
                        }
                    };
                    return userUtil.updateUser(queryObject, updateObject)
                } else {
                    return false;
                }

            })
            .then(function (userUpdateReferralCodeSuccess) {
                if (userUpdateReferralCodeSuccess) {
                    console.log("userUpdateReferralCodeSuccess");
                    console.log(userUpdateReferralCodeSuccess);
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "New regex code", data: {code: referralCode}})});
                    res.send(returnObjectToAndroid);
                } else {
                    res.end();
                }
            })
            .catch(function (userUpdateReferralCodeException) {
                console.log("userUpdateReferralCodeException");
                console.log(userUpdateReferralCodeException);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Referral Code Error", data: userUpdateReferralCodeException})});
                res.send(returnObjectToAndroid);
            });
});


var promiseWhile = function (condition, action) {
    var resolver = Promise.defer();

    var loop = function () {
        if (!condition())
            return resolver.resolve();
        return Promise.cast(action())
                .then(loop)
                .catch(resolver.reject);
    };

    process.nextTick(loop);

    return resolver.promise;
};

//for update user ADMIN ONLY

router.post('/add', function (req, res, next) {
    console.log(req.body.number);
    if (req.body.token != "3.14159") {
        res.send("Added");
    } else {
        userUtil.getUserByFindCriteria({"registrationNumber": parseInt(req.body.number)})
                .then(function (responseObjectFind) {
                    console.log(responseObjectFind);
                    if (responseObjectFind.length > 0) {
                        userUtil.deleteByParam({"registrationNumber": req.body.number})
                                .then(function (responseObject) {
                                    console.log("success");
                                    res.send({response: 1});
                                })
                                .catch(function (errorObject) {
                                    console.log("errorObject");
                                    console.log(errorObject);
                                    res.send(errorObject)
                                });
                        walletUtil.deleteWalletByCustom({userId: responseObjectFind[0]._id})
                                .then(function (responseObjectWallet) {
                                    console.log("responseObjectWallet");
                                    console.log(responseObjectWallet);
                                })
                                .catch(function (errorObjectWallet) {
                                    console.log("errorObjectWallet");
                                    console.log(errorObjectWallet);
                                });
                    } else {
                        res.send({error: "No user with this number"});
                    }
                })
                .catch(function (errorObjectFind) {
                    console.log("errorObject");
                    console.log(errorObjectFind);
                });

    }
});

router.post('/updateWallet', function (req, res, next) {
    console.log(req.body.number);
    console.log(req.body.totalWalletCredits);
    if (req.body.token != "3.14159") {
        res.send("Added");
    } else {
        var findCriteria = {};
        console.log("req.body.number");
        console.log(req.body.number);
        if (req.body.number) {
            findCriteria["registrationNumber"] = req.body.number;
        } else {
            findCriteria = {
                "email": req.body.email,
                "isActive": 0
            };
        }
        userUtil.updateUser(findCriteria, {$set: {"totalWalletCredits": req.body.totalWalletCredits}})
                .then(function (responseObject) {
                    console.log("success");
                    res.send({response: 1});
                })
                .catch(function (errorObject) {
                    console.log("errorObject");
                    console.log(errorObject);
                    res.send(errorObject)
                });
    }
});

router.post('/registerFailedAttempts', function (req, res, next) {
    var operations = [];
    var userId = commonFunctions.stringToObjectId(req.decoded._id);
    var data = eval(req.body.objectsFailed);
    var operations2 = [];
    var globalUserObject;
    for (var i = 0; i < data.length; i++) {
        var callStartTime = new Date(parseInt(data[i].callStartTime));
        var callDuration = parseInt(data[i].callDuration);
        var callEndTime = new Date(callStartTime.getTime() + (callDuration * 1000));
        var callCost = parseInt(data[i].callCost);
        var reason = data[i].reason;
        if (!parseInt(reason) in [1, 2, 3, 4, 5, 6, 21, 23, 13, 123, 1234]) {
            var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({errro: 1, msg: "Invalid reason", data: {}})});
            res.send(returnObjectToAndroid);
        }
        var insertObject = {
            callStartTime: callStartTime,
            callEndTime: callEndTime,
            callDuration: callDuration,
            callCost: callCost,
            reason: reason,
            userId: userId
        };
        console.log(insertObject);
        operations.push(failedAttemptsUtil.insertfailedAttempts(insertObject));
    }
    Promise.all(operations)
            .then(function (responseObject) {
                console.log("responseObject in register failed attempt");
                console.log(responseObject);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Saved successfully", data: {}})});
                res.send(returnObjectToAndroid);
                //this is the array of all operations which returns promises thereby can be executed in Promise.all


                /*NOW CALCULATE OLD COSTS OF NEWLY UPDATED SHEETS*/
                var userId = req.decoded._id;
                return userUtil.getUserById(userId)

            })
            .then(function (responseObject) {
                console.log("responseObject get user");
                console.log(responseObject);
                globalUserObject = JSON.parse(JSON.stringify(responseObject[0]));
                //fetch failedAttempts by user id
                var findCriteria = {
                    userId: commonFunctions.stringToObjectId(userId),
                    oldAmount: {$exists: false}
                };
                return failedAttemptsUtil.getfailedAttemptsByFindCriteria(findCriteria);

            })
            .then(function (responseObjectAllFailedAttempts) {
                console.log("responseObjectAllFailedAttempts");
                console.log(responseObjectAllFailedAttempts);
                console.log(responseObjectAllFailedAttempts.length);
                for (var i = 0; i < responseObjectAllFailedAttempts.length; i++) {
                    operations2.push(computeOldCosts(responseObjectAllFailedAttempts[i], globalUserObject));
                }
                return Promise.all(operations2);
            })
            .then(function (endedAllOperationsAndComputations) {
                console.log("endedAllOperationsAndComputations");
                console.log(endedAllOperationsAndComputations);
                console.log("Completed computations for all oldAmounts");
                console.log("User ID:" + req.decoded._id);
            })
            .catch(function (errorObject) {
                console.log("errorObject in register failed attempt");
                console.log(errorObject);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Save error", data: {}})});
                res.send(returnObjectToAndroid);
            });
});

//API FOR OLD AMOUNT COMPUTATIONS
router.get('/calculateOldCosts', function (req, res, next) {
    //this is the array of all operations which returns promises thereby can be executed in Promise.all
    var operations = [];
    var userId = req.decoded._id;
    var globalUserObject;
    userUtil.getUserById(userId)
            .then(function (responseObject) {
                console.log("responseObject get user");
                console.log(responseObject);
                globalUserObject = JSON.parse(JSON.stringify(responseObject[0]));
                //fetch failedAttempts by user id
                var findCriteria = {
                    userId: commonFunctions.stringToObjectId(userId),
                    oldAmount: {$exists: false}
                };
                return failedAttemptsUtil.getfailedAttemptsByFindCriteria(findCriteria);

            })
            .then(function (responseObjectAllFailedAttempts) {
                console.log("responseObjectAllFailedAttempts");
                console.log(responseObjectAllFailedAttempts);
                console.log(responseObjectAllFailedAttempts.length);
                for (var i = 0; i < responseObjectAllFailedAttempts.length; i++) {
                    operations.push(computeOldCosts(responseObjectAllFailedAttempts[i], globalUserObject));
                }
                return Promise.all(operations);
            })
            .then(function (endedAllOperationsAndComputations) {
                console.log("endedAllOperationsAndComputations");
                console.log(endedAllOperationsAndComputations);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Successfully finished", data: {}})});
                res.send(returnObjectToAndroid);
            })
            .catch(function (errorObjectAllFailedAttempts) {
                console.log("errorObjectAllFailedAttempts");
                console.log(errorObjectAllFailedAttempts);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some error", data: errorObjectAllFailedAttempts})});
                res.send(returnObjectToAndroid);
            });
});
function computeOldCosts(singleObject, globalUserObject) {
    console.log("singleObject");
    console.log(singleObject);
    var callDuration = singleObject.callDuration;
    var callStartTime = singleObject.callStartTime;
    var callEndTime = new Date(callStartTime.getTime() + (callDuration * 1000));
    var callCost = singleObject.callCost;
    var userId = globalUserObject._id;
    console.log(callStartTime);
    console.log(callEndTime);

    var archivedCreditUsage;
    var oldCost;

    var isValidHourly = false;
    var isValidDaily = false;
    var isValidWeekly = false;
    var isValidMonthly = false;

    var resetHourly = false;
    var resetDaily = false;
    var resetWeekly = false;
    var resetMonthly = false;



    //store call logs on server for future reference. [LASTUPDATEDTIME in wallet and elsewhere is callEndTime]
    var insertCallLogObject = {
        callStartTime: callStartTime,
        callEndTime: callEndTime,
        callCost: parseInt(callCost),
        userId: commonFunctions.stringToObjectId(userId),
        callDuration: callDuration
    };

    var globalSettingsObject = {};

    return new Promise(function (resolve, reject) {
        //insert call log
        callLogUtil.insertCallLog(insertCallLogObject)
                .then(function (responseObject) {
                    console.log("Insert call log success");
                    console.log(responseObject);
                })
                .catch(function (errorObject) {
                    console.log("Insert call log error");
                    console.log(errorObject);
                    reject({error: 1, msg: "Call Log insert error", data: errorObject});
                });

        //fetch settings to know limits
        settingsUtil.getSettingsByFindCriteria({createdDate: {$lte: callEndTime}})
                .then(function (responseObjectGetSettings) {
                    // globalUserObject.walletCaps
                    if (responseObjectGetSettings.length > 0) {
                        //current settings is latest
                        return responseObjectGetSettings;
                    } else if (responseObjectGetSettings.length == 0) {
                        return settingsUtil.getArchivedSettingsByFindCriteria({createdDate: {$lte: callEndTime}, archivedDate: {$gte: callEndTime}})
                    }
                })
                .then(function (responseObjectGetSettingsArchived) {
                    if (responseObjectGetSettingsArchived) {

                        //change scope to use in next _then_
                        globalSettingsObject = JSON.parse(JSON.stringify(responseObjectGetSettingsArchived[0]));
                        //check if credit usage is on user or in archives.
                        if (globalUserObject.creditUsage && (globalUserObject.creditUsage.lastUpdated <= callEndTime)) {
                            //this is the latest creditUsage for reference
                            //send a flag 0
                            console.log("Sending 0");
                            return [0];
                        } else {
                            var tempCurrent1 = new Date(callEndTime.getTime());
                            var tempCurrent4 = new Date(callEndTime.getTime());
                            //Same Week
                            var firstTsDay = new Date(tempCurrent1.setDate(tempCurrent1.getDate() - tempCurrent1.getDay()));
                            var lastTsDay = new Date(tempCurrent1.setDate(tempCurrent1.getDate() - tempCurrent1.getDay() + 6));
                            //Same Month
                            var firstTsDate = new Date(tempCurrent4.getFullYear(), tempCurrent4.getMonth(), 1);
                            var lastTsDate = new Date(tempCurrent4.getFullYear(), tempCurrent4.getMonth() + 1, 0);
                            //find documents of same month, hr, day, week
                            var dataObj = {};
                            dataObj["findCriteria"] = {
                                userId: commonFunctions.stringToObjectId(globalUserObject._id),
                                $or: [
                                    //for weekly 
                                    {lastUpdated: {$gte: firstTsDay.getTime(), $lte: lastTsDay.getTime()}},
//                                {lastUpdated: {$gte: firstTsMin.getTime(), $lte: lastTsMin.getTime()}},
//                                {lastUpdated: {$gte: firstTsHour.getTime(), $lte: lastTsHour.getTime()}},
                                    {lastUpdated: {$gte: firstTsDate.getTime(), $lte: lastTsDate.getTime()}}
                                ]
                            };
                            dataObj["sortCriteria"] = {
                                'lastUpdated': -1
                            };
                            console.log("dataObj");
                            console.log(JSON.stringify(dataObj));
                            //send archived record matching date time
                            return creditUsageArchiveUtil.getCreditUsageArchiveByCustom(dataObj)
                        }
                    } else {
                        return false;
                    }
                })
                .then(function (creditUsageArray) {

                    var tempCurrent1 = new Date(callEndTime.getTime());
                    var tempCurrent2 = new Date(callEndTime.getTime());
                    var tempCurrent3 = new Date(callEndTime.getTime());

                    var firstTsDay = new Date(tempCurrent1.setDate(tempCurrent1.getDate() - tempCurrent1.getDay()));
                    var lastTsDay = new Date(tempCurrent1.setDate(tempCurrent1.getDate() - tempCurrent1.getDay() + 6));
                    //Same Hour
                    var firstTsMin = new Date(tempCurrent2.setMinutes(0));
                    var lastTsMin = new Date(tempCurrent2.setMinutes(59));
                    //Same Day
                    var firstTsHour = new Date(tempCurrent3.setHours(0));
                    var lastTsHour = new Date(tempCurrent3.setHours(23));

                    //use global settings
                    maxHourly = globalSettingsObject.walletCaps.perHour;
                    maxDaily = globalSettingsObject.walletCaps.perDay;
                    maxWeekly = globalSettingsObject.walletCaps.perWeek;
                    maxMonthly = globalSettingsObject.walletCaps.perMonth;

                    if (creditUsageArray.length == 1 && creditUsageArray[0] == 0) {
                        console.log("use global user object");
                        //use global user object
                        //0=>min, 1=> Random
                        var indexAndKey = getIndexAndKey(1, callDuration);
                        var currentValue = Math.round((globalSettingsObject.timeCosts["cost" + indexAndKey[0] + "__" + indexAndKey[1]] * callCost) / 100);
                        console.log("currentValue");
                        console.log(currentValue);
                        checkIfCurrentIsValid(currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly, 0, 0, 0, 0);
                        if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                            oldCost = currentValue;
                        } else {
                            //get min current value
                            var indexAndKey = getIndexAndKey(0, callDuration);
                            var currentValue = Math.round((globalSettingsObject.timeCosts["cost" + indexAndKey[0] + "__" + indexAndKey[1]] * callCost) / 100);
                            console.log("currentValue");
                            console.log(currentValue);
                            //check if new computed value is valid i.e which does not exceed the max limits
                            checkIfCurrentIsValid(currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly, monthlyUsed, dailyUsed, hourlyUsed, weeklyUsed);
                            if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                                oldCost = currentValue;
                            } else {
                                oldCost = 0;
                            }
                        }
                        console.log("oldCost");
                        console.log(oldCost);
                    } else if (creditUsageArray.length > 0 && creditUsageArray[0] !== 0) {
                        console.log("in else of use global user object");
                        console.log("creditUsageArray.length");
                        console.log(creditUsageArray.length);
                        console.log("creditUsageArray");
                        console.log(creditUsageArray);
                        var monthlyUsed = creditUsageArray[0].monthly;
                        var weeklyUsed = 0, dailyUsed = 0, hourlyUsed = 0;
                        //get weekly used, monthly used, hourly used, daily used limits of this user
                        var notSkipWeakly = true;
                        var notSkipDaily = true;
                        var notSkipHourly = true;
                        for (var i = 0; i < creditUsageArray.length; i++) {
                            //get weekly consumed. Break after finding first entry. Since this is latest by sorting in mongo query, this is the most recent thereby the exact used amount
                            if (firstTsDay.getTime() < callEndTime < lastTsDay.getTime() && weeklyUsed < creditUsageArray[i].weekly && notSkipWeakly) {
                                weeklyUsed = creditUsageArray[i].weekly;
                                notSkipWeakly = false;
                            }
                            //get dailyUsed
                            if (firstTsHour.getTime() < callEndTime < lastTsHour.getTime() && dailyUsed < creditUsageArray[i].daily && notSkipDaily) {
                                dailyUsed = creditUsageArray[i].daily;
                                notSkipDaily = false;
                            }
                            //get HourlyUsed
                            if (firstTsMin.getTime() < callEndTime < lastTsMin.getTime() && hourlyUsed < creditUsageArray[i].hourly && notSkipHourly) {
                                hourlyUsed = creditUsageArray[i].daily;
                                notSkipHourly = false;
                            }

                            console.log("monthlyUsed");
                            console.log(monthlyUsed);
                            console.log("dailyUsed");
                            console.log(dailyUsed);
                            console.log("hourlyUsed");
                            console.log(hourlyUsed);
                            console.log("weeklyUsed");
                            console.log(weeklyUsed);
                        }
                    } else {
                        var monthlyUsed = 0;
                        var weeklyUsed = 0, dailyUsed = 0, hourlyUsed = 0;
                    }

                    //use global settings
                    maxHourly = globalSettingsObject.walletCaps.perHour;
                    maxDaily = globalSettingsObject.walletCaps.perDay;
                    maxWeekly = globalSettingsObject.walletCaps.perWeek;
                    maxMonthly = globalSettingsObject.walletCaps.perMonth;
                    //0=>min, 1=> Random
                    var indexAndKey = getIndexAndKey(1, callDuration);
                    var currentValue = Math.round((globalSettingsObject.timeCosts["cost" + indexAndKey[0] + "__" + indexAndKey[1]] * callCost) / 100);;
                    checkIfCurrentIsValid(currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly, monthlyUsed, dailyUsed, hourlyUsed, weeklyUsed);
                    if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                        oldCost = currentValue;
                    } else {
                        var indexAndKey = getIndexAndKey(0, callDuration);
                        console.log("indexAndKey");
                        console.log(indexAndKey);
                        var currentValue = Math.round((globalSettingsObject.timeCosts["cost" + indexAndKey[0] + "__" + indexAndKey[1]] * callCost) / 100);
                        console.log("currentValue");
                        console.log(currentValue);
                        checkIfCurrentIsValid(currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly, monthlyUsed, dailyUsed, hourlyUsed, weeklyUsed);
                        if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                            oldCost = currentValue;
                        } else {
                            oldCost = 0;
                        }
                    }

                    //we have old Cost. Now update in failedAttempts. 
                    var findCriteriaObject = {
                        _id: singleObject._id
                    };
                    var updateObject = {
                        $set: {
                            oldAmount: oldCost
                        }
                    };
                    failedAttemptsUtil.updatefailedAttempts(findCriteriaObject, updateObject)
                            .then(function (responseObject) {
                                console.log("success responseObject for object with id");
                                console.log(findCriteriaObject);
                                resolve();
                            })
                            .catch(function (errorExceptionObject) {
                                console.log("error errorExceptionObject for object id");
                                console.log(findCriteriaObject);
                                console.log(errorExceptionObject);
                                reject();
                            });
                })
                .catch(function (errorObjectGetSettings) {
                    console.log("errorObjectGetSettings");
                    console.log(errorObjectGetSettings);
                    reject(errorObjectGetSettings);
                });
    }
    );

    function getIndexAndKey(minOrRand, callDuration) {
        //minOrMax can be 0 => min or 1=> random
        //calculate current cost
        var key = "";
        switch (true) {
            case (callDuration > 0 && callDuration <= 30):
                {
                    key = "30";
                }
                break;
            case (callDuration > 30 && callDuration <= 60):
                {
                    key = "60";
                }
                break;
            case (callDuration > 60 && callDuration <= 90):
                {
                    key = "90";
                }
                break;
            case (callDuration > 90 && callDuration <= 120):
                {
                    key = "120";
                }
                break;
            case (callDuration > 120 && callDuration <= 150):
                {
                    key = "150";
                }
                break;
            case (callDuration > 150 && callDuration <= 180):
                {
                    key = "180";
                }
                break;
            case (callDuration > 180 && callDuration <= 210):
                {
                    key = "210";
                }
                break;
            case (callDuration > 210 && callDuration <= 240):
                {
                    key = "240";
                }
                break;
            case (callDuration > 240 && callDuration <= 270):
                {
                    key = "270";
                }
                break;
            case (callDuration > 270 && callDuration <= 300):
                {
                    key = "300";
                }
                break;
            case (callDuration > 300):
                {
                    key = "300p";
                }
                break;
        }
        var index;
        if (minOrRand == 1) {
            index = Math.floor(Math.random() * 4 + 1);
        } else if (minOrRand == 0) {
            index = 1;
        }
        return [index, key];
    }

//**********************************************************************************
//Function find if current is valid
//**********************************************************************************
    function checkIfCurrentIsValid(currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly, monthlyUsed, dailyUsed, hourlyUsed, weeklyUsed) {
        if (currentValue + monthlyUsed <= maxMonthly) {
            isValidMonthly = true;
        }
        if (currentValue + weeklyUsed <= maxWeekly) {
            isValidWeekly = true;
        }
        if (currentValue + dailyUsed <= maxDaily) {
            isValidDaily = true;
        }
        if (currentValue + hourlyUsed <= maxHourly) {
            isValidHourly = true;
        }

    }
}

router.get('/oneTap', function (req, res, next) {
    console.log(req.decoded._id);

    var isValidHourly = false;
    var isValidDaily = false;
    var isValidWeekly = false;
    var isValidMonthly = false;

    var resetHourly = false;
    var resetDaily = false;
    var resetWeekly = false;
    var resetMonthly = false;

    var newCost = 0;
    var currentUserObject = {};
    var globalSettingsObject = {};
    var globalFailedObject = {};
    var userWalletCaps = {};
    var oldArchivedUsages = {};

    //get user by userId
    userUtil.getUserById(req.decoded._id)
            .then(function (responseObjectUser) {
//                response in an array
                console.log("responseObjectUser");
                console.log(responseObjectUser);
                if (responseObjectUser.length > 0) {
                    currentUserObject = JSON.parse(JSON.stringify(responseObjectUser[0]));
                    //find this user's failed attempt which has computed old cost
                    var findCriteria = {
                        userId: responseObjectUser[0]._id,
                        oldAmount: {$exists: true},
                        isActive: true
                    };
                    return failedAttemptsUtil.getfailedAttemptsByFindCriteria(findCriteria)
                } else {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "No valid user", data: {}})});
                    res.send(returnObjectToAndroid);
                    return false;
                }
            })
            .then(function (failedAttempts) {
                if (failedAttempts.length > 0) {
                    var index = Math.round(Math.random() * (failedAttempts.length - 1));
                    var currentFailedAttempt = failedAttempts[index];
                    currentFailedAttempt["callEndTime"] = new Date();
                    //make current one inActive so it can't be used again
                    failedAttemptsUtil.updatefailedAttempts({_id: currentFailedAttempt._id}, {$set: {"isActive": false}})
                            .then(function (responseObjectUpdateFailedAttempt) {
                                console.log("responseObjectUpdateFailedAttempt");
                                console.log(responseObjectUpdateFailedAttempt);
                            });
//                            .catch(function (errorObjectUpdateFailedAttempt) {
//                                console.log("errorObjectUpdateFailedAttempt");
//                                console.log(errorObjectUpdateFailedAttempt);
//                            });
                    return currentFailedAttempt;
                } else {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "No data available for one tap.", data: {}})});
                    res.send(returnObjectToAndroid);
                    return false;
                }
            })
            .then(function (currentFailedAttempt) {
                if (currentFailedAttempt) {
                    console.log("currentFailedAttempt");
                    console.log(currentFailedAttempt);
                    var callStartTime = currentFailedAttempt.callStart;
                    //use current time for calculating costs considering limits of now
                    var callEndTime = new Date();
                    //get current settings
                    settingsUtil.getAllSettings()
                            .then(function (responseObject) {
                                if (responseObject.length > 0) {
                                    //change scope to use in next _then_
                                    globalSettingsObject = JSON.parse(JSON.stringify(responseObject[0]));
                                } else {
                                    newCost = 0;
                                }
                                return;
                            })
                            .then(function () {
                                if (currentUserObject && Object.keys(currentUserObject).length > 0) {
                                    //calculate current cost
                                    var key = "";
                                    var callDuration = currentFailedAttempt.callDuration;
                                    switch (true) {
                                        case (callDuration > 0 && callDuration <= 30):
                                            {
                                                key = "30";
                                            }
                                            break;
                                        case (callDuration > 30 && callDuration <= 60):
                                            {
                                                key = "60";
                                            }
                                            break;
                                        case (callDuration > 60 && callDuration <= 90):
                                            {
                                                key = "90";
                                            }
                                            break;
                                        case (callDuration > 90 && callDuration <= 120):
                                            {
                                                key = "120";
                                            }
                                            break;
                                        case (callDuration > 120 && callDuration <= 150):
                                            {
                                                key = "150";
                                            }
                                            break;
                                        case (callDuration > 150 && callDuration <= 180):
                                            {
                                                key = "180";
                                            }
                                            break;
                                        case (callDuration > 180 && callDuration <= 210):
                                            {
                                                key = "210";
                                            }
                                            break;
                                        case (callDuration > 210 && callDuration <= 240):
                                            {
                                                key = "240";
                                            }
                                            break;
                                        case (callDuration > 240 && callDuration <= 270):
                                            {
                                                key = "270";
                                            }
                                            break;
                                        case (callDuration > 270 && callDuration <= 300):
                                            {
                                                key = "300";
                                            }
                                            break;
                                        case (callDuration > 300):
                                            {
                                                key = "300p";
                                            }
                                            break;
                                    }
                                    //use global options to find percentage value
                                    var index = Math.floor(Math.random() * 4 + 1);
                                    var currentValue = Math.round((globalSettingsObject.timeCosts["cost" + index + "__" + key] * currentFailedAttempt.callCost) / 100);
                                    if (currentValue == 0) {
                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue}})});
                                        res.send(returnObjectToAndroid);
                                    } else {
                                        //get actual limits from global or user
                                        var maxDaily, maxMonthly, maxHourly, maxWeekly;
//                                        if (typeof userWalletCaps !== "undefined") {
//                                            //user user specific settings
//                                            maxHourly = userWalletCaps.perHour;
//                                            maxDaily = userWalletCaps.perDay;
//                                            maxWeekly = userWalletCaps.perWeek;
//                                            maxMonthly = userWalletCaps.perMonth;
//
//                                        } else {
                                        //use global settings
                                        maxHourly = globalSettingsObject.walletCaps.perHour;
                                        maxDaily = globalSettingsObject.walletCaps.perDay;
                                        maxWeekly = globalSettingsObject.walletCaps.perWeek;
                                        maxMonthly = globalSettingsObject.walletCaps.perMonth;
//                                        }

                                        //if he has last used data on profile
                                        if (currentUserObject.creditUsage) {
                                            if (currentUserObject.creditUsage.lastUpdated) {
                                                var lastUpdatedTime = new Date(parseInt(currentUserObject.creditUsage.lastUpdated));
                                                //find if the reimbursement amount is within the limits or not
                                                checkIfCurrentIsValid(lastUpdatedTime, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);
                                                //if within the limits
                                                if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                                                    if (currentFailedAttempt.oldCost <= currentValue) {
                                                        newCost = currentFailedAttempt.oldCost;
                                                    } else {
                                                        newCost = currentValue;
                                                    }
                                                    //put current data into new variable to later put it into call log schema for archiving and report generation.
                                                    var oldData = JSON.parse(JSON.stringify(currentUserObject.creditUsage));
                                                    oldData["archivedTime"] = callEndTime.getTime();
                                                    oldData["userId"] = commonFunctions.stringToObjectId(currentUserObject._id);
                                                    var returnArray = createUpdateObject(callEndTime, newCost, true);
                                                    if (newCost + currentUserObject.totalWalletCredits >= globalSettingsObject.walletCaps.maxEarn) {
                                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "User limit reached. Please recharge before you start earning again. ", data: {}})});
                                                        res.send(returnObjectToAndroid);
                                                    } else if (currentValue == 0) {
                                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue}})});
                                                        res.send(returnObjectToAndroid);
                                                    } else {
                                                        //update user
                                                        userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                                                                .then(function (responseObject) {

                                                                    if (responseObject && responseObject.ok == "1") {
                                                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: newCost}})});
                                                                        res.send(returnObjectToAndroid);
                                                                    }
                                                                })
                                                                .catch(function (errorObject) {
                                                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some error on update user", data: errorObject})});
                                                                    res.send(returnObjectToAndroid);
                                                                });
                                                        creditUsageArchiveUtil.insertCreditUsageArchive(oldData)
                                                                .then(function (responseObjectArchivedUsage) {
                                                                    console.log("responseObjectArchivedUsage");
                                                                    console.log(responseObjectArchivedUsage);
                                                                })
                                                                .catch(function (errorObjectArchivedUsage) {
                                                                    console.log("errorObjectArchivedUsage");
                                                                    console.log(errorObjectArchivedUsage);
                                                                });
                                                        //*********Update into wallet
                                                        var walletInsertObject = {
                                                            userId: currentUserObject._id,
                                                            creditAmount: newCost,
                                                            percentageApplied: globalSettingsObject.timeCosts["cost" + index + "__" + key],
                                                            createdDate: returnArray[0].lastUpdated,
                                                            isOneTap: true
                                                        };
                                                        walletUtil.insertWallet(walletInsertObject)
                                                                .then(function (responseObjectWalletInsert) {
                                                                    console.log("responseObjectWalletInsert");
                                                                    console.log(responseObjectWalletInsert);
                                                                })
                                                                .catch(function (errorObjectWalletInsert) {
                                                                    console.log("errorObjectWalletInsert");
                                                                    console.log(errorObjectWalletInsert);
                                                                });
                                                    }
                                                } else if (resetHourly == true || resetDaily == true || resetWeekly == true || resetMonthly == true) {
                                                    //create object to store 'to be archived' information
                                                    oldArchivedUsages = JSON.parse(JSON.stringify(currentUserObject));
                                                    console.log("oldArchivedUsages");
                                                    console.log(oldArchivedUsages);
                                                    currentUserObject.creditUsage.hourly = (resetHourly == true ? 0 : currentUserObject.creditUsage.hourly);
                                                    currentUserObject.creditUsage.daily = (resetDaily == true ? 0 : currentUserObject.creditUsage.daily);
                                                    currentUserObject.creditUsage.weekly = (resetWeekly == true ? 0 : currentUserObject.creditUsage.weekly);
                                                    currentUserObject.creditUsage.monthly = (resetMonthly == true ? 0 : currentUserObject.creditUsage.monthly);
                                                    //find if the reimbursement amount is within the limits or not
                                                    checkIfCurrentIsValid(lastUpdatedTime, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);
                                                    //update user and send response
                                                    //if within the limits
                                                    if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                                                        console.log("IS VALID1");
                                                        if (currentFailedAttempt.oldCost <= currentValue) {
                                                            console.log("old cost is lesser");
                                                            newCost = currentFailedAttempt.oldCost;
                                                        } else {
                                                            console.log("new cost is lesser");
                                                            newCost = currentValue;
                                                        }
                                                        if (newCost + currentUserObject.totalWalletCredits >= globalSettingsObject.walletCaps.maxEarn) {
                                                            var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "User limit reached. Please recharge before you start earning again. ", data: {}})});
                                                            res.send(returnObjectToAndroid);
                                                        } else {
                                                            var returnArray = createUpdateObject(callEndTime, newCost, true);
                                                            //put current data into new variable to later put it into call log schema for archiving and report generation.
                                                            var oldData = JSON.parse(JSON.stringify(currentUserObject.creditUsage));
                                                            oldData["archivedTime"] = callEndTime.getTime();
                                                            oldData["userId"] = commonFunctions.stringToObjectId(currentUserObject._id);

                                                            //update user
                                                            userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                                                                    .then(function (responseObject) {

                                                                        if (responseObject && responseObject.ok == "1") {
                                                                            //***************************************************************
                                                                            //BUG here. change this value since this is when hour/month/week/day has changed and not when some limit is reached
                                                                            var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: newCost, resetHourly: resetHourly, resetDaily: resetDaily, resetWeekly: resetWeekly, resetMonthly: resetMonthly}})});
                                                                            res.send(returnObjectToAndroid);
                                                                        }
                                                                    })
                                                                    .catch(function (errorObject) {
                                                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some error on update user", data: errorObject})});
                                                                        res.send(returnObjectToAndroid);
                                                                    });

                                                            failedAttemptsUtil.updatefailedAttempts({_id: currentFailedAttempt._id}, {$set: {"isActive": false}})
                                                                    .then(function (responseObject) {
                                                                        console.log("responseObject in failedAttempt delete");
                                                                        console.log(responseObject);
                                                                    })
                                                                    .catch(function (errorObject) {
                                                                        console.log("errorObject in failedAttempt delete");
                                                                        console.log(errorObject);
                                                                    });
                                                            creditUsageArchiveUtil.insertCreditUsageArchive(oldData)
                                                                    .then(function (responseObjectArchivedUsage) {
                                                                        console.log("responseObjectArchivedUsage");
                                                                        console.log(responseObjectArchivedUsage);
                                                                    })
                                                                    .catch(function (errorObjectArchivedUsage) {
                                                                        console.log("errorObjectArchivedUsage");
                                                                        console.log(errorObjectArchivedUsage);
                                                                    });
                                                            //*********Update into wallet
                                                            var walletInsertObject = {
                                                                userId: currentUserObject._id,
                                                                creditAmount: newCost,
                                                                percentageApplied: globalSettingsObject.timeCosts["cost" + index + "__" + key],
                                                                createdDate: returnArray[0].lastUpdated,
                                                                isOneTap: true
                                                            };
                                                            walletUtil.insertWallet(walletInsertObject)
                                                                    .then(function (responseObjectWalletInsert) {
                                                                        console.log("responseObjectWalletInsert");
                                                                        console.log(responseObjectWalletInsert);
                                                                    })
                                                                    .catch(function (errorObjectWalletInsert) {
                                                                        console.log("errorObjectWalletInsert");
                                                                        console.log(errorObjectWalletInsert);
                                                                    });
                                                        }

                                                    } else {
                                                        forceChooseLowestAmount(req, res, currentFailedAttempt, key, lastUpdatedTime, callEndTime);
                                                    }

                                                } else {
                                                    forceChooseLowestAmount(req, res, currentFailedAttempt, key, lastUpdatedTime, callEndTime);
                                                }

                                            }
                                        } else {
                                            checkIfCurrentIsValid(-1, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);
                                            if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                                                if (currentFailedAttempt.oldCost <= currentValue) {
                                                    console.log("old cost is lesser");
                                                    newCost = currentFailedAttempt.oldCost;
                                                } else {
                                                    console.log("new cost is lesser");
                                                    newCost = currentValue;
                                                }
                                                if (newCost + currentUserObject.totalWalletCredits >= globalSettingsObject.walletCaps.maxEarn) {
                                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "User limit reached. Please recharge before you start earning again. ", data: {}})});
                                                    res.send(returnObjectToAndroid);
                                                } else {
                                                    var returnArray = createUpdateObject(callEndTime, newCost);
                                                    //put current data into new variable to later put it into call log schema for archiving and report generation.

                                                    userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                                                            .then(function (responseObject) {
                                                                if (responseObject && responseObject.ok == "1") {
                                                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: newCost}})});
                                                                    res.send(returnObjectToAndroid);
                                                                }
                                                            })
                                                            .catch(function (errorObject) {
                                                                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Error", data: errorObject})});
                                                                res.send(returnObjectToAndroid);
                                                            });

                                                    failedAttemptsUtil.updatefailedAttempts({_id: currentFailedAttempt._id}, {$set: {"isActive": false}})
                                                            .then(function (responseObject) {
                                                                console.log("responseObject in failedAttempt delete");
                                                                console.log(responseObject);
                                                            })
                                                            .catch(function (errorObject) {
                                                                console.log("errorObject in failedAttempt delete");
                                                                console.log(errorObject);
                                                            });
                                                    //*********Update into wallet
                                                    var walletInsertObject = {
                                                        userId: currentUserObject._id,
                                                        creditAmount: newCost,
                                                        percentageApplied: globalSettingsObject.timeCosts["cost" + 1 + "__" + key],
                                                        createdDate: returnArray[0].lastUpdated,
                                                        isOneTap: true
                                                    };
                                                    walletUtil.insertWallet(walletInsertObject)
                                                            .then(function (responseObjectWalletInsert) {
                                                                console.log("responseObjectWalletInsert");
                                                                console.log(responseObjectWalletInsert);
                                                            })
                                                            .catch(function (errorObjectWalletInsert) {
                                                                console.log("errorObjectWalletInsert");
                                                                console.log(errorObjectWalletInsert);
                                                            });
                                                }

                                            } else {
                                                //select minimum value out of options
                                                currentValue = Math.round((globalSettingsObject.timeCosts["cost" + 1 + "__" + key] * currentFailedAttempt.callCost) / 100);
                                                checkIfCurrentIsValid(-1, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);

                                                if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                                                    if (currentFailedAttempt.oldCost <= currentValue) {
                                                        console.log("old cost is lesser");
                                                        newCost = currentFailedAttempt.oldCost;
                                                    } else {
                                                        console.log("new cost is lesser");
                                                        newCost = currentValue;
                                                    }
                                                    if (newCost + currentUserObject.totalWalletCredits >= globalSettingsObject.walletCaps.maxEarn) {
                                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "User limit reached. Please recharge before you start earning again. ", data: {}})});
                                                        res.send(returnObjectToAndroid);
                                                    } else {
                                                        var returnArray = createUpdateObject(callEndTime, newCost);
                                                        userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                                                                .then(function (responseObject) {
                                                                    if (responseObject && responseObject.ok == "1") {
                                                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "User limit reached. Please recharge before you start earning again. ", data: {}})});
                                                                        res.send({error: 0, msg: "Reimbursement amount", data: {amount: newCost}});
                                                                    }
                                                                })
                                                                .catch(function (errorObject) {
                                                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Error on update user", data: errorObject})});
                                                                    res.send(returnObjectToAndroid);
                                                                });

                                                        failedAttemptsUtil.updatefailedAttempts({_id: currentFailedAttempt._id}, {$set: {"isActive": false}})
                                                                .then(function (responseObject) {
                                                                    console.log("responseObject in failedAttempt delete");
                                                                    console.log(responseObject);
                                                                })
                                                                .catch(function (errorObject) {
                                                                    console.log("errorObject in failedAttempt delete");
                                                                    console.log(errorObject);
                                                                });
                                                        //*********Update into wallet
                                                        var walletInsertObject = {
                                                            userId: currentUserObject._id,
                                                            creditAmount: newCost,
                                                            percentageApplied: globalSettingsObject.timeCosts["cost" + 1 + "__" + key],
                                                            createdDate: returnArray[0].lastUpdated,
                                                            isOneTap: true
                                                        };
                                                        walletUtil.insertWallet(walletInsertObject)
                                                                .then(function (responseObjectWalletInsert) {
                                                                    console.log("responseObjectWalletInsert");
                                                                    console.log(responseObjectWalletInsert);
                                                                })
                                                                .catch(function (errorObjectWalletInsert) {
                                                                    console.log("errorObjectWalletInsert");
                                                                    console.log(errorObjectWalletInsert);
                                                                });
                                                    }
                                                } else {
                                                    //send no money. optimise later
                                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Reimbursement amount", data: {amount: 0.0}})});
                                                    res.send(returnObjectToAndroid);
                                                }
                                            }
                                        }
                                    }
                                }
                            });

                } else {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "No data available for one tap.", data: {}})});
                    res.send(returnObjectToAndroid);
                }
            })
            .catch(function (errorObjectOneTap) {
                console.log("errorObjectOneTap");
                console.log(errorObjectOneTap);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Something went wrong", data: errorObjectOneTap})});
                res.send(returnObjectToAndroid);
            });


//**********************************************************************************
//Function for lowest amount
//**********************************************************************************

    function forceChooseLowestAmount(req, res, currentFailedAttempt, key, lastUpdatedTime, callEndTime) {
        //force choose lowest amount
        currentValue = Math.round((globalSettingsObject.timeCosts["cost" + 1 + "__" + key] * currentFailedAttempt.callCost) / 100);
        //check if lowest amount is within the limits
        if (currentValue == 0) {
            var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: currentValue}})});
            res.send(returnObjectToAndroid);
        } else {
            checkIfCurrentIsValid(lastUpdatedTime, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);
            console.log("BEFORE IS VALID2");

            if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                if (currentFailedAttempt.oldCost <= currentValue) {
                    console.log("old cost is lesser");
                    newCost = currentFailedAttempt.oldCost;
                } else {
                    console.log("new cost is lesser");
                    newCost = currentValue;
                }
                if (newCost + currentUserObject.totalWalletCredits >= globalSettingsObject.walletCaps.maxEarn) {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "User limit reached. Please recharge before you start earning again. ", data: {}})});
                    res.send(returnObjectToAndroid);
                } else {
                    var returnArray = createUpdateObject(callEndTime, newCost, true);
                    //put current data into new variable to later put it into call log schema for archiving and report generation.
                    var oldData = JSON.parse(JSON.stringify(currentUserObject.creditUsage));
                    oldData["archivedTime"] = callEndTime.getTime();
                    oldData["userId"] = commonFunctions.stringToObjectId(currentUserObject._id);

                    userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                            .then(function (responseObject) {
                                if (responseObject && responseObject.ok == "1") {
                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: newCost}})});
                                    res.send(returnObjectToAndroid);
                                }
                            })
                            .catch(function (errorObject) {
                                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some error in user update", data: errorObject})});
                                res.send(returnObjectToAndroid);
                            });
                    creditUsageArchiveUtil.insertCreditUsageArchive(oldData)
                            .then(function (responseObjectArchivedUsage) {
                                console.log("responseObjectArchivedUsage");
                                console.log(responseObjectArchivedUsage);
                            })
                            .catch(function (errorObjectArchivedUsage) {
                                console.log("errorObjectArchivedUsage");
                                console.log(errorObjectArchivedUsage);
                            });

                    failedAttemptsUtil.updatefailedAttempts({_id: currentFailedAttempt._id}, {$set: {"isActive": false}})
                            .then(function (responseObject) {
                                console.log("responseObject in failedAttempt delete");
                                console.log(responseObject);
                            })
                            .catch(function (errorObject) {
                                console.log("errorObject in failedAttempt delete");
                                console.log(errorObject);
                            });
                    //*********Update into wallet
                    var walletInsertObject = {
                        userId: currentUserObject._id,
                        creditAmount: newCost,
                        percentageApplied: globalSettingsObject.timeCosts["cost" + 1 + "__" + key],
                        createdDate: returnArray[0].lastUpdated,
                        isOneTap: true
                    };
                    walletUtil.insertWallet(walletInsertObject)
                            .then(function (responseObjectWalletInsert) {
                                console.log("responseObjectWalletInsert");
                                console.log(responseObjectWalletInsert);
                            })
                            .catch(function (errorObjectWalletInsert) {
                                console.log("errorObjectWalletInsert");
                                console.log(errorObjectWalletInsert);
                            });
                }

            } else if (resetHourly == true || resetDaily == true || resetWeekly == true || resetMonthly == true) {
                oldArchivedUsages = JSON.parse(JSON.stringify(currentUserObject));
                console.log("oldArchivedUsages");
                console.log(oldArchivedUsages);

                currentUserObject.creditUsage.hourly = (resetHourly == true ? 0 : currentUserObject.creditUsage.hourly);
                currentUserObject.creditUsage.daily = (resetDaily == true ? 0 : currentUserObject.creditUsage.daily);
                currentUserObject.creditUsage.weekly = (resetWeekly == true ? 0 : currentUserObject.creditUsage.weekly);
                currentUserObject.creditUsage.monthly = (resetMonthly == true ? 0 : currentUserObject.creditUsage.monthly);
                checkIfCurrentIsValid(lastUpdatedTime, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly);
                //update user and send response
                if (isValidHourly == true && isValidDaily == true && isValidWeekly == true && isValidMonthly == true) {
                    if (currentFailedAttempt.oldCost <= currentValue) {
                        console.log("old cost is lesser");
                        newCost = currentFailedAttempt.oldCost;
                    } else {
                        console.log("new cost is lesser");
                        newCost = currentValue;
                    }
                    if (newCost + currentUserObject.totalWalletCredits >= globalSettingsObject.walletCaps.maxEarn) {
                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "User limit reached. Please recharge before you start earning again. ", data: {}})});
                        res.send(returnObjectToAndroid);
                    } else {
                        var returnArray = createUpdateObject(callEndTime, newCost, true);
                        //put current data into new variable to later put it into call log schema for archiving and report generation.
                        var oldData = JSON.parse(JSON.stringify(currentUserObject.creditUsage));
                        oldData["archivedTime"] = callEndTime.getTime();
                        oldData["userId"] = commonFunctions.stringToObjectId(currentUserObject._id);

                        userUtil.updateUser({_id: currentUserObject._id}, {$set: {creditUsage: returnArray[0], totalWalletCredits: returnArray[1]}})
                                .then(function (responseObject) {
                                    if (responseObject && responseObject.ok == "1") {
                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Reimbursement amount", data: {amount: newCost, resetHourly: resetHourly, resetDaily: resetDaily, resetWeekly: resetWeekly, resetMonthly: resetMonthly}})});
                                        res.send(returnObjectToAndroid);
                                    } else {
                                        console.log("Error in user update.. ResponseObject =>");
                                        console.log(responseObject);
                                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Error", data: responseObject})});
                                        res.send(returnObjectToAndroid);
                                    }
                                })
                                .catch(function (errorObject) {
                                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some error in user update", data: errorObject})});
                                    res.send(returnObjectToAndroid);
                                });
                        failedAttemptsUtil.updatefailedAttempts({_id: currentFailedAttempt._id}, {$set: {"isActive": false}})
                                .then(function (responseObject) {
                                    console.log("responseObject in failedAttempt delete");
                                    console.log(responseObject);
                                })
                                .catch(function (errorObject) {
                                    console.log("errorObject in failedAttempt delete");
                                    console.log(errorObject);
                                });
                        creditUsageArchiveUtil.insertCreditUsageArchive(oldData)
                                .then(function (responseObjectArchivedUsage) {
                                    console.log("responseObjectArchivedUsage");
                                    console.log(responseObjectArchivedUsage);
                                })
                                .catch(function (errorObjectArchivedUsage) {
                                    console.log("errorObjectArchivedUsage");
                                    console.log(errorObjectArchivedUsage);
                                });

                        //*********Update into wallet
                        var walletInsertObject = {
                            userId: currentUserObject._id,
                            creditAmount: newCost,
                            percentageApplied: globalSettingsObject.timeCosts["cost" + 1 + "__" + key],
                            createdDate: returnArray[0].lastUpdated,
                            isOneTap: true
                        };
                        walletUtil.insertWallet(walletInsertObject)
                                .then(function (responseObjectWalletInsert) {
                                    console.log("responseObjectWalletInsert");
                                    console.log(responseObjectWalletInsert);
                                })
                                .catch(function (errorObjectWalletInsert) {
                                    console.log("errorObjectWalletInsert");
                                    console.log(errorObjectWalletInsert);
                                });
                    }
                }
                //send no money. optimise later
                else {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Reimbursement amount", data: {amount: 0.0, resetHourly: resetHourly, resetDaily: resetDaily, resetWeekly: resetWeekly, resetMonthly: resetMonthly}})});
                    res.send(returnObjectToAndroid);
                }
            }
            //send no money. optimise later
            else {
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Reimbursement amount", data: {amount: 0.0, resetHourly: resetHourly, resetDaily: resetDaily, resetWeekly: resetWeekly, resetMonthly: resetMonthly}})});
                res.send(returnObjectToAndroid);
            }
        }
    }

//**********************************************************************************
//Function to find if current is valid
//**********************************************************************************
    function checkIfCurrentIsValid(lastUpdatedTime, callEndTime, currentValue, maxHourly, maxDaily, maxWeekly, maxMonthly) {
        if (lastUpdatedTime == -1) {
//            This only executes for the first time.
            if (currentValue <= maxHourly) {
                isValidHourly = true;
            }
            if (currentValue <= maxDaily) {
                isValidDaily = true;
            }
            if (currentValue <= maxWeekly) {
                isValidWeekly = true;
            }
            if (currentValue <= maxMonthly) {
                isValidMonthly = true;
            }
        } else {
            if (lastUpdatedTime.getMonth() == callEndTime.getMonth()) {
                //check if same week
                if (lastUpdatedTime.getWeek() == callEndTime.getWeek()) {
                    //check if same day. Days start from Monday

                    if (lastUpdatedTime.getDay() == callEndTime.getDay()) {
                        //check if same hour
                        if (lastUpdatedTime.getHours() == callEndTime.getHours()) {
                            if ((currentValue + currentUserObject.creditUsage.hourly <= maxHourly)) {
                                isValidHourly = true;
                            }
                        } else {
                            //reset hourly used because hour has changed after last updated
                            resetHourly = true;
                            if ((currentValue <= maxHourly)) {
                                isValidHourly = true;
                            }
                        }
                        if ((currentValue + currentUserObject.creditUsage.daily <= maxDaily)) {
                            isValidDaily = true;
                        }
                    } else {
                        //reset daily and hourly used
                        resetHourly = true;
                        resetDaily = true;
                        if ((currentValue <= maxHourly)) {
                            isValidHourly = true;
                        }
                        if ((currentValue <= maxDaily)) {
                            isValidDaily = true;
                        }
                    }
                    if ((currentValue + currentUserObject.creditUsage.weekly <= maxWeekly)) {
                        isValidWeekly = true;
                    }
                } else {
                    //reset weekly, daily, hourly used
                    resetHourly = true;
                    resetDaily = true;
                    resetWeekly = true;
                    if ((currentValue <= maxHourly)) {
                        isValidHourly = true;
                    }
                    if ((currentValue <= maxDaily)) {
                        isValidDaily = true;
                    }
                    if ((currentValue <= maxWeekly)) {
                        isValidWeekly = true;
                    }
                }
                if ((currentValue + currentUserObject.creditUsage.monthly <= maxMonthly)) {
                    isValidMonthly = true;
                }
            } else {
                //******************Since month has changed, reset daily, hourly & monthly limit in the end
                //check if same week
                if (lastUpdatedTime.getWeek() == callEndTime.getWeek()) {
                    //check if same day. Days start from Monday
                    if (lastUpdatedTime.getDay() == callEndTime.getDay()) {
                        //check if same hour
                        if (lastUpdatedTime.getHours() == callEndTime.getHours()) {
                            if ((currentValue + currentUserObject.creditUsage.hourly <= maxHourly)) {
                                isValidHourly = true;
                            }
                        }
                        if ((currentValue + currentUserObject.creditUsage.daily <= maxDaily)) {
                            isValidDaily = true;
                        }
                    }

                    if ((currentValue + currentUserObject.creditUsage.weekly <= maxWeekly)) {
                        isValidWeekly = true;
                    }
                } else {
                    //reset weekly limits
                    resetWeekly = true;
                    if ((currentValue <= maxWeekly)) {
                        isValidWeekly = true;
                    }
                    //check if same day. Days start from Monday
                    if (lastUpdatedTime.getDay() == callEndTime.getDay()) {
                        //check if same hour
                        if (lastUpdatedTime.getHours() == callEndTime.getHours()) {
                            if ((currentValue + currentUserObject.creditUsage.hourly <= maxHourly)) {
                                isValidHourly = true;
                            }

                        }
                        if ((currentValue + currentUserObject.creditUsage.daily <= maxDaily)) {
                            isValidDaily = true;
                        }
                    } else {
                        //check if same hour
                        if (lastUpdatedTime.getHours() == callEndTime.getHours()) {
                            if ((currentValue + currentUserObject.creditUsage.hourly <= maxHourly)) {
                                isValidHourly = true;
                            }
                        }
                    }
                }
                //Since month has changed, resetting daily, hourly & monthly limit
                resetHourly = true;
                resetDaily = true;
                resetMonthly = true;
                if ((currentValue <= maxHourly)) {
                    isValidHourly = true;
                }
                if ((currentValue <= maxDaily)) {
                    isValidDaily = true;
                }
                if ((currentValue <= maxMonthly)) {
                    isValidMonthly = true;
                }
            }
        }

    }

//**********************************************************************************
//find if the current is within limits and create update object
//**********************************************************************************

    function createUpdateObject(callEndTime, currentValue, condition = false) {
        if (!condition) {
            var creditUsageUpdateObject = {
                lastUpdated: callEndTime.getTime()
            };
            //since first entry, equate directly
            creditUsageUpdateObject["hourly"] = currentValue;
            creditUsageUpdateObject["daily"] = currentValue;
            creditUsageUpdateObject["weekly"] = currentValue;
            creditUsageUpdateObject["monthly"] = currentValue;

            var wallet = currentUserObject.totalWalletCredits + currentValue;

        } else {
            //create new update object
            var creditUsageUpdateObject = {
                lastUpdated: callEndTime.getTime()
            };

            //reset if necessary
            creditUsageUpdateObject["hourly"] = (resetHourly == true ? currentValue : currentUserObject.creditUsage.hourly + currentValue);
            creditUsageUpdateObject["daily"] = (resetDaily == true ? currentValue : currentUserObject.creditUsage.daily + currentValue);
            creditUsageUpdateObject["weekly"] = (resetWeekly == true ? currentValue : currentUserObject.creditUsage.weekly + currentValue);
            creditUsageUpdateObject["monthly"] = (resetMonthly == true ? currentValue : currentUserObject.creditUsage.monthly + currentValue);
            var wallet = currentUserObject.totalWalletCredits + currentValue;

        }
        return [creditUsageUpdateObject, wallet];
    }
});

//API for recharge
router.post('/recharge', function (req, res, next) {
    console.log("req.body.amount");
    console.log(req.body.amount);
    console.log("req.body.circle");
    console.log(req.body.circle);
    console.log("req.body.service");
    console.log(req.body.service);
    var responseSent = false;
    if (!req.body.service) {
        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "No service provider name specified", data: {}})});
        res.send(returnObjectToAndroid);
    }
    if (!req.body.circle) {
        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "No location circle specified", data: {}})});
        res.send(returnObjectToAndroid);
    }
    if (!req.body.amount) {
        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "No amount specified", data: {}})});
        res.send(returnObjectToAndroid);
    }
    //check time for last recharge. If within permissible limits(MIN AND MAX), then allow. Else block.
    settingsUtil.getAllSettings()
            .then(function (responseObject) {
                console.log("responseObject");
                console.log(responseObject);
                if (responseObject.length == 0) {
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "No global settings", data: {}})});
                    res.send(returnObjectToAndroid);
                } else {
                    if (responseObject[0].rechargeLimits.minRecharge < parseInt(req.body.amount) && responseObject[0].rechargeLimits.maxRecharge > parseInt(req.body.amount)) {
                        return userUtil.getUserById(req.decoded._id);
                    } else {
                        var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Recharge amount is not in the limits", data: {}})});
                        res.send(returnObjectToAndroid);
                        responseSent = true;
                        return;
                    }
                }
            })
            .then(function (responseObject) {
                if (responseObject && responseObject.length > 0 && responseObject[0].totalWalletCredits > req.body.amount) {
                    console.log("responseObject");
                    console.log(responseObject[0].totalWalletCredits);
                    var updatedWalletAmount = responseObject[0].totalWalletCredits - parseInt(req.body.amount);
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "Recharge success", data: {}})});
                    res.send(returnObjectToAndroid);
                    userUtil.updateUser({_id: responseObject[0]._id}, {$set: {totalWalletCredits: updatedWalletAmount}})
                            .then(function (responseObjectUpdateUser) {
                                console.log("responseObjectUpdateUser");
                                console.log(responseObjectUpdateUser);
                            })
                            .catch(function (errorObjectUpdateUser) {
                                console.log("errorObjectUpdateUser");
                                console.log(errorObjectUpdateUser);
                            });

                    var rechargeInsertObject = {
                        userId: responseObject[0]._id,
                        operator: req.body.service,
                        amount: req.body.amount,
                        circle: req.body.circle
                    };
                    rechargeUtil.insertRechargeAttempts(rechargeInsertObject)
                            .then(function (responseObjectInsertRecharge) {
                                console.log("responseObjectInsertRecharge");
                                console.log(responseObjectInsertRecharge);
                                var walletInsertObject = {
                                    userId: responseObject[0]._id,
                                    creditAmount: 0 - parseInt(req.body.amount),
                                    createdDate: Date.now(),
                                    rechargeRef: responseObjectInsertRecharge._id
                                };
                                walletUtil.insertWallet(walletInsertObject)
                            })
                            .then(function (responseObjectInsertWallet) {
                                console.log("responseObjectInsertWallet");
                                console.log(responseObjectInsertWallet);
                            })
                            .catch(function (errorObjectInsertRecharge) {
                                console.log("errorObjectInsertRecharge");
                                console.log(errorObjectInsertRecharge);
                            });
                } else if (responseSent === false) {
                    console.log("Not enough funds for recharge");
                    var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Not enough funds for recharge. Please earn money on the app to recharge", data: {}})});
                    res.send(returnObjectToAndroid);
                } else {
                    res.end();
                }

            })
            .catch(function (errorObject) {
                console.log("errorObject");
                console.log(errorObject);
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 1, msg: "Some technical error in recharge API", data: errorObject})});
                res.send(returnObjectToAndroid);
            });
});

router.post('/getAllBlockedPackages/', function (req, res) {
    blockedPackagesUtil.getAllBlockedPackages()
            .then(function (responseObject) {
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "All blocked packages", data: responseObject})});
                res.send(returnObjectToAndroid);
            })
            .catch(function (errorObject) {
                var returnObjectToAndroid = commonFunctions.encryptData({data: JSON.stringify({error: 0, msg: "error in blocked packages", data: errorObject})});
                res.send(returnObjectToAndroid);
            });
});

router.post('/sendNotification/', function (req, res, next) {
    var registrationTokens = [
        "dB_zMmlq-Sc:APA91bGSc4hOuIgiee9fblhY3oiJC1KH3nrMbu6DRCsOVnJz-1XdN4el4Dkpfn5Vym6WQC7z-9KCQT2j_5JpHtvCvix3gH3SurJv-qOXpGPToF0xIBW2ZawJ3A-Lq7hm7KyIBjfXgnEy"
    ];

    var notification = {
        title: "Random Test1",
        body: "Hurrah. Enjoy max bonuses since you are a developer of the project.1"

    };
    var data = {
        type: "2"
    };
    commonFunctions.sendNotification(registrationTokens, notification, data);
});


router.post('/getData/', function(req, res, next){
    console.log("req.body");
    console.log(req.body);
    console.log(typeof req.body);
    var a= req.body;
    console.log("a");
    console.log(a);
//    var decryptedData = commonFunctions.decryptData({data:a});
    
    console.log("a");
    console.log(a);
    res.send(a);
});

router.post('/setData/', function(req, res, next){
    console.log("req.body");
    console.log(req.body);
//    console.log(typeof req.body.data);
//    var a= req.body.data;
//    console.log(a);
    console.log(Object.keys(req.body));
//    for (var i = 0; i < Object.keys(req.body).length; i++) {
//        
//    }
    var encryptedData = commonFunctions.encryptData({data:JSON.stringify(req.body)});
    
    console.log("encryptedData");
    console.log(encryptedData);
    res.send(encryptedData);
});

module.exports = router;