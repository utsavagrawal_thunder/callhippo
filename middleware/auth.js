//To check authentication
var commonFunctions = require('../utils/commonFunctions');
var common = require('../config');
var config = common.config();
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var userUtil = require('../utils/userUtil');

var auth = {
    checkAuthentication: function (req, res, next) {
        // skip uploads folder and apis
//        if((req.path).startsWith('/user/insert') || (req.originalUrl).startsWith('/user/insert') ) {
//            return next();
//        }

//        var isNotAllowed = req.originalUrl !== '/login' && typeof req.session.userInfo === "undefined";
        var exceptionBool = false;
        var sourceUrl = "";
        if (req.originalUrl.indexOf("?") != -1) {
            sourceUrl = req.originalUrl.substring(0, req.originalUrl.indexOf("?"));
        } else {
            sourceUrl = req.originalUrl;
        }
        //decrypt data
        var dataObject = {};
        if (req.body["data"]) {
            console.log("req.body.data");
            console.log(req.body.data);
//            console.log(commonFunctions.decryptData({data: JSON.stringify(req.body["data"])}));
            var decryptedData = commonFunctions.decryptData({data: req.body["data"]});
            console.log("decryptedData");
            console.log(decryptedData);
            console.log(typeof decryptedData);
            try {
                dataObject = JSON.parse(decryptedData.trim());
            } catch (e) {
                var returnToAndroidObject = commonFunctions.encryptData({data: JSON.stringify({error: 2, msg: "error in token", data: {}})});
                return res.send(returnToAndroidObject);
            }
            var keysArray = Object.keys(dataObject);
            for (var i = 0; i < keysArray.length; i++) {
                req.body[keysArray[i]] = dataObject[keysArray[i]];
            }
        } else if (req.query.data) {
            console.log("req.query.data");
            console.log(req.query.data);
//            console.log(commonFunctions.decryptData({data: JSON.stringify(req.body["data"])}));
            var decryptedData = commonFunctions.decryptData({data: req.query["data"]});
            console.log("decryptedData");
            console.log(decryptedData);
            console.log(typeof decryptedData);
            try {
                dataObject = JSON.parse(decryptedData.trim());
            } catch (e) {
                var returnToAndroidObject = commonFunctions.encryptData({data: JSON.stringify({error: 2, msg: "error in token", data: {}})});
                return res.send(returnToAndroidObject);
            }
            var keysArray = Object.keys(dataObject);
            for (var i = 0; i < keysArray.length; i++) {
                req.query[keysArray[i]] = dataObject[keysArray[i]];
            }
        }
//        else {
//            return res.send({error: 1, msg: "No valid data", data: {}});
//        }
        var isAllowedArray = ['/user/insert', '/user/insert/', '/user/verifyOTP', '/user/verifyOTP/', '/user/add', '/user/add', '/ussd/regexp/get', '/ussd/regexp/get/', '/user/updateWallet', '/ussd/codes/get', '/ussd/codes/get/', '/ussd/regexp/get/sync', '/ussd/regexp/get/sync/', '/user/getAllBlockedPackages', '/user/getAllBlockedPackages/', '/user/sendNotification/', '/user/getData/', '/user/setData/'];
        if (isAllowedArray.indexOf(sourceUrl) != -1) {
            next();
        } else {
            // check header or url parameters or post parameters for token
            var encryptedToken = req.body.token || req.query.token || req.headers['x-access-token'];
            var token;
            try {
                token = commonFunctions.decryptData({data: encryptedToken});
            } catch (e) {
                console.log("Error in decrypt jwt token. " + e);
                exceptionBool = true;
                return res.send({error: 2, msg: "error in token", data: {}});
            }
            // decode token
            if (token) {
                // verifies secret and checks exp
                jwt.verify(token, config['secret'], function (err, decoded) {
                    if (err) {
                        return res.json({error: 2, message: 'Failed to authenticate token. ', data: {}});
                    } else {
                        // if everything is good, save to request for use in other routes
                        req.decoded = decoded;
                        if (commonFunctions.isMongoId(req.decoded._id)) {
                            //check if user is valid
                            var findCriteria = {
                                _id: commonFunctions.stringToObjectId(req.decoded._id),
                                primaryRegistrationDeviceToken: req.decoded.primaryRegistrationDeviceToken,
                                secondaryRegistrationDeviceToken: req.decoded.secondaryRegistrationDeviceToken
                            };
                            userUtil.getUserByFindCriteria(findCriteria)
                                    .then(function (responseObject) {
                                        console.log("responseObject");
                                        console.log(responseObject);
                                        if (responseObject.length == 1) {
                                            next();
                                        } else if (responseObject.length == 0) {
                                            return res.json({error: 2, message: 'Failed to authenticate token. ', data: {}});
                                        } else if (responseObject.length > 1) {
                                            console.log("********************");
                                            console.log("Many dupllicate users found");
                                            console.log("********************");
                                            console.log(responseObject);
                                            return res.json({error: 2, message: 'Failed to authenticate token. ', data: {}});
                                        }
                                    })
                                    .catch(function (errorObject) {
                                        console.log("errorObject");
                                        console.log(errorObject);
                                        res.json({error: 2, msg: "Failed to authenticate token. ", data: errorObject});
                                    });
                        } else {
                            return res.json({error: 2, message: 'Failed to authenticate token. ', data: {}});
                        }
                    }
                });

            } else if (exceptionBool == false) {

                // if there is no token
                // return an error
                return res.status(403).send({
                    success: false,
                    message: 'No token provided.'
                });

            }
        }
    }
};
module.exports = auth;