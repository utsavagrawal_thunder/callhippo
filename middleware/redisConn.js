// @RedisConn - connect to redis & set redis connection to request

// Required modules
var common = require('../config');
var config = common.config();
var redis = require('redis');

// redis connections
var redisStoreConn = null;

var redisConn = function (req, res, next) {

  // Check if connection
  if(!redisStoreConn) {
    // Connect to redis
    redisStoreConn = redis.createClient(config.redis_port, config.redis_host, {no_ready_check:true});
    
    // on connection error
    redisStoreConn.on("error", function (err) {
      console.log("Redis Master Connection Error - ", err);
      throw err;
    });    
  }

  // set redis client connection
  req.redisStoreConn = redisStoreConn;
  next();
}

module.exports = redisConn;