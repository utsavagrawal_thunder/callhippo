var s3_bucket = "http://clinito.s3.ap-south-1.amazonaws.com/";

// check if string contains all special characters
function onlySpecialChars(str) {
	return (/^[a-zA-Z0-9]/).test(str);
}

// check if object is empty
function isObjectEmpty(obj) {
    return (Object.keys(obj).length === 0 && obj.constructor === Object);
}

// check if number only
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if((charCode >= 35 && charCode <= 40)) {
    	return true;
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

// show toaster message
function showToastMsg(type, msg) {
    // success
    if(type == 1) {
        $.toaster({ message: msg, priority: 'success', title: 'Success' });
    }
    // error
    else {
        $.toaster({ message: msg, priority: 'danger', title: 'Error' });
    }
}

// get date in YMD
function getDateYMD(dateVal) {
    var date  = new Date(dateVal);
    var year  = date.getFullYear();
    var month = addZeroToDate(date.getMonth() + 1);
    var day   = addZeroToDate(date.getDate());

    var date_str = year+'-'+month+'-'+day;
    return date_str;
}

// @Add 0 to dates
function addZeroToDate(i) {
    if(i < 10){
        i = '0'+i;
    }
    return i;
}
// Check if string is a valid URL
function isURL(str) {
  var pattern = new RegExp(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi);
  return pattern.test(str);
}
    
// check if string is mongo id
function isMongoId(str) {
    return isHexadecimal(str) && str.length === 24;
}

function isHexadecimal(str) {
    var hexadecimal = /^[0-9A-F]+$/i;
    return hexadecimal.test(str);
}

// Get resized image
function getResizedProductImage(imgName, width, height) {
    var ext = imgName.substr(imgName.lastIndexOf('.')+1);
    var fileNameWithoutExt = imgName.substr(0, imgName.length - ext.length - 1);
    var resizedImgName = fileNameWithoutExt+'-'+width+'x'+height+'.'+ext;
    return resizedImgName;
}

// Get india states
function getIndiaStates() {
    var states = [
        'Andhra Pradesh',
        'Assam',
        'Bihar',
        'Chhattisgarh',
        'Goa',
        'Gujarat',
        'Haryana',
        'Himachal Pradesh',
        'Jammu & Kashmir',
        'Jharkhand',
        'Karnataka',
        'Kerala',
        'Madhya Pradesh',
        'Maharashtra',
        'Manipur',
        'Meghalaya',
        'Mizoram',
        'Nagaland',
        'Odisha',
        'Punjab',
        'Rajasthan',
        'Sikkim',
        'Tamil Nadu',
        'Telangana',
        'Tripura',
        'Uttar Pradesh',
        'Uttarakhand',
        'West Bengal'
    ];

    return states;
}
