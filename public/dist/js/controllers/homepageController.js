//Promotions Create/Edit controller
app.controller('PromotionsAddController', function ($scope, $http, $timeout, $location, $q) {
    loadInitialDataAndFunctions($scope, $http, $timeout);
    $scope.supplierInfo = {}; 
    $scope.dividerNumber = 0;
    $scope.hasDataForCurrentDivider = false;
    $scope.localData = {};
    $scope.getTimes = function (n) {
        return new Array(n);
    };
    $scope.initialiseTab = function (dividerNumber) {
        $scope.dividerNumber = dividerNumber;
//        chosenSelectInit($scope, $http, $timeout, dividerNumber);
        loadInitialDataAndFunctions($scope, $http, $timeout);
    };
//    #############################################
//    ##############   FUNCTIONS    ###############
//    #############################################

    function chosenSelectInit($scope, $http, $timeout, dividerNumber = 0) {
        if (dividerNumber == 0 || dividerNumber == 1) {
            $("#tab_1 .chosen-select.category").chosen({width: "100%"});
        } else {
//            $("#tab_"+dividerNumber+" .chosen-select.categoryDivider" + dividerNumber+" ")
            $("#tab_"+dividerNumber+" .chosen-select.categoryDivider" + dividerNumber).chosen({width: "100%"}).change(function (event, params) {
                getRelationsByCatId(params.selected, dividerNumber);

                //loadInitialDataAndFunctions($scope, $http, $timeout);
            });
        }
        return;
    }

    function imageUploaderInit(configObject, imageDivId) {
        $(imageDivId).fileinput(configObject)
                .on('fileuploaded', function(event, data, previewId, index) {  
                    if(!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if(!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if(!configObject.initialPreview){
                        configObject.initialPreview = [];
                    }
                    $scope.serverImages = data.response[0];
                    configObject.initialPreviewConfig.push({
                        caption: data.response[0].originalname,
                        size: data.response[0].size,
                        width: "120px",
                        key: data.response[i].key
                    });
                    configObject.initialPreview.push(data.response[0].location);
                    $scope.sortedImages.push({
                        caption: data.response[0].key.split('/')[2]
                    });
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filebatchuploadsuccess', function (event, data) {
                    //data.response is response from server
                    console.log("data.response");
                    console.log(data.response);
                    $scope.serverImages = data.response;
                    if(!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if(!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if(!configObject.initialPreview){
                        configObject.initialPreview = [];
                    }
                    for (var i = 0; i < data.response.length; i++) {
                        configObject.initialPreviewConfig.push({
                            caption: data.response[i].originalname,
                            size: data.response[i].size,
                            width: "120px",
                            key: data.response[i].key
                        });
                        configObject.initialPreview.push(data.response[i].location);
                        $scope.sortedImages.push({
                            caption: data.response[i].key.split('/')[2]
                        });
                    }
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filesorted', function (event, params) {
                    var oldImages = [];
                    for (var j = 0; j < $scope.sortedImages.length; j++) {
                        oldImages.push($scope.sortedImages[j]);
                    }
                    $scope.sortedImages = [];
                    for (var i = 0; i < params.stack.length; i++) {
                        $.each(oldImages, function (index) {
                            //use regex to see if uploaded image(without timestamp) matches the sorted current image
                            var regex = new RegExp("^"+params.stack[i].caption.split('.')[0]+"-[0-9]{12,20}\.[a-z]{2,4}$")
                            if (params.stack[i].caption == oldImages[index].caption || regex.test(oldImages[index].caption)) {
                                $scope.sortedImages.push({
                                    caption: oldImages[index].caption,
                                    url: oldImages[index].url
                                });
                            }
                        });
                    }
                    //update url set order  
                    $timeout(function(){
                        $scope.$apply();
                    });
                })
                .on('filedeleted', function(event, key){
                    $scope.sortedImages.splice(key-1, 1);
                    for (var i = 0; i < configObject.initialPreviewConfig.length; i++) {
                        if(configObject.initialPreviewConfig[i].key == key){
                            configObject.initialPreviewConfig.splice(i,1);
                            configObject.initialPreview.splice(i,1);
                        }
                    }
                    console.log($scope.sortedImages);
                    $scope.$apply(); 
                });
    }

    function loadInitialDataAndFunctions($scope, $http, $timeout) {
        //onload get data
        $scope.serverImages = [];
        $scope.variantData = {};
        $scope.selectedProducts = [];
            
        //get all main categories
        $http.get('/category/getAllMainCategories')
                .then(function(response){
                    $scope.mainCategories = response.data.data;
                    if(response.data.data.length == 0) {
                        $scope.promotionsError = "No active  main categories found";
                        $(".error-modal-lg").modal();
                        return;
                    }
                    $timeout(function () {
                        $scope.productTypes = [];
                        $scope.catalogueIds = [];
                        $scope.$apply();
                        //create multiselect div
                        var configObject = {
                            initialPreviewAsData: true,
                            uploadUrl: "/promotions/uploadImage/",
                            deleteUrl: "/promotions/deleteImage/",
                            uploadAsync: false,
                            previewSettings: {image: {width: "120px", height: "40px"}},
                            overwriteInitial: false,
                            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],
                            maxFileSize: 3000,
                            fileActionSettings: {
                                showUpload: false
                            }
                        };
                        
                        //get all promotions
                        $http.get('/promotions/get')
                                .then(function (response) {
                                    if (response.data.data.length > 0) {
                                        configObject.initialPreviewConfig = [];
                                        configObject.initialPreview = [];
                                        $scope.sortedImages = [];
                                        $scope.promotionsId = response.data.data[0]._id;
                                        switch ($scope.dividerNumber) {
                                            case 0:
                                            case 1:
                                                {
                                                    for (var i = 0; i < response.data.data[0].bannerImages.length; i++) {
                                                        configObject.initialPreviewConfig.push({
                                                            caption: response.data.data[0].bannerImages[i].name,
                                                            width: "120px",
                                                            key: "promotions/home/"+response.data.data[0].bannerImages[i].name
                                                        });
                                                        var url = s3_bucket + "promotions/home/" + response.data.data[0].bannerImages[i].name;
                                                        configObject.initialPreview.push(url);
                                                        $scope.sortedImages.push({
                                                            caption: response.data.data[0].bannerImages[i].name,
                                                            url: response.data.data[0].bannerImages[i].url
                                                        });
                                                    }
                                                    //show selected categories
                                                    if (response.data.data[0].widgets && response.data.data[0].widgets[0].name == "Top Categories") {
                                                        $.each(response.data.data[0].widgets[0].categories, function (index) {
                                                            var currentCategory = response.data.data[0].widgets[0].categories[index];
                                                            $('#tab_'+$scope.dividerNumber+' .chosen-select option[value="' + currentCategory._id + '"]').attr('selected', 'selected');
                                                            $('select option[value="' + currentCategory._id + '"]').selected('selected');
                                                            $('#tab_'+$scope.dividerNumber+'.chosen-select').trigger("chosen:updated");
                                                        });
                                                    }
                                                    $timeout(function(){
                                                        chosenSelectInit($scope, $http, $timeout, $scope.dividerNumber);
                                                    });
                                                }
                                                break;
                                            case 2:
                                                {
                                                    var currentCategory;
                                                    var currentData = {};
                                                    for (var i = 0; i < response.data.data[0].dividers.length; i++) {
                                                        if (response.data.data[0].dividers[i].dividerNumber == 2) {
                                                            currentData = response.data.data[0].dividers[i];
                                                            currentCategory = response.data.data[0].dividers[i].category;
                                                            for (var j = 0; j < response.data.data[0].dividers[i].bannerImages.length; j++) {
                                                                configObject.initialPreviewConfig.push({
                                                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                                                    width: "120px",
                                                                    key: "promotions/home/"+response.data.data[0].bannerImages[i].name
                                                                });
                                                                var imageUrl = s3_bucket + "promotions/home/" + response.data.data[0].dividers[i].bannerImages[j].name;
                                                                configObject.initialPreview.push(imageUrl);
                                                                $scope.sortedImages.push({
                                                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                                                    url: response.data.data[0].dividers[i].bannerImages[j].url
                                                                });
                                                            }
                                                        }
                                                    }
                                                    
                                                    $timeout(function(){
                                                        if(currentCategory) {
                                                            getRelationsByCatId(currentCategory._id, 2, currentData);
                                                        }
                                                    });
                                                    
                                                }
                                                break;
                                            case 3:
                                                {
                                                    var currentCategory;
                                                    var currentData = {};
                                                    for (var i = 0; i < response.data.data[0].dividers.length; i++) {
                                                        if (response.data.data[0].dividers[i].dividerNumber == 3) {
                                                            currentData = response.data.data[0].dividers[i];
                                                            currentCategory = response.data.data[0].dividers[i].category;
                                                            for (var j = 0; j < response.data.data[0].dividers[i].bannerImages.length; j++) {
                                                                configObject.initialPreviewConfig.push({
                                                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                                                    width: "120px",
                                                                    key: "promotions/home/"+response.data.data[0].bannerImages[i].name
                                                                });
                                                                var imageUrl = s3_bucket + "promotions/home/" + response.data.data[0].dividers[i].bannerImages[j].name;
                                                                configObject.initialPreview.push(imageUrl);
                                                                $scope.sortedImages.push({
                                                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                                                    url: response.data.data[0].dividers[i].bannerImages[j].url
                                                                });
                                                            }
                                                        }
                                                    }
                                                    
                                                    $timeout(function(){
                                                        if(currentCategory) {
                                                            getRelationsByCatId(currentCategory._id, 3, currentData);
                                                        }
                                                    });
                                                }
                                                break;
                                            case 4:
                                                {
                                                    
                                                    var currentCategory;
                                                    var currentData = {};
                                                    for (var i = 0; i < response.data.data[0].dividers.length; i++) {
                                                        if (response.data.data[0].dividers[i].dividerNumber == 4) {
                                                            currentData = response.data.data[0].dividers[i];
                                                            currentCategory = response.data.data[0].dividers[i].category;
                                                            for (var j = 0; j < response.data.data[0].dividers[i].bannerImages.length; j++) {
                                                                configObject.initialPreviewConfig.push({
                                                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                                                    width: "120px",
                                                                    key: "promotions/home/"+response.data.data[0].bannerImages[i].name
                                                                });
                                                                var imageUrl = s3_bucket + "promotions/home/" + response.data.data[0].dividers[i].bannerImages[j].name;
                                                                configObject.initialPreview.push(imageUrl);
                                                                $scope.sortedImages.push({
                                                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                                                    url: response.data.data[0].dividers[i].bannerImages[j].url
                                                                });
                                                            }
                                                        }
                                                    }
                                                    
                                                    $timeout(function(){
                                                        if(currentCategory) {
                                                            getRelationsByCatId(currentCategory._id, 2, currentData);
                                                        }
                                                    });
                                                }
                                                break;
                                            case 5:
                                                {
                                                    if (response.data.data[0].featuredProducts.length > 0) {
                                                        $scope.featuredProducts = response.data.data[0].featuredProducts;
                                                    } else {
                                                        $scope.featuredProducts = [];
                                                    }
                                                    $("#tab_5 .chosen-select.mainCat").chosen({width: "100%"}).change(function (event, params) {
                                                        $scope.selectedProducts = [{}];
                                                        $scope.selectedProducts[0].category_id = params.selected;
                                                        $scope.localData[5] = {
                                                            selected_category_id : params.selected
                                                        };
                                                        console.log("$scope.selectedProducts");
                                                        console.log($scope.selectedProducts);
                                                        $http.get('/relations/getById?catId=' + params.selected)
                                                                .then(function (response) {
                                                                    if (response.data.length > 0) {
                                                                        //remove error if data found
                                                                        $scope.error = "";
                                                                        $scope.subCategories = [];
                                                                        $scope.relations = response.data[0];
                                                                        //check if any direct subcategories exist
                                                                        for (var i = 0; i < response.data[0].children.length; i++) {
                                        //                                    var tempObject = {};
                                        //                                    load only sub categories under this category 
                                                                            if (response.data[0].children[i].isProductType == false) {
                                                                                var tempObject = {
                                                                                    _id: response.data[0].children[i]._id,
                                                                                    name: response.data[0].children[i].name,
                                                                                    slug: response.data[0].children[i].slug
                                                                                };
                                                                                console.log("tempObject");
                                                                                console.log(tempObject);
                                                                                $scope.subCategories.push(tempObject);
                                                                                //check if subcategories exist on level 2
                                                                                for (var j = 0; j < response.data[0].children[i].children.length; j++) {
                                                                                    if (response.data[0].children[i].children[j].isProductType == false) {
                                                                                        var tempObject = {
                                                                                            _id: response.data[0].children[i].children[j]._id,
                                                                                            name: response.data[0].children[i].children[j].name,
                                                                                            slug: response.data[0].children[i].children[j].slug
                                                                                        };
                                                                                        $scope.subCategories.push(tempObject);
                                                                                    }
                                                                                }
                                                                                //subcategories cannot exist on level 3
                                                                            }
                                                                        }
                                                                        $timeout(function () {
                                                                            console.log("$scope.subCategories");
                                                                            console.log($scope.subCategories);
                                                                            $scope.$apply();
                                                                            $("#tab_"+$scope.dividerNumber+" .chosen-select.subcat").trigger("chosen:updated");
                                                                            $("#tab_"+$scope.dividerNumber+" .chosen-select.subcat").chosen({width: "100%"}).change(function (event, params) {
                                                                                $scope.selectedProducts[0].sub_category_id = params.selected;
                                                                                $scope.loadProductTypes(params.selected, 0);
                                                                            });

                                                                        });
                                                                    }
                                                                });
                                                    });
                                                    //edit added new launches product
//                                                    $scope.editFeaturedProducts = function (productInfo) {
//                                                        $('chosen-select.mainCat option[value="'+productInfo.category_id+'"]').attr('selected', 'selected').trigger("chosen:updated")
//                                                        console.log(productInfo);
//                                                        $timeout(function(){
//                                                            $scope.$apply();
//                                                        });
//                                                    };
                                                    //delete existing new launches product
                                                    $scope.deleteFeaturedProducts= function (productInfo) {
                                                        for (var i = 0; i < $scope.featuredProducts.length; i++) {
                                                            if(productInfo.catalogue_id == $scope.featuredProducts[i].catalogue_id && productInfo.category_id == $scope.featuredProducts[i].category_id && productInfo.product_type_id == $scope.featuredProducts[i].product_type_id && productInfo.sub_category_id == $scope.featuredProducts[i].sub_category_id && productInfo.supplier_id == $scope.featuredProducts[i].supplier_id) {
                                                                $scope.featuredProducts.splice(i, 1);
                                                            }
                                                        }
                                                        $timeout(function(){
                                                            $scope.$apply();
                                                        });
                                                    };
                                                }
                                                break;
                                            case 6:
                                                {
                                                    if (response.data.data[0].newLaunches.length > 0) {
                                                        $scope.newLaunches = response.data.data[0].newLaunches;
                                                    } else {
                                                        $scope.newLaunches = [];
                                                    }
                                                    $(".chosen-select.mainCat").chosen({width: "100%"}).change(function (event, params) {
                                                        $scope.selectedProducts = [{}];
                                                        $scope.selectedProducts[0].category_id = params.selected;
                                                        $scope.localData[6] = {
                                                            selected_category_id : params.selected
                                                        };
                                                        console.log("$scope.selectedProducts");
                                                        console.log($scope.selectedProducts);
                                                        $http.get('/relations/getById?catId=' + params.selected)
                                                                .then(function (response) {
                                                                    if (response.data.length > 0) {
                                                                        //remove error if data found
                                                                        $scope.error = "";
                                                                        $scope.subCategories = [];
                                                                        $scope.relations = response.data[0];
                                                                        //check if any direct subcategories exist
                                                                        for (var i = 0; i < response.data[0].children.length; i++) {
                                                                            //                                    var tempObject = {};
                                                                            //                                    load only sub categories under this category 
                                                                            if (response.data[0].children[i].isProductType == false) {
                                                                                var tempObject = {
                                                                                    _id: response.data[0].children[i]._id,
                                                                                    name: response.data[0].children[i].name,
                                                                                    slug: response.data[0].children[i].slug
                                                                                };
                                                                                $scope.subCategories.push(tempObject);
                                                                                //check if subcategories exist on level 2
                                                                                for (var j = 0; j < response.data[0].children[i].children.length; j++) {
                                                                                    if (response.data[0].children[i].children[j].isProductType == false) {
                                                                                        var tempObject = {
                                                                                            _id: response.data[0].children[i].children[j]._id,
                                                                                            name: response.data[0].children[i].children[j].name,
                                                                                            slug: response.data[0].children[i].children[j].slug
                                                                                        };
                                                                                        $scope.subCategories.push(tempObject);
                                                                                    }
                                                                                }
                                                                                //subcategories cannot exist on level 3
                                                                            }
                                                                        }
                                                                        $timeout(function () {
                                                                            $scope.$apply();
                                                                            $("#tab_"+$scope.dividerNumber+" .chosen-select.subcat").trigger("chosen:updated");
                                                                            $("#tab_"+$scope.dividerNumber+" .chosen-select.subcat").chosen({width: "100%"}).change(function (event, params) {
                                                                                $scope.selectedProducts[0].sub_category_id = params.selected;
                                                                                $scope.loadProductTypes(params.selected, 0);
                                                                            });

                                                                        });
                                                                    }
                                                                });
                                                    });
                                                    //edit added new launches product
//                                                    $scope.editNewLaunches = function (productInfo) {
//                                                        $('chosen-select.mainCat option[value="'+productInfo.category_id+'"]').attr('selected', 'selected').trigger("chosen:updated")
//                                                        console.log(productInfo);
//                                                        $timeout(function(){
//                                                            $scope.$apply();
//                                                        });
//                                                    };
                                                    //delete existing new launches product
                                                    $scope.deleteNewLaunches = function (productInfo) {
                                                        console.log(productInfo);
                                                        for (var i = 0; i < $scope.newLaunches.length; i++) {
                                                            if(productInfo.catalogue_id == $scope.newLaunches[i].catalogue_id && productInfo.category_id == $scope.newLaunches[i].category_id && productInfo.product_type_id == $scope.newLaunches[i].product_type_id && productInfo.sub_category_id == $scope.newLaunches[i].sub_category_id && productInfo.supplier_id == $scope.newLaunches[i].supplier_id) {
                                                                $scope.newLaunches.splice(i, 1);
                                                            }
                                                        }
                                                        $timeout(function(){
                                                            $scope.$apply();
                                                        });
                                                    };
                                                }
                                                break;
                                            case 7:
                                                {
                                                    if (response.data.data[0].featuredBrands.length > 0) {
                                                        $scope.brands = response.data.data[0].featuredBrands;
                                                    }
                                                    $http.get('/brand/get')
                                                            .then(function (responseBrands) {
                                                                console.log("responseBrands");
                                                                console.log(responseBrands);
                                                                if (responseBrands.data.data.length > 0) {
                                                                    $scope.allBrands = responseBrands.data.data;
//                                                                    for (var i = 0; i < response.data.data[0].featuredBrands.length; i++) {
//                                                                        $('.chosen-select.featuredBrands').val(responseBrands.data.data[0].featuredBrands[i]._id).trigger("chosen:updated");
//                                                                    }
                                                                }
                                                                $timeout(function () {
                                                                    if (!$scope.brands) {
                                                                        $scope.brands = [];
                                                                    }
                                                                    $('.chosen-select.featuredBrands').chosen({width: "100%"}).change(function (event, params) {
                                                                        if (params.selected) {
                                                                            $scope.brands.push({
                                                                                _id: params.selected,
                                                                                name: $('.chosen-select.featuredBrands option[value="' + params.selected + '"]').attr('data-name'),
                                                                                slug: $('.chosen-select.featuredBrands option[value="' + params.selected + '"]').attr('data-slug')
                                                                            });
                                                                        } else if (params.deselected && $scope.brands.length > 0) {
                                                                            for (var i = 0; i < $scope.brands.length; i++) {
                                                                                if ($scope.brands[i]._id == params.deselected) {
                                                                                    $scope.brands.splice(i, 1);
                                                                                }
                                                                            }
                                                                        }
                                                                        $scope.$apply();

                                                                    });

                                                                });
                                                            });

                                                    $scope.removeExistingBrand = function (eachBrand) {
                                                        console.log("removeExistingBrand");
                                                        console.log(eachBrand);
                                                        for (var i = 0; i < $scope.brands.length; i++) {
                                                            if ($scope.brands[i]._id == eachBrand._id)
                                                            {
                                                                $scope.brands.splice(i, 1);
                                                                $('.chosen-select.featuredBrands option[value="' + eachBrand._id + '"]').removeAttr("selected").trigger("chosen:updated");
                                                            }
                                                        }
                                                        $timeout(function () {
                                                            $scope.$apply();
                                                        });
                                                    };
                                                }
                                                break;
                                        }
                                    }
                                    //initialise image uploader
                                    $timeout(function () {
                                        console.log("$scope.dividerNumber");
                                        console.log($scope.dividerNumber);
                                        $scope.$apply();
                                        if (7 > $scope.dividerNumber > 0) {
                                            var imageId = "#bannerImages" + $scope.dividerNumber;
                                            imageUploaderInit(configObject, imageId);
                                            chosenSelectInit($scope, $http, $timeout, $scope.dividerNumber);
                                        }
                                    });
                                });
                    });
                    
                    
                })
                .catch(function(errObject){
                    console.log("errObject");
                    console.log(errObject);
                });

        //find product types under selected subcategory    
        $scope.loadProductTypes = function (subCatId, i, selected_category_id = 0, currentData={}) {
            $scope.productTypes = [];
            if ($scope.localData[$scope.dividerNumber].selected_category_id == $scope.relations.main_category_id) {
                for (var j = 0; j < $scope.relations.children.length; j++) {
                    if (subCatId == $scope.relations.children[j]._id) {
                        if ($scope.relations.children[j].isProductType == true) {
                            $scope.productTypes.push({
                                _id: $scope.relations.children[j]._id,
                                name: $scope.relations.children[j].name,
                                slug: $scope.relations.children[j].slug
                            });
                        }
                        //also check in further children trees about existence of a product type
                        for (var k = 0; k < $scope.relations.children[j].children.length; k++) {
                            if ($scope.relations.children[j].children[k].isProductType == true) {
                                $scope.productTypes.push({
                                    _id: $scope.relations.children[j].children[k]._id,
                                    name: $scope.relations.children[j].children[k].name,
                                    slug: $scope.relations.children[j].children[k].slug
                                });
                            }
                            for (var l = 0; l < $scope.relations.children[j].children[k].children.length; l++) {
                                if ($scope.relations.children[j].children[k].children[l].isProductType == true) {
                                    $scope.productTypes.push({
                                        _id: $scope.relations.children[j].children[k].children[l]._id,
                                        name: $scope.relations.children[j].children[k].children[l].name,
                                        slug: $scope.relations.children[j].children[k].children[l].slug
                                    });
                                }
                            }
                        }
                    }
                }
                $timeout(function () {
                    $scope.$apply();
                    if(currentData && currentData.products && currentData.products.length > 0){
                        $("#tab_"+$scope.dividerNumber+" .chosen-select.prodType__"+i+" option[value='"+currentData.products[i].product_type_id+"__"+i+"']").attr('selected', 'selected');
                        $scope.selectedProducts[i].product_type_id = currentData.products[i].product_type_id;
                        $scope.loadCatalogueIds(currentData.products[i].product_type_id, i, currentData);
                    }
                    $('#tab_'+$scope.dividerNumber+' .chosen-select.prodType__' + i).chosen({width: "100%"}).trigger("chosen:updated").change(function (event, params) {
                        $scope.catalogueIds = [];
                        //********************************************//
                        $scope.supplierInfo[i] = [];
                        
                        $timeout(function(){
                            $scope.$apply();
                            $("#tab_"+$scope.dividerNumber+" .chosen-select.catId__"+i).trigger("chosen:updated");    
                            $("#tab_"+$scope.dividerNumber+" .chosen-select.supId__"+i).trigger("chosen:updated");    
                        });
                        $scope.selectedProducts[i].product_type_id = params.selected.split("__")[0];
                        $scope.loadCatalogueIds(params.selected.split("__")[0], i);
                    });
                });
            }
        };

        //get all catalogue ids 
        $scope.loadCatalogueIds = function (prodTypeId, index, currentData = {}) {
            //get all products by product type
            $http.get('/product/getCatIdsByProductType?prodTypeId=' + prodTypeId)
                    .then(function (responseCatIds) {
                        $scope.catalogueIds = [];
                        if (responseCatIds.data.data.length == 0 || responseCatIds.data.error == 1) {
                            $scope.promotionsError = responseCatIds.data.msg;
                            $(".error-modal-lg").modal();
                            return;
                            
                        } else if (responseCatIds.data.data.length > 0) {
                            $scope.variantData[index] = responseCatIds.data.data;
                            $timeout(function(){
                                for (var i = 0; i < responseCatIds.data.data.length; i++) {
                                    for (var j = 0; j < responseCatIds.data.data[i].variants.length; j++) {
                                        $scope.catalogueIds.push({
                                            _id: responseCatIds.data.data[i].variants[j].catalogueId
                                        });
                                    }
                                }
                                console.log("$scope.catalogueIds");
                                console.log($scope.catalogueIds);
                                $scope.$apply();
                                $("#tab_"+$scope.dividerNumber+" .chosen-select.catId__"+index).trigger("chosen:updated");
                                if(currentData && currentData.products && currentData.products.length > 0){
                                    $("#tab_"+$scope.dividerNumber+" .chosen-select.catId__"+index+" option[value='"+currentData.products[index].catalogue_id+"__"+index+"']").attr('selected', 'selected');
                                    $scope.selectedProducts[index].catalogue_id = currentData.products[index].catalogue_id;
                                    $scope.loadSupplierIds(currentData.products[index].catalogue_id, index, currentData);
                                }

                                $('#tab_'+$scope.dividerNumber+' .chosen-select.catId__' + index).chosen({width: "100%"}).trigger("chosen:updated").change(function (event, params) {
                                    $scope.selectedProducts[index].catalogue_id = params.selected.split("__")[0];
                                    console.log("params.selected catalogue id");
                                    console.log(params.selected);
                                    $scope.loadSupplierIds(params.selected.split("__")[0], index);
                                });
                            });
                        }
                    });
        };

        //get all supplier ids
        $scope.loadSupplierIds = function (catalogueId, index, currentData= {}) {
            console.log("index");
            console.log(index);
            $scope.supplierInfo[index] = [];
            $timeout(function () {
                $scope.$apply();
                for (var i = 0; i < $scope.variantData[index].length; i++) {
                    for (var j = 0; j < $scope.variantData[index][i].variants.length; j++) {
                        if ($scope.variantData[index][i].variants[j].catalogueId == catalogueId) {
                            $scope.supplierInfo[index] = [];
                            for (var k = 0; k < $scope.variantData[index][i].variants[j].suppliers.length; k++) {
                                $scope.supplierInfo[index].push({
                                    sku: $scope.variantData[index][i].variants[j].suppliers[k].sku
                                });
                            }
                        }
                    }
                }
                console.log("$scope.supplierInfo");
                console.log($scope.supplierInfo);
                $scope.$apply();
                if(currentData && currentData.products && currentData.products.length > 0){
                    $("#tab_"+$scope.dividerNumber+" .chosen-select.supId__"+index+" option[value='"+currentData.products[index].supplier_id+"__"+index+"']").attr('selected', 'selected');
                    $('.chosen-select.supId__' + index).trigger("chosen:updated");
                    $scope.selectedProducts[index].supplier_id = currentData.products[index].supplier_id;
                }
                $scope.$apply();
                $('.chosen-select.supId__' + index).chosen({width: "100%"}).trigger("chosen:updated").change(function (event, params) {
                    $scope.selectedProducts[index].supplier_id = params.selected.split("__")[0];
                });
            });
           
            
        };

        $scope.submitDivider = function (dividerNumber) {
            console.log(dividerNumber);
            var serverObject = {};
            if ($scope.promotionsId) {
                serverObject._id = $scope.promotionsId;
            }
            if (dividerNumber) {
                serverObject.dividerNumber = dividerNumber;
            }
            switch (dividerNumber) {
                case 0:
                case 1:
                    {
                        var hasErrors = false;
                        if($('#categories option:checked').length == 0) {
                            hasErrors = true;
                            $scope.promotionsError = "No categories selected for widget";
                            $(".error-modal-lg").modal();
                            return;
                        }
                        if(!$scope.sortedImages || $scope.sortedImages.length == 0) {
                            hasErrors = true;
                            $scope.promotionsError = "No banner images selected";
                            $(".error-modal-lg").modal();
                            return;
                        }
                        $('#tab_1 [data-name="urlForImage"]').each(function(index){
                            if($(this).val()=="" || isURL($(this).val()) == false){
                                hasErrors = true;
                                $scope.promotionsError = "Please enter valid URLs for all images";
                                $(".error-modal-lg").modal();
                                return;
                            }
                        });
                        if(hasErrors == false){
                            serverObject.widgets = [{
                                name: "Top Categories",
                                categories: []
                            }];
                            $('#categories option:checked').each(function (index) {
                                serverObject.widgets[0].categories.push({
                                    _id: $(this).val(),
                                    name: $(this).attr('data-name'),
                                    slug: $(this).attr('data-slug')
                                });
                            });
                            console.log("serverObject");
                            console.log(JSON.stringify(serverObject));
                            console.log($scope.sortedImages);
                            serverObject.bannerImages = [];
                            $.each($scope.sortedImages, function (index) {
                                    //existing data
                                    console.log($scope.sortedImages[index]);
                                    serverObject.bannerImages.push({
                                        name: $scope.sortedImages[index].caption,
                                        order: index,
                                        url: $('[urlContainer="' + $scope.sortedImages[index].caption + '"]').find('[data-name="urlForImage"]').val()
                                    });
                                    
//                                }
                            });
                            $http.post('/promotions/addPromotionData', serverObject)
                                    .then(function (response) {
                                        console.log(response);
                                        if (response.status == 200) {
                                            $scope.successStatus = "Successfully saved";
                                            $('.success-modal-lg').modal();
                                            $('.nav-tabs a[data-target="#tab_'+(dividerNumber+1)+'"]').tab('show')
                                            $scope.initialiseTab(dividerNumber+1);
                                        } 
                                    });
                        }
                    }
                    break;
                case 2:
                    {
                        var hasErrors = false;
                        if(!$scope.localData["2"]) {
                            hasErrors = true;
                            $scope.promotionsError = "No main category selected";
                            $(".error-modal-lg").modal();
                            return;
                        }
                        if(!$scope.sortedImages || $scope.sortedImages.length == 0) {
                            hasErrors = true;
                            $scope.promotionsError = "No images uploaded for banner";
                            $(".error-modal-lg").modal();
                            return;
                        }
                        $('#tab_2 [data-name="urlForImage"]').each(function(index){
                            if($(this).val()== "" || isURL($(this).val()) == false){
                                console.log("inside isURL if");
                                hasErrors = true;
                                $scope.promotionsError = "Please enter valid URLs for all images";
                                console.log("before modal popup");
                                $(".error-modal-lg").modal();
                                return;
                            }
                        });
             
                        var errorString = "";
                        for (var i = 1; i < 10; i++) {
                            if($('#tab_2 select#subCategories__'+i+' option:selected').length == 0) {
                                errorString += "Subcategory number "+i+". ";
                            }
                        }
                        if(errorString.length > 0){
                            hasErrors = true;
                            $scope.promotionsError = "No subcategory selected for the following. "+ errorString;
                            $(".error-modal-lg").modal();
                            return;
                        }
                        if($scope.selectedProducts.length < 4){
                            hasErrors = true;
                            $scope.promotionsError = "Please select all 4 products.";
                            $(".error-modal-lg").modal();
                            return;
                        }
                        if(hasErrors == false) {
                            for (var i = 0; i < $scope.mainCategories.length; i++) {
                                if ($scope.mainCategories[i]._id == $scope.localData["2"].selected_category_id) {
                                    serverObject.category = {
                                        _id: $scope.localData["2"].selected_category_id,
                                        name: $scope.mainCategories[i].name,
                                        slug: $scope.mainCategories[i].slug
                                    };
                                }
                            }
                            serverObject.sub_categories = [];
                            for (var i = 1; i < 10; i++) {
                                if ($('#subCategories__' + i + ' option:selected').length == 0) {
                                    $scope.promotionsError = "No subcategory selected for " + i;
                                    $(".error-modal-lg").modal();
                                }
                                var order = [];
                                $('ul[identifier="childrenSubcategoryList"] li[eachPromotionalSubcatLi]').each(function(index){
                                    order.push($(this).attr('identity'));
                                });
                                console.log($('#subCategories__' +i).parents('li'));
                                serverObject.sub_categories.push({
                                    _id: $('#subCategories__' + i + ' option:selected').val(),
                                    name: $('#subCategories__' + i + ' option:selected').attr('data-name'),
                                    slug: $('#subCategories__' + i + ' option:selected').attr('data-slug'),
                                    order: order.indexOf($('#subCategories__'+i).parents('li').attr('identity'))
                                });
                            }
                            $('#categories option:checked').each(function (index) {
                                serverObject.widgets[0].categories.push({
                                    _id: $(this).val(),
                                    name: $(this).attr('data-name'),
                                    slug: $(this).attr('data-slug')
                                });
                            });
                            serverObject.bannerImages = [];
                            $.each($scope.sortedImages, function (index) {
                                    //existing data
                                    console.log($scope.sortedImages[index]);
                                    serverObject.bannerImages.push({
                                        name: $scope.sortedImages[index].caption,
                                        order: index,
                                        url: $('[urlContainer="' + $scope.sortedImages[index].caption + '"]').find('[data-name="urlForImage"]').val()
                                    });
                            });
                            serverObject.products = [];
                            serverObject.products = $scope.selectedProducts;
                        
                        $http.post('/promotions/addPromotionData', serverObject)
                            .then(function (response) {
                                console.log(response);
                                if (response.status == 200) {
                                    $scope.successStatus = "Successfully saved";
                                    $('.success-modal-lg').modal();
                                    $('.nav-tabs a[data-target="#tab_'+(dividerNumber+1)+'"]').tab('show')
                                    $scope.initialiseTab(dividerNumber+1);
                                }
                            });
                        }
                    }
                    break;
                case 3:
                    {
                        for (var i = 0; i < $scope.mainCategories.length; i++) {
                            if ($scope.mainCategories[i]._id == $scope.localData["3"].selected_category_id) {
                                serverObject.category = {
                                    _id: $scope.localData["3"].selected_category_id,
                                    name: $scope.mainCategories[i].name,
                                    slug: $scope.mainCategories[i].slug
                                };
                            }
                        }
                        serverObject.sub_categories = [];
                        for (var i = 1; i < 10; i++) {
                            if ($('#subCategories__' + i + ' option:selected').length == 0) {
                                $scope.promotionsError = "No subcategory selected for " + i;
                                $(".error-modal-lg").modal();
                            }
                            serverObject.sub_categories.push({
                                _id: $('#subCategories__' + i + ' option:selected').val(),
                                name: $('#subCategories__' + i + ' option:selected').attr('data-name'),
                                slug: $('#subCategories__' + i + ' option:selected').attr('data-slug'),
                                order: i
                            });
                        }
                        $('#categories option:checked').each(function (index) {
                            serverObject.widgets[0].categories.push({
                                _id: $(this).val(),
                                name: $(this).attr('data-name'),
                                slug: $(this).attr('data-slug')
                            });
                        });
                        serverObject.bannerImages = [];
                        $.each($scope.sortedImages, function (index) {
                            for (var i = 0; i < $scope.serverImages.length; i++) {
                                if ($scope.serverImages[i].originalname == $scope.sortedImages[index].caption)
                                    serverObject.bannerImages.push({
                                        name: $scope.serverImages[i].filename,
                                        order: index,
                                        url: $('[urlContainer="' + $scope.serverImages[i].originalname + '"]').find('[data-name="urlForImage"]').val()
                                    });
                            }
                        });
                        serverObject.products = [];
                        var hasErrors = false;

                        if (!hasErrors) {
                            serverObject.products = $scope.selectedProducts;
                        }
                        console.log("serverObject  before sending");
                        console.log(JSON.stringify(serverObject));
                        if (hasErrors) {
                            $scope.promotionsError = "Please select all 4 products with supplier id, catalogue id";
                            $(".error-modal-lg").modal();
                        } else {
                            $http.post('/promotions/addPromotionData', serverObject)
                                    .then(function (response) {
                                        console.log(response);
                                        if (response.status == 200) {
                                            $scope.response = "Successfully saved";
                                        }
                                    });
                        }

                    }
                    break;
                case 4:
                    {
                        for (var i = 0; i < $scope.mainCategories.length; i++) {
                            if ($scope.mainCategories[i]._id == $scope.localData["4"].selected_category_id) {
                                serverObject.category = {
                                    _id: $scope.localData["4"].selected_category_id,
                                    name: $scope.mainCategories[i].name,
                                    slug: $scope.mainCategories[i].slug
                                };
                            }
                        }
                        serverObject.sub_categories = [];
                        for (var i = 1; i < 10; i++) {
                            if ($('#subCategories__' + i + ' option:selected').length == 0) {
                                $scope.promotionsError = "No subcategory selected for " + i;
                                $(".error-modal-lg").modal();
                            }
                            serverObject.sub_categories.push({
                                _id: $('#subCategories__' + i + ' option:selected').val(),
                                name: $('#subCategories__' + i + ' option:selected').attr('data-name'),
                                slug: $('#subCategories__' + i + ' option:selected').attr('data-slug'),
                                order: i
                            });
                        }
                        $('#categories option:checked').each(function (index) {
                            serverObject.widgets[0].categories.push({
                                _id: $(this).val(),
                                name: $(this).attr('data-name'),
                                slug: $(this).attr('data-slug')
                            });
                        });
                        serverObject.bannerImages = [];
                        $.each($scope.sortedImages, function (index) {
                            for (var i = 0; i < $scope.serverImages.length; i++) {
                                if ($scope.serverImages[i].originalname == $scope.sortedImages[index].caption)
                                    serverObject.bannerImages.push({
                                        name: $scope.serverImages[i].filename,
                                        order: index,
                                        url: $('[urlContainer="' + $scope.serverImages[i].originalname + '"]').find('[data-name="urlForImage"]').val()
                                    });
                            }
                        });
                        serverObject.products = [];
                        var hasErrors = false;
                        if (!hasErrors) {
                            serverObject.products = $scope.selectedProducts;
                        }
                        console.log("serverObject  before sending");
                        console.log(JSON.stringify(serverObject));
                        if (hasErrors) {
                            $scope.promotionsError = "Please select all 4 products with supplier id, catalogue id";
                            $(".error-modal-lg").modal();
                        } else {
                            $http.post('/promotions/addPromotionData', serverObject)
                                    .then(function (response) {
                                        console.log(response);
                                        if (response.status == 200) {
                                            $scope.response = "Successfully saved";
                                        }
                                    });
                        }

                    }
                    break;
                case 5:
                    {
                        console.log("$scope.selectedProducts");
                        console.log($scope.selectedProducts);
                        if($scope.featuredProducts.length == 0){
                            $scope.promotionsError = "Please select products to add";
                            $(".error-modal-lg").modal();
                            return;
                        }
                        if(Object.keys($scope.selectedProducts).length > 0) {
                            for (var i = 0; i < $scope.featuredProducts.length; i++) {
                                if($scope.featuredProducts[i].catalogue_id == $scope.selectedProducts[0].catalogue_id && $scope.featuredProducts[i].supplier_id == $scope.selectedProducts[0].supplier_id)
                                {
                                    $scope.promotionsError = "This product already exists. Please add another product.";
                                    $(".error-modal-lg").modal();
                                    return;
                                }
                            }
                            $scope.featuredProducts.push($scope.selectedProducts[0]);
                        }
                        serverObject.featuredProducts = $scope.featuredProducts;
                        $http.post('/promotions/addPromotionData', serverObject)
                                    .then(function (response) {
                                        if(response.data.error = 0){
                                            $scope.successStatus = "Successfully saved";
                                            $(".success-modal-lg").modal();
                                        }
                                    });
                    }
                    break;
                case 6:
                    {
                        if($scope.newLaunches.length == 0){
                            $scope.promotionsError = "Please select products to add";
                            $(".error-modal-lg").modal();
                            return;
                        }
                        if(Object.keys($scope.selectedProducts).length > 0 && $scope.selectedProducts.supplier_id) {
                            for (var i = 0; i < $scope.newLaunches.length; i++) {
                                if($scope.newLaunches[i].catalogue_id == $scope.selectedProducts[0].catalogue_id && $scope.newLaunches[i].supplier_id == $scope.selectedProducts[0].supplier_id)
                                {
                                    $scope.promotionsError = "This product already exists. Please add another product.";
                                    $(".error-modal-lg").modal();
                                    return;
                                }
                            }
                            $scope.newLaunches.push($scope.selectedProducts[0]);
                        }
                        serverObject.newLaunches = $scope.newLaunches;
                        $http.post('/promotions/addPromotionData', serverObject)
                            .then(function (response) {
                                console.log(response);
                                if(response.data.error = 0){
                                    $scope.successStatus = "Successfully saved";
                                    $(".success-modal-lg").modal();
                                }
                            });
                    }
                    break;
                case 7:
                    {
                        if ($scope.brands.length <= 0) {
                            $scope.promotionsError = "Please selected brands";
                            $(".error-modal-lg").modal();
                        } else {
                            serverObject.featuredBrands = $scope.brands;
                            $http.post('/promotions/addPromotionData', serverObject)
                                    .then(function (response) {
                                        console.log(response);
                                        if(response.data.error = 0){
                                            $scope.successStatus = "Successfully saved";
                                            $(".success-modal-lg").modal();
                                        }
                                    });
                        }
                    }
                    break;
            }
        };
    }
    ;

    //get relations by category idparams.selected
    function getRelationsByCatId(category_id, dividerNumber, currentData = {}) {
        $scope.localData[dividerNumber] = {
            selected_category_id: category_id
        };
        $http.get('/relations/getById?catId=' + category_id)
                .then(function (response) {
                    if (response.data.length > 0) {
                        //remove error if data found
                        $scope.error = "";
                        $scope.subCategories = [];
                        var selectedSubCategories = [];
                        //All relations
                        $scope.relations = response.data[0];
                        for (var i = 0; i < response.data[0].children.length; i++) {
//                          var tempObject = {};
//                          load only sub categories under this category 
                            if (response.data[0].children[i].isProductType == false) {
                                var tempObject = {
                                    _id: response.data[0].children[i]._id,
                                    name: response.data[0].children[i].name,
                                    slug: response.data[0].children[i].slug
                                };
                                $scope.subCategories.push(tempObject);
                            }
                        }
                        $timeout(function () {
                            $scope.models = {
                                selected: null,
                                lists: {"A": [{label: "1"}, {label: "2"}, {label: "3"}, {label: "4"}, {label: "5"}, {label: "6"}, {label: "7"}, {label: "8"}, {label: "9"}]}
                            };
                            $scope.reInitThisChosen = function (item) {
                                var $selectedOption = $("#tab_"+dividerNumber+" li[identity='" + item.label + "'] select option:selected");
                                $("#tab_"+dividerNumber+" li[identity='" + item.label + "'] select option[value='" + $selectedOption.val() + "']").attr('selected', 'selected');
                                $("#tab_"+dividerNumber+" li[identity='" + item.label + "'] .chosen-select").chosen({width: "90%"});
                            };
                            //apply scope value to view
                            $scope.$apply();
                            //show already saved subcategories
                            if(Object.keys(currentData).length > 0 && currentData.sub_categories && currentData.sub_categories.length > 0) {
                                for(var p=0;p< currentData.sub_categories.length;p++){
                                    $("#tab_"+dividerNumber+" .chosen-select.subCategory#subCategories__"+(p+1)+" option[value='"+currentData.sub_categories[p]._id+"']").attr('selected', 'selected');
                                    $("#tab_"+dividerNumber+" .chosen-select.subcat__"+p).trigger("chosen:updated");
                                }
                            }
        //                  Update chosen wherever applicable
                            $("#tab_"+dividerNumber+" .chosen-select.subCategory").chosen("destroy");
                            $("#tab_"+dividerNumber+" .chosen-select.subCategory").chosen({width: "90%"}).trigger("chosen:updated");

                            //init chosen for all products
                            //PRODUCT 1
                            if(currentData && currentData.products && currentData.products.length > 0){
                                $("#tab_"+dividerNumber+" .chosen-select.subcat__0 option[value='"+currentData.products[0].sub_category_id+"__0']").attr('selected', 'selected');
                                $scope.selectedProducts[0] = {
                                    sub_category_id: currentData.products[0].sub_category_id,
                                };
                                $scope.loadProductTypes(currentData.products[0].sub_category_id, 0, $scope.localData[dividerNumber].selected_category_id, currentData);
                            }
                            $("#tab_"+dividerNumber+" .chosen-select.subcat__0").trigger("chosen:updated");
                            $("#tab_"+dividerNumber+" .chosen-select.subcat__0").chosen({width: "100%"}).change(function (event, params) {
                                
                                //Refresh all select dropdowns of this product
                                $scope.productTypes = [];
                                $scope.catalogueIds = [];
                                $scope.supplierInfo[0] = [];
                                
                                $timeout(function(){
                                    $scope.$apply();
                                    $('#tab_'+dividerNumber+' .chosen-select.prodType__0').trigger("chosen:updated");
                                    $('#tab_'+dividerNumber+' .chosen-select.catId__0').trigger("chosen:updated");
                                    $('#tab_'+dividerNumber+' .chosen-select.supId__0').trigger("chosen:updated");
                                    
                                    $scope.selectedProducts[0] = {
                                        sub_category_id: params.selected.split("__")[0],
                                    };
                                    $scope.loadProductTypes(params.selected.split("__")[0], 0);
                                });
                            });
                            
                            //PRODUCT 2
                            if(currentData && currentData.products && currentData.products.length > 0){
                                $("#tab_"+dividerNumber+" .chosen-select.subcat__1 option[value='"+currentData.products[1].sub_category_id+"__1']").attr('selected', 'selected');
                                $scope.selectedProducts[1] = {
                                    sub_category_id: currentData.products[1].sub_category_id,
                                };
                                $scope.loadProductTypes(currentData.products[1].sub_category_id, 1, $scope.localData[dividerNumber].selected_category_id, currentData);
                            }
                            
                            $("#tab_"+dividerNumber+" .chosen-select.subcat__1").trigger("chosen:updated");
                            $("#tab_"+dividerNumber+" .chosen-select.subcat__1").chosen({width: "100%"}).change(function (event, params) {
                                
                                //Refresh all select dropdowns of this product
                                $scope.supplierInfo[1] = [];
                                $scope.productTypes = [];
                                $scope.catalogueIds = [];
                                $timeout(function(){
                                    $scope.$apply();
                                    $('#tab_'+dividerNumber+' .chosen-select.prodType__1').trigger("chosen:updated");
                                    $('#tab_'+dividerNumber+' .chosen-select.catId__1').trigger("chosen:updated");
                                    $('#tab_'+dividerNumber+' .chosen-select.supId__1').trigger("chosen:updated");
                                    $scope.selectedProducts[1] = {
                                        sub_category_id: params.selected.split("__")[0],
                                    };
                                    $scope.loadProductTypes(params.selected.split("__")[0], 1);
                                });
                            });
                            
                            
                            //PRODUCT 3
                            if(currentData && currentData.products && currentData.products.length > 0){
                                $("#tab_"+dividerNumber+" .chosen-select.subcat__2 option[value='"+currentData.products[2].sub_category_id+"__2']").attr('selected', 'selected');
                                $scope.selectedProducts[2] = {
                                    sub_category_id: currentData.products[2].sub_category_id,
                                };
                                $scope.loadProductTypes(currentData.products[2].sub_category_id, 2, $scope.localData[dividerNumber].selected_category_id, currentData);
                            }
                            
                            $("#tab_"+dividerNumber+" .chosen-select.subcat__2").trigger("chosen:updated");
                            $("#tab_"+dividerNumber+" .chosen-select.subcat__2").chosen({width: "100%"}).change(function (event, params) {
                                
                                //Refresh all select dropdowns of this product
                                $scope.supplierInfo[2] = [];
                                $scope.productTypes = [];
                                $scope.catalogueIds = [];
                                $timeout(function(){
                                    $scope.$apply();
                                    $('#tab_'+dividerNumber+' .chosen-select.prodType__2').trigger("chosen:updated");
                                    $('#tab_'+dividerNumber+' .chosen-select.catId__2').trigger("chosen:updated");
                                    $('#tab_'+dividerNumber+' .chosen-select.supId__2').trigger("chosen:updated");
                                    $scope.selectedProducts[2] = {
                                        sub_category_id: params.selected.split("__")[0],
                                    };
                                    $scope.loadProductTypes(params.selected.split("__")[0], 2);
                                });
                            });


                            //PRODUCT 4
                            if(currentData && currentData.products && currentData.products.length > 0){
                                $("#tab_"+dividerNumber+" .chosen-select.subcat__3 option[value='"+currentData.products[3].sub_category_id+"__3']").attr('selected', 'selected');
                                $scope.selectedProducts[3] = {
                                    sub_category_id: currentData.products[3].sub_category_id,
                                };
                                $scope.loadProductTypes(currentData.products[3].sub_category_id, 3, $scope.localData[dividerNumber].selected_category_id, currentData);
                            }
                            
                            $("#tab_"+dividerNumber+" .chosen-select.subcat__3").trigger("chosen:updated");
                            $("#tab_"+dividerNumber+" .chosen-select.subcat__3").chosen({width: "100%"}).change(function (event, params) {
                                
                                //Refresh all select dropdowns of this product
                                $scope.productTypes = [];
                                $scope.catalogueIds = [];
                                $scope.supplierInfo[3] = [];
                                
                                $timeout(function(){
                                    $scope.$apply();
                                    $('#tab_'+dividerNumber+' .chosen-select.prodType__3').trigger("chosen:updated");
                                    $('#tab_'+dividerNumber+' .chosen-select.catId__3').trigger("chosen:updated");
                                    $('#tab_'+dividerNumber+' .chosen-select.supId__3').trigger("chosen:updated");
                                    $scope.selectedProducts[3] = {
                                        sub_category_id: params.selected.split("__")[0],
                                    };
                                    $scope.loadProductTypes(params.selected.split("__")[0], 3);
                                });
                            });
                        });
                        return;
                    } else {
                        $scope.promotionsError = "No Relations defined for the following category hence no children found!";
                        $(".error-modal-lg").modal();
                        return;
                    }
                });
    }
});
//create multiselect div
var configObject = {
    initialPreviewAsData: true,
    uploadUrl: "/promotions/uploadImage/",
    deleteUrl: "/promotions/deleteImage/",
    uploadAsync: false,
    previewSettings: {image: {width: "120px", height: "40px"}},
    overwriteInitial: false,
    allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],
    maxFileSize: 3000,
    fileActionSettings: {
        showUpload: false
    }
};

app.controller('PromotionsTab1Controller', function ($scope, $http, $timeout, $location){
    
    $scope.selectedProducts = [];
    
    $("#categories1.chosen-select").chosen({width: "inherit"});
    
    $scope.sortedImages = [];
    //Image initialisation
    var configObject = {
        initialPreviewAsData: true,
        uploadUrl: "/promotions/uploadImage/",
        deleteUrl: "/promotions/deleteImage/",
        uploadAsync: false,
        previewSettings: {image: {width: "120px", height: "40px"}},
        overwriteInitial: false,
        allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],
        maxFileSize: 3000,
        fileActionSettings: {
            showUpload: false
        }
    };

    //toggle divider visiblity
    $scope.toggleVisiblity = function () {
        $scope.isVisible = !$scope.isVisible;
    };

    //Get All Main categories
    $http.get('/category/getAllMainCategories')
                .then(function(response){
                    if(response.data.data.length == 0) {
                        $scope.promotionsError = "No active  main categories found";
                        $(".error-modal-lg").modal();
                        return;
                    }
                    else {
                        console.log("response.data.data");
                        console.log(response.data.data);
                        $scope.mainCategories = response.data.data;
                        
                        $timeout(function(){
                            $scope.$apply();
                            $("#categories1.chosen-select").trigger("chosen:updated");
                        });
                    }
                });

    //get existing data for selected category
    $http.get('/promotions/get')
            .then(function (response) {
                console.log("response");
                console.log(response);
                if (response.data.data.length == 0) {
                    console.log("before init");
                    imageUploaderInit(configObject, '#bannerImages1');
                } else {
//                    $scope.category = response.data.data[0].category;
//                    $scope.parentCategory = response.data.data[0].category;
                    $scope._id = response.data.data[0]._id;
                    /* Code for page edit i.e viewing page after saving it earlier */
                    //show selected categories
                    if(response.data.data[0].isVisible){
                        $scope.isVisible = response.data.data[0].isVisible;
                    }
                    if (response.data.data[0].widgets.length > 0) {
                        if(response.data.data[0].widgets[0].name == "Top Categories") {
                            $.each(response.data.data[0].widgets[0].categories, function (index) {
                                var currentCategory = response.data.data[0].widgets[0].categories[index];
                                $('.chosen-select option[value="' + currentCategory._id + '"]').attr('selected', 'selected');
                                $('select option[value="' + currentCategory._id + '"]').selected('selected');
                                $('.chosen-select').trigger("chosen:updated");
                            });
                        }
                    }
                    //category whose promotion page is being edited/created
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }

                    /* Show banner images if already selected */
                    for (var j = 0; j < response.data.data[0].bannerImages.length; j++) {
                        configObject.initialPreviewConfig.push({
                            caption: response.data.data[0].bannerImages[j].name,
                            width: "120px",
                            key: "promotions/home/" + response.data.data[0].bannerImages[j].name
                        });
                        var imageUrl = s3_bucket + "promotions/home/" + response.data.data[0].bannerImages[j].name;
                        configObject.initialPreview.push(imageUrl);
                        $scope.sortedImages.push({
                            caption: response.data.data[0].bannerImages[j].name,
                            url: response.data.data[0].bannerImages[j].url
                        });
                    }
                    imageUploaderInit(configObject, '#bannerImages1');
                }
            });
            
    //save data
    $scope.submitDivider = function () {
        var serverObject = {};
        var hasErrors = false;
        if($('#categories1 option:checked').length == 0) {
            hasErrors = true;
            $scope.promotionsError = "No categories selected for widget";
            $(".error-modal-lg").modal();
            return;
        }
        if(!$scope.sortedImages || $scope.sortedImages.length == 0) {
            hasErrors = true;
            $scope.promotionsError = "No banner images selected";
            $(".error-modal-lg").modal();
            return;
        }
        $('[data-name="urlForImage"]').each(function(index){
            if($(this).val()=="" || isURL($(this).val()) == false){
                hasErrors = true;
                $scope.promotionsError = "Please enter valid URLs for all images";
                $(".error-modal-lg").modal();
                return;
            }
        });
        serverObject.isVisible = $scope.isVisible;
        if(hasErrors == false){
            serverObject.widgets = [{
                name: "Top Categories",
                categories: []
            }];
            $('#categories1 option:checked').each(function (index) {
                serverObject.widgets[0].categories.push({
                    _id: $(this).val(),
                    name: $(this).attr('data-name'),
                    slug: $(this).attr('data-slug')
                });
            });
            if($scope._id){
                serverObject._id = $scope._id;
            }
            console.log("serverObject");
            console.log(JSON.stringify(serverObject));
            console.log($scope.sortedImages);
            serverObject.bannerImages = [];
            $.each($scope.sortedImages, function (index) {
                    //existing data
                    console.log($scope.sortedImages[index]);
                    serverObject.bannerImages.push({
                        name: $scope.sortedImages[index].caption,
                        order: index,
                        url: $('[urlContainer="' + $scope.sortedImages[index].caption + '"]').find('[data-name="urlForImage"]').val()
                    });

            });
            $http.post('/promotions/addPromotionData', serverObject)
                    .then(function (response) {
                        console.log(response);
                        if (response.status == 200) {
                            $scope.successStatus = "Successfully saved";
                            $('.success-modal-lg').modal();
                        } 
                    });
        }
    };

    //image uploader init
    function imageUploaderInit(configObject, imageDivId) {
        $(imageDivId).fileinput(configObject)
                .on('fileuploaded', function (event, data, previewId, index) {
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    $scope.serverImages = data.response[0];
                    configObject.initialPreviewConfig.push({
                        caption: data.response[0].originalname,
                        size: data.response[0].size,
                        width: "120px",
                        key: data.response[i].key
//                        key: data.response[i].filename
                    });
                    configObject.initialPreview.push(data.response[0].location);
                    $scope.sortedImages.push({
                        caption: data.response[0].key.split('/')[2]
//                        caption: data.response[0].filename
                    });
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filebatchuploadsuccess', function (event, data) {
                    //data.response is response from server
                    $scope.serverImages = data.response;
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    for (var i = 0; i < data.response.length; i++) {
                        configObject.initialPreviewConfig.push({
                            caption: data.response[i].originalname,
                            size: data.response[i].size,
                            width: "120px",
                            key: data.response[i].key
//                            key: data.response[i].filename
                        });
                        configObject.initialPreview.push(data.response[i].location);
//                        configObject.initialPreview.push(data.response[i].path);
                        $scope.sortedImages.push({
                            caption: data.response[i].key.split('/')[2]
//                            caption: data.response[i].filename
                        });
                    }
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filesorted', function (event, params) {
                    var oldImages = [];
                    for (var j = 0; j < $scope.sortedImages.length; j++) {
                        oldImages.push($scope.sortedImages[j]);
                    }
                    $scope.sortedImages = [];
                    for (var i = 0; i < params.stack.length; i++) {
                        $.each(oldImages, function (index) {
                            //use regex to see if uploaded image(without timestamp) matches the sorted current image
                            var regex = new RegExp("^" + params.stack[i].caption.split('.')[0] + "-[0-9]{12,20}\.[a-z]{2,4}$")
                            if (params.stack[i].caption == oldImages[index].caption || regex.test(oldImages[index].caption)) {
                                $scope.sortedImages.push({
                                    caption: oldImages[index].caption,
                                    url: oldImages[index].url
                                });
                            }
                        });
                    }
                    //update url set order  
                    $timeout(function () {
                        $scope.$apply();
                    });
                })
                .on('filedeleted', function (event, key) {
                    $scope.sortedImages.splice(key - 1, 1);
                    for (var i = 0; i < configObject.initialPreviewConfig.length; i++) {
                        if (configObject.initialPreviewConfig[i].key == key) {
                            configObject.initialPreviewConfig.splice(i, 1);
                            configObject.initialPreview.splice(i, 1);
                        }
                    }
                    $scope.$apply();
                });
    }
    
});

app.controller('PromotionsTab2Controller', function ($scope, $http, $timeout, $location) {

//    $scope.selectedCategorySlug = (window.location.href.split('#')[1]).split('/')[3];
    $scope.selectedProducts = [];
    
    $scope.sortedImages = [];
    console.log("inside divider 2 controller");
    //Image initialisation
    var configObject = {
        initialPreviewAsData: true,
        uploadUrl: "/promotions/uploadImage/",
        deleteUrl: "/promotions/deleteImage/",
        uploadAsync: false,
        previewSettings: {image: {width: "120px", height: "40px"}},
        overwriteInitial: false,
        allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],
        maxFileSize: 3000,
        fileActionSettings: {
            showUpload: false
        }
    };

    //toggle divider visiblity
    $scope.toggleVisiblity = function () {
        $scope.isVisible = !$scope.isVisible;
    };
    
    $("#categories2.chosen-select").chosen({width: "100%"}).change(function(event, params){
        //on change current sectional category
            $scope.parentCategory = {
                _id: params.selected,
                name: $("[value='"+params.selected+"']").attr("data-name"),
                slug: $("[value='"+params.selected+"']").attr("data-slug")
            }
            console.log("$scope.parentCategory");
            console.log($scope.parentCategory);
            getAllRelations(params);
            
    });

    //Get All Main categories
    $http.get('/category/getAllMainCategories')
                .then(function(response){
                    if(response.data.data.length == 0) {
                        $scope.promotionsError = "No active  main categories found";
                        $(".error-modal-lg").modal();
                        return;
                    }
                    else {
                        console.log("response.data.data");
                        console.log(response.data.data);
                        $scope.mainCategories = response.data.data;
                        
                        $timeout(function(){
                            $scope.$apply();
                            $("#categories2.chosen-select").trigger("chosen:updated");
                        });
                    }
                });

    $scope.reInitThisChosen = function (item) {
        var $selectedOption = $("li[identity='" + item.label + "'] select option:selected");
        $("li[identity='" + item.label + "'] select option[value='" + $selectedOption.val() + "']").attr('selected', 'selected');
        $("li[identity='" + item.label + "'] .chosen-select").chosen({width: "90%"});
    };

    //get existing data for selected category
    $http.get('/promotions/get')
            .then(function (response) {
                console.log("response");
                console.log(response);
                if (response.data.data.length == 0) {
                    console.log("before init");
                    imageUploaderInit(configObject, '#bannerImages2');
                } else {
//                    $scope.category = response.data.data[0].category;
//                    $scope.parentCategory = response.data.data[0].category;
                    $scope._id = response.data.data[0]._id;
                    /* Code for page edit i.e viewing page after saving it earlier */
                    for (var i = 0; i < response.data.data[0].dividers.length; i++) {
                        if (response.data.data[0].dividers[i].dividerNumber == 2) {
                            $scope.isVisible = response.data.data[0].dividers[i].isVisible;
                            $scope.selectedProducts = response.data.data[0].dividers[i].products;
                            $scope.subCategories = response.data.data[0].dividers[i].sub_categories;
                            //selected sub category for this divider section
                            $scope.category = response.data.data[0].dividers[i].category;
                            //category whose promotion page is being edited/created
                            if (!configObject.initialPreviewConfig) {
                                configObject.initialPreviewConfig = [];
                            }
                            if (!configObject.initialPreview) {
                                configObject.initialPreview = [];
                            }
                            if (!$scope.sortedImages) {
                                $scope.sortedImages = [];
                            }

                            /* Show banner images if already selected */
                            for (var j = 0; j < response.data.data[0].dividers[i].bannerImages.length; j++) {
                                configObject.initialPreviewConfig.push({
                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                    width: "120px",
                                    key: "promotions/home/" + response.data.data[0].dividers[i].bannerImages[j].name
                                });
                                var imageUrl = s3_bucket + "promotions/home/" + response.data.data[0].dividers[i].bannerImages[j].name;
                                configObject.initialPreview.push(imageUrl);
                                $scope.sortedImages.push({
                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                    url: response.data.data[0].dividers[i].bannerImages[j].url
                                });
                            }

                            /* For selected products */
                            if (!$scope.catalogueIds) {
                                $scope.catalogueIds = [];
                            }
                            if (!$scope.supplierInfo) {
                                $scope.supplierInfo = [];
                            }
                            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                                loadCatalogueIds($scope, $scope.selectedProducts[i].product_type_id, $http, i+1, false);
                            }
                            $scope.parentCategory = $scope.category
                            getAllRelations({selected : $scope.category._id});
                        }
                    }
                    imageUploaderInit(configObject, '#bannerImages2');
                }
            });

    function getAllRelations(params){
        $http.get('/relations/getById?catId=' + $scope.parentCategory._id)
                            .then(function (response) {
                                console.log("response of relations");
                                console.log(response);
                                var relationsObject = response.data[0];
                                $scope.allChildren = extractAllChildren(response.data[0]);
                                $scope.category = $scope.parentCategory;
                                //check if category is already selected previously i.e page is being edited and not created
                                if ($scope.category) {
                                    console.log("$scope.allChildren");
                                    console.log($scope.allChildren);
                                    var noChildren = false;
                                    for (var i = 0; i < $scope.allChildren.length; i++) {
                                        if ($scope.allChildren[i]._id == params.selected && $scope.allChildren[i].isProductType == true) {
                                            $(".child-categories .error").html("Selected category is a product type, hence no child categories");
                                            $(".child-categories .error").css('display', 'block');
                                            noChildren = true;
                                            return;
                                        }
                                    }
                                    if (noChildren === false) {
                                        console.log("relationsObject");
                                        console.log(relationsObject);
                                        $(".child-categories .error").css('display', 'none');
                                        $scope.thisCategoryChildren = extractAllChildren(relationsObject);
                                        console.log("$scope.thisCategoryChildren");
                                        console.log($scope.thisCategoryChildren);
                                        $scope.models = {
                                            selected: null,
                                            lists: {"A": []}
                                        };
                                        var max;
                                        max = $scope.thisCategoryChildren.length < 9 ? $scope.thisCategoryChildren.length : 9;                                        
                                        for (var i = 1; i <= max; i++) {
                                            $scope.models.lists["A"].push({
                                                label: i
                                            });
                                        }
                                        $timeout(function () {
                                            //apply scope value to view
                                            $scope.$apply();
                                            $('.chosen-select.childCategory').chosen({"width": "90%"});
                                        });
                                    }   

                                }
                                $timeout(function ()
                                {
                                    if ($scope.category) {
                                        $(".chosen-select.categoryDivider2 option[value='" + $scope.category._id + "']").attr('selected', 'selected');
                                    }
                                    $(".chosen-select.categoryDivider2").trigger("chosen:updated");

                                    //chosen init all subcategories which are labelled
                                    if ($scope.models && $scope.models.lists) {
                                        for (var i = 1; i <= $scope.models.lists["A"].length; i++) {
                                            $("li[identity='" + i + "'] .chosen-select").chosen({width: "90%"});
                                            if ($scope.subCategories) {
                                                //check for order and show selected subcategories if exists
                                                for (var j = 0; j < $scope.subCategories.length; j++) {
                                                    $("li[identity='" + i + "'] .chosen-select option[value='" + $scope.subCategories[i - 1]._id + "']").attr('selected', 'selected');
                                                    $("li[identity='" + i + "'] .chosen-select").trigger("chosen:updated");
                                                }
                                            }
                                        }
                                    }
                                    
                                    $scope.$apply();

                                });
                            });
                            
        //load product types for each product
        $http.get('/relations/getProductTypesByMainCategory?cat_slug=' + $scope.parentCategory.slug)
                .then(function (response) {
                    if (response.data.error == 0) {
                        $scope.productTypes = response.data.data;
                    } else {
                        $scope.error = "No product types for the selected category";
                    }
                    $timeout(function () {
                        //$scope.$apply();

                        /* Initialise all chosen selects*/

                        $('.chosen-select.catId__1').chosen({width: "100%"});
                        $('.chosen-select.catId__2').chosen({width: "100%"});
                        $('.chosen-select.catId__3').chosen({width: "100%"});
                        $('.chosen-select.catId__4').chosen({width: "100%"});

                        $('.chosen-select.supId__1').chosen({width: "100%"});
                        $('.chosen-select.supId__2').chosen({width: "100%"});
                        $('.chosen-select.supId__3').chosen({width: "100%"});
                        $('.chosen-select.supId__4').chosen({width: "100%"});


                        $('.chosen-select.prodType__1').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 1);
                        });
                        $('.chosen-select.prodType__2').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 2);
                        });
                        $('.chosen-select.prodType__3').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 3);
                        });
                        $('.chosen-select.prodType__4').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 4);
                        });
                    });
                });
    }

    //save data
    $scope.submitDivider = function () {
        $scope.error = "";
        var data = {
            dividerNumber: 2,
            isVisible: $scope.isVisible,
        };
        /* Validations */
        //perform validations only if visiblity set to true
        if ($scope.isVisible == true) {

            //check if sectional category selected
            if ($(".chosen-select.categoryDivider2 option:selected").val() == "Select") {
                $scope.error = "Please select the category for this divider section";
                return;
            }

            //check if banner images are uploaded. Atleast 1
            if (!$scope.sortedImages || $scope.sortedImages.length == 0) {
                $scope.error = "No images uploaded for banner";
                return;
            }
            //check if URL's are valid
            $('[data-name="urlForImage"]').each(function (index) {
                if ($(this).val() == "" || isURL($(this).val()) == false) {
                    $scope.error = "Please enter valid URLs for all images";
                    return;
                }
            });

            //check if all 4 products are selected. 
            if ($scope.selectedProducts.length != 4) {
                $scope.error = "Please select all 4 products for this promotional page";
                return;
            }
            
        }
        /* No errors. Fomulate data object to be inserted */
        
        if($scope._id){
            data["_id"] = $scope._id;
        }
        data["products"] = $scope.selectedProducts;
        data["bannerImages"] = [];
        for (var i = 0; i < $scope.sortedImages.length; i++) {
            data["bannerImages"].push({
                name: $scope.sortedImages[i].caption,
                order: i,
                url: $scope.sortedImages[i].url
            });
        }
        var order = [];
        $('ul[identifier="childrenSubcategoryList"] li[eachPromotionalSubcatLi]').each(function (index) {
            order.push($(this).attr('identity'));
        });

        data["sub_categories"] = [];
        if($scope.models){
            for (var i = 1; i <= $scope.models.lists["A"].length; i++) {
                data.sub_categories.push({
                    _id: $('#subCategories__' + i + ' option:selected').val(),
                    name: $('#subCategories__' + i + ' option:selected').attr('data-name'),
                    slug: $('#subCategories__' + i + ' option:selected').attr('data-slug'),
                    order: order.indexOf($('#subCategories__' + i).parents('li').attr('identity'))
                });
            }
        }
        
        data["category"] = {
            _id: $(".chosen-select.categoryDivider2 option:selected").val(),
            name: $(".chosen-select.categoryDivider2 option:selected").attr('data-name'),
            slug: $(".chosen-select.categoryDivider2 option:selected").attr('data-slug')
        };
        //ajax post submit
        if($scope.error == ""){
            $http.post('/promotions/addPromotionData', data, {})
                .success(function (data, status, headers, config) {
                    console.log('success add - ', data);
                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

//                $timeout(function () {
//                    $location.path('/cate');
//                }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error add - ', data);
                    $scope.error = 'Unable to add divider 2 data';
                });
        }

    };

    //image uploader init
    function imageUploaderInit(configObject, imageDivId) {
        $(imageDivId).fileinput(configObject)
                .on('fileuploaded', function (event, data, previewId, index) {
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    $scope.serverImages = data.response[0];
                    configObject.initialPreviewConfig.push({
                        caption: data.response[0].originalname,
                        size: data.response[0].size,
                        width: "120px",
                        key: data.response[i].key
//                        key: data.response[i].filename
                    });
                    configObject.initialPreview.push(data.response[0].location);
                    $scope.sortedImages.push({
                        caption: data.response[0].key.split('/')[2]
//                        caption: data.response[0].filename
                    });
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filebatchuploadsuccess', function (event, data) {
                    //data.response is response from server
                    $scope.serverImages = data.response;
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    for (var i = 0; i < data.response.length; i++) {
                        configObject.initialPreviewConfig.push({
                            caption: data.response[i].originalname,
                            size: data.response[i].size,
                            width: "120px",
                            key: data.response[i].key
//                            key: data.response[i].filename
                        });
                        configObject.initialPreview.push(data.response[i].location);
//                        configObject.initialPreview.push(data.response[i].path);
                        $scope.sortedImages.push({
                            caption: data.response[i].key.split('/')[2]
//                            caption: data.response[i].filename
                        });
                    }
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filesorted', function (event, params) {
                    var oldImages = [];
                    for (var j = 0; j < $scope.sortedImages.length; j++) {
                        oldImages.push($scope.sortedImages[j]);
                    }
                    $scope.sortedImages = [];
                    for (var i = 0; i < params.stack.length; i++) {
                        $.each(oldImages, function (index) {
                            //use regex to see if uploaded image(without timestamp) matches the sorted current image
                            var regex = new RegExp("^" + params.stack[i].caption.split('.')[0] + "-[0-9]{12,20}\.[a-z]{2,4}$")
                            if (params.stack[i].caption == oldImages[index].caption || regex.test(oldImages[index].caption)) {
                                $scope.sortedImages.push({
                                    caption: oldImages[index].caption,
                                    url: oldImages[index].url
                                });
                            }
                        });
                    }
                    //update url set order  
                    $timeout(function () {
                        $scope.$apply();
                    });
                })
                .on('filedeleted', function (event, key) {
                    $scope.sortedImages.splice(key - 1, 1);
                    for (var i = 0; i < configObject.initialPreviewConfig.length; i++) {
                        if (configObject.initialPreviewConfig[i].key == key) {
                            configObject.initialPreviewConfig.splice(i, 1);
                            configObject.initialPreview.splice(i, 1);
                        }
                    }
                    $scope.$apply();
                });
    }

    //extract all children
    function extractAllChildren(relationsObject) {
        var allChildren = [];
        for (var i = 0; i < relationsObject.children.length; i++) {
            allChildren.push({
                _id: relationsObject.children[i]._id,
                name: relationsObject.children[i].name,
                slug: relationsObject.children[i].slug,
                isProductType: relationsObject.children[i].isProductType
            });
            if (relationsObject.children[i].children) {
                for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                    allChildren.push({
                        _id: relationsObject.children[i].children[j]._id,
                        name: relationsObject.children[i].children[j].name,
                        slug: relationsObject.children[i].children[j].slug,
                        isProductType: relationsObject.children[i].children[j].isProductType
                    });
                    if (relationsObject.children[i].children[j].children) {
                        for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                            allChildren.push({
                                _id: relationsObject.children[i].children[j].children[k]._id,
                                name: relationsObject.children[i].children[j].children[k].name,
                                slug: relationsObject.children[i].children[j].children[k].slug,
                                isProductType: relationsObject.children[i].children[j].children[k].isProductType
                            });
                        }
                    }
                }
            }
        }
        return allChildren;
    }
    ;


    //add product(catalogue) ids into dropdowns
    function loadCatalogueIds($scope, selectedProductType, $http, index, isParamsSelected = true) {
        var variantData;
//        $scope.catalogueIds[index] = [];
        if (!$scope.catalogueIds) {
            $scope.catalogueIds = [];
        }
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = []
        }
        $scope.supplierInfo[index] = [];
        $http.get('/product/getCatIdsByProductType?prodTypeId=' + selectedProductType)
                .then(function (responseCatIds) {
                    $scope.catalogueIds[index] = [];
                    if (responseCatIds.data.error == 0) {
                        variantData = responseCatIds.data.data;
                        for (var i = 0; i < responseCatIds.data.data.length; i++) {
                            for (var j = 0; j < responseCatIds.data.data[i].variants.length; j++) {
                                $scope.catalogueIds[index].push({
                                    _id: responseCatIds.data.data[i].variants[j].catalogueId
                                });
                            }
                        }
                    } else {
                        $scope.error = "No catalogue ids under this product type.";
                    }
                    $timeout(function () {
                        $scope.$apply();
                        //update supplier sku list of current indexed selected product
                        if($scope.selectedProducts[index-1] && isParamsSelected == false){
                            //show selected product type ids
                            $('.chosen-select.prodType__' + index + ' option[value="' + $scope.selectedProducts[index-1].product_type_id + '"]').attr('selected', 'selected');
                            $('.chosen-select.prodType__' + index).trigger("chosen:updated");
                            //show selected catalogue ids
                            $('.chosen-select.catId__' + index +' option[value="'+$scope.selectedProducts[index-1].catalogue_id+'"]').attr('selected', 'selected');
                            $('.chosen-select.catId__' + index).trigger('chosen:updated');
                            loadSupplierIds($scope, $scope.selectedProducts[index-1].catalogue_id, variantData, index, false);
                        }
                        //update supplier sku list of current indexed selected product
                        $('.chosen-select.supId__' + index).trigger("chosen:updated");
                        $('.chosen-select.catId__' + index).trigger('chosen:updated');
                        $('.chosen-select.catId__' + index).chosen({width: "100%"}).change(function (event, params) {
                            loadSupplierIds($scope, params.selected, variantData, index);
                        });
                    });
                });
    }

    //add supplier SKU to supplier dropdown
    function loadSupplierIds($scope, selectedCatalogueId, variantData, index, isParamsSelected = true) {
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = [];
        }
        $scope.supplierInfo[index] = [];
        for (var i = 0; i < variantData.length; i++) {
            for (var j = 0; j < variantData[i].variants.length; j++) {
                if (variantData[i].variants[j].catalogueId == selectedCatalogueId) {
                    for (var k = 0; k < variantData[i].variants[j].suppliers.length; k++) {
                        $scope.supplierInfo[index].push({
                            sku: variantData[i].variants[j].suppliers[k].sku
                        });
                    }
                }
            }
        }
        $timeout(function () {
            $scope.$apply();
            $('.chosen-select.supId__' + index).chosen({width: "100%"}).trigger("chosen:updated").change(function (event, params) {
//                if(!$scope.selectedProducts[index]) {
//                $scope.selectedProducts[index - 1] = {}
//                }
                $scope.selectedProducts[index - 1] = {
                    product_type_id: $('.chosen-select.prodType__' + index + ' option:selected').val(),
                    catalogue_id: $('.chosen-select.catId__' + index + ' option:selected').val(),
                    supplier_id: params.selected
                };
            });
            if($scope.selectedProducts[index-1] && isParamsSelected == false){
                $('.chosen-select.supId__' + index+' option[value="'+$scope.selectedProducts[index-1].supplier_id+'"]').attr('selected', 'selected');
                $('.chosen-select.supId__' + index).trigger("chosen:updated");
            }
        });
    }

    //extract children for selected category id
    function extractAllChildrenWithCondition(relationsObject, category_id) {
        var returnArray = [];
        console.log(category_id);
        console.log(category_id);
        for (var i = 0; i < relationsObject.children.length; i++) {
            //found in first column
            if (relationsObject.children[i]._id == category_id) {
                //add all child categories from second column
                for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                    returnArray.push({
                        _id: relationsObject.children[i].children[j]._id,
                        name: relationsObject.children[i].children[j].name,
                        slug: relationsObject.children[i].children[j].slug
                    });
                    //add all child categories from second column
                    for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                        returnArray.push({
                            _id: relationsObject.children[i].children[j].children[k]._id,
                            name: relationsObject.children[i].children[j].children[k].name,
                            slug: relationsObject.children[i].children[j].children[k].slug
                        });
                    }
                }
            }
            /*also look in second column for matches of selected category 
             since category maybe attached at multiple places */
//            else {
            for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                //if match found in second column
                if (relationsObject.children[i].children[j]._id == category_id) {
                    //add all child categories from column 3
                    for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                        returnArray.push({
                            _id: relationsObject.children[i].children[j].children[k]._id,
                            name: relationsObject.children[i].children[j].children[k].name,
                            slug: relationsObject.children[i].children[j].children[k].slug
                        });
                    }
                }
            }
//            }
        }
        return returnArray;
    }
});


app.controller('PromotionsTab3Controller', function ($scope, $http, $timeout, $location) {

//    $scope.selectedCategorySlug = (window.location.href.split('#')[1]).split('/')[3];
    $scope.selectedProducts = [];
    
    $scope.sortedImages = [];
    //Image initialisation
    var configObject = {
        initialPreviewAsData: true,
        uploadUrl: "/promotions/uploadImage/",
        deleteUrl: "/promotions/deleteImage/",
        uploadAsync: false,
        previewSettings: {image: {width: "120px", height: "40px"}},
        overwriteInitial: false,
        allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],
        maxFileSize: 3000,
        fileActionSettings: {
            showUpload: false
        }
    };

    //toggle divider visiblity
    $scope.toggleVisiblity = function () {
        $scope.isVisible = !$scope.isVisible;
    };
    
    $("#categories3.chosen-select").chosen({width: "100%"}).change(function(event, params){
        //on change current sectional category
            $scope.parentCategory = {
                _id: params.selected,
                name: $("[value='"+params.selected+"']").attr("data-name"),
                slug: $("[value='"+params.selected+"']").attr("data-slug")
            }
            console.log("$scope.parentCategory");
            console.log($scope.parentCategory);
            getAllRelations(params);
            
    });

    //Get All Main categories
    $http.get('/category/getAllMainCategories')
                .then(function(response){
                    if(response.data.data.length == 0) {
                        $scope.promotionsError = "No active  main categories found";
                        $(".error-modal-lg").modal();
                        return;
                    }
                    else {
                        console.log("response.data.data");
                        console.log(response.data.data);
                        $scope.mainCategories = response.data.data;
                        
                        $timeout(function(){
                            $scope.$apply();
                            $("#categories3.chosen-select").trigger("chosen:updated");
                        });
                    }
                });

    $scope.reInitThisChosen = function (item) {
        var $selectedOption = $("li[identity='" + item.label + "'] select option:selected");
        $("li[identity='" + item.label + "'] select option[value='" + $selectedOption.val() + "']").attr('selected', 'selected');
        $("li[identity='" + item.label + "'] .chosen-select").chosen({width: "90%"});
    };

    //get existing data for selected category
    $http.get('/promotions/get')
            .then(function (response) {
                console.log("response");
                console.log(response);
                if (response.data.data.length == 0) {
                    console.log("before init");
                    imageUploaderInit(configObject, '#bannerImages3');
                } else {
//                    $scope.category = response.data.data[0].category;
//                    $scope.parentCategory = response.data.data[0].category;
                    $scope._id = response.data.data[0]._id;
                    /* Code for page edit i.e viewing page after saving it earlier */
                    for (var i = 0; i < response.data.data[0].dividers.length; i++) {
                        if (response.data.data[0].dividers[i].dividerNumber == 3) {
                            $scope.isVisible = response.data.data[0].dividers[i].isVisible;
                            $scope.selectedProducts = response.data.data[0].dividers[i].products;
                            $scope.subCategories = response.data.data[0].dividers[i].sub_categories;
                            //selected sub category for this divider section
                            $scope.category = response.data.data[0].dividers[i].category;
                            //category whose promotion page is being edited/created
                            if (!configObject.initialPreviewConfig) {
                                configObject.initialPreviewConfig = [];
                            }
                            if (!configObject.initialPreview) {
                                configObject.initialPreview = [];
                            }
                            if (!$scope.sortedImages) {
                                $scope.sortedImages = [];
                            }

                            /* Show banner images if already selected */
                            for (var j = 0; j < response.data.data[0].dividers[i].bannerImages.length; j++) {
                                configObject.initialPreviewConfig.push({
                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                    width: "120px",
                                    key: "promotions/home/" + response.data.data[0].dividers[i].bannerImages[j].name
                                });
                                var imageUrl = s3_bucket + "promotions/home/" + response.data.data[0].dividers[i].bannerImages[j].name;
                                configObject.initialPreview.push(imageUrl);
                                $scope.sortedImages.push({
                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                    url: response.data.data[0].dividers[i].bannerImages[j].url
                                });
                            }

                            /* For selected products */
                            if (!$scope.catalogueIds) {
                                $scope.catalogueIds = [];
                            }
                            if (!$scope.supplierInfo) {
                                $scope.supplierInfo = [];
                            }
                            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                                loadCatalogueIds($scope, $scope.selectedProducts[i].product_type_id, $http, i+1, false);
                            }
                            $scope.parentCategory = $scope.category
                            getAllRelations({selected : $scope.category._id});
                        }
                    }
                    imageUploaderInit(configObject, '#bannerImages3');
                }
            });

    function getAllRelations(params){
        $http.get('/relations/getById?catId=' + $scope.parentCategory._id)
                            .then(function (response) {
                                console.log("response of relations");
                                console.log(response);
                                var relationsObject = response.data[0];
                                $scope.allChildren = extractAllChildren(response.data[0]);
                                $scope.category = $scope.parentCategory;
                                //check if category is already selected previously i.e page is being edited and not created
                                if ($scope.category) {
                                    console.log("$scope.allChildren");
                                    console.log($scope.allChildren);
                                    var noChildren = false;
                                    for (var i = 0; i < $scope.allChildren.length; i++) {
                                        if ($scope.allChildren[i]._id == params.selected && $scope.allChildren[i].isProductType == true) {
                                            $(".child-categories .error").html("Selected category is a product type, hence no child categories");
                                            $(".child-categories .error").css('display', 'block');
                                            noChildren = true;
                                            return;
                                        }
                                    }
                                    if (noChildren === false) {
                                        console.log("relationsObject");
                                        console.log(relationsObject);
                                        $(".child-categories .error").css('display', 'none');
                                        $scope.thisCategoryChildren = extractAllChildren(relationsObject);
                                        console.log("$scope.thisCategoryChildren");
                                        console.log($scope.thisCategoryChildren);
                                        $scope.models = {
                                            selected: null,
                                            lists: {"A": []}
                                        };
                                        var max;
                                        max = $scope.thisCategoryChildren.length < 9 ? $scope.thisCategoryChildren.length : 9;                                        
                                        for (var i = 1; i <= max; i++) {
                                            $scope.models.lists["A"].push({
                                                label: i
                                            });
                                        }
                                        $timeout(function () {
                                            //apply scope value to view
                                            $scope.$apply();
                                            $('.chosen-select.childCategory').chosen({"width": "90%"});
                                        });
                                    }   

                                }
                                $timeout(function ()
                                {
                                    if ($scope.category) {
                                        $(".chosen-select.categoryDivider3 option[value='" + $scope.category._id + "']").attr('selected', 'selected');
                                    }
                                    $(".chosen-select.categoryDivider3").trigger("chosen:updated");

                                    //chosen init all subcategories which are labelled
                                    if ($scope.models && $scope.models.lists) {
                                        for (var i = 1; i <= $scope.models.lists["A"].length; i++) {
                                            $("li[identity='" + i + "'] .chosen-select").chosen({width: "90%"});
                                            if ($scope.subCategories) {
                                                //check for order and show selected subcategories if exists
                                                for (var j = 0; j < $scope.subCategories.length; j++) {
                                                    $("li[identity='" + i + "'] .chosen-select option[value='" + $scope.subCategories[i - 1]._id + "']").attr('selected', 'selected');
                                                    $("li[identity='" + i + "'] .chosen-select").trigger("chosen:updated");
                                                }
                                            }
                                        }
                                    }
                                    
                                    $scope.$apply();

                                });
                            });
                            
        //load product types for each product
        $http.get('/relations/getProductTypesByMainCategory?cat_slug=' + $scope.parentCategory.slug)
                .then(function (response) {
                    if (response.data.error == 0) {
                        $scope.productTypes = response.data.data;
                    } else {
                        $scope.error = "No product types for the selected category";
                    }
                    $timeout(function () {
                        //$scope.$apply();

                        /* Initialise all chosen selects*/

                        $('.chosen-select.catId__1').chosen({width: "100%"});
                        $('.chosen-select.catId__2').chosen({width: "100%"});
                        $('.chosen-select.catId__3').chosen({width: "100%"});
                        $('.chosen-select.catId__4').chosen({width: "100%"});

                        $('.chosen-select.supId__1').chosen({width: "100%"});
                        $('.chosen-select.supId__2').chosen({width: "100%"});
                        $('.chosen-select.supId__3').chosen({width: "100%"});
                        $('.chosen-select.supId__4').chosen({width: "100%"});


                        $('.chosen-select.prodType__1').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 1);
                        });
                        $('.chosen-select.prodType__2').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 2);
                        });
                        $('.chosen-select.prodType__3').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 3);
                        });
                        $('.chosen-select.prodType__4').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 4);
                        });
                    });
                });
    }

    //save data
    $scope.submitDivider = function () {
        $scope.error = "";
        var data = {
            dividerNumber: 3,
            isVisible: $scope.isVisible,
        };
        /* Validations */
        //perform validations only if visiblity set to true
        if ($scope.isVisible == true) {

            //check if sectional category selected
            if ($(".chosen-select.categoryDivider3 option:selected").val() == "Select") {
                $scope.error = "Please select the category for this divider section";
                return;
            }

            //check if banner images are uploaded. Atleast 1
            if (!$scope.sortedImages || $scope.sortedImages.length == 0) {
                $scope.error = "No images uploaded for banner";
                return;
            }
            //check if URL's are valid
            $('[data-name="urlForImage"]').each(function (index) {
                if ($(this).val() == "" || isURL($(this).val()) == false) {
                    $scope.error = "Please enter valid URLs for all images";
                    return;
                }
            });

            //check if all 4 products are selected. 
            if ($scope.selectedProducts.length != 4) {
                $scope.error = "Please select all 4 products for this promotional page";
                return;
            }
            
        }
        /* No errors. Fomulate data object to be inserted */
        
        if($scope._id){
            data["_id"] = $scope._id;
        }
        data["products"] = $scope.selectedProducts;
        data["bannerImages"] = [];
        for (var i = 0; i < $scope.sortedImages.length; i++) {
            data["bannerImages"].push({
                name: $scope.sortedImages[i].caption,
                order: i,
                url: $scope.sortedImages[i].url
            });
        }
        var order = [];
        $('ul[identifier="childrenSubcategoryList"] li[eachPromotionalSubcatLi]').each(function (index) {
            order.push($(this).attr('identity'));
        });

        data["sub_categories"] = [];
        if($scope.models){
            for (var i = 1; i <= $scope.models.lists["A"].length; i++) {
                data.sub_categories.push({
                    _id: $('#subCategories__' + i + ' option:selected').val(),
                    name: $('#subCategories__' + i + ' option:selected').attr('data-name'),
                    slug: $('#subCategories__' + i + ' option:selected').attr('data-slug'),
                    order: order.indexOf($('#subCategories__' + i).parents('li').attr('identity'))
                });
            }
        }
        
        data["category"] = {
            _id: $(".chosen-select.categoryDivider3 option:selected").val(),
            name: $(".chosen-select.categoryDivider3 option:selected").attr('data-name'),
            slug: $(".chosen-select.categoryDivider3 option:selected").attr('data-slug')
        };
        //ajax post submit
        if($scope.error == ""){
            $http.post('/promotions/addPromotionData', data, {})
                .success(function (data, status, headers, config) {
                    console.log('success add - ', data);
                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

//                $timeout(function () {
//                    $location.path('/cate');
//                }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error add - ', data);
                    $scope.error = 'Unable to add divider 3 data';
                });
        }

    };

    //image uploader init
    function imageUploaderInit(configObject, imageDivId) {
        $(imageDivId).fileinput(configObject)
                .on('fileuploaded', function (event, data, previewId, index) {
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    $scope.serverImages = data.response[0];
                    configObject.initialPreviewConfig.push({
                        caption: data.response[0].originalname,
                        size: data.response[0].size,
                        width: "120px",
                        key: data.response[i].key
//                        key: data.response[i].filename
                    });
                    configObject.initialPreview.push(data.response[0].location);
                    $scope.sortedImages.push({
                        caption: data.response[0].key.split('/')[2]
//                        caption: data.response[0].filename
                    });
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filebatchuploadsuccess', function (event, data) {
                    //data.response is response from server
                    $scope.serverImages = data.response;
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    for (var i = 0; i < data.response.length; i++) {
                        configObject.initialPreviewConfig.push({
                            caption: data.response[i].originalname,
                            size: data.response[i].size,
                            width: "120px",
                            key: data.response[i].key
//                            key: data.response[i].filename
                        });
                        configObject.initialPreview.push(data.response[i].location);
//                        configObject.initialPreview.push(data.response[i].path);
                        $scope.sortedImages.push({
                            caption: data.response[i].key.split('/')[2]
//                            caption: data.response[i].filename
                        });
                    }
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filesorted', function (event, params) {
                    var oldImages = [];
                    for (var j = 0; j < $scope.sortedImages.length; j++) {
                        oldImages.push($scope.sortedImages[j]);
                    }
                    $scope.sortedImages = [];
                    for (var i = 0; i < params.stack.length; i++) {
                        $.each(oldImages, function (index) {
                            //use regex to see if uploaded image(without timestamp) matches the sorted current image
                            var regex = new RegExp("^" + params.stack[i].caption.split('.')[0] + "-[0-9]{12,20}\.[a-z]{2,4}$")
                            if (params.stack[i].caption == oldImages[index].caption || regex.test(oldImages[index].caption)) {
                                $scope.sortedImages.push({
                                    caption: oldImages[index].caption,
                                    url: oldImages[index].url
                                });
                            }
                        });
                    }
                    //update url set order  
                    $timeout(function () {
                        $scope.$apply();
                    });
                })
                .on('filedeleted', function (event, key) {
                    $scope.sortedImages.splice(key - 1, 1);
                    for (var i = 0; i < configObject.initialPreviewConfig.length; i++) {
                        if (configObject.initialPreviewConfig[i].key == key) {
                            configObject.initialPreviewConfig.splice(i, 1);
                            configObject.initialPreview.splice(i, 1);
                        }
                    }
                    $scope.$apply();
                });
    }

    //extract all children
    function extractAllChildren(relationsObject) {
        var allChildren = [];
        for (var i = 0; i < relationsObject.children.length; i++) {
            allChildren.push({
                _id: relationsObject.children[i]._id,
                name: relationsObject.children[i].name,
                slug: relationsObject.children[i].slug,
                isProductType: relationsObject.children[i].isProductType
            });
            if (relationsObject.children[i].children) {
                for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                    allChildren.push({
                        _id: relationsObject.children[i].children[j]._id,
                        name: relationsObject.children[i].children[j].name,
                        slug: relationsObject.children[i].children[j].slug,
                        isProductType: relationsObject.children[i].children[j].isProductType
                    });
                    if (relationsObject.children[i].children[j].children) {
                        for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                            allChildren.push({
                                _id: relationsObject.children[i].children[j].children[k]._id,
                                name: relationsObject.children[i].children[j].children[k].name,
                                slug: relationsObject.children[i].children[j].children[k].slug,
                                isProductType: relationsObject.children[i].children[j].children[k].isProductType
                            });
                        }
                    }
                }
            }
        }
        return allChildren;
    }
    ;


    //add product(catalogue) ids into dropdowns
    function loadCatalogueIds($scope, selectedProductType, $http, index, isParamsSelected = true) {
        var variantData;
//        $scope.catalogueIds[index] = [];
        if (!$scope.catalogueIds) {
            $scope.catalogueIds = [];
        }
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = []
        }
        $scope.supplierInfo[index] = [];
        $http.get('/product/getCatIdsByProductType?prodTypeId=' + selectedProductType)
                .then(function (responseCatIds) {
                    $scope.catalogueIds[index] = [];
                    if (responseCatIds.data.error == 0) {
                        variantData = responseCatIds.data.data;
                        for (var i = 0; i < responseCatIds.data.data.length; i++) {
                            for (var j = 0; j < responseCatIds.data.data[i].variants.length; j++) {
                                $scope.catalogueIds[index].push({
                                    _id: responseCatIds.data.data[i].variants[j].catalogueId
                                });
                            }
                        }
                    } else {
                        $scope.error = "No catalogue ids under this product type.";
                    }
                    $timeout(function () {
                        $scope.$apply();
                        //update supplier sku list of current indexed selected product
                        if($scope.selectedProducts[index-1] && isParamsSelected == false){
                            //show selected product type ids
                            $('.chosen-select.prodType__' + index + ' option[value="' + $scope.selectedProducts[index-1].product_type_id + '"]').attr('selected', 'selected');
                            $('.chosen-select.prodType__' + index).trigger("chosen:updated");
                            //show selected catalogue ids
                            $('.chosen-select.catId__' + index +' option[value="'+$scope.selectedProducts[index-1].catalogue_id+'"]').attr('selected', 'selected');
                            $('.chosen-select.catId__' + index).trigger('chosen:updated');
                            loadSupplierIds($scope, $scope.selectedProducts[index-1].catalogue_id, variantData, index, false);
                        }
                        //update supplier sku list of current indexed selected product
                        $('.chosen-select.supId__' + index).trigger("chosen:updated");
                        $('.chosen-select.catId__' + index).trigger('chosen:updated');
                        $('.chosen-select.catId__' + index).chosen({width: "100%"}).change(function (event, params) {
                            loadSupplierIds($scope, params.selected, variantData, index);
                        });
                    });
                });
    }

    //add supplier SKU to supplier dropdown
    function loadSupplierIds($scope, selectedCatalogueId, variantData, index, isParamsSelected = true) {
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = [];
        }
        $scope.supplierInfo[index] = [];
        for (var i = 0; i < variantData.length; i++) {
            for (var j = 0; j < variantData[i].variants.length; j++) {
                if (variantData[i].variants[j].catalogueId == selectedCatalogueId) {
                    for (var k = 0; k < variantData[i].variants[j].suppliers.length; k++) {
                        $scope.supplierInfo[index].push({
                            sku: variantData[i].variants[j].suppliers[k].sku
                        });
                    }
                }
            }
        }
        $timeout(function () {
            $scope.$apply();
            $('.chosen-select.supId__' + index).chosen({width: "100%"}).trigger("chosen:updated").change(function (event, params) {
//                if(!$scope.selectedProducts[index]) {
//                $scope.selectedProducts[index - 1] = {}
//                }
                $scope.selectedProducts[index - 1] = {
                    product_type_id: $('.chosen-select.prodType__' + index + ' option:selected').val(),
                    catalogue_id: $('.chosen-select.catId__' + index + ' option:selected').val(),
                    supplier_id: params.selected
                };
            });
            if($scope.selectedProducts[index-1] && isParamsSelected == false){
                $('.chosen-select.supId__' + index+' option[value="'+$scope.selectedProducts[index-1].supplier_id+'"]').attr('selected', 'selected');
                $('.chosen-select.supId__' + index).trigger("chosen:updated");
            }
        });
    }

    //extract children for selected category id
    function extractAllChildrenWithCondition(relationsObject, category_id) {
        var returnArray = [];
        console.log(category_id);
        console.log(category_id);
        for (var i = 0; i < relationsObject.children.length; i++) {
            //found in first column
            if (relationsObject.children[i]._id == category_id) {
                //add all child categories from second column
                for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                    returnArray.push({
                        _id: relationsObject.children[i].children[j]._id,
                        name: relationsObject.children[i].children[j].name,
                        slug: relationsObject.children[i].children[j].slug
                    });
                    //add all child categories from second column
                    for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                        returnArray.push({
                            _id: relationsObject.children[i].children[j].children[k]._id,
                            name: relationsObject.children[i].children[j].children[k].name,
                            slug: relationsObject.children[i].children[j].children[k].slug
                        });
                    }
                }
            }
            /*also look in second column for matches of selected category 
             since category maybe attached at multiple places */
//            else {
            for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                //if match found in second column
                if (relationsObject.children[i].children[j]._id == category_id) {
                    //add all child categories from column 3
                    for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                        returnArray.push({
                            _id: relationsObject.children[i].children[j].children[k]._id,
                            name: relationsObject.children[i].children[j].children[k].name,
                            slug: relationsObject.children[i].children[j].children[k].slug
                        });
                    }
                }
            }
//            }
        }
        return returnArray;
    }
});

app.controller('PromotionsTab4Controller', function ($scope, $http, $timeout, $location) {

//    $scope.selectedCategorySlug = (window.location.href.split('#')[1]).split('/')[3];
    $scope.selectedProducts = [];
    
    $scope.sortedImages = [];
    
    //Image initialisation
    var configObject = {
        initialPreviewAsData: true,
        uploadUrl: "/promotions/uploadImage/",
        deleteUrl: "/promotions/deleteImage/",
        uploadAsync: false,
        previewSettings: {image: {width: "120px", height: "40px"}},
        overwriteInitial: false,
        allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],
        maxFileSize: 3000,
        fileActionSettings: {
            showUpload: false
        }
    };

    //toggle divider visiblity
    $scope.toggleVisiblity = function () {
        $scope.isVisible = !$scope.isVisible;
    };
    
    $("#categories4.chosen-select").chosen({width: "100%"}).change(function(event, params){
        //on change current sectional category
            console.log(params.selected);
            console.log("Params.selected");
            $scope.parentCategory = {
                _id: params.selected,
                name: $("[value='"+params.selected+"']").attr("data-name"),
                slug: $("[value='"+params.selected+"']").attr("data-slug")
            }
            console.log("$scope.parentCategory");
            console.log($scope.parentCategory);
            getAllRelations(params);
            
    });

    //Get All Main categories
    $http.get('/category/getAllMainCategories')
                .then(function(response){
                    if(response.data.data.length == 0) {
                        $scope.promotionsError = "No active  main categories found";
                        $(".error-modal-lg").modal();
                        return;
                    }
                    else {
                        console.log("response.data.data");
                        console.log(response.data.data);
                        $scope.mainCategories = response.data.data;
                        
                        $timeout(function(){
                            $scope.$apply();
                            $("#categories4.chosen-select").trigger("chosen:updated");
                        });
                    }
                });

    $scope.reInitThisChosen = function (item) {
        var $selectedOption = $("li[identity='" + item.label + "'] select option:selected");
        $("li[identity='" + item.label + "'] select option[value='" + $selectedOption.val() + "']").attr('selected', 'selected');
        $("li[identity='" + item.label + "'] .chosen-select").chosen({width: "90%"});
    };

    //get existing data for selected category
    $http.get('/promotions/get')
            .then(function (response) {
                console.log("response");
                console.log(response);
                if (response.data.data.length == 0) {
                    console.log("before init");
                    imageUploaderInit(configObject, '#bannerImages4');
                } else {
//                    $scope.category = response.data.data[0].category;
//                    $scope.parentCategory = response.data.data[0].category;

                    /* Code for page edit i.e viewing page after saving it earlier */
                    for (var i = 0; i < response.data.data[0].dividers.length; i++) {
                        if (response.data.data[0].dividers[i].dividerNumber == 4) {
                            $scope.isVisible = response.data.data[0].dividers[i].isVisible;
                            $scope.selectedProducts = response.data.data[0].dividers[i].products;
                            $scope.subCategories = response.data.data[0].dividers[i].sub_categories;
                            //selected sub category for this divider section
                            $scope.category = response.data.data[0].dividers[i].category;
                            //category whose promotion page is being edited/created
                            if (!configObject.initialPreviewConfig) {
                                configObject.initialPreviewConfig = [];
                            }
                            if (!configObject.initialPreview) {
                                configObject.initialPreview = [];
                            }
                            if (!$scope.sortedImages) {
                                $scope.sortedImages = [];
                            }

                            /* Show banner images if already selected */
                            for (var j = 0; j < response.data.data[0].dividers[i].bannerImages.length; j++) {
                                configObject.initialPreviewConfig.push({
                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                    width: "120px",
                                    key: "promotions/home/" + response.data.data[0].dividers[i].bannerImages[j].name
                                });
                                var imageUrl = s3_bucket + "promotions/home/" + response.data.data[0].dividers[i].bannerImages[j].name;
                                configObject.initialPreview.push(imageUrl);
                                $scope.sortedImages.push({
                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                    url: response.data.data[0].dividers[i].bannerImages[j].url
                                });
                            }

                            /* For selected products */
                            if (!$scope.catalogueIds) {
                                $scope.catalogueIds = [];
                            }
                            if (!$scope.supplierInfo) {
                                $scope.supplierInfo = [];
                            }
                            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                                loadCatalogueIds($scope, $scope.selectedProducts[i].product_type_id, $http, i+1, false);
                            }
                           
                            $scope.parentCategory = $scope.category
                            getAllRelations({selected : $scope.category._id});
                        }
                    }
                    imageUploaderInit(configObject, '#bannerImages4');
                    $scope._id = response.data.data[0]._id;
                }
            });

    function getAllRelations(params){
        $http.get('/relations/getById?catId=' + $scope.parentCategory._id)
                            .then(function (response) {
                                console.log("response of relations");
                                console.log(response);
                                var relationsObject = response.data[0];
                                $scope.allChildren = extractAllChildren(response.data[0]);
                                $scope.category = $scope.parentCategory;
                                //check if category is already selected previously i.e page is being edited and not created
                                if ($scope.category) {
                                    console.log("$scope.allChildren");
                                    console.log($scope.allChildren);
                                    var noChildren = false;
                                    for (var i = 0; i < $scope.allChildren.length; i++) {
                                        if ($scope.allChildren[i]._id == params.selected && $scope.allChildren[i].isProductType == true) {
                                            $(".child-categories .error").html("Selected category is a product type, hence no child categories");
                                            $(".child-categories .error").css('display', 'block');
                                            noChildren = true;
                                            return;
                                        }
                                    }
                                    if (noChildren === false) {
                                        console.log("relationsObject");
                                        console.log(relationsObject);
                                        $(".child-categories .error").css('display', 'none');
                                        $scope.thisCategoryChildren = extractAllChildren(relationsObject);
                                        console.log("$scope.thisCategoryChildren");
                                        console.log($scope.thisCategoryChildren);
                                        $scope.models = {
                                            selected: null,
                                            lists: {"A": []}
                                        };
                                        var max;
                                        max = $scope.thisCategoryChildren.length < 9 ? $scope.thisCategoryChildren.length : 9;                                        
                                        for (var i = 1; i <= max; i++) {
                                            $scope.models.lists["A"].push({
                                                label: i
                                            });
                                        }
                                        $timeout(function () {
                                            //apply scope value to view
                                            $scope.$apply();
                                            $('.chosen-select.childCategory').chosen({"width": "90%"});
                                        });
                                    }   

                                }
                                $timeout(function ()
                                {
//                                    if ($scope.category) {
//                                        $(".chosen-select.categoryDivider3 option[value='" + $scope.category._id + "']").attr('selected', 'selected');
//                                    }
//                                    $(".chosen-select.categoryDivider3").trigger("chosen:updated");

                                    //chosen init all subcategories which are labelled
                                    if ($scope.models && $scope.models.lists) {
                                        for (var i = 1; i <= $scope.models.lists["A"].length; i++) {
                                            $("li[identity='" + i + "'] .chosen-select").chosen({width: "90%"});
                                            if ($scope.subCategories) {
                                                //check for order and show selected subcategories if exists
                                                for (var j = 0; j < $scope.subCategories.length; j++) {
                                                    $("li[identity='" + i + "'] .chosen-select option[value='" + $scope.subCategories[i - 1]._id + "']").attr('selected', 'selected');
                                                    $("li[identity='" + i + "'] .chosen-select").trigger("chosen:updated");
                                                }
                                            }
                                        }
                                    }
                                    
                                    $scope.$apply();

                                });
                            });
                            
        //load product types for each product
        $http.get('/relations/getProductTypesByMainCategory?cat_slug=' + $scope.parentCategory.slug)
                .then(function (response) {
                    if (response.data.error == 0) {
                        $scope.productTypes = response.data.data;
                    } else {
                        $scope.error = "No product types for the selected category";
                    }
                    $timeout(function () {
                        //$scope.$apply();

                        /* Initialise all chosen selects*/

                        $('.chosen-select.catId__1').chosen({width: "100%"});
                        $('.chosen-select.catId__2').chosen({width: "100%"});
                        $('.chosen-select.catId__3').chosen({width: "100%"});
                        $('.chosen-select.catId__4').chosen({width: "100%"});

                        $('.chosen-select.supId__1').chosen({width: "100%"});
                        $('.chosen-select.supId__2').chosen({width: "100%"});
                        $('.chosen-select.supId__3').chosen({width: "100%"});
                        $('.chosen-select.supId__4').chosen({width: "100%"});


                        $('.chosen-select.prodType__1').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 1);
                        });
                        $('.chosen-select.prodType__2').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 2);
                        });
                        $('.chosen-select.prodType__3').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 3);
                        });
                        $('.chosen-select.prodType__4').chosen({width: "100%"}).change(function (event, params) {
                            loadCatalogueIds($scope, params.selected, $http, 4);
                        });
                    });
                });
    }

    //save data
    $scope.submitDivider = function () {
        $scope.error = "";
        var data = {
            dividerNumber: 4,
            isVisible: $scope.isVisible,
        };
        /* Validations */
        //perform validations only if visiblity set to true
        if ($scope.isVisible == true) {

            //check if sectional category selected
            if ($(".chosen-select.categoryDivider4 option:selected").val() == "Select") {
                $scope.error = "Please select the category for this divider section";
                return;
            }

            //check if banner images are uploaded. Atleast 1
            if (!$scope.sortedImages || $scope.sortedImages.length == 0) {
                $scope.error = "No images uploaded for banner";
                return;
            }
            //check if URL's are valid
            $('[data-name="urlForImage"]').each(function (index) {
                if ($(this).val() == "" || isURL($(this).val()) == false) {
                    $scope.error = "Please enter valid URLs for all images";
                    return;
                }
            });

            //check if all 4 products are selected. 
            if ($scope.selectedProducts.length != 4) {
                $scope.error = "Please select all 4 products for this promotional page";
                return;
            }

        }
        /* No errors. Fomulate data object to be inserted */
        
        if($scope._id){
            data["_id"] = $scope._id;
        }
        data["products"] = $scope.selectedProducts;
        data["bannerImages"] = [];
        for (var i = 0; i < $scope.sortedImages.length; i++) {
            data["bannerImages"].push({
                name: $scope.sortedImages[i].caption,
                order: i,
                url: $scope.sortedImages[i].url
            });
        }
        var order = [];
        $('ul[identifier="childrenSubcategoryList"] li[eachPromotionalSubcatLi]').each(function (index) {
            order.push($(this).attr('identity'));
        });

        data["sub_categories"] = [];
        if($scope.models){
            for (var i = 1; i <= $scope.models.lists["A"].length; i++) {
                data.sub_categories.push({
                    _id: $('#subCategories__' + i + ' option:selected').val(),
                    name: $('#subCategories__' + i + ' option:selected').attr('data-name'),
                    slug: $('#subCategories__' + i + ' option:selected').attr('data-slug'),
                    order: order.indexOf($('#subCategories__' + i).parents('li').attr('identity'))
                });
            }
        }
        
        data["category"] = {
            _id: $(".chosen-select.categoryDivider4 option:selected").val(),
            name: $(".chosen-select.categoryDivider4 option:selected").attr('data-name'),
            slug: $(".chosen-select.categoryDivider4 option:selected").attr('data-slug')
        };
        //ajax post submit
        if($scope.error == ""){
            $http.post('/promotions/addPromotionData', data, {})
                .success(function (data, status, headers, config) {
                    console.log('success add - ', data);
                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

//                $timeout(function () {
//                    $location.path('/cate');
//                }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error add - ', data);
                    $scope.error = 'Unable to add divider 4 data';
                });
        }

    };

    //image uploader init
    function imageUploaderInit(configObject, imageDivId) {
        $(imageDivId).fileinput(configObject)
                .on('fileuploaded', function (event, data, previewId, index) {
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    $scope.serverImages = data.response[0];
                    configObject.initialPreviewConfig.push({
                        caption: data.response[0].originalname,
                        size: data.response[0].size,
                        width: "120px",
                        key: data.response[i].key
//                        key: data.response[i].filename
                    });
                    configObject.initialPreview.push(data.response[0].location);
                    $scope.sortedImages.push({
                        caption: data.response[0].key.split('/')[2]
//                        caption: data.response[0].filename
                    });
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filebatchuploadsuccess', function (event, data) {
                    //data.response is response from server
                    $scope.serverImages = data.response;
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    for (var i = 0; i < data.response.length; i++) {
                        configObject.initialPreviewConfig.push({
                            caption: data.response[i].originalname,
                            size: data.response[i].size,
                            width: "120px",
                            key: data.response[i].key
//                            key: data.response[i].filename
                        });
                        configObject.initialPreview.push(data.response[i].location);
//                        configObject.initialPreview.push(data.response[i].path);
                        $scope.sortedImages.push({
                            caption: data.response[i].key.split('/')[2]
//                            caption: data.response[i].filename
                        });
                    }
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filesorted', function (event, params) {
                    var oldImages = [];
                    for (var j = 0; j < $scope.sortedImages.length; j++) {
                        oldImages.push($scope.sortedImages[j]);
                    }
                    $scope.sortedImages = [];
                    for (var i = 0; i < params.stack.length; i++) {
                        $.each(oldImages, function (index) {
                            //use regex to see if uploaded image(without timestamp) matches the sorted current image
                            var regex = new RegExp("^" + params.stack[i].caption.split('.')[0] + "-[0-9]{12,20}\.[a-z]{2,4}$")
                            if (params.stack[i].caption == oldImages[index].caption || regex.test(oldImages[index].caption)) {
                                $scope.sortedImages.push({
                                    caption: oldImages[index].caption,
                                    url: oldImages[index].url
                                });
                            }
                        });
                    }
                    //update url set order  
                    $timeout(function () {
                        $scope.$apply();
                    });
                })
                .on('filedeleted', function (event, key) {
                    $scope.sortedImages.splice(key - 1, 1);
                    for (var i = 0; i < configObject.initialPreviewConfig.length; i++) {
                        if (configObject.initialPreviewConfig[i].key == key) {
                            configObject.initialPreviewConfig.splice(i, 1);
                            configObject.initialPreview.splice(i, 1);
                        }
                    }
                    $scope.$apply();
                });
    }

    //extract all children
    function extractAllChildren(relationsObject) {
        var allChildren = [];
        for (var i = 0; i < relationsObject.children.length; i++) {
            allChildren.push({
                _id: relationsObject.children[i]._id,
                name: relationsObject.children[i].name,
                slug: relationsObject.children[i].slug,
                isProductType: relationsObject.children[i].isProductType
            });
            if (relationsObject.children[i].children) {
                for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                    allChildren.push({
                        _id: relationsObject.children[i].children[j]._id,
                        name: relationsObject.children[i].children[j].name,
                        slug: relationsObject.children[i].children[j].slug,
                        isProductType: relationsObject.children[i].children[j].isProductType
                    });
                    if (relationsObject.children[i].children[j].children) {
                        for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                            allChildren.push({
                                _id: relationsObject.children[i].children[j].children[k]._id,
                                name: relationsObject.children[i].children[j].children[k].name,
                                slug: relationsObject.children[i].children[j].children[k].slug,
                                isProductType: relationsObject.children[i].children[j].children[k].isProductType
                            });
                        }
                    }
                }
            }
        }
        return allChildren;
    }
    ;


    //add product(catalogue) ids into dropdowns
    function loadCatalogueIds($scope, selectedProductType, $http, index, isParamsSelected = true) {
        var variantData;
//        $scope.catalogueIds[index] = [];
        if (!$scope.catalogueIds) {
            $scope.catalogueIds = [];
        }
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = []
        }
        $scope.supplierInfo[index] = [];
        $http.get('/product/getCatIdsByProductType?prodTypeId=' + selectedProductType)
                .then(function (responseCatIds) {
                    $scope.catalogueIds[index] = [];
                    if (responseCatIds.data.error == 0) {
                        variantData = responseCatIds.data.data;
                        for (var i = 0; i < responseCatIds.data.data.length; i++) {
                            for (var j = 0; j < responseCatIds.data.data[i].variants.length; j++) {
                                $scope.catalogueIds[index].push({
                                    _id: responseCatIds.data.data[i].variants[j].catalogueId
                                });
                            }
                        }
                    } else {
                        $scope.error = "No catalogue ids under this product type.";
                    }
                    $timeout(function () {
                        $scope.$apply();
                        //update supplier sku list of current indexed selected product
                        if($scope.selectedProducts[index-1] && isParamsSelected == false){
                            //show selected product type ids
                            $('.chosen-select.prodType__' + index + ' option[value="' + $scope.selectedProducts[index-1].product_type_id + '"]').attr('selected', 'selected');
                            $('.chosen-select.prodType__' + index).trigger("chosen:updated");
                            //show selected catalogue ids
                            $('.chosen-select.catId__' + index +' option[value="'+$scope.selectedProducts[index-1].catalogue_id+'"]').attr('selected', 'selected');
                            $('.chosen-select.catId__' + index).trigger('chosen:updated');
                            loadSupplierIds($scope, $scope.selectedProducts[index-1].catalogue_id, variantData, index, false);
                        }
                        //update supplier sku list of current indexed selected product
                        $('.chosen-select.supId__' + index).trigger("chosen:updated");
                        $('.chosen-select.catId__' + index).trigger('chosen:updated');
                        $('.chosen-select.catId__' + index).chosen({width: "100%"}).change(function (event, params) {
                            loadSupplierIds($scope, params.selected, variantData, index);
                        });
                    });
                });
    }

    //add supplier SKU to supplier dropdown
    function loadSupplierIds($scope, selectedCatalogueId, variantData, index, isParamsSelected = true) {
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = [];
        }
        $scope.supplierInfo[index] = [];
        for (var i = 0; i < variantData.length; i++) {
            for (var j = 0; j < variantData[i].variants.length; j++) {
                if (variantData[i].variants[j].catalogueId == selectedCatalogueId) {
                    for (var k = 0; k < variantData[i].variants[j].suppliers.length; k++) {
                        $scope.supplierInfo[index].push({
                            sku: variantData[i].variants[j].suppliers[k].sku
                        });
                    }
                }
            }
        }
        $timeout(function () {
            $scope.$apply();
            $('.chosen-select.supId__' + index).chosen({width: "100%"}).trigger("chosen:updated").change(function (event, params) {
//                if(!$scope.selectedProducts[index]) {
//                $scope.selectedProducts[index - 1] = {}
//                }
                $scope.selectedProducts[index - 1] = {
                    product_type_id: $('.chosen-select.prodType__' + index + ' option:selected').val(),
                    catalogue_id: $('.chosen-select.catId__' + index + ' option:selected').val(),
                    supplier_id: params.selected
                };
            });
            if($scope.selectedProducts[index-1] && isParamsSelected == false){
                $('.chosen-select.supId__' + index+' option[value="'+$scope.selectedProducts[index-1].supplier_id+'"]').attr('selected', 'selected');
                $('.chosen-select.supId__' + index).trigger("chosen:updated");
            }
        });
    }

    //extract children for selected category id
    function extractAllChildrenWithCondition(relationsObject, category_id) {
        var returnArray = [];
        console.log(category_id);
        console.log(category_id);
        for (var i = 0; i < relationsObject.children.length; i++) {
            //found in first column
            if (relationsObject.children[i]._id == category_id) {
                //add all child categories from second column
                for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                    returnArray.push({
                        _id: relationsObject.children[i].children[j]._id,
                        name: relationsObject.children[i].children[j].name,
                        slug: relationsObject.children[i].children[j].slug
                    });
                    //add all child categories from second column
                    for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                        returnArray.push({
                            _id: relationsObject.children[i].children[j].children[k]._id,
                            name: relationsObject.children[i].children[j].children[k].name,
                            slug: relationsObject.children[i].children[j].children[k].slug
                        });
                    }
                }
            }
            /*also look in second column for matches of selected category 
             since category maybe attached at multiple places */
//            else {
            for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                //if match found in second column
                if (relationsObject.children[i].children[j]._id == category_id) {
                    //add all child categories from column 3
                    for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                        returnArray.push({
                            _id: relationsObject.children[i].children[j].children[k]._id,
                            name: relationsObject.children[i].children[j].children[k].name,
                            slug: relationsObject.children[i].children[j].children[k].slug
                        });
                    }
                }
            }
//            }
        }
        return returnArray;
    }

});

app.controller('PromotionsTab5Controller', function ($scope, $http, $timeout, $location) {
    $scope.isVisible = false;
    var index = 0;
    $scope.variantData = [];
    $scope.featuredProducts = [];
    //toggle divider visiblity
    $scope.toggleVisiblity = function(){
        $scope.isVisible = !$scope.isVisible;
    };
    
    $('.chosen-select.prodType__0').chosen({width: "100%"}).change(function (event, params) {
        loadCatalogueIds($scope, params.selected, $http);
    });
    
    $('.chosen-select.catId__0').chosen({width: "100%"}).change(function (event, params) {
        loadSupplierIds($scope, params.selected, 0);
    });
    
    $('.chosen-select.supId__0').chosen({width: "100%"});
    
    //get existing data if any
    $http.get('/promotions/get')
            .then(function (response) {
                if(response.data.data.length > 0){
                    $scope._id = response.data.data[0]._id;
                }
                //isVisible not set on featuredProducts property in schema
                if(response.data.data[0].isVisible){
                    $scope.isVisible = response.data.data[0].isVisible;
                }
                if (response.data.data[0].featuredProducts.length > 0) {
                    $scope.featuredProducts = response.data.data[0].featuredProducts;
                } else {
                    $scope.featuredProducts = [];
                }
                    
                //edit added new launches product
//                    $scope.editFeaturedProducts = function (productInfo) {
//                        $('chosen-select.mainCat option[value="'+productInfo.category_id+'"]').attr('selected', 'selected').trigger("chosen:updated")
//                        console.log(productInfo);
//                        $timeout(function(){
//                            $scope.$apply();
//                        });
//                    };
                //delete existing new launches product
                $scope.deleteFeaturedProducts= function (productInfo) {
                    for (var i = 0; i < $scope.featuredProducts.length; i++) {
                        if(productInfo.catalogue_id == $scope.featuredProducts[i].catalogue_id && productInfo.category_id == $scope.featuredProducts[i].category_id && productInfo.product_type_id == $scope.featuredProducts[i].product_type_id && productInfo.supplier_id == $scope.featuredProducts[i].supplier_id) {
                            $scope.featuredProducts.splice(i, 1);
                        }
                    }
                    $timeout(function(){
                        $scope.$apply();
                    });
                };
            });
            
    //get all main categories
        //Get All Main categories
    $http.get('/category/getAllMainCategories')
                .then(function(response){
                    if(response.data.data.length == 0) {
                        $scope.promotionsError = "No active  main categories found";
                        $(".error-modal-lg").modal();
                        return;
                    }
                    else {
                        console.log("response.data.data");
                        console.log(response.data.data);
                        $scope.mainCategories = response.data.data;
                        
                        $timeout(function(){
                            $scope.$apply();
                            $(".categories.chosen-select").trigger("chosen:updated");
                        });
                    }
                });
    
    
    $(".categories.chosen-select").chosen({width: "100%"}).change(function(event, params){
        var selectedCategorySlug = $("[value='"+params.selected+"']").attr('data-slug');
        $http.get('/relations/getProductTypesByMainCategory?cat_slug=' + selectedCategorySlug)
            .then(function (response) {
                
                $scope.catalogueIds = [];
                if(!$scope.supplierInfo){
                    $scope.supplierInfo = [];
                }
                $scope.supplierInfo[index] = [];
                $scope.productTypes = [];
                
                if (response.data.error == 0) {
                    $scope.productTypes = response.data.data;
                } else {
                    $scope.error = "No product types for the selected category";
                }
                $timeout(function () {
                    $scope.$apply();
                    
                    $('.chosen-select.prodType__0').trigger("chosen:updated");
                    $('.chosen-select.catId__0').chosen({width: "100%"});
                    $('.chosen-select.supId__0').chosen({width: "100%"}).trigger("chosen:updated");
                });
            });
    
    });
    
    //save divider data
    $scope.submitDivider = function(){
        var category_id = $(".chosen-select.categories option:selected").val();
        var supplier_id = $(".chosen-select.supId__0 option:selected").val();
        var catalogue_id = $(".chosen-select.catId__0 option:selected").val();
        var product_type_id = $(".chosen-select.prodType__0 option:selected").val();
        //validation
        for (var i = 0; i < $scope.featuredProducts.length; i++) {
            if ($scope.featuredProducts[i].supplier_id == supplier_id && $scope.featuredProducts[i].catalogue_id == catalogue_id) {
                $scope.error = "Same product already exists. Please select another";
                return;
            }
        }
        if($(".chosen-select.categories option:selected").val() == "Select"){
            $scope.error = "Select a category";
            return;
        }
        if($(".chosen-select.prodType__0 option:selected").val() == "Select"){
            $scope.error = "Select a product type";
            return;
        }
        if($(".chosen-select.catId__0 option:selected").val() == "Select"){
            $scope.error = "Select a catalogue id";
            return;
        }
        if($(".chosen-select.supId__0 option:selected").val() == "Select"){
            $scope.error = "Select a supplier id";
            return;
        }
        $scope.featuredProducts.push({
            category_id: category_id,
            product_type_id: product_type_id,
            catalogue_id: catalogue_id,
            supplier_id: supplier_id
        });
        var data = {
            dividerNumber: 5,
            featuredProducts: $scope.featuredProducts,
            isVisible: $scope.isVisible
        };
        if($scope._id){
            data._id = $scope._id;
        }
        $http.post('/promotions/addPromotionData', data, {})
                .success(function (data, status, headers, config) {
                    console.log('success add - ', data);

                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    console.log("data.msg");
                    console.log(data.msg);
                    $scope.success = data.msg;

//                $timeout(function () {
//                    $location.path('/cate');
//                }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error add - ', data);
                    $scope.error = 'Unable to add product';
                });
    };
    
    //add product(catalogue) ids into dropdowns
    function loadCatalogueIds($scope, selectedProductType, $http) {
        index = 0;
        if (!$scope.catalogueIds) {
            $scope.catalogueIds = [];
        }
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = []
        }
        $scope.supplierInfo[index] = [];
        $http.get('/product/getCatIdsByProductType?prodTypeId=' + selectedProductType)
                .then(function (responseCatIds) {
                    $scope.catalogueIds = [];
                    if (responseCatIds.data.error == 0) {
                        $scope.variantData = responseCatIds.data.data;
                        for (var i = 0; i < responseCatIds.data.data.length; i++) {
                            for (var j = 0; j < responseCatIds.data.data[i].variants.length; j++) {
                                $scope.catalogueIds.push({
                                    _id: responseCatIds.data.data[i].variants[j].catalogueId
                                });
                            }
                        }
                    } else {
                        $scope.error = "No catalogue ids under this product type.";
                    }
                    $timeout(function () {
                        $scope.$apply();
                        //update supplier sku list of current indexed selected product
                        //show selected product type ids
                        if($scope.selectedProducts && $scope.selectedProducts[index]) {
                            $('.chosen-select.prodType__' + index + ' option[value="' + $scope.selectedProducts[index].product_type_id + '"]').attr('selected', 'selected');
                            $('.chosen-select.prodType__' + index).trigger("chosen:updated");
                            //show selected catalogue ids
                            $('.chosen-select.catId__' + index +' option[value="'+$scope.selectedProducts[index].catalogue_id+'"]').attr('selected', 'selected');
                            $('.chosen-select.catId__' + index).trigger('chosen:updated');
                            loadSupplierIds($scope, $scope.selectedProducts[index].catalogue_id, variantData, index);
                        }
                        //update supplier sku list of current indexed selected product
                        $('.chosen-select.supId__' + index).trigger("chosen:updated");
                        $('.chosen-select.catId__' + index).trigger('chosen:updated');      
                    });
                });
    }
    
    //add supplier SKU to supplier dropdown
    function loadSupplierIds($scope, selectedCatalogueId, index) {
        $scope.supplierInfo[index] = [];
        console.log("$scope.variantData");
        console.log($scope.variantData);
        console.log(selectedCatalogueId);
        for (var i = 0; i < $scope.variantData.length; i++) {
            for (var j = 0; j < $scope.variantData[i].variants.length; j++) {
                if ($scope.variantData[i].variants[j].catalogueId == selectedCatalogueId) {
                    for (var k = 0; k < $scope.variantData[i].variants[j].suppliers.length; k++) {
                        $scope.supplierInfo[index].push({
                            sku: $scope.variantData[i].variants[j].suppliers[k].sku
                        });
                    }
                }
            }
        }
        $timeout(function () {
            $scope.$apply();
            $('.chosen-select.supId__0').trigger("chosen:updated");
        });
    }


});

app.controller('PromotionsTab6Controller', function ($scope, $http, $timeout, $location) {
    $scope.isVisible = false;
    var index = 0;
    $scope.variantData = [];
    $scope.newLaunches = [];
    //toggle divider visiblity
    $scope.toggleVisiblity = function(){
        $scope.isVisible = !$scope.isVisible;
    };
    
    $('.chosen-select.prodType__0').chosen({width: "100%"}).change(function (event, params) {
        loadCatalogueIds($scope, params.selected, $http);
    });
    
    $('.chosen-select.catId__0').chosen({width: "100%"}).change(function (event, params) {
        loadSupplierIds($scope, params.selected, 0);
    });
    
    $('.chosen-select.supId__0').chosen({width: "100%"});
    
    //get existing data if any
    $http.get('/promotions/get')
            .then(function (response) {
                if(response.data.data.length > 0){
                    $scope._id = response.data.data[0]._id;
                }
                //isVisible not set on featuredProducts property in schema
                if(response.data.data[0].isVisible){
                    $scope.isVisible = response.data.data[0].isVisible;
                }
                if (response.data.data[0].newLaunches.length > 0) {
                    $scope.newLaunches = response.data.data[0].newLaunches;
                } else {
                    $scope.newLaunches = [];
                }
                    
                //edit added new launches product
//                    $scope.editFeaturedProducts = function (productInfo) {
//                        $('chosen-select.mainCat option[value="'+productInfo.category_id+'"]').attr('selected', 'selected').trigger("chosen:updated")
//                        console.log(productInfo);
//                        $timeout(function(){
//                            $scope.$apply();
//                        });
//                    };
                //delete existing new launches product
                $scope.deleteNewLaunches= function (productInfo) {
                    for (var i = 0; i < $scope.newLaunches.length; i++) {
                        if(productInfo.catalogue_id == $scope.newLaunches[i].catalogue_id && productInfo.category_id == $scope.newLaunches[i].category_id && productInfo.product_type_id == $scope.newLaunches[i].product_type_id && productInfo.supplier_id == $scope.newLaunches[i].supplier_id) {
                            $scope.newLaunches.splice(i, 1);
                        }
                    }
                    $timeout(function(){
                        $scope.$apply();
                    });
                };
            });
            
    //get all main categories
        //Get All Main categories
    $http.get('/category/getAllMainCategories')
                .then(function(response){
                    if(response.data.data.length == 0) {
                        $scope.promotionsError = "No active  main categories found";
                        $(".error-modal-lg").modal();
                        return;
                    }
                    else {
                        console.log("response.data.data");
                        console.log(response.data.data);
                        $scope.mainCategories = response.data.data;
                        
                        $timeout(function(){
                            $scope.$apply();
                            $(".categories.chosen-select").trigger("chosen:updated");
                        });
                    }
                });
    
    
    $(".categories.chosen-select").chosen({width: "100%"}).change(function(event, params){
        var selectedCategorySlug = $("[value='"+params.selected+"']").attr('data-slug');
        $http.get('/relations/getProductTypesByMainCategory?cat_slug=' + selectedCategorySlug)
            .then(function (response) {
                
                $scope.catalogueIds = [];
                if(!$scope.supplierInfo){
                    $scope.supplierInfo = [];
                }
                $scope.supplierInfo[index] = [];
                $scope.productTypes = [];
                
                if (response.data.error == 0) {
                    $scope.productTypes = response.data.data;
                } else {
                    $scope.error = "No product types for the selected category";
                }
                $timeout(function () {
                    $scope.$apply();
                    
                    $('.chosen-select.prodType__0').trigger("chosen:updated");
                    $('.chosen-select.catId__0').chosen({width: "100%"});
                    $('.chosen-select.supId__0').chosen({width: "100%"}).trigger("chosen:updated");
                });
            });
    
    });
    
    //save divider data
    $scope.submitDivider = function(){
        var category_id = $(".chosen-select.categories option:selected").val();
        var supplier_id = $(".chosen-select.supId__0 option:selected").val();
        var catalogue_id = $(".chosen-select.catId__0 option:selected").val();
        var product_type_id = $(".chosen-select.prodType__0 option:selected").val();
        //validation
        for (var i = 0; i < $scope.newLaunches.length; i++) {
            if ($scope.newLaunches[i].supplier_id == supplier_id && $scope.newLaunches[i].catalogue_id == catalogue_id) {
                $scope.error = "Same product already exists. Please select another";
                return;
            }
        }
        if($(".chosen-select.categories option:selected").val() == "Select"){
            $scope.error = "Select a category";
            return;
        }
        if($(".chosen-select.prodType__0 option:selected").val() == "Select"){
            $scope.error = "Select a product type";
            return;
        }
        if($(".chosen-select.catId__0 option:selected").val() == "Select"){
            $scope.error = "Select a catalogue id";
            return;
        }
        if($(".chosen-select.supId__0 option:selected").val() == "Select"){
            $scope.error = "Select a supplier id";
            return;
        }
        $scope.newLaunches.push({
            category_id: category_id,
            product_type_id: product_type_id,
            catalogue_id: catalogue_id,
            supplier_id: supplier_id
        });
        var data = {
            dividerNumber: 6,
            newLaunches: $scope.newLaunches,
            isVisible: $scope.isVisible
        };
        if($scope._id){
            data._id = $scope._id;
        }
        $http.post('/promotions/addPromotionData', data, {})
                .success(function (data, status, headers, config) {
                    console.log('success add - ', data);

                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    console.log("data.msg");
                    console.log(data.msg);
                    $scope.success = data.msg;

//                $timeout(function () {
//                    $location.path('/cate');
//                }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error add - ', data);
                    $scope.error = 'Unable to add product';
                });
    };
    
    //add product(catalogue) ids into dropdowns
    function loadCatalogueIds($scope, selectedProductType, $http) {
        index = 0;
        if (!$scope.catalogueIds) {
            $scope.catalogueIds = [];
        }
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = []
        }
        $scope.supplierInfo[index] = [];
        $http.get('/product/getCatIdsByProductType?prodTypeId=' + selectedProductType)
                .then(function (responseCatIds) {
                    $scope.catalogueIds = [];
                    if (responseCatIds.data.error == 0) {
                        $scope.variantData = responseCatIds.data.data;
                        for (var i = 0; i < responseCatIds.data.data.length; i++) {
                            for (var j = 0; j < responseCatIds.data.data[i].variants.length; j++) {
                                $scope.catalogueIds.push({
                                    _id: responseCatIds.data.data[i].variants[j].catalogueId
                                });
                            }
                        }
                    } else {
                        $scope.error = "No catalogue ids under this product type.";
                    }
                    $timeout(function () {
                        $scope.$apply();
                        //update supplier sku list of current indexed selected product
                        //show selected product type ids
                        if($scope.selectedProducts && $scope.selectedProducts[index]) {
                            $('.chosen-select.prodType__' + index + ' option[value="' + $scope.selectedProducts[index].product_type_id + '"]').attr('selected', 'selected');
                            $('.chosen-select.prodType__' + index).trigger("chosen:updated");
                            //show selected catalogue ids
                            $('.chosen-select.catId__' + index +' option[value="'+$scope.selectedProducts[index].catalogue_id+'"]').attr('selected', 'selected');
                            $('.chosen-select.catId__' + index).trigger('chosen:updated');
                            loadSupplierIds($scope, $scope.selectedProducts[index].catalogue_id, variantData, index);
                        }
                        //update supplier sku list of current indexed selected product
                        $('.chosen-select.supId__' + index).trigger("chosen:updated");
                        $('.chosen-select.catId__' + index).trigger('chosen:updated');      
                    });
                });
    }
    
    //add supplier SKU to supplier dropdown
    function loadSupplierIds($scope, selectedCatalogueId, index) {
        $scope.supplierInfo[index] = [];
        console.log("$scope.variantData");
        console.log($scope.variantData);
        console.log(selectedCatalogueId);
        for (var i = 0; i < $scope.variantData.length; i++) {
            for (var j = 0; j < $scope.variantData[i].variants.length; j++) {
                if ($scope.variantData[i].variants[j].catalogueId == selectedCatalogueId) {
                    for (var k = 0; k < $scope.variantData[i].variants[j].suppliers.length; k++) {
                        $scope.supplierInfo[index].push({
                            sku: $scope.variantData[i].variants[j].suppliers[k].sku
                        });
                    }
                }
            }
        }
        $timeout(function () {
            $scope.$apply();
            $('.chosen-select.supId__0').trigger("chosen:updated");
        });
    }


});

app.controller('PromotionsAdd7Controller', function ($scope, $http, $timeout, $location, $q) {
    $scope.isVisible = false;
    
    //get all promotions
    $http.get('/promotions/get')
            .then(function (response) {
                if (response.data.data.length > 0) {
                    $scope.sortedImages = [];
                    $scope.promotionsId = response.data.data[0]._id;
                    
                    if (response.data.data[0].featuredBrands.length > 0) {
                        $scope.isVisible = true;
                        $scope.brands = response.data.data[0].featuredBrands;
                    }
                }
            });
            
    //get all brands        
    $http.get('/brand/getAllActiveBrands')
        .then(function (responseBrands) {
            console.log("responseBrands");
            console.log(responseBrands);
            if (responseBrands.data.data.length > 0) {
                $scope.allBrands = responseBrands.data.data;
            }
            $timeout(function () {
                if (!$scope.brands) {
                    $scope.brands = [];
                }
                $('.chosen-select.featuredBrands').chosen({width: "100%"}).change(function (event, params) {
                    if (params.selected) {
                        for (var i = 0; i < $scope.brands.length; i++) {
                            if($scope.brands[i]._id == params.selected){
                                $scope.promotionsError = "Brand already exists";
                                $('.error-modal-lg').modal();
                                return;
                            }
                        }
                        $scope.brands.push({
                            _id: params.selected,
                            name: $('.chosen-select.featuredBrands option[value="' + params.selected + '"]').attr('data-name'),
                            slug: $('.chosen-select.featuredBrands option[value="' + params.selected + '"]').attr('data-slug')
                        });
                    } else if (params.deselected && $scope.brands.length > 0) {
                        for (var i = 0; i < $scope.brands.length; i++) {
                            if ($scope.brands[i]._id == params.deselected) {
                                $scope.brands.splice(i, 1);
                            }
                        }
                    }
                    $scope.$apply();

                });

            });
                    
        });
    
    //delete brand
    $scope.removeExistingBrand = function (eachBrand) {
        console.log("removeExistingBrand");
        console.log(eachBrand);
        for (var i = 0; i < $scope.brands.length; i++) {
            if ($scope.brands[i]._id == eachBrand._id)
            {
                $scope.brands.splice(i, 1);
                $('.chosen-select.featuredBrands option[value="' + eachBrand._id + '"]').removeAttr("selected").trigger("chosen:updated");
            }
        }
        $timeout(function () {
            $scope.$apply();
        });
    };
    
    //save divider
    $scope.submitDivider = function(dividerNumber){
        var serverObject = {};
        if ($scope.promotionsId) {
            serverObject._id = $scope.promotionsId;
        }
        if (dividerNumber) {
            serverObject.dividerNumber = dividerNumber;
        }
        if ($scope.brands.length <= 0) {
            $scope.promotionsError = "Please select brands";
            $(".error-modal-lg").modal();
            return;
        } 
        else {
            serverObject.featuredBrands = $scope.brands;
            serverObject.isVisible = $scope.isVisible
            $http.post('/promotions/addPromotionData', serverObject)
                    .then(function (response) {
                        console.log(response);
                        if(response.data.error == 0){
                            $scope.successStatus = "Successfully saved";
                            $(".success-modal-lg").modal();
                        }
                    });
        }
    }
    
    //toggle divider visiblity
    $scope.toggleVisiblity = function(){
        $scope.isVisible = !$scope.isVisible;
    }
});
