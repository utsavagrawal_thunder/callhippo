// closure for this controller // private
(function() {

// catalogue & [discount, offer] price difference in %
const priceDiffUp = 10;
const priceDiffDown = 50;

// Image & Catalogue file paths
var productImgAwsPath = s3_bucket+'product/images/';
var catalogueFileAwsPath = s3_bucket+'product/catalogue/';

// product get controller
app.controller('ProductListController', function ($timeout, $scope, $http, $location, NgTableParams) {
    // check for access
    noAccessRedirect($location);

    // get data
    $scope.searchProduct = '';
    $scope.getProductList = function() {        
        getProductList($scope, $http, $timeout, NgTableParams);
    };
    $scope.getProductList();

    // Get variants counts
    $scope.activeVariants = 0;
    $scope.pendingVariants = 0;
    $scope.inactiveVariants = 0;
    $scope.rejectedVariants = 0;
    $http.get('/product/getProductVariantsCounts')
        .then(function (response) {
            var allData = response.data.data;
            var allDataCnt = allData.length;
            for(var i = 0; i < allDataCnt; i++) {
                // active
                if(allData[i].status == 1) {
                    $scope.activeVariants = allData[i].count;
                }
                // inactive
                if(allData[i].status == 0) {
                    $scope.inactiveVariants = allData[i].count;
                }
                // pending
                if(allData[i].status == 4) {
                    $scope.pendingVariants = allData[i].count;
                }
                // rejected
                if(allData[i].status == 3) {
                    $scope.rejectedVariants = allData[i].count;
                }
            }            
        });

    // Get suppliers & open catalogue price modal
    $scope.openCataloguePriceModal = function(productId, variantId) {
        $scope.newCataloguePrice = '';
        $scope.currentCataloguePrice = '';
        $scope.modalProductId = productId;
        $scope.modalVariantId = variantId;
        // draw empty chart
        var chartDataArray = [
            ['Supplier', 'Price']
        ];
        google.charts.setOnLoadCallback(drawSupplierPriceChart(chartDataArray));

        $http.get('/product/getVariantDetails/'+productId+'/'+variantId)
            .then(function (response) {
                if(response.data.error == 0 && response.data.data != '') {
                    var variantDetails = response.data.data[0].variants[0];
                    $scope.currentCataloguePrice = (typeof(variantDetails.cataloguePrice) != 'undefined') ? variantDetails.cataloguePrice : 0;
                    var suppliers = variantDetails.suppliers;
                    var suppliersCnt = suppliers.length;

                    if(suppliersCnt > 0) {
                        for(var i = 0; i < suppliersCnt; i++) {
                            // show only active suppliers prices
                            if(suppliers[i]['isActive'] == 1) {
                                var tmpSupplierCataloguePrice = suppliers[i]['supplierCataloguePrice'];
                                tmpSupplierCataloguePrice = (typeof(tmpSupplierCataloguePrice) != 'undefined') ? tmpSupplierCataloguePrice : 0;
                                
                                var tmpArr = ['Supplier '+(i+1), tmpSupplierCataloguePrice];
                                chartDataArray.push(tmpArr);
                            }                            
                        }

                        // draw chart
                        google.charts.setOnLoadCallback(drawSupplierPriceChart(chartDataArray));
                    }

                    $("#cataloguePriceModal").modal();
                }
                else {
                    showToastMsg(0, 'Product suppliers not found');
                }
            });
    };

    // update catalogue price
    $scope.updateCataloguePrice = function() {
        var newCataloguePrice = $scope.newCataloguePrice;

        if((typeof(newCataloguePrice) == 'undefined') || newCataloguePrice == '') {
            showToastMsg(0, 'Please enter catalogue price');
            $('#newCataloguePrice').focus();
            return false;
        }
        if(newCataloguePrice == 0) {
            showToastMsg(0, 'Catalogue price cannot be zero');
            $('#newCataloguePrice').focus();
            return false;
        }

        // data
        var data = {
            productId: $scope.modalProductId,
            variantId: $scope.modalVariantId,
            cataloguePrice: newCataloguePrice
        };
        $http.post('/product/updateCataloguePrice', data)
            .success(function (data, status, headers, config) {
                if(data.error == 1) {
                    showToastMsg(0, data.msg);
                    return false;
                }
                showToastMsg(1, data.msg);

                // close modal
                $('#cataloguePriceModalClose').click();
            })
            .error(function (data, status, header, config) {
                showToastMsg(0, 'Unable to update Catalogue price');
            });
    }    

    // draws it
    function drawSupplierPriceChart(dataArray) {
        // Create the data table.    
        var data = google.visualization.arrayToDataTable(dataArray);

        // Set chart options
        var options = {
            'title': 'Supplier catalogue prices',
            'width': 400,            
            'legend': { position: "none" }
        };

        var noOfRows = data.getNumberOfRows();
        if(noOfRows > 5) {
            options.height = noOfRows * 40;
        }

        // set price on bar
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1, {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            }
        ]);

        if(noOfRows == 0) {
            $("#supplierPriceChart").html("Sorry, no suppliers available"); 
        }
        else {
            // Instantiate and draw our chart, passing in some options
            var chart = new google.visualization.BarChart(document.getElementById('supplierPriceChart'));
            chart.draw(view, options);
        }
    }

    // Upload product excel
    $(document).ready(function(){
        $("#productExcelUploadForm").ajaxForm({
            url: '/product/uploadProductsExcel',
            clearForm: true,
            beforeSubmit: function(arr, $form, options) {
                var excelFile = arr[0]['value'];
                if(excelFile == '') {
                    showToastMsg(0, 'Please select file');
                    return false;
                }
            },
            complete: function(response) {
                // error: Invalid file
                if(response.status == 422) {
                    showToastMsg(0, response.responseJSON.error);
                    return;
                }

                showToastMsg(1, 'File uploaded on server. You will get confirmation mail once all Products uploaded to database.');
            }
        });
    });

    // Upload product excel
    $scope.uploadProductExcel = function() {
        var excelFile = $('#productExcel').val();
        if(excelFile == '') {
            showToastMsg(0, 'Please select file');
            return false;
        }

        return false;
    };
});

// get product list
function getProductList($scope, $http, $timeout, NgTableParams) {
    $scope.tableParams = new NgTableParams(
        {count: 25, page: 1, sorting: { createdDate: 'desc' }},
        {
            counts: [25, 50, 100],
            total: 0,
            getData: function (params) {
                var count = params.count();
                var page = params.page();
                // for sorting
                var sorting = params.sorting();
                var sortParam = 'createdDate';
                var sortOrder = 'DESC';
                if(!isObjectEmpty(sorting)) {
                    sortParam = Object.keys(sorting)[0];
                    sortOrder = sorting[sortParam];                                        
                }

                var dataObj = {
                    searchTerm: $.trim($scope.searchProduct),
                    status: $('input[name=listingStatus]:checked').val(),
                    perPage: count,
                    pageNum: page,
                    sortParam: sortParam,
                    sortOrder: sortOrder
                };
                return new Promise(function (resolve, reject) {
                    $http.post('/product/getProductVariants', dataObj)
                        .success(function (data, status, headers, config) {
                            $scope.tableParams.total(data.totalCnt);
                            // update data
                            var tmpProductData = data.data;
                            var tmpProductDataCnt = tmpProductData.length;
                            for(var i = 0; i < tmpProductDataCnt; i++) {
                                // For added by user text
                                var addedByUserStr = '';
                                if(tmpProductData[i].variants.addedByUserType == 'SUPPLIER') {
                                    addedByUserStr = 'Supplier';
                                }
                                else {
                                    // for role text
                                    switch (tmpProductData[i].variants.addedBy.role) {
                                        case 'SA':
                                            addedByUserStr = 'Super Admin';
                                            break;
                                        case 'CONTENT-MNGR':
                                            addedByUserStr = 'Content Manager';
                                            break; 
                                        case 'MIS-EXEC':
                                            addedByUserStr = 'MIS Executive';
                                            break; 
                                        case 'BD-HEAD':
                                            addedByUserStr = 'Business Head';
                                            break; 
                                        default: 
                                            addedByUserStr = 'Super Admin';
                                    }
                                }

                                tmpProductData[i].variants.addedByUserStr = addedByUserStr;
                            }
                            resolve(tmpProductData);
                        })
                        .error(function (data, status, header, config) {
                            reject([]);
                        });
                });
            }
        });
}

// product supplier get controller
app.controller('ProductSupplierListController', function ($timeout, $scope, $http, $location, NgTableParams) {
    // check for access
    noAccessRedirect($location);

    // get data
    $scope.searchProduct = '';
    $scope.getProductList = function() {        
        getProductSupplierList($scope, $http, $timeout, NgTableParams);
    };
    $scope.getProductList();
});

// get product supplier list
function getProductSupplierList($scope, $http, $timeout, NgTableParams) {
    $scope.tableParams = new NgTableParams(
        {count: 25, page: 1, sorting: { createdDate: 'desc' }}, 
        {
            counts: [25, 50, 100],
            total: 0,
            getData: function (params) {
                var count = params.count();
                var page = params.page();
                // for sorting
                var sorting = params.sorting();
                var sortParam = 'createdDate';
                var sortOrder = 'DESC';
                if(!isObjectEmpty(sorting)) {
                    sortParam = Object.keys(sorting)[0];
                    sortOrder = sorting[sortParam];                                        
                }

                var dataObj = {
                    searchTerm: $.trim($scope.searchProduct),
                    status: $('input[name=listingStatus]:checked').val(),
                    perPage: count,
                    pageNum: page,
                    sortParam: sortParam,
                    sortOrder: sortOrder
                };
                return new Promise(function (resolve, reject) {
                    $http.post('/product/getProductSuppliers', dataObj)
                        .success(function (data, status, headers, config) {                            
                            var result = data.data;
                            var resultCnt = result.length;
                            for(var i = 0; i < resultCnt; i++) {
                                var productImages = result[i].variants.images;
                                var productImgCnt = productImages.length;
                                var showImageUrl = '/dist/img/no-image.jpg';
                                if(productImgCnt > 0) {
                                    var defaultImgPresent = false;
                                    for(var j = 0; j < productImgCnt; j++) {
                                        if(productImages[j].isDefault == true) {
                                            defaultImgPresent = true;
                                            showImageUrl = productImgAwsPath+productImages[j].name;
                                            break;
                                        }
                                    }
                                    // default image not present
                                    if(!defaultImgPresent) {
                                        showImageUrl = productImgAwsPath+productImages[0].name;
                                    }
                                }
                                result[i]['variants']['showImageUrl'] = showImageUrl;

                                // for discount
                                var discountPerc = 0;
                                var tmpCataloguePrice = result[i].variants.cataloguePrice;
                                var tmpOfferPrice = result[i].variants.suppliers.offerPrice;
                                if(tmpCataloguePrice > 0) {
                                    discountPerc = (tmpOfferPrice / tmpCataloguePrice) * (tmpCataloguePrice - tmpOfferPrice)
                                    discountPerc = Math.ceil(discountPerc);
                                }
                                result[i]['variants']['discountPerc'] = discountPerc;
                            }
                            $scope.tableParams.total(data.totalCnt);
                            resolve(result);
                        })
                        .error(function (data, status, header, config) {
                            reject([]);
                        });
                });
            }
        });
}

// Add product filter value to DB
function addProductFilterValue(filterId, $scope, $http, $timeout) {
    $scope.hideSEMsgs();

    var value = $('#productFilterNewValue_'+filterId).val();
    if(value == '' || typeof(value) == 'undefined') {
        $scope.error = 'Please enter filter value';
        showToastMsg(0, 'Please enter filter value');
        $('#productFilterNewValue_'+filterId).focus();
        return false;
    }

    // Add to category collection
    var productTypeVal = $scope.productType;
    var productTypeSplit = productTypeVal.split('_##_');
    var productTypeId = productTypeSplit[0];    
    var dataToCat = {
        value: value,
        filterId: filterId,
        categoryId: productTypeId,
        filterType: 'product'
    };
    $http.post('/category/insertFilterValue', dataToCat)
    .success(function (data, status, headers, config) {
        if(data.error == 1) {
            $scope.error = data.msg;
            showToastMsg(0, data.msg);
            return false;
        }

        // add to filter collection
        var dataToFilter = {
            value: value,
            _id: filterId
        };
        $http.post('/filter/insertValueById', dataToFilter)
        .success(function (data, status, headers, config) {        
        })
        .error(function (data, status, header, config) {
        });

        // success
        $scope.success = data.msg;
        showToastMsg(1, data.msg);
        $('#productFilterNewValue_'+filterId).val('');

        // add to filter & show selected
        var option = '<option value="'+value+'">'+value+'</option>';
        $('#productFilterValue_'+filterId).append(option);
        $timeout(function () {
            $('#productFilterValue_'+filterId).val(value);
        });

        $timeout(function () {
            $scope.hideSEMsgs();
        }, 2000);
    })
    .error(function (data, status, header, config) {
        $scope.error = 'Unable to add filter value';
        showToastMsg(0, 'Unable to add filter value');
    });    
}

// Add variant filter value to DB
function addVariantFilterValue(filterId, $scope, $http, $timeout) {
    $scope.hideSEMsgs();

    var value = $('#variantFilterNewValue_'+filterId).val();
    if(value == '' || typeof(value) == 'undefined') {
        $scope.error = 'Please enter filter value';
        showToastMsg(0, 'Please enter filter value');
        $('#variantFilterNewValue_'+filterId).focus();
        return false;
    }

    // Add to category collection
    var productTypeVal = $scope.productType;
    var productTypeSplit = productTypeVal.split('_##_');
    var productTypeId = productTypeSplit[0];    
    var dataToCat = {
        value: value,
        filterId: filterId,
        categoryId: productTypeId,
        filterType: 'variant'
    };
    $http.post('/category/insertFilterValue', dataToCat)
    .success(function (data, status, headers, config) {
        if(data.error == 1) {
            $scope.error = data.msg;
            showToastMsg(0, data.msg);
            return false;
        }

        // add to filter collection
        var dataToFilter = {
            value: value,
            _id: filterId
        };
        $http.post('/filter/insertValueById', dataToFilter)
        .success(function (data, status, headers, config) {        
        })
        .error(function (data, status, header, config) {
        });

        // success
        $scope.success = data.msg;
        showToastMsg(1, data.msg);
        $('#variantFilterNewValue_'+filterId).val('');

        // add to filter & show selected
        var option = '<option value="'+value+'">'+value+'</option>';
        $('#variantFilterValue_'+filterId).append(option);
        $timeout(function () {
            $('#variantFilterValue_'+filterId).val(value);
        });

        $timeout(function () {
            $scope.hideSEMsgs();
        }, 2000);
    })
    .error(function (data, status, header, config) {
        $scope.error = 'Unable to add filter value';
        showToastMsg(0, 'Unable to add filter value');
    });
}

// get token input status
function getTokenItemStatus(item) {
    var dispStatus = 'Active';
    if(item.isActive == 0) {
        dispStatus = 'Inactive';
    }
    else if(item.isActive == 2) {
        dispStatus = 'Deleted';
    }

    return dispStatus;
}

// get all brands
function getAllBrands($scope, $http) {
    $http.get('/brand/getAllBrands')
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.brandList = response.data.data;

                // autocomplete list
                $(document).ready(function() {
                    $("#brand").tokenInput($scope.brandList, {
                        minChars: 2,
                        propertyToSearch: 'name',
                        tokenValue: '_id',
                        resultsLimit: 10,
                        tokenLimit: 1,
                        hintText: 'Type a brand name',
                        onAdd: function (item) {
                            $scope.selectedBrand = item;
                            delete $scope.selectedBrand.isActive;
                        },
                        resultsFormatter: function(item) {
                            var dispStatus = $scope.getTokenItemStatus(item);
                            return '<li>'+item.name+' - '+dispStatus+'</li>';
                        },
                        tokenFormatter: function(item) {
                            // find out brand status
                            if(typeof(item.checkStatus) != 'undefined') {
                                var brandListCnt = $scope.brandList.length;
                                for(var i = 0; i < brandListCnt; i++) {
                                    if($scope.brandList[i]._id == item._id) {
                                        item.isActive = $scope.brandList[i].isActive;
                                        break;
                                    }
                                }
                            }

                            var dispStatus = $scope.getTokenItemStatus(item);
                            return '<li><p>'+item.name+' - '+dispStatus+'</p></li>';
                        }
                    });
                });
            }
        });
}

// get all manufacturers
function getAllManufacturers($scope, $http) {
    $http.get('/manufacturer/getAllManufacturers')
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.manufacturerList = response.data.data;

                // autocomplete list
                $(document).ready(function() {
                    $("#manufacturer").tokenInput($scope.manufacturerList, {
                        minChars: 2,
                        propertyToSearch: 'name',
                        tokenValue: '_id',
                        resultsLimit: 10,
                        tokenLimit: 1,
                        hintText: 'Type a manufacturer name',
                        onAdd: function (item) {
                            $scope.selectedManufacturer = item;
                            delete $scope.selectedManufacturer.isActive;
                        },
                        resultsFormatter: function(item) {
                            var dispStatus = $scope.getTokenItemStatus(item);
                            return '<li>'+item.name+' - '+dispStatus+'</li>';
                        },
                        tokenFormatter: function(item) {
                            // find out brand status
                            if(typeof(item.checkStatus) != 'undefined') {
                                var manufacturerListCnt = $scope.manufacturerList.length;
                                for(var i = 0; i < manufacturerListCnt; i++) {
                                    if($scope.manufacturerList[i]._id == item._id) {
                                        item.isActive = $scope.manufacturerList[i].isActive;
                                        break;
                                    }
                                }
                            }

                            var dispStatus = $scope.getTokenItemStatus(item);
                            return '<li><p>'+item.name+' - '+dispStatus+'</p></li>';
                        }
                    });
                });
            }
        });
}

// get related products
function getRelatedVariants() {
    $(document).ready(function() {
        $("#relatedProducts").tokenInput(function(){
                return '/product/getRelatedVariants?catalogueId='+$('#catalogueId').val()
            }, {
            method: 'POST',
            queryParam: 'searchTerm',
            theme: 'facebook',
            minChars: 2,
            propertyToSearch: 'name',
            resultsLimit: 10,
            preventDuplicates: true,
            hintText: 'Type a catalogue id'
        });
    });
}

// add new brand
function addBrand($scope, $http, $timeout) {
    $scope.hideSEMsgs();

    var name = $scope.brandName;
    if(name == '' || typeof(name) == 'undefined') {
        $scope.error = 'Please enter brand name';
        showToastMsg(0, 'Please enter brand name');
        return false;
    }

    // data 
    var data = {
        name: name
    };
    var config = {};

    $http.post('/brand/insertName', data, config)
    .success(function (data, status, headers, config) {
        if(data.error == 1) {
            $scope.error = data.msg;
            showToastMsg(0, data.msg);
            return false;
        }
        $scope.success = data.msg;
        showToastMsg(1, data.msg);
        $scope.brandName = '';
        var isBrandActive = data.data.isActive;

        // add to brand dropdown & show selected
        // var value = data.data._id+'_##_'+name;
        $scope.brandList.push({_id: data.data._id, name: name, isActive: isBrandActive});
        // $timeout(function () {
        //     $('#brand').val(value);
        // });

        $scope.selectedBrand = {_id: data.data._id, name: name};
        $('#brand').tokenInput("clear");
        $('#brand').tokenInput("add", {_id: data.data._id, name: name, isActive: isBrandActive});

        $timeout(function () {
            $scope.hideSEMsgs();
        }, 2000);
    })
    .error(function (data, status, header, config) {
        $scope.error = 'Unable to add brand';
        showToastMsg(0, 'Unable to add brand');
    });
}

// add new manufacturer
function addManufacturer($scope, $http, $timeout) {
    $scope.hideSEMsgs();

    var name = $scope.manufacturerName;
    if(name == '' || typeof(name) == 'undefined') {
        $scope.error = 'Please enter manufacturer name';
        showToastMsg(0, 'Please enter manufacturer name');
        return false;
    }

    // data 
    var data = {
        name: name
    };
    var config = {};

    $http.post('/manufacturer/insertName', data, config)
    .success(function (data, status, headers, config) {
        if(data.error == 1) {
            $scope.error = data.msg;
            showToastMsg(0, data.msg);
            return false;
        }
        $scope.success = data.msg;
        showToastMsg(1, data.msg);
        $scope.manufacturerName = '';
        var isManufacturerActive = data.data.isActive;

        // add to manufacturer dropdown & show selected
        // var value = data.data._id+'_##_'+name;
        $scope.manufacturerList.push({_id: data.data._id, name: name, isActive: isManufacturerActive});
        // $timeout(function () {
        //     $('#manufacturer').val(value);
        // });

        $scope.selectedManufacturer = {_id: data.data._id, name: name};
        $('#manufacturer').tokenInput("clear");
        $('#manufacturer').tokenInput("add", {_id: data.data._id, name: name, isActive: isManufacturerActive});

        $timeout(function () {
            $scope.hideSEMsgs();
        }, 2000);
    })
    .error(function (data, status, header, config) {
        $scope.error = 'Unable to add manufacturer';
        showToastMsg(0, 'Unable to add manufacturer');
    });
}

// get dropzone config
function getDropzoneConfig($scope, $http) {
    var obj = {
        'options': { // passed into the Dropzone constructor
            url: '/product/galleryImageUpload',
            // previewTemplate: previewTemplate,
            // previewsContainer: "#productImages",
            maxFilesize: 8, // MB
            maxFiles: 5,
            dictDefaultMessage: 'Drag an image here to upload, or click to select one',
            acceptedFiles: 'image/*',
            addRemoveLinks: true,
            removedfile: function(file) {
                var name = file.name;

                // find out renamed file name
                var upCnt = $scope.uploadedImages.length;
                var renamedName = name;
                for(var i = 0; i < upCnt; i++) {
                    if($scope.uploadedImages[i].originalName == name) {
                        renamedName = $scope.uploadedImages[i].renamedName;
                        // remove from array
                        $scope.uploadedImages.splice(i, 1);
                        break;
                    }
                }

                // set max files limit
                // this.options.maxFiles = 5 - $scope.uploadedImages.length;

                // remove from dropdown
                $("#defaultImage option[value='"+renamedName+"']").remove();
                
                // remove file from server
                $http.delete('/product/galleryImageDelete/'+renamedName)
                .then(function (response) {});

                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
            },
            init: function() {
                this.on('success', function( file, resp ) {
                    var tmpRenameName = resp.key.split('/')[2];
                    $scope.uploadedImages.push({'originalName': file.name, 'renamedName': tmpRenameName});
                    $('#defaultImage').append('<option value="'+tmpRenameName+'">'+tmpRenameName+'</option>');
                });

                // keep dropzone refernce
                $scope.dropzoneGallery = this;
            },
        },
        'eventHandlers': {
            'sending': function (file, xhr, formData) {
            },
            'success': function (file, response) {
            }
        }
    };

    return obj;
}

// get dropzone catalogue config
function getDropzoneConfigCatalogue($scope, $http) {
    var obj = {
        'options': { // passed into the Dropzone constructor
            url: '/product/catalogueUpload',
            maxFilesize: 8, // MB
            maxFiles: 1,
            dictDefaultMessage: 'Drag an image/pdf/doc here to upload, or click to select one',
            acceptedFiles: 'image/*, application/pdf',
            addRemoveLinks: true,
            removedfile: function(file) {
                // remove file from server
                $http.delete('/product/catalogueDelete/'+$scope.catalogueFile)
                .then(function (response) {});

                // empty file
                $scope.catalogueFile = '';
                this.options.maxFiles = 1;

                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
            },
            init: function() {
                this.on('success', function( file, resp ) {
                    $scope.catalogueFile = resp.key.split('/')[2];
                });

                // keep dropzone refernce
                $scope.dropzoneCatalogue = this;
            },
        },
        'eventHandlers': {
            'sending': function (file, xhr, formData) {
            },
            'success': function (file, response) {
            }
        }
    };

    return obj;
}

// get product type filters
function getProductTypeFilters($scope) {
    var productTypeVal = $scope.productType;

    if(productTypeVal != '' && typeof(productTypeVal) != 'undefined') {
        var productTypeSplit = productTypeVal.split('_##_');
        var productType = productTypeSplit[0];

        // loop through to match product type
        var ptLen = $scope.productTypeList.length;
        for(var i = 0; i < ptLen; i++) {
            if(productType == $scope.productTypeList[i]._id) {
                var filtersTmp = $scope.productTypeList[i].productFilters;
                if(typeof(filtersTmp) != 'undefined') {
                    $scope.productTypeFilters = filtersTmp;
                }
                break;
            }
        }
    }
    else {
        $scope.productTypeFilters = new Array();
    }
}

// get variant type filters
function getVaraintFilters($scope) {
    var productTypeVal = $scope.productType;

    if(productTypeVal != '' && typeof(productTypeVal) != 'undefined') {
        var productTypeSplit = productTypeVal.split('_##_');
        var productType = productTypeSplit[0];

        // loop through to match product type
        var ptLen = $scope.productTypeList.length;
        for(var i = 0; i < ptLen; i++) {
            if(productType == $scope.productTypeList[i]._id) {
                var filtersTmp = $scope.productTypeList[i].variantFilters;
                if(typeof(filtersTmp) != 'undefined') {
                    $scope.variantFilters = filtersTmp;
                }
                break;
            }
        }
    }
    else {
        $scope.variantFilters = new Array();
    }
}

// get product type bulk ranges
function getProductTypeBulkRanges($scope) {
    var productTypeVal = $scope.productType;

    if(productTypeVal != '' && typeof(productTypeVal) != 'undefined') {
        var productTypeSplit = productTypeVal.split('_##_');
        var productType = productTypeSplit[0];

        // loop through to match product type
        var ptLen = $scope.productTypeList.length;
        for(var i = 0; i < ptLen; i++) {
            if(productType == $scope.productTypeList[i]._id) {
                var bulkRangesTmp = $scope.productTypeList[i].bulkRanges;
                if(typeof(bulkRangesTmp) != 'undefined') {
                    $scope.bulkRanges = bulkRangesTmp;
                }
                break;
            }
        }
    }
    else {
        $scope.bulkRanges = new Array();
    }
}

// add only bulk range qty
function updateOnlyProductTypeBulkRangeQtyValue($scope) {
    var productTypeVal = $scope.productType;

    if(productTypeVal != '' && typeof(productTypeVal) != 'undefined') {
        var productTypeSplit = productTypeVal.split('_##_');
        var productType = productTypeSplit[0];

        // loop through to match product type
        var ptLen = $scope.productTypeList.length;
        for(var i = 0; i < ptLen; i++) {
            if(productType == $scope.productTypeList[i]._id) {
                var bulkRangesTmp = $scope.productTypeList[i].bulkRanges;
                if(typeof(bulkRangesTmp) != 'undefined') {
                    var bulkRangesTmpCnt = bulkRangesTmp.length;
                    for(var i = 0; i < bulkRangesTmpCnt; i++) {
                        $('#bulkRangeDropdown_'+i).html('<option value="'+bulkRangesTmp[i]+'">'+bulkRangesTmp[i]+'</option>');
                    }
                }
                break;
            }
        }
    }
}

// get product commission
function getProductTypeCommission($scope, $http) {
    var productTypeVal = $scope.productType;
    $scope.productTypeCommission = 0;

    if(productTypeVal != '' && typeof(productTypeVal) != 'undefined') {
        var productTypeSplit = productTypeVal.split('_##_');
        var productType = productTypeSplit[0];

        // loop through to match product type
        var ptLen = $scope.productTypeList.length;
        for(var i = 0; i < ptLen; i++) {
            if(productType == $scope.productTypeList[i]._id) {
                $scope.productTypeCommission = $scope.productTypeList[i].commissionValues;

                // get parent commission if product type commission is 0
                if($scope.productTypeCommission == 0) {
                    $http.get('/category/getCommission/'+productType)
                        .success(function (data, status, headers, config) {
                            if(data.error == 1) {
                                $scope.productTypeCommission = 0;    
                                return false;
                            }

                            $scope.productTypeCommission = data.data.commission;
                        })
                        .error(function (data, status, header, config) {
                            $scope.productTypeCommission = 0;
                        });
                }
                break;
            }
        }
    }
}

// Step 1 validation
function step1Validation(showNextStep, command, $scope) {
    $scope.hideSEMsgs();
    showNextStep = (typeof(showNextStep) == 'undefined') ? 1 : showNextStep; 

    // get data
    var productType = $scope.productType;
    var productCode = $scope.productCode;
    var tag = $scope.tag;
    var brand = $scope.selectedBrand;
    var manufacturer = $scope.selectedManufacturer;
    
    // validations
    if(productType == '' || typeof(productType) == 'undefined') {
        $scope.error = 'Please select product type';
        showToastMsg(0, 'Please select product type');
        return false;
    }
    if(productCode == '' || typeof(productCode) == 'undefined') {
        $scope.error = 'Please insert product code';
        showToastMsg(0, 'Please insert product code');
        return false;
    }
    if(tag == '' || typeof(tag) == 'undefined') {
        $scope.error = 'Please select tag';
        showToastMsg(0, 'Please select tag');
        return false;
    }
    if(isObjectEmpty(brand)) {
        $scope.error = 'Please select brand';
        showToastMsg(0, 'Please select brand');
        return false;
    }
    if(isObjectEmpty(manufacturer)) {
        $scope.error = 'Please select manufacturer';
        showToastMsg(0, 'Please select manufacturer');
        return false;
    }

    // show next step
    if(showNextStep == 1) {
        // for add
        if(command == 'ADD') {
            $('#step2MinBtn, #step1MinBtn').click();
            $('#step2').show();
        }
        // for edit
        else {
            // $('#step2MinBtn').click();
        }
    }
    
    $scope.hideSEMsgs();
    return true;
}

// product id validation
function productIdValidation($scope) {
    if($scope.productId == '' || typeof($scope.productId) == 'undefined') {
        $scope.error = 'Complete step 1 & 2 first';
        showToastMsg(0, 'Complete step 1 & 2 first');
        return false;
    }
    return true;
}

// Step 3 validation
function step3Validation(showNextStep, command, $scope) {
    $scope.hideSEMsgs();
    showNextStep = (typeof(showNextStep) == 'undefined') ? 1 : showNextStep;

    // product id validation
    if(!$scope.productIdValidation()) {
        return false;
    }

    // get data
    var productType = $scope.productType;
    var catalogueId = $scope.catalogueId;
    var productVariantName = $scope.productVariantName;
    var variantFilterNames = $("input[name='variantFilterName[]']").map(function(){return $(this).val();}).get();
    var variantFilterIds = $("input[name='variantFilterId[]']").map(function(){return $(this).val();}).get();
    var variantFilterValues = $("select[name='variantFilterValue[]']").map(function(){return $(this).val();}).get();

    // validations
    if(catalogueId == '' || typeof(catalogueId) == 'undefined') {
        $scope.error = 'Please enter catalogue id';
        showToastMsg(0, 'Please enter catalogue id');
        return false;
    }
    if(productType == '' || typeof(productType) == 'undefined') {
        $scope.error = 'Please select product type';
        showToastMsg(0, 'Please select product type');
        return false;
    }        
    if(productVariantName == '' || typeof(productVariantName) == 'undefined') {
        $scope.error = 'Please select product variant name';
        showToastMsg(0, 'Please select product variant name');
        return false;
    }
    if(onlySpecialChars(productVariantName) == false) {
        $scope.error = 'Product variant name should not contain only special characters';
        showToastMsg(0, 'Product variant name should not contain only special characters');
        return false;
    }
    if(variantFilterNames.length == 0) {
        $scope.error = 'Please add product variant filters';
        showToastMsg(0, 'Please add product variant filters');
        return false;
    }

    // show next step
    if(showNextStep == 1) {
        $('#step4MinBtn, #step3MinBtn').click();

        // for add
        if(command == 'ADD') {
            $('#step4').show();
        }

        // add specs
        $scope.setSpecsFromFilters();
    }        

    $scope.hideSEMsgs();
    return true;
}

// Step 4 validation
function step4Validation(showNextStep, command, $scope) {
    $scope.hideSEMsgs();
    showNextStep = (typeof(showNextStep) == 'undefined') ? 1 : showNextStep;

    // product id validation
    if(!$scope.productIdValidation()) {
        return false;
    }

    /*
    if($scope.uploadedImages.length == 0) {
        $scope.error = 'Please upload product images';
        showToastMsg(0, 'Please upload product images');
        return false;    
    }
    */

    // for default image
    var defaultImage = $('#defaultImage').val();
    if(($scope.uploadedImages.length > 0) && (defaultImage == '' || typeof(defaultImage) == 'undefined')) {
        $scope.error = 'Please select default image';
        showToastMsg(0, 'Please select default image');
        return false;
    }    

    // show next step
    if(showNextStep == 1) {
        $('#step5MinBtn, #step4MinBtn').click();

        // for add
        if(command == 'ADD') {
            $('#step5').show();
        }
    }

    $scope.hideSEMsgs();
    return true;
}

// Step 5 validation
function step5Validation(showNextStep, command, $scope) {
    $scope.hideSEMsgs();
    showNextStep = (typeof(showNextStep) == 'undefined') ? 1 : showNextStep;

    // product id validation
    if(!$scope.productIdValidation()) {
        return false;
    }

    // get data
    var package = $scope.package;
    // var description = $scope.description;
    var description = CKEDITOR.instances.description.getData();
    // var specifications = $scope.specifications;
    var specifications = CKEDITOR.instances.specifications.getData();

    // validations
    if(package == '' || typeof(package) == 'undefined') {
        $scope.error = 'Please enter package';
        showToastMsg(0, 'Please enter package');
        return false;
    }
    if(description == '' || typeof(description) == 'undefined') {
        $scope.error = 'Please insert description';
        showToastMsg(0, 'Please insert description');
        return false;
    }
    if(specifications == '' || typeof(specifications) == 'undefined') {
        $scope.error = 'Please insert specifications';
        showToastMsg(0, 'Please insert specifications');
        return false;
    }

    // show next step
    if(showNextStep == 1) {
        $('#step6MinBtn, #step5MinBtn').click();

        // for add
        if(command == 'ADD') {
            $('#step6').show();
        }
    }

    $scope.hideSEMsgs();
    return true;
}

// step 6 validations
function step6Validation(showNextStep, command, $scope) {
    showNextStep = (typeof(showNextStep) == 'undefined') ? 1 : showNextStep;

    // show next step
    if(showNextStep == 1) {
        $('#step7MinBtn, #step6MinBtn').click();

        // on add
        if(command == 'ADD') {
            $('#step7').show();
        }
    }
    return true;
}

// step 6 validations
function step7Validation(command, $scope) {
    $scope.hideSEMsgs();

    // product id validation
    if(!$scope.productIdValidation()) {
        return false;
    }

    // get data
    var status = $('#status').val();

    // validations
    if(status == '' || typeof(status) == 'undefined') {
        $scope.error = 'Please select status';
        showToastMsg(0, 'Please select status');
        return false;
    }

    $scope.hideSEMsgs();
    return true;
}

// add specifications from selected filters
function setSpecsFromFilters() {
    var productFilterNames = $("input[name='productFilterName[]']").map(function(){return $(this).val();}).get();
    var productFilterIds = $("input[name='productFilterId[]']").map(function(){return $(this).val();}).get();
    var productFilterValues = $("select[name='productFilterValue[]']").map(function(){return $(this).val();}).get();
    var variantFilterNames = $("input[name='variantFilterName[]']").map(function(){return $(this).val();}).get();
    var variantFilterIds = $("input[name='variantFilterId[]']").map(function(){return $(this).val();}).get();
    var variantFilterValues = $("select[name='variantFilterValue[]']").map(function(){return $(this).val();}).get();

    var tableData = '<table border="1" cellpadding="1" cellspacing="1" style="width:100%">';
    tableData += '<tbody>';
    
    // product filters
    var pfLen = productFilterNames.length;
    for(var i = 0; i < pfLen; i++) {
        var trRow = '<tr>';
        trRow += '<td>'+productFilterNames[i]+'</td>';
        trRow += '<td>'+productFilterValues[i]+'</td>';
        trRow += '</tr>';
        tableData += trRow;
    }

    // variant filters
    var vfLen = variantFilterNames.length;
    for(var i = 0; i < vfLen; i++) {
        var trRow = '<tr>';
        trRow += '<td>'+variantFilterNames[i]+'</td>';
        trRow += '<td>'+variantFilterValues[i]+'</td>';
        trRow += '</tr>';
        tableData += trRow;
    }

    tableData += '</tbody>';
    tableData += '</table>';

    // set
    CKEDITOR.instances.specifications.setData(tableData);
}

// show & hide add brand panel
function showAddBrandPanel() {
    $('#showBrandBtnPanel').hide();
    $('#addBrandNamePanel, #addBrandBtnPanel').show();
}
function hideAddBrandPanel() {
    $('#showBrandBtnPanel').show();
    $('#addBrandNamePanel, #addBrandBtnPanel').hide();
}

// show & hide add manufacture panel
function showAddManufacturePanel() {
    $('#showManufactureBtnPanel').hide();
    $('#addManufactureNamePanel, #addManufactureBtnPanel').show();
}
function hideAddManufacturePanel() {
    $('#showManufactureBtnPanel').show();
    $('#addManufactureNamePanel, #addManufactureBtnPanel').hide();
}

// show & hide add product filter value panel
function showAddProductFilterPanel(filterId) {
    $('#showAddProductFilterBtnPanel_'+filterId).hide();
    $('#addProductFilterValuePanel_'+filterId+', #addProductFilterBtnPanel_'+filterId).show();
}
function hideAddProductFilterPanel(filterId) {
    $('#showAddProductFilterBtnPanel_'+filterId).show();
    $('#addProductFilterValuePanel_'+filterId+', #addProductFilterBtnPanel_'+filterId).hide();
}

// show & hide add variant filter value panel
function showAddVariantFilterPanel(filterId) {
    $('#showAddVariantFilterBtnPanel_'+filterId).hide();
    $('#addVariantFilterValuePanel_'+filterId+', #addVariantFilterBtnPanel_'+filterId).show();
}
function hideAddVariantFilterPanel(filterId) {
    $('#showAddVariantFilterBtnPanel_'+filterId).show();
    $('#addVariantFilterValuePanel_'+filterId+', #addVariantFilterBtnPanel_'+filterId).hide();
}

// Step 8 validation
function step8Validation(command, $scope) {
    $scope.hideSEMsgs();

    // get data
    var sku = $scope.sku;
    var country = $scope.country;
    var sellerWarrantyDesc = $scope.sellerWarrantyDesc;
    var conditionNote = $scope.conditionNote;
    var legalDisclaimer = $scope.legalDisclaimer;
    var condition = $scope.condition;
    var quantity = $scope.quantity;
    var discountPrice = $('#discountPrice').val();
    var offerPrice = $('#offerPrice').val();
    var totalQuantity = $scope.totalQuantity;
    var packagingLatency = $scope.packagingLatency;
    var supplierStatus = $('#supplierStatus').val();
    var reasonForRejection = $scope.reasonForRejection;
    var cataloguePrice = $scope.cataloguePrice;
    var bulkRangesCnt = ($scope.bulkRanges).length;
    
    // validations
    if(sku == '' || typeof(sku) == 'undefined') {
        $scope.error = 'Please enter SKU';
        showToastMsg(0, 'Please enter SKU');
        return false;
    }
    if(country == '' || typeof(country) == 'undefined') {
        $scope.error = 'Please select country';
        showToastMsg(0, 'Please select country');
        return false;
    }
    if(sellerWarrantyDesc == '' || typeof(sellerWarrantyDesc) == 'undefined') {
        $scope.error = 'Please enter seller warranty';
        showToastMsg(0, 'Please enter seller warranty');
        return false;
    }
    if(conditionNote == '' || typeof(conditionNote) == 'undefined') {
        $scope.error = 'Please enter condition note';
        showToastMsg(0, 'Please enter condition note');
        return false;
    }
    if(legalDisclaimer == '' || typeof(legalDisclaimer) == 'undefined') {
        $scope.error = 'Please enter legal disclaimer';
        showToastMsg(0, 'Please enter legal disclaimer');
        return false;
    }
    if(condition == '' || typeof(condition) == 'undefined') {
        $scope.error = 'Please select condition';
        showToastMsg(0, 'Please select condition');
        return false;
    }
    if(quantity == '' || typeof(quantity) == 'undefined') {
        $scope.error = 'Please enter quantity';
        showToastMsg(0, 'Please enter quantity');
        return false;
    }
    // For custom bulk qty range
    if(1) {
        for(var i = 0; i < bulkRangesCnt; i++) {
            var elemCustomBulkQtyFrom = $('#customBulkRangeFrom_'+i);
            var elemCustomBulkQtyTo = $('#customBulkRangeTo_'+i);
            var customBulkQtyFrom = parseInt($.trim(elemCustomBulkQtyFrom.val()), 10);
            customBulkQtyFrom = isNaN(customBulkQtyFrom) ? 0 : customBulkQtyFrom;
            var customBulkQtyTo = parseInt($.trim(elemCustomBulkQtyTo.val()), 10);
            customBulkQtyTo = isNaN(customBulkQtyTo) ? 0 : customBulkQtyTo;

            // both not empty
            if((customBulkQtyFrom != 0) && (customBulkQtyTo != 0)) {
                // check for same value and range check
                if(!validateCustomBulkQtyRange(i)) {
                    return false;
                }

                // Check if prev range is smaller or not
                // skip 1st record
                // If one range is 0 for custom then get product type range for same
                if(i != 0) {
                    var prevCustomBulkQtyFrom = parseInt($.trim($('#customBulkRangeFrom_'+(i-1)).val()), 10);
                    prevCustomBulkQtyFrom = isNaN(prevCustomBulkQtyFrom) ? 0 : prevCustomBulkQtyFrom;
                    var prevCustomBulkQtyTo = parseInt($.trim($('#customBulkRangeTo_'+(i-1)).val()), 10);
                    prevCustomBulkQtyTo = isNaN(prevCustomBulkQtyTo) ? 0 : prevCustomBulkQtyTo;

                    // if anyone prev qty empty then get product type range
                    if(prevCustomBulkQtyFrom == 0 || prevCustomBulkQtyTo == 0) {
                        var ptSplitQtyRange = ($('#bulkRangeDropdown_'+(i-1)).val()).split('-');
                        prevCustomBulkQtyFrom = ptSplitQtyRange[0];
                        prevCustomBulkQtyTo = ptSplitQtyRange[1];
                    }

                    // now check
                    if((customBulkQtyFrom <= prevCustomBulkQtyTo) || (customBulkQtyFrom <= prevCustomBulkQtyFrom)) {
                        elemCustomBulkQtyFrom.focus();
                        showToastMsg(0, 'Bulk Range cannot be lower than previous range');
                        return false;
                    }

                    // now check
                    if((customBulkQtyTo <= prevCustomBulkQtyTo) || (customBulkQtyTo <= prevCustomBulkQtyFrom)) {
                        elemCustomBulkQtyTo.focus();
                        showToastMsg(0, 'Bulk Range cannot be lower than previous range');
                        return false;
                    }
                }
            }
            // empty both
            else {
                elemCustomBulkQtyFrom.val('');
                elemCustomBulkQtyTo.val('');
            }
        }
    }
    if(discountPrice == '' || typeof(discountPrice) == 'undefined') {
        $scope.error = 'Please enter discount price';
        showToastMsg(0, 'Please enter discount price');
        return false;
    }
    if(offerPrice == '' || typeof(offerPrice) == 'undefined') {
        $scope.error = 'Please enter offer price';
        showToastMsg(0, 'Please enter offer price');
        return false;
    }
    // price difference check
    if(1) {
        var cmPriceGreaterMsg = 'cannot be greater than catalogue price';
        
        // check if discount price is greater than of catalogue price
        if((cataloguePrice > 0) && (discountPrice > cataloguePrice)) {
            $scope.error = 'Discount price '+cmPriceGreaterMsg;
            showToastMsg(0, 'Discount price '+cmPriceGreaterMsg);
            return false;
        }
        // check if offer price is greater than of catalogue price
        if((cataloguePrice > 0) && (offerPrice > cataloguePrice)) {
            $scope.error = 'Offer price '+cmPriceGreaterMsg;
            showToastMsg(0, 'Offer price '+cmPriceGreaterMsg);
            return false;
        }

        // check if discount price is changed 
        // And changed value is out of range of 10-50% of old discount price
        if(command == 'EDIT') {
            var oldDiscountPriceMsg = 'cannot be '+priceDiffUp+'% extra or '+priceDiffDown+'% less than of last added discount price';
            var oldDiscountPrice = $scope.oldDiscountPrice;
            var oldDiscountPriceUp = oldDiscountPrice + (oldDiscountPrice * (priceDiffUp/100));
            var oldDiscountPriceDown = oldDiscountPrice - (oldDiscountPrice * (priceDiffDown/100));
            if((discountPrice > oldDiscountPriceUp) || (discountPrice < oldDiscountPriceDown)) {
                $('#discountPrice').focus();
                showToastMsg(0, 'New Discount price '+oldDiscountPriceMsg);
                return false;
            }
        }

        // for bulk discount prices
        for(var i = 0; i < bulkRangesCnt; i++) {
            var elem = $('#discountPrice_'+i);
            var tmpPrice = parseInt(elem.val(), 10);
            if(tmpPrice != '' && tmpPrice != 0) {
                // for greater than catalogue price check
                if((cataloguePrice > 0) && (tmpPrice > cataloguePrice)) {
                    elem.focus();
                    showToastMsg(0, 'Discount price '+cmPriceGreaterMsg);
                    return false;
                }

                // for previous value > next value check
                var prevVal;
                if(i == 0) {
                    prevVal = $('#discountPrice').val();
                }
                else {
                    prevVal = $('#discountPrice_'+(i-1)).val();
                }
                prevVal = parseInt(prevVal, 10);

                if(tmpPrice > prevVal) {
                    elem.focus();
                    showToastMsg(0, 'Discount price cannot be larger than previous price');
                    return false;
                }
            }
        }

        // for bulk offer prices
        for(var i = 0; i < bulkRangesCnt; i++) {
            var elem = $('#offerPrice_'+i);
            var tmpPrice = parseInt(elem.val(), 10);
            if(tmpPrice != '' && tmpPrice != 0) {
                // for greater than catalogue price check
                if((cataloguePrice > 0) && (tmpPrice > cataloguePrice)) {
                    elem.focus();
                    showToastMsg(0, 'Offer price '+cmPriceGreaterMsg);
                    return false;
                }

                // for previous value > next value check
                var prevVal;
                if(i == 0) {
                    prevVal = $('#offerPrice').val();
                }
                else {
                    prevVal = $('#offerPrice_'+(i-1)).val();
                }
                prevVal = parseInt(prevVal, 10);

                if(tmpPrice > prevVal) {
                    elem.focus();
                    showToastMsg(0, 'Offer price cannot be larger than previous price');
                    return false;
                }
            }
        }
    }
    if(totalQuantity == '' || typeof(totalQuantity) == 'undefined') {
        $scope.error = 'Please enter total quantity';
        showToastMsg(0, 'Please enter total quantity');
        return false;
    }
    if(packagingLatency == '' || typeof(packagingLatency) == 'undefined') {
        $scope.error = 'Please enter packaging latency';
        showToastMsg(0, 'Please enter packaging latency');
        return false;
    }
    if(supplierStatus == '' || typeof(supplierStatus) == 'undefined') {
        $scope.error = 'Please select supplier status';
        showToastMsg(0, 'Please select supplier status');
        return false;
    }
    if(supplierStatus == 3 && (reasonForRejection == '' || typeof(reasonForRejection) == 'undefined')) {
        $scope.error = 'Please enter reason for rejection';
        showToastMsg(0, 'Please enter reason for rejection');
        return false;
    }
    
    $scope.hideSEMsgs();
    return true;
}

// fill prices to bulk
function fillBulkDiscountPrice() {
    var discountPrice = $('#discountPrice').val();
    $("input[name='bulkDiscountPrice[]']").map(function() {
        var tmpPrice = $(this).val();
        if(tmpPrice == '' || tmpPrice == 0) {
            // $(this).val(discountPrice);
            $(this).val(0);
        }
        return;
    });
}

// fill prices to bulk
function fillBulkOfferPrice() {
    var offerPrice = $('#offerPrice').val();
    $("input[name='bulkOfferPrice[]']").map(function() {
        var tmpPrice = $(this).val();
        if(tmpPrice == '' || tmpPrice == 0) {
            // $(this).val(offerPrice);
            $(this).val(0);
        }
        return;
    });
}

// calculate offer price
function calculateAutoDOPrice(discountInputId, offerInputId, type, $scope) {
    // for calculating offer price
    if(type == 1) {
        var tmpDiscountPrice = parseInt($('#'+discountInputId).val(), 10);
        var tmpProductTypeCommission = $scope.productTypeCommission;
        var tmpOfferPrice = tmpDiscountPrice + Math.ceil(tmpDiscountPrice * (tmpProductTypeCommission/100));        
        $('#'+offerInputId).val(tmpOfferPrice);
    }
    // for calculating discount price
    else {
        var tmpOfferPrice = parseInt($('#'+offerInputId).val(), 10);
        var tmpProductTypeCommission = $scope.productTypeCommission;
        var tmpDiscountPrice = tmpOfferPrice - Math.ceil(tmpOfferPrice * (tmpProductTypeCommission/100));
        $('#'+discountInputId).val(tmpDiscountPrice);
    }
}

// show reason for rejection
function showReasonForRejection() {
    var supplierStatus = $('#supplierStatus').val();
    if(supplierStatus == 3) {
        $('#reasonForRejectionPanel').show();
    }
    else {
        $('#reasonForRejectionPanel').hide();
    }
}

// get search product code
function getSearchedProductCodes(command, $scope, $http, $timeout) {
    $(document).ready(function() {
        $("#searchProductCode").tokenInput('/product/getSearchedProductCodes', {
            method: 'POST',
            queryParam: 'searchTerm',
            minChars: 2,
            tokenLimit: 1,
            propertyToSearch: 'name',
            resultsLimit: 10,
            preventDuplicates: true,
            hintText: 'Type a product code',
            onAdd: function (item) {
                getProductBasicDetails(item.id, 0, command, $scope, $http, $timeout);
                showHideProductCodeSearch(0);
            }
        });
    });
}

// get product basic details
function getProductBasicDetails(id, btnClick, command, $scope, $http, $timeout) {
    // save product id
    if(command == 'ADD') {
        $scope.productId = id;
    }

    // set this id to new id for edit supplier 
    if(command == 'EDIT-SUPPLIER') {
        $scope.changedProductId = id;
    }

    // get product details
    $http.get('/product/getBasicDetails/'+id)
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                // hide step 1,2 , show step 3
                if(btnClick == 1) {
                    $('#step1MinBtn, #step3MinBtn').click();
                }
                $('#step2, #step3').show();

                // hide step 1 & 2 btns
                $('#saveBasicDetailBtn, #step1NextBtn').hide();

                $scope.headers = response.headers('Product');
                var productDetails = response.data.data[0];

                $scope.tag = productDetails.tag;
                $scope.productCode = productDetails.productCode;
                var category = productDetails.category;
                var brand = productDetails.brand;
                var manufacturer = productDetails.manufacturer;
                var productFilters = productDetails.productFilters;

                // fill data
                // product type
                $scope.productType = category._id+'_##_'+category.name;                    

                $timeout(function () {
                    $('#productType').attr("disabled", true);
                    // show product & variant filters
                    $scope.getProductTypeFilters();                        
                    $scope.getVaraintFilters();

                    // update only bulk qty
                    if(command == 'EDIT-SUPPLIER') {
                        updateOnlyProductTypeBulkRangeQtyValue($scope);
                    }

                    $(document).ready(function() {
                        // brand // check if brand still inactive
                        $scope.selectedBrand = {_id: brand._id, name: brand.name};
                        $('#brand').tokenInput("clear");
                        $('#brand').tokenInput("add", {_id: brand._id, name: brand.name, isActive: 0, checkStatus: 1});
                        // manufacturer // check if manufacturer still inactive
                        $scope.selectedManufacturer = {_id: manufacturer._id, name: manufacturer.name};
                        $('#manufacturer').tokenInput("clear");
                        $('#manufacturer').tokenInput("add", {_id: manufacturer._id, name: manufacturer.name, isActive: 0, checkStatus: 1});
                    });

                    // product type filters
                    $timeout(function () {
                        angular.forEach(productFilters, function(value, key) {
                            $("#productFilterValue_"+value._id).val(value.value);
                        });
                    }, 200);

                }, 500);

            }
        });
}

// toggle product search box
function showHideProductCodeSearch(value) {
    if(value == 1) {
        $('#productCodePanel .token-input-list').show();
        $('#productCode, #searchCodeText').hide();        
    }
    else {
        $('#productCodePanel .token-input-list').hide();
        $('#productCode, #searchCodeText').show();
    }
}

// toogle add related product box
function toggleRelatedProductsPanel() {
    var defaultLogicChecked = $('#defaultLogicRelatedProduct').is(":checked");
    
    // hide related product
    if(defaultLogicChecked == true) {
        $('#relatedProductsPanel').hide();
    }
    // show
    else {
        $('#relatedProductsPanel').show();
    }
}

// Get variant details for edit
function getVariantDetailsForEdit(command, $scope, $http, $timeout) {
    $http.get('/product/getVariantDetails/'+$scope.productId+'/'+$scope.variantId)
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                var variantDetails = response.data.data[0].variants[0];

                // fill datas
                $scope.catalogueId = variantDetails.catalogueId;
                $scope.productVariantName = variantDetails.name;
                var variantFilters = variantDetails.filters;
                $scope.package = variantDetails.package;                    
                $scope.catalogueFile = variantDetails.catalogueFile;                    
                var relatedProducts = variantDetails.relatedProducts;                
                $scope.status = (variantDetails.isActive).toString();
                var isOpen = variantDetails.isOpen;                    
                if(isOpen == false) {
                    $('#isOpenNo').prop('checked', true);
                }

                // for supplier edit
                if(command == 'SUPPLIER-EDIT') {
                    var suppliers = variantDetails.suppliers;
                    var suppliersCnt = suppliers.length;
                    $scope.cataloguePrice = (typeof(variantDetails.cataloguePrice) != 'undefined') ? variantDetails.cataloguePrice : 0;
                }

                // For preloading DB images to dropzone
                var productImages = variantDetails.images;
                var productImgCnt = productImages.length;
                if(productImgCnt > 0) {
                    for(var i = 0; i < productImgCnt; i++) {
                        var tmpImgName = productImages[i].name;
                        var tmpImgDefault = productImages[i].isDefault;
                        var imgFile = { name: tmpImgName, size: 0 };
                        $scope.dropzoneGallery.options.addedfile.call($scope.dropzoneGallery, imgFile);
                        $scope.dropzoneGallery.options.thumbnail.call($scope.dropzoneGallery, imgFile, productImgAwsPath+tmpImgName);

                        // add to array
                        $scope.uploadedImages.push({'originalName': tmpImgName, 'renamedName': tmpImgName});

                        // add images to default dropdown
                        var selected = '';
                        if(tmpImgDefault == true) {
                            selected = 'selected';
                        }
                        $('#defaultImage').append('<option value="'+tmpImgName+'" '+selected+'>'+tmpImgName+'</option>');
                    }
                }
                // reduce max files option
                $scope.dropzoneGallery.options.maxFiles = 5 - productImgCnt;                    

                // for preloading catalogue file
                if($scope.catalogueFile != '' && typeof($scope.catalogueFile) != 'undefined') {
                    var tmpFile = { name: $scope.catalogueFile, size: 0 };
                    $scope.dropzoneCatalogue.options.addedfile.call($scope.dropzoneCatalogue, tmpFile);
                    $scope.dropzoneCatalogue.options.thumbnail.call($scope.dropzoneCatalogue, tmpFile, catalogueFileAwsPath+$scope.catalogueFile);
                    // reduce max files option
                    $scope.dropzoneCatalogue.options.maxFiles = 0;
                }
                // hide image sizes
                $('.dz-size, .dz-progress').hide();

                // fill data
                $timeout(function () {
                    $(document).ready(function() {
                        CKEDITOR.instances.description.setData(variantDetails.description);
                        CKEDITOR.instances.specifications.setData(variantDetails.specifications);
                    });
                }, 500);

                // variant level filters
                $timeout(function () {
                    angular.forEach(variantFilters, function(value, key) {
                        $("#variantFilterValue_"+value._id).val(value.value);
                    });

                    // for supplier edit
                    if(command == 'SUPPLIER-EDIT') {
                        // show bulk ranges & commission
                        $scope.getProductTypeBulkRanges();
                        $scope.getProductTypeCommission();
                    }
                }, 1000);

                // add related products
                $(document).ready(function() {
                    var relatedProductsCnt = relatedProducts.length;
                    for(var i = 0; i < relatedProductsCnt; i++) {
                        $('#relatedProducts').tokenInput("add", {id: relatedProducts[i], name: relatedProducts[i]});
                    }
                });

                // for supplier edit
                if(command == 'SUPPLIER-EDIT') {
                    // fill pricing data
                    $scope.supplierIndex = -1;
                    for(var i = 0; i < suppliersCnt; i++) {
                        if($scope.supplierId == suppliers[i]._id) {
                            $scope.supplierIndex = i;
                            $scope.sku = suppliers[i].sku;
                            $scope.sellerWarrantyDesc = suppliers[i].sellerWarranty;
                            $scope.conditionNote = suppliers[i].conditionNote;
                            $scope.legalDisclaimer = suppliers[i].legalDisclaimer;
                            $scope.quantity = suppliers[i].quantity;
                            $scope.discountPrice = suppliers[i].discountPrice;
                            $scope.oldDiscountPrice = suppliers[i].discountPrice;
                            $scope.offerPrice = suppliers[i].offerPrice;
                            $scope.totalQuantity = suppliers[i].totalQuantity;
                            $scope.packagingLatency = suppliers[i].packagingLatency;
                            $scope.supplierStatus = (suppliers[i].isActive).toString();
                            $scope.reasonForRejection = suppliers[i].rejectedReason;
                            if($scope.supplierStatus == 3 && typeof($scope.reasonForRejection) != 'undefined') {
                                $('#reasonForRejectionPanel').show();
                            }

                            var bulkOrder = suppliers[i].bulkOrder;
                            var bulkOrderCnt = bulkOrder.length;
                            $timeout(function () {
                                for(var j = 0; j < bulkOrderCnt; j++) {
                                    $('#discountPrice_'+j).val(bulkOrder[j].discountPrice);
                                    $('#offerPrice_'+j).val(bulkOrder[j].offerPrice);

                                    // check if supplier added custom range
                                    var supplierBulkRangeQty = bulkOrder[j].quantity;
                                    var productTypeBulkRangeQty = $.trim($('#bulkRangeDropdown_'+j).val());
                                    if(supplierBulkRangeQty != productTypeBulkRangeQty) {
                                        // show custom input box
                                        showCustomBulkRangeInputs(j);
                                        // fill qty
                                        var splitBulkQtyRange = supplierBulkRangeQty.split('-');
                                        $('#customBulkRangeFrom_'+j).val(splitBulkQtyRange[0]);
                                        $('#customBulkRangeTo_'+j).val(splitBulkQtyRange[1]);
                                    }
                                }
                            }, 1300);
                            break;
                        }
                    }
                }

                // show step 4, 5, 6, 7
                $('#step4MinBtn, #step5MinBtn, #step6MinBtn, #step7MinBtn').click();
            }
            else {
                $scope.error = 'Product variant not found';
                showToastMsg(0, 'Product variant not found');
            }
        });
}

// Show custom bulk ranges
function showCustomBulkRangeInputs(index) {
    $('#customBulkRangeInputs_'+index).show();
    $('#customBulkRangeBtn_'+index).hide();
}

// Get bulk order array for save
function getBulkOrderArrayForSave(bulkRangesValues, bulkDiscountPriceValues, bulkOfferPriceValues) {
    // for bulk order object
    var bulkOrderArr = new Array();
    var boLen = bulkRangesValues.length;
    for(var i = 0; i < boLen; i++) {
        // Custom range
        var tmpQtyRangeFrom = $.trim($('#customBulkRangeFrom_'+i).val());
        var tmpQtyRangeTo = $.trim($('#customBulkRangeTo_'+i).val());
        if(tmpQtyRangeFrom != '' && tmpQtyRangeTo != '') {
            var finalBulkRangeQty = tmpQtyRangeFrom+'-'+tmpQtyRangeTo;
        }
        else {
            var finalBulkRangeQty = bulkRangesValues[i];
        }

        var tmpObj = new Object();
        tmpObj.quantity = finalBulkRangeQty;
        tmpObj.discountPrice = (bulkDiscountPriceValues[i] != '') ? bulkDiscountPriceValues[i] : 0;
        tmpObj.offerPrice = (bulkOfferPriceValues[i] != '') ? bulkOfferPriceValues[i] : 0;

        bulkOrderArr.push(tmpObj);
    }

    return bulkOrderArr;
}

// validate custom bulk range qty on input blur
function validateCustomBulkQtyRange(index) {
    var customBulkQtyFrom = parseInt($.trim($('#customBulkRangeFrom_'+index).val()), 10);
    customBulkQtyFrom = isNaN(customBulkQtyFrom) ? 0 : customBulkQtyFrom;
    var customBulkQtyTo = parseInt($.trim($('#customBulkRangeTo_'+index).val()), 10);
    customBulkQtyTo = isNaN(customBulkQtyTo) ? 0 : customBulkQtyTo;

    // both values same
    if((customBulkQtyFrom != 0) && (customBulkQtyTo != 0) && (customBulkQtyTo == customBulkQtyFrom)) {
        $('#customBulkRangeTo_'+index).val('');
        showToastMsg(0, 'Quantity range cannot have same value for both from & to');
        return false;
    }

    // check if to < from
    if((customBulkQtyFrom != 0) && (customBulkQtyTo != 0) && (customBulkQtyTo < customBulkQtyFrom)) {
        $('#customBulkRangeTo_'+index).val('');
        showToastMsg(0, 'Quantity range not proper');
        return false;
    }

    return true;
}

// Add product
app.controller('ProductAddController', function ($timeout, $scope, $http, $location, $routeParams, $route) {
    // check for access
    noAccessRedirect($location);

    // for gallery images uploading
    $scope.uploadedImages = new Array();
    $scope.dropzoneConfig = getDropzoneConfig($scope, $http);

    // for catalogue file uploading
    $scope.catalogueFile = '';
    $scope.dropzoneConfigCatalogue = getDropzoneConfigCatalogue($scope, $http);

    // for ckeditor
    $(function () {
        // instance, using default configuration.
        CKEDITOR.replace('description');
        CKEDITOR.replace('specifications');
    });

    // get product type list
    $scope.productTypeList = new Array();
    // only get if full add
    if($routeParams.id == '' || typeof($routeParams.id) == 'undefined') {
        $http.get('/category/getAllProductTypes')
            .then(function (response) {
                if(response.data.error == 0 && response.data.data != '') {
                    $scope.productTypeList = response.data.data;
                }
            });
    }

    // get token input item display status
    $scope.getTokenItemStatus = function(item) {
        return getTokenItemStatus(item);
    };

    // get brand list
    $scope.brandList = new Array();
    $scope.selectedBrand = new Object();
    getAllBrands($scope, $http);

    // get manufacturer list
    $scope.manufacturerList = new Array();
    $scope.selectedManufacturer = new Object();
    getAllManufacturers($scope, $http);    

    // toggle product search box
    $scope.showHideProductCodeSearch = function(value) {
        showHideProductCodeSearch(value);
    };

    // show filters
    $scope.showFilters = function() {
        $scope.getProductTypeFilters();
        $scope.getVaraintFilters();
    };

    // get product type filters
    $scope.productTypeFilters = new Array();
    $scope.getProductTypeFilters = function() {
        getProductTypeFilters($scope);
    }

    // get variant filters
    $scope.variantFilters = new Array();
    $scope.getVaraintFilters = function() {
        getVaraintFilters($scope);
    }

    // hide success & error message
    $scope.hideSEMsgs = function() {
        $scope.error = '';
        $scope.success = '';
    }

    // show add product filter value panel
    $scope.showAddProductFilterPanel = function(filterId) {
        showAddProductFilterPanel(filterId);
    };
    // show add variant filter value panel
    $scope.showAddVariantFilterPanel = function(filterId) {
        showAddVariantFilterPanel(filterId);
    };    
    // show add brand panel
    $scope.showAddBrandPanel = function() {
        showAddBrandPanel();
    };
    // show add manufacture panel
    $scope.showAddManufacturePanel = function() {
        showAddManufacturePanel();
    };

    // insert brand
    $scope.addBrand = function() {
        addBrand($scope, $http, $timeout);
    };

    // insert manufacturer
    $scope.addManufacturer = function() {
        addManufacturer($scope, $http, $timeout);
    };

    // Add product filter value
    $scope.addProductFilterValue = function(filterId) {
        addProductFilterValue(filterId, $scope, $http, $timeout);
    };

    // Add variant filter value    
    $scope.addVariantFilterValue = function(filterId) {
        addVariantFilterValue(filterId, $scope, $http, $timeout);
    };    

    // Step 1 validation
    $scope.step1Validation = function (showNextStep) {
        return step1Validation(showNextStep, 'ADD', $scope);
    }

    // save basic details of product
    $scope.saveBasicDetails = function () {
        $scope.hideSEMsgs();

        // get data
        var productType = $scope.productType;
        var productCode = $scope.productCode;
        var tag = $scope.tag;
        // var brand = $('#brand').val();
        var brand = $scope.selectedBrand;
        // var manufacturer = $('#manufacturer').val();
        var manufacturer = $scope.selectedManufacturer;
        var productFilterNames = $("input[name='productFilterName[]']").map(function(){return $(this).val();}).get();
        var productFilterIds = $("input[name='productFilterId[]']").map(function(){return $(this).val();}).get();
        var productFilterValues = $("select[name='productFilterValue[]']").map(function(){return $(this).val();}).get();

        // validations
        if(!$scope.step1Validation(0)) {
            return false;
        }
        if(productFilterNames.length == 0) {
            $scope.error = 'Please add product filters';
            showToastMsg(0, 'Please add product filters');
            return false;    
        }

        // create category, brand, manufacturer object
        var productTypeSplit = productType.split('_##_');
        var productTypeObj = {_id: productTypeSplit[0], name: productTypeSplit[1]};
        // var brandSplit = brand.split('_##_');
        // var brandObj = {_id: brandSplit[0], name: brandSplit[1]};
        // var manufacturerSplit = manufacturer.split('_##_');
        // var manufacturerObj = {_id: manufacturerSplit[0], name: manufacturerSplit[1]};
        // for product filter object
        var productFilterArr = new Array();
        var pfLen = productFilterNames.length;
        for(var i = 0; i < pfLen; i++) {
            var tmpObj = new Object();
            tmpObj._id = productFilterIds[i];
            tmpObj.name = productFilterNames[i];
            tmpObj.value = productFilterValues[i];

            productFilterArr.push(tmpObj);
        }

        // data
        var data = {
            tag: tag,
            productCode: productCode,
            category: JSON.stringify(productTypeObj),
            brand: JSON.stringify(brand),
            manufacturer: JSON.stringify(manufacturer),
            productFilters: JSON.stringify(productFilterArr)
        };
        var config = {};

        $http.post('/product/basicInsert', data, config)
        .success(function (data, status, headers, config) {
            console.log('success add - ', data);

            if(data.error == 1) {
                $scope.error = data.msg;
                showToastMsg(0, data.msg);
                return false;
            }
            $scope.success = data.msg;
            showToastMsg(1, data.msg);

            // save product id in $scope
            $scope.productId = data.data._id;

            // show step 3 & hide step 2
            $('#step3MinBtn, #step2MinBtn').click();
            $('#step3').show();
            // disable category selection
            $('#productType').attr("disabled", true);

            // hide step 1 & 2 btns
            $('#saveBasicDetailBtn, #step1NextBtn').hide();
        })
        .error(function (data, status, header, config) {
            console.log('error add - ', data);
            $scope.error = 'Unable to add product basic details';
            showToastMsg(0, 'Unable to add product basic details');
        });
    };

    // product id validation
    $scope.productIdValidation = function () {
        return productIdValidation($scope);
    }

    // Step 3 validation
    $scope.step3Validation = function(showNextStep) {
        return step3Validation(showNextStep, 'ADD', $scope);
    }

    // Step 4 validation
    $scope.step4Validation = function (showNextStep) {
        return step4Validation(showNextStep, 'ADD', $scope);
    }

    // Step 5 validation
    $scope.step5Validation = function(showNextStep) {
        return step5Validation(showNextStep, 'ADD', $scope);
    }

    // step 6 validations
    $scope.step6Validation = function(showNextStep) {
        return step6Validation(showNextStep, 'ADD', $scope);
    }

    // step 7 validations
    $scope.step7Validation = function() {
        return step7Validation('ADD', $scope);
    }

    // add specifications from selected filters
    $scope.setSpecsFromFilters = function() {
        setSpecsFromFilters();
    }

    // get related products
    getRelatedVariants();

    // hide/show related prod box    
    $scope.toggleRelatedProductsPanel = function() {
        toggleRelatedProductsPanel();
    };

    // get search product code
    getSearchedProductCodes('ADD', $scope, $http, $timeout);

    // save variants
    $scope.saveVariant = function () {
        $scope.hideSEMsgs();

        // for product id
        var productId = $scope.productId;
        if(!$scope.productIdValidation()) {
            return false;
        }

        // do validations
        if(!$scope.step1Validation(0)) {
            return false;
        }
        if(!$scope.step3Validation(0)) {
            return false;
        }
        if(!$scope.step4Validation(0)) {
            return false;
        }
        if(!$scope.step5Validation(0)) {
            return false;
        }
        if(!$scope.step7Validation()) {
            return false;
        }

        // get data
        var catalogueId = $scope.catalogueId;
        var productVariantName = $scope.productVariantName;
        var variantFilterNames = $("input[name='variantFilterName[]']").map(function(){return $(this).val();}).get();
        var variantFilterIds = $("input[name='variantFilterId[]']").map(function(){return $(this).val();}).get();
        var variantFilterValues = $("select[name='variantFilterValue[]']").map(function(){return $(this).val();}).get();
        var uploadedImages = $scope.uploadedImages;
        var defaultImage = $('#defaultImage').val();
        var package = $scope.package;
        var isOpen = $('input[name=isOpen]:checked').val();
        // var description = $scope.description;
        var description = CKEDITOR.instances.description.getData();
        // var specifications = $scope.specifications;
        var specifications = CKEDITOR.instances.specifications.getData();
        var catalogueFile = $scope.catalogueFile;
        var status = $scope.status;
        var relatedProductsRawValue = $('#relatedProducts').tokenInput("get");
        var defaultLogicRelatedProduct = ($('#defaultLogicRelatedProduct').is(":checked") == true) ? 1 : 0;

        // for variant filter object
        var variantFilterArr = new Array();
        var pfLen = variantFilterNames.length;
        for(var i = 0; i < pfLen; i++) {
            var tmpObj = new Object();
            tmpObj._id = variantFilterIds[i];
            tmpObj.name = variantFilterNames[i];
            tmpObj.value = variantFilterValues[i];

            variantFilterArr.push(tmpObj);
        }

        // for images
        var prdImgCnt = uploadedImages.length;
        var finalImagesArr = new Array();
        for(var i = 0; i < prdImgCnt; i++) {
            var tmpObj = new Object();
            tmpObj.name = uploadedImages[i].renamedName;

            // check for default
            var defaultChk = false;
            if(defaultImage == uploadedImages[i].renamedName) {
                defaultChk = true;
            }
            tmpObj.isDefault = defaultChk;

            finalImagesArr.push(tmpObj);
        }

        // get proper related poducts values
        var relatedProductsRawValueCnt = relatedProductsRawValue.length;
        var relatedProducts = new Array();
        for(var i = 0; i < relatedProductsRawValueCnt; i++) {
            relatedProducts.push(relatedProductsRawValue[i]['id']);
        }

        // data
        var data = {
            productId: productId,
            catalogueId: catalogueId,
            productVariantName: productVariantName,
            variantFilters: JSON.stringify(variantFilterArr),
            productImages: JSON.stringify(finalImagesArr),
            package: package,
            description: description,
            specifications: specifications,
            isOpen: isOpen,
            catalogueFile: catalogueFile,
            relatedProducts: JSON.stringify(relatedProducts),
            status: status,
            brand: JSON.stringify($scope.selectedBrand),
            manufacturer: JSON.stringify($scope.selectedManufacturer),
            defaultLogicRelatedProduct: defaultLogicRelatedProduct
        };
        var config = {};

        $http.post('/product/insertVariant', data, config)
        .success(function (data, status, headers, config) {
            console.log('success add - ', data);

            if(data.error == 1) {
                $scope.error = data.msg;
                showToastMsg(0, data.msg);
                return false;
            }
            $scope.success = data.msg;
            showToastMsg(1, data.msg);

            // show & hide btn            
            $('#saveVariantBtn').hide();
            $('#addMoreVariantBtn').show();
        })
        .error(function (data, status, header, config) {
            console.log('error add - ', data);
            $scope.error = 'Unable to add product variant';
            showToastMsg(0, 'Unable to add product variant');
        });
    };

    // Add more variant call
    $scope.addMoreVariant = function() {
        // product id validation
        if(!$scope.productIdValidation()) {
            return false;
        }

        // reload page
        if($routeParams.id != '' && typeof($routeParams.id) != 'undefined') {
            $route.reload();
        }
        else {
            $location.path('/product/add/'+$scope.productId);
        }
    }

    // Get product basic details // this is not variant details
    if($routeParams.id != '' && typeof($routeParams.id) != 'undefined') {
        $scope.productId = $routeParams.id;

        // get product types first
        $http.get('/category/getAllProductTypes')
            .then(function (response) {
                if(response.data.error == 0 && response.data.data != '') {
                    $scope.productTypeList = response.data.data;

                    // get product details
                    getProductBasicDetails($scope.productId, 1, 'ADD', $scope, $http, $timeout);
                }
            });
    }
});

// Edit product
app.controller('ProductEditController', function ($timeout, $scope, $http, $location, $routeParams) {
    // check for access
    noAccessRedirect($location);

    // get route params
    $scope.productId = $routeParams.productId;
    $scope.variantId = $routeParams.variantId;

    // for gallery images uploading
    $scope.uploadedImages = new Array();
    $scope.dropzoneConfig = getDropzoneConfig($scope, $http);


    // for catalogue file uploading
    $scope.catalogueFile = '';
    $scope.dropzoneConfigCatalogue = getDropzoneConfigCatalogue($scope, $http);

    // for ckeditor
    $(function () {
        // instance, using default configuration.
        CKEDITOR.replace('description');
        CKEDITOR.replace('specifications');
    });

    // store product type list
    $scope.productTypeList = new Array();    

    // get token input item display status
    $scope.getTokenItemStatus = function(item) {
        return getTokenItemStatus(item);
    }

    // get brand list
    $scope.brandList = new Array();
    $scope.selectedBrand = new Object();
    getAllBrands($scope, $http);

    // get manufacturer list
    $scope.manufacturerList = new Array();
    $scope.selectedManufacturer = new Object();
    getAllManufacturers($scope, $http);

    // show add product filter value panel
    $scope.showAddProductFilterPanel = function(filterId) {
        showAddProductFilterPanel(filterId);
    };
    // show add variant filter value panel
    $scope.showAddVariantFilterPanel = function(filterId) {
        showAddVariantFilterPanel(filterId);
    };    
    // show add brand panel
    $scope.showAddBrandPanel = function() {
        showAddBrandPanel();
    };
    // show add manufacture panel
    $scope.showAddManufacturePanel = function() {
        showAddManufacturePanel();
    };

    // get related products
    getRelatedVariants();

    // hide/show related prod box    
    $scope.toggleRelatedProductsPanel = function() {
        toggleRelatedProductsPanel();
    };

    // get search product code
    getSearchedProductCodes('EDIT', $scope, $http, $timeout);

    // Get product basic details
    $http.get('/product/getBasicDetails/'+$scope.productId)
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                // show step 1,2,3
                $('#step3MinBtn').click();
                // hide step 1 & 2 btns
                // $('#saveBasicDetailBtn, #step1NextBtn').hide();

                $scope.headers = response.headers('Product');
                var productDetails = response.data.data[0];

                $scope.tag = productDetails.tag;
                $scope.productCode = productDetails.productCode;
                var category = productDetails.category;
                var brand = productDetails.brand;
                var manufacturer = productDetails.manufacturer;
                var productFilters = productDetails.productFilters;

                // get product type list
                $http.get('/category/getAllProductTypes')
                    .then(function (response) {
                        if(response.data.error == 0 && response.data.data != '') {
                            $scope.productTypeList = response.data.data;

                            // product type
                            $scope.productType = category._id+'_##_'+category.name;

                            // get product variant details
                            getVariantDetailsForEdit('EDIT', $scope, $http, $timeout);

                            // show product & variant filters
                            $timeout(function () {
                                $('#productType').attr("disabled", true);
                                $scope.getProductTypeFilters();
                                $scope.getVaraintFilters();

                                // product type filters
                                $timeout(function () {
                                    angular.forEach(productFilters, function(value, key) {
                                        $("#productFilterValue_"+value._id).val(value.value);
                                    });
                                }, 200);

                            }, 200);
                        }
                    });

                // fill data
                $timeout(function () {
                    $(document).ready(function() {
                        // brand
                        $scope.selectedBrand = {_id: brand._id, name: brand.name};
                        $('#brand').tokenInput("add", {_id: brand._id, name: brand.name, isActive: 0, checkStatus: 1});
                        // manufacturer
                        $scope.selectedManufacturer = {_id: manufacturer._id, name: manufacturer.name};
                        $('#manufacturer').tokenInput("add", {_id: manufacturer._id, name: manufacturer.name, isActive: 0, checkStatus: 1});
                    });
                }, 500);

            }
            else {
                $scope.error = 'Product details not found';
                showToastMsg(0, 'Product details not found');
            }
        });    

    // show filters
    $scope.showFilters = function() {
        $scope.getProductTypeFilters();
        $scope.getVaraintFilters();
    };

    // get product type filters
    $scope.productTypeFilters = new Array();
    $scope.getProductTypeFilters = function() {
        getProductTypeFilters($scope);
    }

    // get variant filters
    $scope.variantFilters = new Array();
    $scope.getVaraintFilters = function() {
        getVaraintFilters($scope);
    }

    // hide success & error message
    $scope.hideSEMsgs = function() {
        $scope.error = '';
        $scope.success = '';
    }

    // insert brand
    $scope.addBrand = function() {
        addBrand($scope, $http, $timeout);
    };

    // insert manufacturer
    $scope.addManufacturer = function() {
        addManufacturer($scope, $http, $timeout);
    }

    // Add product filter value
    $scope.addProductFilterValue = function(filterId) {
        addProductFilterValue(filterId, $scope, $http, $timeout);
    }

    // Add variant filter value    
    $scope.addVariantFilterValue = function(filterId) {
        addVariantFilterValue(filterId, $scope, $http, $timeout);
    }

    // Step 1 validation
    $scope.step1Validation = function (showNextStep) {
        return step1Validation(showNextStep, 'EDIT', $scope);
    }

    // update basic details of product
    $scope.updateBasicDetails = function () {
        $scope.hideSEMsgs();

        // get data
        var productType = $scope.productType;
        var productCode = $scope.productCode;
        var tag = $scope.tag;
        var brand = $scope.selectedBrand;
        var manufacturer = $scope.selectedManufacturer;
        var productFilterNames = $("input[name='productFilterName[]']").map(function(){return $(this).val();}).get();
        var productFilterIds = $("input[name='productFilterId[]']").map(function(){return $(this).val();}).get();
        var productFilterValues = $("select[name='productFilterValue[]']").map(function(){return $(this).val();}).get();

        // validations
        if(!$scope.step1Validation(0)) {
            return false;
        }
        if(productFilterNames.length == 0) {
            $scope.error = 'Please add product filters';
            showToastMsg(0, 'Please add product filters');
            return false;    
        }

        // create category, brand, manufacturer object
        var productTypeSplit = productType.split('_##_');
        var productTypeObj = {_id: productTypeSplit[0], name: productTypeSplit[1]};
        // for product filter object
        var productFilterArr = new Array();
        var pfLen = productFilterNames.length;
        for(var i = 0; i < pfLen; i++) {
            var tmpObj = new Object();
            tmpObj._id = productFilterIds[i];
            tmpObj.name = productFilterNames[i];
            tmpObj.value = productFilterValues[i];

            productFilterArr.push(tmpObj);
        }

        // data
        var data = {
            tag: tag,
            productCode: productCode,
            category: JSON.stringify(productTypeObj),
            brand: JSON.stringify(brand),
            manufacturer: JSON.stringify(manufacturer),
            productFilters: JSON.stringify(productFilterArr)
        };
        var config = {};

        $http.put('/product/basicUpdate/'+$scope.productId, data, config)
        .success(function (data, status, headers, config) {
            console.log('success add - ', data);

            if(data.error == 1) {
                $scope.error = data.msg;
                showToastMsg(0, data.msg);
                return false;
            }
            $scope.success = data.msg;
            showToastMsg(1, data.msg);
        })
        .error(function (data, status, header, config) {
            console.log('error add - ', data);
            $scope.error = 'Unable to add product basic details';
            showToastMsg(0, 'Unable to add product basic details');
        });
    };

    // product id validation
    $scope.productIdValidation = function () {
        return productIdValidation($scope);
    }

    // Step 3 validation
    $scope.step3Validation = function(showNextStep) {
        return step3Validation(showNextStep, 'EDIT', $scope);
    }

    // Step 4 validation
    $scope.step4Validation = function (showNextStep) {
        return step4Validation(showNextStep, 'EDIT', $scope);
    }

    // Step 5 validation
    $scope.step5Validation = function(showNextStep) {
        return step5Validation(showNextStep, 'EDIT', $scope);
    }

    // step 6 validations
    $scope.step6Validation = function(showNextStep) {
        return step6Validation(showNextStep, 'EDIT', $scope);
    }

    // step 7 validations
    $scope.step7Validation = function() {
        return step7Validation('EDIT', $scope);
    }

    // add specifications from selected filters
    $scope.setSpecsFromFilters = function() {
        setSpecsFromFilters();
    }

    // update variants
    $scope.updateVariant = function () {
        $scope.hideSEMsgs();

        // for product id
        var productId = $scope.productId;
        if(!$scope.productIdValidation()) {
            return false;
        }

        // do validations
        if(!$scope.step1Validation(0)) {
            return false;
        }
        if(!$scope.step3Validation(0)) {
            return false;
        }
        if(!$scope.step4Validation(0)) {
            return false;
        }
        if(!$scope.step5Validation(0)) {
            return false;
        }
        if(!$scope.step7Validation()) {
            return false;
        }

        // get data
        var catalogueId = $scope.catalogueId;
        var productVariantName = $scope.productVariantName;
        var variantFilterNames = $("input[name='variantFilterName[]']").map(function(){return $(this).val();}).get();
        var variantFilterIds = $("input[name='variantFilterId[]']").map(function(){return $(this).val();}).get();
        var variantFilterValues = $("select[name='variantFilterValue[]']").map(function(){return $(this).val();}).get();
        var uploadedImages = $scope.uploadedImages;
        var defaultImage = $('#defaultImage').val();
        var package = $scope.package;
        var isOpen = $('input[name=isOpen]:checked').val();
        var description = CKEDITOR.instances.description.getData();
        var specifications = CKEDITOR.instances.specifications.getData();
        var catalogueFile = $scope.catalogueFile;
        // var status = $scope.status;
        var status = $('#status').val();
        var relatedProductsRawValue = $('#relatedProducts').tokenInput("get");
        var defaultLogicRelatedProduct = ($('#defaultLogicRelatedProduct').is(":checked") == true) ? 1 : 0;

        // for variant filter object
        var variantFilterArr = new Array();
        var pfLen = variantFilterNames.length;
        for(var i = 0; i < pfLen; i++) {
            var tmpObj = new Object();
            tmpObj._id = variantFilterIds[i];
            tmpObj.name = variantFilterNames[i];
            tmpObj.value = variantFilterValues[i];

            variantFilterArr.push(tmpObj);
        }

        // for images
        var prdImgCnt = uploadedImages.length;
        var finalImagesArr = new Array();
        for(var i = 0; i < prdImgCnt; i++) {
            var tmpObj = new Object();
            tmpObj.name = uploadedImages[i].renamedName;

            // check for default
            var defaultChk = false;
            if(defaultImage == uploadedImages[i].renamedName) {
                defaultChk = true;
            }
            tmpObj.isDefault = defaultChk;

            finalImagesArr.push(tmpObj);
        }

        // get proper related poducts values
        var relatedProductsRawValueCnt = relatedProductsRawValue.length;
        var relatedProducts = new Array();
        for(var i = 0; i < relatedProductsRawValueCnt; i++) {
            relatedProducts.push(relatedProductsRawValue[i]['id']);
        }

        // data 
        var data = {
            productId: productId,
            catalogueId: catalogueId,
            productVariantName: productVariantName,
            variantFilters: JSON.stringify(variantFilterArr),
            productImages: JSON.stringify(finalImagesArr),
            package: package,
            description: description,
            specifications: specifications,
            isOpen: isOpen,
            catalogueFile: catalogueFile,
            relatedProducts: JSON.stringify(relatedProducts),
            status: status,
            brand: JSON.stringify($scope.selectedBrand),
            manufacturer: JSON.stringify($scope.selectedManufacturer),
            defaultLogicRelatedProduct: defaultLogicRelatedProduct
        };
        var config = {};

        $http.put('/product/updateVariant/'+$scope.variantId, data, config)
        .success(function (data, status, headers, config) {
            console.log('success edit - ', data);

            if(data.error == 1) {
                $scope.error = data.msg;
                showToastMsg(0, data.msg);
                return false;
            }
            $scope.success = data.msg;
            showToastMsg(1, data.msg);

            // show & hide btn            
            $('#saveVariantBtn').hide();
            // $('#addMoreVariantBtn').show();

            $timeout(function () {
                $location.path('/product');
            }, 2000);
        })
        .error(function (data, status, header, config) {
            console.log('error edit - ', data);
            $scope.error = 'Unable to update product variant';
            showToastMsg(0, 'Unable to update product variant');
        });
    };

    // Add more variant call
    $scope.addMoreVariant = function() {
        // product id validation
        if(!$scope.productIdValidation()) {
            return false;
        }

        $location.path('/product/add/'+$scope.productId);
    }
});

// Edit product supplier
app.controller('ProductEditSupplierController', function ($timeout, $scope, $http, $location, $routeParams) {
    // check for access
    noAccessRedirect($location);

    // get route params
    $scope.productId = $routeParams.productId;
    $scope.variantId = $routeParams.variantId;
    $scope.supplierId = $routeParams.supplierId;
    $scope.changedProductId = 0;

    // for gallery images uploading
    $scope.uploadedImages = new Array();
    $scope.dropzoneConfig = getDropzoneConfig($scope, $http);

    // for catalogue file uploading
    $scope.catalogueFile = '';
    $scope.dropzoneConfigCatalogue = getDropzoneConfigCatalogue($scope, $http);

    // for ckeditor
    $(function () {
        // instance, using default configuration.
        CKEDITOR.replace('description');
        CKEDITOR.replace('specifications');
    });

    // store product type list
    $scope.productTypeList = new Array();    

    // get token input item display status
    $scope.getTokenItemStatus = function(item) {
        return getTokenItemStatus(item);
    }

    // get brand list
    $scope.brandList = new Array();
    $scope.selectedBrand = new Object();
    getAllBrands($scope, $http);

    // get manufacturer list
    $scope.manufacturerList = new Array();
    $scope.selectedManufacturer = new Object();
    getAllManufacturers($scope, $http);

    // toggle product search box
    $scope.showHideProductCodeSearch = function(value) {
        showHideProductCodeSearch(value);
    };

    // show add product filter value panel
    $scope.showAddProductFilterPanel = function(filterId) {
        showAddProductFilterPanel(filterId);
    };
    // show add variant filter value panel
    $scope.showAddVariantFilterPanel = function(filterId) {
        showAddVariantFilterPanel(filterId);
    };    
    // show add brand panel
    $scope.showAddBrandPanel = function() {
        showAddBrandPanel();
    };
    // show add manufacture panel
    $scope.showAddManufacturePanel = function() {
        showAddManufacturePanel();
    };

    // Calculate offer & discount price
    $scope.calculateAutoDOPrice = function(discountInputId, offerInputId, type) {
        calculateAutoDOPrice(discountInputId, offerInputId, type, $scope);
    };

    // get related products
    getRelatedVariants();

    // hide/show related prod box    
    $scope.toggleRelatedProductsPanel = function() {
        toggleRelatedProductsPanel();
    };

    // get search product code
    getSearchedProductCodes('EDIT-SUPPLIER', $scope, $http, $timeout);

    // show reason for rejection panel
    $scope.showReasonForRejection = function() {
        showReasonForRejection();
    };    

    // Get product basic details
    $http.get('/product/getBasicDetails/'+$scope.productId)
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                // show step 1,2,3
                $('#step3MinBtn').click();
                // hide step 1 & 2 btns
                // $('#saveBasicDetailBtn, #step1NextBtn').hide();

                $scope.headers = response.headers('Product');
                var productDetails = response.data.data[0];

                $scope.tag = productDetails.tag;
                $scope.productCode = productDetails.productCode;
                var category = productDetails.category;
                var brand = productDetails.brand;
                var manufacturer = productDetails.manufacturer;
                var productFilters = productDetails.productFilters;

                // get product type list
                $http.get('/category/getAllProductTypes')
                    .then(function (response) {
                        if(response.data.error == 0 && response.data.data != '') {
                            $scope.productTypeList = response.data.data;

                            // product type
                            $scope.productType = category._id+'_##_'+category.name;

                            // get product variant details
                            getVariantDetailsForEdit('SUPPLIER-EDIT', $scope, $http, $timeout);

                            // show product & variant filters
                            $timeout(function () {
                                $('#productType').attr("disabled", true);
                                $scope.getProductTypeFilters();
                                $scope.getVaraintFilters();

                                // product type filters
                                $timeout(function () {
                                    angular.forEach(productFilters, function(value, key) {
                                        $("#productFilterValue_"+value._id).val(value.value);
                                    });
                                }, 200);

                            }, 200);
                        }
                    });

                // fill data
                $timeout(function () {
                    $(document).ready(function() {
                        // brand
                        $scope.selectedBrand = {_id: brand._id, name: brand.name};
                        $('#brand').tokenInput("add", {_id: brand._id, name: brand.name, isActive: 0, checkStatus: 1});
                        // manufacturer
                        $scope.selectedManufacturer = {_id: manufacturer._id, name: manufacturer.name};
                        $('#manufacturer').tokenInput("add", {_id: manufacturer._id, name: manufacturer.name, isActive: 0, checkStatus: 1});
                    });
                }, 500);

            }
            else {
                $scope.error = 'Product details not found';
                showToastMsg(0, 'Product details not found');
            }
        });

    // show filters
    $scope.showFilters = function() {
        $scope.getProductTypeFilters();
        $scope.getVaraintFilters();
    };

    // get product type filters
    $scope.productTypeFilters = new Array();
    $scope.getProductTypeFilters = function() {
        getProductTypeFilters($scope);
    }

    // get variant filters
    $scope.variantFilters = new Array();
    $scope.getVaraintFilters = function() {
        getVaraintFilters($scope);
    }

    // get product type bulk ranges
    $scope.bulkRanges = new Array();
    $scope.getProductTypeBulkRanges = function() {
        getProductTypeBulkRanges($scope);
    }

    // get product type commission
    $scope.productTypeCommission = 0;
    $scope.getProductTypeCommission = function() {
        getProductTypeCommission($scope, $http);
    }

    // Show custom bulk ranges
    $scope.showCustomBulkRangeInputs = function(index) {
        showCustomBulkRangeInputs(index);
    };

    // validate custom bulk range qty on input blur
    $scope.validateCustomBulkQtyRange = function(index) {
        validateCustomBulkQtyRange(index);
    };

    // hide success & error message
    $scope.hideSEMsgs = function() {
        $scope.error = '';
        $scope.success = '';
    }

    // insert brand
    $scope.addBrand = function() {
        addBrand($scope, $http, $timeout);
    };

    // insert manufacturer
    $scope.addManufacturer = function() {
        addManufacturer($scope, $http, $timeout);
    }

    // Add product filter value
    $scope.addProductFilterValue = function(filterId) {
        addProductFilterValue(filterId, $scope, $http, $timeout);
    }

    // Add variant filter value    
    $scope.addVariantFilterValue = function(filterId) {
        addVariantFilterValue(filterId, $scope, $http, $timeout);
    }

    // Step 1 validation
    $scope.step1Validation = function (showNextStep) {
        return step1Validation(showNextStep, 'EDIT', $scope);
    }

    // update basic details of product
    $scope.updateBasicDetails = function () {
        $scope.hideSEMsgs();

        // get data
        var productType = $scope.productType;
        var productCode = $scope.productCode;
        var tag = $scope.tag;
        var brand = $scope.selectedBrand;
        var manufacturer = $scope.selectedManufacturer;
        var productFilterNames = $("input[name='productFilterName[]']").map(function(){return $(this).val();}).get();
        var productFilterIds = $("input[name='productFilterId[]']").map(function(){return $(this).val();}).get();
        var productFilterValues = $("select[name='productFilterValue[]']").map(function(){return $(this).val();}).get();

        // validations
        if(!$scope.step1Validation(0)) {
            return false;
        }
        if(productFilterNames.length == 0) {
            $scope.error = 'Please add product filters';
            showToastMsg(0, 'Please add product filters');
            return false;    
        }

        // create category, brand, manufacturer object
        var productTypeSplit = productType.split('_##_');
        var productTypeObj = {_id: productTypeSplit[0], name: productTypeSplit[1]};
        // for product filter object
        var productFilterArr = new Array();
        var pfLen = productFilterNames.length;
        for(var i = 0; i < pfLen; i++) {
            var tmpObj = new Object();
            tmpObj._id = productFilterIds[i];
            tmpObj.name = productFilterNames[i];
            tmpObj.value = productFilterValues[i];

            productFilterArr.push(tmpObj);
        }

        // data
        var data = {
            tag: tag,
            productCode: productCode,
            category: JSON.stringify(productTypeObj),
            brand: JSON.stringify(brand),
            manufacturer: JSON.stringify(manufacturer),
            productFilters: JSON.stringify(productFilterArr)
        };
        var config = {};

        $http.put('/product/basicUpdate/'+$scope.productId, data, config)
        .success(function (data, status, headers, config) {
            console.log('success add - ', data);

            if(data.error == 1) {
                $scope.error = data.msg;
                showToastMsg(0, data.msg);
                return false;
            }
            $scope.success = data.msg;
            showToastMsg(1, data.msg);
        })
        .error(function (data, status, header, config) {
            console.log('error add - ', data);
            $scope.error = 'Unable to add product basic details';
            showToastMsg(0, 'Unable to add product basic details');
        });
    };

    // product id validation
    $scope.productIdValidation = function () {
        return productIdValidation($scope);
    }

    // Step 3 validation
    $scope.step3Validation = function(showNextStep) {
        return step3Validation(showNextStep, 'EDIT', $scope);
    }

    // Step 4 validation
    $scope.step4Validation = function (showNextStep) {
        return step4Validation(showNextStep, 'EDIT', $scope);
    }

    // Step 5 validation
    $scope.step5Validation = function(showNextStep) {
        return step5Validation(showNextStep, 'EDIT', $scope);
    }

    // step 6 validations
    $scope.step6Validation = function(showNextStep) {
        return step6Validation(showNextStep, 'EDIT', $scope);
    }

    // step 7 validations
    $scope.step7Validation = function() {
        return step7Validation('EDIT', $scope);
    }

    // step 8 validations    
    $scope.step8Validation = function() {
        return step8Validation('EDIT', $scope);
    }

    // add specifications from selected filters
    $scope.setSpecsFromFilters = function() {
        setSpecsFromFilters();
    }

    // update variants
    $scope.updateSupplierVariant = function () {
        $scope.hideSEMsgs();

        // for product id
        var productId = $scope.productId;
        if(!$scope.productIdValidation()) {
            return false;
        }

        // do validations
        if(!$scope.step1Validation(0)) {
            return false;
        }
        if(!$scope.step3Validation(0)) {
            return false;
        }
        if(!$scope.step4Validation(0)) {
            return false;
        }
        if(!$scope.step5Validation(0)) {
            return false;
        }
        if(!$scope.step7Validation()) {
            return false;
        }
        if(!$scope.step8Validation()) {
            return false;
        }

        // get data
        var catalogueId = $scope.catalogueId;
        var productVariantName = $scope.productVariantName;
        var variantFilterNames = $("input[name='variantFilterName[]']").map(function(){return $(this).val();}).get();
        var variantFilterIds = $("input[name='variantFilterId[]']").map(function(){return $(this).val();}).get();
        var variantFilterValues = $("select[name='variantFilterValue[]']").map(function(){return $(this).val();}).get();
        var uploadedImages = $scope.uploadedImages;
        var defaultImage = $('#defaultImage').val();
        var package = $scope.package;
        var isOpen = $('input[name=isOpen]:checked').val();
        var description = CKEDITOR.instances.description.getData();
        var specifications = CKEDITOR.instances.specifications.getData();
        var catalogueFile = $scope.catalogueFile;
        var status = $('#status').val();
        var relatedProductsRawValue = $('#relatedProducts').tokenInput("get");
        var defaultLogicRelatedProduct = ($('#defaultLogicRelatedProduct').is(":checked") == true) ? 1 : 0;
        // supplier pricing details
        var sku = $scope.sku;
        var country = $scope.country;
        var sellerWarrantyDesc = $scope.sellerWarrantyDesc;
        var conditionNote = $scope.conditionNote;
        var legalDisclaimer = $scope.legalDisclaimer;
        var condition = $scope.condition;
        var quantity = $scope.quantity;
        var discountPrice = $('#discountPrice').val();
        var offerPrice = $('#offerPrice').val();
        var totalQuantity = $scope.totalQuantity;
        var packagingLatency = $scope.packagingLatency;
        var bulkRangesValues = $("select[name='bulkRanges[]']").map(function(){return $(this).val();}).get();
        var bulkDiscountPriceValues = $("input[name='bulkDiscountPrice[]']").map(function(){return $(this).val();}).get();
        var bulkOfferPriceValues = $("input[name='bulkOfferPrice[]']").map(function(){return $(this).val();}).get();        
        var reasonForRejection = $scope.reasonForRejection;
        var supplierStatus = $('#supplierStatus').val();

        // for variant filter object
        var variantFilterArr = new Array();
        var pfLen = variantFilterNames.length;
        for(var i = 0; i < pfLen; i++) {
            var tmpObj = new Object();
            tmpObj._id = variantFilterIds[i];
            tmpObj.name = variantFilterNames[i];
            tmpObj.value = variantFilterValues[i];

            variantFilterArr.push(tmpObj);
        }

        // for images
        var prdImgCnt = uploadedImages.length;
        var finalImagesArr = new Array();
        for(var i = 0; i < prdImgCnt; i++) {
            var tmpObj = new Object();
            tmpObj.name = uploadedImages[i].renamedName;

            // check for default
            var defaultChk = false;
            if(defaultImage == uploadedImages[i].renamedName) {
                defaultChk = true;
            }
            tmpObj.isDefault = defaultChk;

            finalImagesArr.push(tmpObj);
        }

        // get proper related poducts values
        var relatedProductsRawValueCnt = relatedProductsRawValue.length;
        var relatedProducts = new Array();
        for(var i = 0; i < relatedProductsRawValueCnt; i++) {
            relatedProducts.push(relatedProductsRawValue[i]['id']);
        }

        // for bulk order object
        var bulkOrderArr = getBulkOrderArrayForSave(bulkRangesValues, bulkDiscountPriceValues, bulkOfferPriceValues);

        // data 
        var data = {
            productId: productId,
            changedProductId: $scope.changedProductId,
            variantId: $scope.variantId,
            catalogueId: catalogueId,
            productVariantName: productVariantName,
            variantFilters: JSON.stringify(variantFilterArr),
            productImages: JSON.stringify(finalImagesArr),
            package: package,
            description: description,
            specifications: specifications,
            isOpen: isOpen,
            catalogueFile: catalogueFile,
            relatedProducts: JSON.stringify(relatedProducts),
            defaultLogicRelatedProduct: defaultLogicRelatedProduct,
            status: status,
            brand: JSON.stringify($scope.selectedBrand),
            manufacturer: JSON.stringify($scope.selectedManufacturer),
            sku: sku,
            country: country,
            sellerWarrantyDesc: sellerWarrantyDesc,
            conditionNote: conditionNote,
            legalDisclaimer: legalDisclaimer,
            condition: condition,
            quantity: quantity,
            discountPrice: discountPrice,
            offerPrice: offerPrice,
            totalQuantity: totalQuantity,
            packagingLatency: packagingLatency,
            bulkOrder: JSON.stringify(bulkOrderArr),
            reasonForRejection: reasonForRejection,
            supplierIndex: $scope.supplierIndex,
            supplierStatus: supplierStatus,
            cataloguePrice: $scope.cataloguePrice,
            oldDiscountPrice: $scope.oldDiscountPrice,
            commission: $scope.productTypeCommission
        };
        var config = {};

        $http.put('/product/updateSupplierVariant/'+$scope.supplierId, data, config)
        .success(function (data, status, headers, config) {
            console.log('success edit - ', data);

            if(data.error == 1) {
                $scope.error = data.msg;
                showToastMsg(0, data.msg);
                return false;
            }
            $scope.success = data.msg;
            showToastMsg(1, data.msg);

            // show & hide btn
            $('#saveVariantBtn').hide();

            // Update product basic details as well if update btn still not submitted manually
            $scope.updateBasicDetails();

            $timeout(function () {
                $location.path('/product/supplier/');
            }, 2000);
        })
        .error(function (data, status, header, config) {
            console.log('error edit - ', data);
            $scope.error = 'Unable to update product';
            showToastMsg(0, 'Unable to update product');
        });
    };

    // Add more variant call
    $scope.addMoreVariant = function() {
        // product id validation
        if(!$scope.productIdValidation()) {
            return false;
        }

        $location.path('/product/add/'+$scope.productId);
    }
});

})(); // End of closure