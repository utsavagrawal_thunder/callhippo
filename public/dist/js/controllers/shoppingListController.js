// closure for this controller // private
(function() {

    // Product images url
    var productImgAwsPath = s3_bucket+'product/images/';

    // Shopping list controller
    app.controller('ShoppingListController', function ($timeout, $scope, $http, $location, NgTableParams) {
        // check for access
        noAccessRedirect($location);

        // get data
        $scope.searchShoppingList = '';
        $scope.getShoppingList = function() {
            getShoppingList($scope, $http, $timeout, NgTableParams);
        };
        $scope.getShoppingList();

        // Reset search
        $scope.resetSearch = function() {
            $scope.searchShoppingList = '';
            $scope.getShoppingList();
        };
    });

    // get list from DB
    function getShoppingList($scope, $http, $timeout, NgTableParams) {
        $scope.tableParams = new NgTableParams(
            {count: 25, page: 1, sorting: { createdDate: 'desc' }},
            {
                counts: [25, 50, 100],
                total: 0,
                getData: function (params) {
                    var count = params.count();
                    var page = params.page();
                    // for sorting
                    var sorting = params.sorting();
                    var sortParam = 'createdDate';
                    var sortOrder = 'DESC';
                    if(!isObjectEmpty(sorting)) {
                        sortParam = Object.keys(sorting)[0];
                        sortOrder = sorting[sortParam];                                        
                    }

                    var dataObj = {
                        searchTerm: $.trim($scope.searchShoppingList),
                        status: $('input[name=listingStatus]:checked').val(),
                        perPage: count,
                        pageNum: page,
                        sortParam: sortParam,
                        sortOrder: sortOrder
                    };
                    return new Promise(function (resolve, reject) {
                        $http.post('/shopping-list/get', dataObj)
                            .success(function (data, status, headers, config) {
                                $scope.tableParams.total(data.totalCnt);
                                resolve(data.data);
                            })
                            .error(function (data, status, header, config) {
                                reject([]);
                            });
                    });
                }
            });
    }

    // shopping list - supplier get controller
    app.controller('ShoppingListSupplierController', function ($timeout, $scope, $http, $location, $routeParams) {
        // check for access
        noAccessRedirect($location);

        // defaults
        $scope.results = new Array();
        $scope.shoppingList = new Object();
        $scope.orderTotal = 0;

        // order id
        var orderId = $routeParams.orderId;

        // Get supplier list
        $http.get('/shopping-list/getSuppliers/'+orderId)
            .then(function (response) {
                $scope.results = response.data.suppliers;
                $scope.shoppingList = response.data.shoppingList;
                $scope.orderTotal = response.data.orderTotal;                

                $timeout(function () {
                    $('#suppliersListTable').DataTable();
                });
            });
    });

    // shopping list - get supplier products
    app.controller('ShoppingListSupplierProductsController', function ($timeout, $scope, $http, $location, $routeParams) {
        // check for access
        noAccessRedirect($location);        

        // order id
        var orderId = $routeParams.orderId;
        var supplierCode = $routeParams.supplierCode;
        $scope.supplierCode = supplierCode;

        // Set defaults
        $scope.supplierDetails = {};
        $scope.products = [];
        $scope.productTotalPrice = 0;
        $scope.totalProduct = 0;
        $scope.totalItemQty = 0;
        $scope.shoppingList = {};

        // Get supplier list
        $http.get('/shopping-list/getSuppliersProducts/'+orderId+'/'+supplierCode)
            .success(function (data, status, headers, config) {
                if(data.error == 1) {
                    showToastMsg(0 , data.msg);
                    return false;
                }

                // shopping list products
                $scope.supplierDetails = data.data;
                $scope.shoppingList = $scope.supplierDetails.shoppingList;
                $scope.products = $scope.supplierDetails.variants;
                $scope.totalProduct = ($scope.products).length;
                $scope.totalItemQty = $scope.supplierDetails.supplierCartTotalQty;
                $scope.productTotalPrice = $scope.supplierDetails.supplierCartTotalPrice;

                // Generate show image url
                var productsCnt = $scope.products.length;
                for(var i = 0; i < productsCnt; i++) {
                    // get image
                    $scope.products[i]['showImageUrl'] = productImgAwsPath+getResizedProductImage($scope.products[i]['image'], 85, 85);
                }
            })
            .error(function (data, status, header, config) {
                return false;
            });
    });

})(); // End of closure