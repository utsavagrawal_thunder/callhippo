// closure for this controller // private
(function () {

    /*
     * Category List Controller
     */
    app.controller('CategoryListController', function ($scope, $http, $timeout, $location, $route) {
        // check for access
        noAccessRedirect($location);

        // csv file header & column order
        $scope.getCSVHeader = function ()
        {
            return ['Name', 'Description', 'Category', 'Product Level Filters', 'Varient Level Filters', 'Commission Percentage', 'Status'];
        };
        $scope.getCSVColumnOrder = function ()
        {
            return ['name', 'description', 'categoryType', 'productFilters', 'variantFilters', 'commissionValues', 'isActive'];
        };

        $http.get('/category/get')
                .then(function (response) {
                    console.log(response);
                    $scope.headers = response.headers('Category Ids');
                    $scope.results = response.data;


                    $scope.excelFormatData = $scope.results;
                    for (var i = 0; i < $scope.excelFormatData.length; i++) {

                        // variant
                        var tmpVariendFilterArr = $scope.excelFormatData[i].variantFilters;
                        var varientFilterStr = '';
                        for (var j = 0; j < tmpVariendFilterArr.length; j++) {
                            varientFilterStr += tmpVariendFilterArr[j].filterGroup + ' :: ' + tmpVariendFilterArr[j].values.toString() + '||';
                        }
                        $scope.excelFormatData[i].variantFilters = varientFilterStr;

                        // product filter
                        var tmpProductFilterArr = $scope.excelFormatData[i].productFilters;
                        var productFilterStr = '';
                        for (var j = 0; j < tmpProductFilterArr.length; j++) {
                            productFilterStr += tmpProductFilterArr[j].filterGroup + ' :: ' + tmpProductFilterArr[j].values.toString() + '||';
                        }
                        $scope.excelFormatData[i].productFilters = productFilterStr;


                        // main category
                        var categoryType = '';
                        if ($scope.excelFormatData[i].isMainCategory == true) {
                            categoryType = 'Main Category';
                        }
                        // sub category
                        if ($scope.excelFormatData[i].isMainCategory == false && $scope.excelFormatData[i].isProductType == false) {
                            categoryType = 'Sub Category';
                        }
                        // product type
                        if ($scope.excelFormatData[i].isProductType == true) {
                            categoryType = 'Product Type';
                        }
                        $scope.excelFormatData[i].categoryType = categoryType;

                    }
                    console.log($scope.excelFormatData);

                    $timeout(function ()
                    {
                        $('#categoryListTable').DataTable();
                    });
                });
    });

    /*
     * Category Add Controller
     */


    app.controller('CategoryAddController', function ($timeout, $scope, $http, $location) {
        // check for access
        noAccessRedirect($location);

        $scope.productFilterValues = [];
        $scope.variantFilterValues = [];

        // for category image
        $scope.logoImg = '';
        // for dropzone file uploader logo
        $scope.dropzoneConfig1 = {
            'options': {// passed into the Dropzone constructor
                url: '/category/upload',
                maxFilesize: 8, // MB
                maxFiles: 1,
                dictDefaultMessage: 'Drag an image here to upload, or click to select one',
                acceptedFiles: 'image/*',
                addRemoveLinks: true,
                removedfile: function (file) {
                    // empty img
                    // $scope.logoImg = '';
                    var name = file.name;

                    // remove file from server
                    $http.delete('/category/delete/' + $scope.logoImg)
                            .then(function (response) {});

                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                },
                init: function () {
                    this.on('success', function (file, resp) {
                        //                        console.log();
                        $scope.logoImg = resp.key.split('/')[1];
                        console.log("$scope.logoImg");
                        console.log($scope.logoImg);
                    });
                }
            },
            'eventHandlers': {
                'sending': function (file, xhr, formData) {
                },
                'success': function (file, response) {
                }
            }
        };

        // for dropzone file uploader icon
        $scope.dropzoneConfig2 = {
            'options': {// passed into the Dropzone constructor
                url: '/category/upload',
                maxFilesize: 8, // MB
                maxFiles: 1,
                dictDefaultMessage: 'Drag an image here to upload, or click to select one',
                acceptedFiles: 'image/*',
                addRemoveLinks: true,
                removedfile: function (file) {
                    // empty img
                    // $scope.iconImg = '';
                    var name = file.name;

                    // remove file from server
                    $http.delete('/category/delete/' + $scope.iconImg)
                            .then(function (response) {});

                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                },
                init: function () {
                    this.on('success', function (file, resp) {
                        //                        console.log();
                        $scope.iconImg = resp.key.split('/')[1];
                        console.log("$scope.iconImg");
                        console.log($scope.iconImg);
                    });
                }
            },
            'eventHandlers': {
                'sending': function (file, xhr, formData) {
                },
                'success': function (file, response) {
                }
            }
        };


        $http.get('/filter/getAllActiveFilters')
                .then(function (response) {
                    var filters = response.data;
                    $scope.filters = filters;
                    $scope.variantFilters = filters;
                    $timeout(function () {
                        $(".chosen-select.product").chosen({width: "inherit"}).change(function (event, params) {
                            createOptionsDiv($(this), "product", filters, params, $scope, $timeout);
                            $scope.$apply();
                        });
                        $(".chosen-select.variant").chosen({width: "inherit"}).change(function (event, params) {
                            createOptionsDiv($(this), "variant", filters, params, $scope, $timeout);
                            $scope.$apply();
                        });
                        $timeout(function () {
//                            $('.filterValueSelector').multiselect();
                                
                                $('.chosen-select').chosen({width: "100%"});
                        });

                    });
                });
        $scope.submit = function () {
            submitInsertCategory('insert', $scope, $http, $timeout, $location);

        };
        $scope.validationOptions = {
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: "Please specify category name"
            }
        };
    });

    /*
     * Category Edit Controller
     */
    app.controller('CategoryEditController', function ($scope, $routeParams, $http, $timeout, $location) {
        // check for access
        noAccessRedirect($location);

        $scope.productFilterValues = [];
        $scope.variantFilterValues = [];

        // for category image
        $scope.logoImg = '';

        // for category icon
        $scope.iconImg = '';

        // for dropzone file uploader image
        $scope.dropzoneConfig1 = {
            'options': {// passed into the Dropzone constructor
                url: '/category/upload',
                maxFilesize: 8, // MB
                maxFiles: 1,
                dictDefaultMessage: 'Drag an image here to upload, or click to select one',
                acceptedFiles: 'image/*',
                addRemoveLinks: true,
                removedfile: function (file) {
                    // empty img
                    // $scope.logoImg = '';
                    var name = file.name;
                    this.options.maxFiles = 1;

                    // remove file from server
                    $http.delete('/category/delete/' + $scope.logoImg)
                            .then(function (response) {
                                console.log(response);
                                if (response.data.error == 0) {
                                    $scope.logoImg = undefined;
                                    var _ref;
                                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                                }
                            });
                },
                init: function () {
                    this.on('success', function (file, resp) {
                        console.log("resp.key");
                        console.log(resp.key);
                        $scope.logoImg = resp.key.split('/')[1];
                    });
                    // keep dropzone refernce
                    $scope.dropzone1 = this;
                }
            },
            'eventHandlers': {
                'sending': function (file, xhr, formData) {
                },
                'success': function (file, response) {
                }
            }
        };

        // for dropzone file uploader icon
        $scope.dropzoneConfig2 = {
            'options': {// passed into the Dropzone constructor
                url: '/category/upload',
                maxFilesize: 8, // MB
                maxFiles: 1,
                dictDefaultMessage: 'Drag an image here to upload, or click to select one',
                acceptedFiles: 'image/*',
                addRemoveLinks: true,
                removedfile: function (file) {
                    // empty img
                    // $scope.logoImg = '';
                    var name = file.name;
                    this.options.maxFiles = 1;

                    // remove file from server
                    $http.delete('/category/delete/' + $scope.iconImg)
                            .then(function (response) {
                                console.log(response);
                                if (response.data.error == 0) {
                                    $scope.iconImg = undefined;
                                    var _ref;
                                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                                }
                            });
                },
                init: function () {
                    this.on('success', function (file, resp) {
                        console.log("resp.key");
                        console.log(resp.key);
                        $scope.iconImg = resp.key.split('/')[1];
                    });
                    // keep dropzone refernce
                    $scope.dropzone2 = this;
                }
            },
            'eventHandlers': {
                'sending': function (file, xhr, formData) {
                },
                'success': function (file, response) {
                }
            }
        };

        $http.get('/category/getById?_id=' + $routeParams.id)
                .then(function (response) {
                    console.log('Got the response');
                    console.log(response);
                    $scope._id = $routeParams.id;
                    $scope.name = response.data[0].name;
                    $scope.description = response.data[0].description;
                    $scope.isMainCategory = response.data[0].isMainCategory == false ? 'No' : 'Yes';
                    $scope.isProductType = response.data[0].isProductType == false ? 'No' : 'Yes';
                    $scope.productFilters = response.data[0].productFilters;
                    $scope.variantFilters = response.data[0].variantFilters;
                    $scope.logoImg = response.data[0].image;
                    $scope.iconImg = response.data[0].icon;
                    if (response.data[0].isProductType == true) {
                        $scope.bulkFrom1 = parseInt(response.data[0].bulkRanges[0].split("-")[0]);
                        $scope.bulkFrom2 = parseInt(response.data[0].bulkRanges[1].split("-")[0]);
                        $scope.bulkFrom3 = parseInt(response.data[0].bulkRanges[2].split("-")[0]);
                        //$scope.bulkFrom4 = parseInt(response.data[0].bulkRanges[3].split("-")[0]);
                        $scope.bulkTo1 = parseInt(response.data[0].bulkRanges[0].split("-")[1]);
                        $scope.bulkTo2 = parseInt(response.data[0].bulkRanges[1].split("-")[1]);
                        $scope.bulkTo3 = parseInt(response.data[0].bulkRanges[2].split("-")[1]);
                        //$scope.bulkTo4 = parseInt(response.data[0].bulkRanges[3].split("-")[1]);

                    }

                    // For preloading DB images to dropzone
                    if ($scope.logoImg != '' && typeof ($scope.logoImg) != 'undefined') {
                        var imgFile = {name: $scope.logoImg, size: 0};
                        $scope.dropzone1.options.addedfile.call($scope.dropzone1, imgFile);
                        $scope.dropzone1.options.thumbnail.call($scope.dropzone1, imgFile, s3_bucket + 'category/' + $scope.logoImg);
                        // reduce max files option
                        $scope.dropzone1.options.maxFiles = 0;

                        // hide image sizes
                        $('.dz-size, .dz-progress').hide();
                    }
                    // For preloading DB images to dropzone
                    if ($scope.iconImg != '' && typeof ($scope.iconImg) != 'undefined') {
                        var imgFile = {name: $scope.iconImg, size: 0};
                        $scope.dropzone2.options.addedfile.call($scope.dropzone2, imgFile);
                        $scope.dropzone2.options.thumbnail.call($scope.dropzone2, imgFile, s3_bucket + 'category/' + $scope.iconImg);
                        // reduce max files option
                        $scope.dropzone2.options.maxFiles = 0;

                        // hide image sizes
                        $('.dz-size, .dz-progress').hide();
                    }

                    $scope.isSetCommission = response.data[0].commissionValues === 0 ? 'No' : 'Yes';
                    $scope.commissionValues = response.data[0].commissionValues;
                    $scope.status = response.data[0].isActive == false ? 'Inactive' : 'Active';

                    $http.get('/filter/getAllActiveFilters')
                            .then(function (responseFilter) {
                                var filters = responseFilter.data;
                                $scope.filters = filters;
                                $scope.variantFilters = filters;
                                $timeout(function () {
                                    if (response.data.length > 0) {
                                        for (var i = 0; i < response.data[0].productFilters.length; i++) {
                                            var value = response.data[0].productFilters[i];
                                            $('.chosen-select.product option[value="' + value._id + '"]').attr('selected', 'selected');
                                            $('.chosen-select.product').trigger("chosen:updated");
                                            createOptionsDivForEdit("product", filters, value._id, value.values, $scope, $timeout);
                                        }
                                        ;
                                        for (var i = 0; i < response.data[0].variantFilters.length; i++) {
                                            var value = response.data[0].variantFilters[i];
                                            $('.chosen-select.variant option[value="' + value._id + '"]').attr('selected', 'selected');
                                            $('.chosen-select.variant').trigger("chosen:updated");
                                            createOptionsDivForEdit("variant", filters, value._id, value.values, $scope, $timeout);
                                        }
                                        ;
                                    }
                                    $(".chosen-select.product").chosen({width: "100%"}).change(function (event, params) {
                                        createOptionsDiv($(this), "product", filters, params, $scope, $timeout);
                                    });
                                    $(".chosen-select.variant").chosen({width: "100%"}).change(function (event, params) {
                                        createOptionsDiv($(this), "variant", filters, params, $scope, $timeout);
                                    });
                                    //init existing filters multiselect
                                    $timeout(function () {
//                                        $('.filterValueSelector').multiselect();
                                        $('.chosen-select').chosen({width: "100%"});
                                    });
//                                }
                                });
                            });
                });
        $scope.submit = function () {
            submitInsertCategory('update', $scope, $http, $timeout, $location);
        };
    });


    /*
     * Functions
     */

    /**
     * Create choose options tab
     * @param {Jquery Dom} $this $(this)
     * @param {jquery dom} $divId where to add option values
     * @param {filters} filters array of objects containing all filters from DB
     * @param {object} params selected and deselected values
     * 
     */
    function createOptionsDiv($this, $divId, filters, params, $scope, $timeout) {
        var selectedValue = params.selected ? params.selected : null;
        console.log(params.deselected);
        if (params.deselected) {
            $('[identifier="' + params.deselected + '"]').remove();
        }
        var filterValues = [];
        var filterId;
        var $html = '';
        $.each(filters, function (index, filter) {
            if (filter._id == selectedValue) {
                filterValues = filter.possibleValues;
                filterId = filter._id;
//            $html += '<div filterName="' + filter.name + '" identifier=' + filter._id + '><h5>' + filter.name + '</h5>';
                var filterValuesArray = [];
                if ($divId == "product") {
                    filterValuesArray = $scope.productFilterValues;
                } else {
                    filterValuesArray = $scope.variantFilterValues;
                }
                var index1 = filterValuesArray.length;
                filterValuesArray[index1] = {
                    _id: filterId,
                    name: filter.name,
                    slug: filter.slug ? filter.slug : "",
                    values: []
                };
                $.each(filterValues, function (index, eachfilterValue) {
                    filterValuesArray[index1].values.push({
                        value: eachfilterValue.value,
                        slug: eachfilterValue.slug
                    });
                });
            }
        });

//    $.each(filterValues, function (index, filterValue) {
//        $html += '<input type="checkbox" ng-model="' + filterId + '" value="' + filterValue.value + '"/>' + filterValue.value + '<br/>';
//    });
//    $html += '</div>';
//    $divId.append($html);
        $timeout(function () {
            $scope.$apply();
//            $('.filterValueSelector').multiselect();
                $('.chosen-select').chosen({width: "100%"});
        });
    }

    function createOptionsDivForEdit($divId, filters, selectedValue, values, $scope, $timeout) {
        var filterValues = [];
        var filterId;
//    var $html = '';
        var filterValuesArray = [];
        var index1;
        $.each(filters, function (index, filter) {
            if (filter._id == selectedValue) {
                filterValues = filter.possibleValues;
                filterId = filter._id;
//            $html += '<div filterName="' + filter.name + '" identifier=' + filter._id + '><h5>' + filter.name + '</h5>';
                if ($divId == "product") {
                    filterValuesArray = $scope.productFilterValues;
                } else {
                    filterValuesArray = $scope.variantFilterValues;
                }
                index1 = filterValuesArray.length;
                filterValuesArray[index1] = {
                    _id: filterId,
                    name: filter.name,
                    slug: filter.slug ? filter.slug : "",
                    values: []
                };
            }
        });
//        $.each(filterValues, function (index, eachfilterValue) {
        /*
         * filterValues => contains existing values
         * values => contains all possible values for particular group
         */
        for (var i = 0; i < filterValues.length; i++) {
            var hasPushed = false;
            for (var j = 0; j < values.length; j++) {
                if (filterValues[i].value == values[j].name) {
                    filterValuesArray[index1].values.push({
                        value: filterValues[i].value,
                        slug: filterValues[i].slug ? filterValues[i].slug : "",
                        checked: "true"
                    });
                    hasPushed = true;
                }
            }
            if (hasPushed == false) {
                filterValuesArray[index1].values.push({
                    value: filterValues[i].value,
                    slug: filterValues[i].slug ? filterValues[i].slug : "",
                    checked: "false"
                });
            }
        }
        ;
        if ($divId == "product") {
            $scope.productFilterValues = filterValuesArray;
        } else {
            $scope.variantFilterValues = filterValuesArray;
        }
//    $.each(filterValues, function (index, filterValue) {
//        if (values.indexOf(filterValue.value) > -1) {
//            $html += '<input type="checkbox" checked="true" ng-model="' + filterId + '" value="' + filterValue.value + '"/> ' + filterValue.value + '<br/>';
//        } else {
//            $html += '<input type="checkbox" ng-model="' + filterId + '" value="' + filterValue.value + '"/> ' + filterValue.value + '<br/>';
//        }
//    });
        $timeout(function () {
            $scope.$apply();
//            $('.filterValueSelector').multiselect();
                $('.chosen-select').chosen({width: "100%"});
        });
        return;
    }

    function submitInsertCategory(caller, $scope, $http, $timeout, $location) {
        // check for name empty & special chars
        if ($scope.name == '' || typeof ($scope.name) == 'undefined') {
            $scope.error = 'Please enter Name';
            return false;
        }
        if (onlySpecialChars($scope.name) == false) {
            $scope.error = 'Name should not contain only special characters';
            return false;
        }
        if (!$scope.logoImg) {
            $scope.error = 'Please select a main image for the selected category';
            return false;
        }
        if (!$scope.iconImg) {
            $scope.error = 'Please select an icon image for the selected category';
            return false;
        }
        var filters = $scope.filters;
        var productValues = {};
        var variantValues = {};
        $.each($('.chosen-select.product').val(), function (index, productFilterId) {
            productValues[productFilterId] = {};
            var filterName = $('[parent_id = "' + productFilterId + '"]').parent().attr('filtername');
            productValues[productFilterId][filterName] = [];
            productValues[productFilterId]["filterGroupSlug"] = $('[parent_id = "' + productFilterId + '"]').parent().attr('filterslug');
            $('[parent_id ="' + productFilterId + '"]:checked').each(function (index2, eachFilterValue) {
                productValues[productFilterId][filterName].push({
                    name: $(this).val(),
                    slug: $(this).attr('data-slug')
                });
            });

        });
        $.each($('.chosen-select.variant').val(), function (index, variantFilterId) {
            variantValues[variantFilterId] = {};
            var filterName = $('[parent_id = "' + variantFilterId + '"]').parent().attr('filtername');
            variantValues[variantFilterId][filterName] = [];
            variantValues[variantFilterId]["filterGroupSlug"] = $('[parent_id = "' + variantFilterId + '"]').parent().attr('filterslug');
            $('[parent_id ="' + variantFilterId + '"]:checked').each(function (index2, eachFilterValue) {
                variantValues[variantFilterId][filterName].push({
                    name: $(this).val(),
                    slug: $(this).attr('data-slug')
                });
            });
        });
        // check for product & variant filters
        if ($scope.isProductType == 'Yes') {
            // For product filter
            if (isObjectEmpty(productValues)) {
                $scope.error = 'Please add product level filters';
                return false;
            }
            var productFilterValueSelected = true;
            var errorMsgPF = '';
            ploop1:
                    for (var filterIdkey in productValues) {
                ploop2:
                        for (var filterNameKey in productValues[filterIdkey]) {
                    if ((productValues[filterIdkey][filterNameKey]).length == 0) {
                        errorMsgPF = 'Please select filter values for ' + filterNameKey + ' filter';
                        productFilterValueSelected = false;
                        break ploop1;
                    }
                }
            }
            if (productFilterValueSelected == false) {
                $scope.error = errorMsgPF;
                return false;
            }

            // variant filter
            if (isObjectEmpty(variantValues)) {
                $scope.error = 'Please add variant level filters';
                return false;
            }
            var variantFilterValueSelected = true;
            var errorMsgPF = '';
            vloop1:
                    for (var filterIdkey in variantValues) {
                vloop2:
                        for (var filterNameKey in variantValues[filterIdkey]) {
                    if ((variantValues[filterIdkey][filterNameKey]).length == 0) {
                        errorMsgPF = 'Please select filter values for ' + filterNameKey + ' filter';
                        variantFilterValueSelected = false;
                        break vloop1;
                    }
                }
            }
            if (variantFilterValueSelected == false) {
                $scope.error = errorMsgPF;
                return false;
            }

            // For bulk ranges
            if (typeof ($scope.bulkFrom1) == 'undefined' || $scope.bulkFrom1 == null || $scope.bulkFrom1 == '' ||
                    typeof ($scope.bulkTo1) == 'undefined' || $scope.bulkTo1 == null || $scope.bulkTo1 == '' ||
                    typeof ($scope.bulkFrom2) == 'undefined' || $scope.bulkFrom2 == null || $scope.bulkFrom2 == '' ||
                    typeof ($scope.bulkTo2) == 'undefined' || $scope.bulkTo2 == null || $scope.bulkTo2 == '' ||
                    typeof ($scope.bulkFrom3) == 'undefined' || $scope.bulkFrom3 == null || $scope.bulkFrom3 == '' ||
                    typeof ($scope.bulkTo3) == 'undefined' || $scope.bulkTo3 == null || $scope.bulkTo3 == '') {

                $scope.error = 'Please add all bulk quantity ranges';
                return false;
            }
        }

        var serverObj = {
            name: $scope.name,
            description: $scope.description ? $scope.description : "",
            image: $scope.logoImg,
            icon: $scope.iconImg,
            isMainCategory: $scope.isMainCategory,
            isProductType: $scope.isProductType,
            productFilters: productValues,
            variantFilters: variantValues,
            bulkFrom1: $scope.bulkFrom1,
            bulkFrom2: $scope.bulkFrom2,
            bulkFrom3: $scope.bulkFrom3,
            //bulkFrom4: $scope.bulkFrom4,
            bulkTo1: $scope.bulkTo1,
            bulkTo2: $scope.bulkTo2,
            bulkTo3: $scope.bulkTo3,
            //bulkTo4: $scope.bulkTo4,
            isSetCommision: $scope.isSetCommision,
            commissionValues: $scope.commissionValues ? $scope.commissionValues : 0,
            status: $scope.status
        };
        var url = '';
        if (caller == 'update') {
            url = '/category/update';
            serverObj._id = $scope._id;
        } else if (caller == 'insert') {
            url = '/category/insert';
        }
        $scope.error = null;
        $http.post(url, serverObj)
                .then(function (response) {
                    console.log("Following is response from server");
                    console.log(response);
                    if (!response.data.error) {
                        $scope.success = response.statusText;
                    } else if (response.data.data.length > 0) {
                        $scope.error = 'Errors: ';
                        for (var i = 0; i < response.data.data.length; i++) {
                            $scope.error += response.data.data[i].message + ". ";
                        }
                    }
                    $timeout(function () {
                        if (!$scope.error) {
                            $location.path('/category');
                            $location.replace();
                        }
                    }, 2000);
                });
    }

})(); // End of closure
