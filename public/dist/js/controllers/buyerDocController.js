app.controller('BuyerDocController', function ($timeout, $scope, $http, $window, $routeParams, $location) {

	$scope.isInProgress = false;
	$scope.buyerId = $routeParams.buyerId;

	$(document).ready(function () {

		var currentForm = "";
		$('.upload_form').ajaxForm({
			clearForm: false,
	        beforeSubmit: function(arr, $form, options) {
	            var excelFile = arr[0]['value'];
	            if(arr.length >0){
	            	for(i = 0 ; i < arr.length;i++){
	            		if(arr[i]['required'] != undefined){
	            			if(arr[i]['required'] == true){
	            				//we will check if value is set
	            				val = $.trim(arr[i]['value']);
	            				if(val == ""){
	            					//please fill the compulsory fill
	            					showToastMsg(0, 'Please fill all the compulsory fields!');
	            					return false;
	            				}
	            			}
	            		}

	            		if(arr[i]['name'] == "uploadfrom"){
	            			currentForm = $.trim(arr[i]['value']); 	
	            		}
	            	}
	            }
	            if(excelFile == '') {
	                showToastMsg(0, 'Please select file');
	                return false;
	            }
	            $('#'+currentForm+'_sect').html('<img src="/dist/img/loader.gif" title="loading"/> Please Wait, uploading!');
	        },
	        success: function(response) {
	            // error: Invalid file
	            if(response.status == 422) {
	                showToastMsg(0, response.responseJSON.error);
	                return;
	            }
	            showToastMsg(1, 'File uploaded on server.');
	            filename = response.key.split('/')[2];
	            $('#'+currentForm+'_filename').val(filename);
	            $('#'+currentForm+'_sect').html('<a href="'+s3_bucket+'buyer/business/'+filename+'" target="_blank"><i class="fa fa-search"></i></a> '+response.originalname+' <a href="javascript:void(0)" file="'+filename+'" originalfile = "'+response.originalname+'" currentForm="'+currentForm+'" class="removeUpload"><i class="fa fa-times redtext"></i></a>');
	            return false;
	        },
	        error: function (xhr, textstat, errorthrown){
	        	showToastMsg(0, "Something Went Wrong, "+errorthrown+"!");
	        	$('#'+currentForm+'_sect').html('');
	        	return;
	        }
		});

		$('#doc_sect .upload_form').on('click','.removeUpload',function (){
			file = $.trim($(this).attr('file'));
			currentForm = $.trim($(this).attr('currentForm'));
			originalfile = $.trim($(this).attr('originalfile'));

			if(file != undefined && file != ""){
				$.ajax({
	                type: "DELETE",
	                url: "/buyer/delFile/"+file,
	                data: {from : 'business'}, 
	                dataType: 'json',
	                beforeSend:function(){
	            		$('#'+currentForm+'_sect').html('<img src="/dist/img/loader.gif" title="loading"/> Please Wait, deleting!');
	               	},
	                success: function(data)
	                {
	                    if(data.error !== undefined){
	                        if(data.error == 1){
	                        	showToastMsg(0,data.msg);
	                        	$('#'+currentForm+'_sect').html('<a href="'+s3_bucket+'buyer/business/'+file+'" target="_blank"><i class="fa fa-search"></i></a> '+originalfile+' <a href="javascript:void(0)" file="'+file+'" originalfile = "'+originalfile+'" currentForm="'+currentForm+'" class="removeUpload"><i class="fa fa-times redtext"></i></a>');
	                        }else{
	                            showToastMsg(1,data.msg);
	                            $('#'+currentForm+'_filename').val('');
	                            $('#'+currentForm+'_sect').html('')
	                        }
	                    }else{
	                    	showToastMsg(0,"Something Went Wrong, Please Try Again!");
	                    	$('#'+currentForm+'_sect').html('<a href="'+s3_bucket+'buyer/business/'+file+'" target="_blank"><i class="fa fa-search"></i></a> '+originalfile+' <a href="javascript:void(0)" file="'+file+'" originalfile = "'+originalfile+'" currentForm="'+currentForm+'" class="removeUpload"><i class="fa fa-times redtext"></i></a>');
	                    }
	                }
	            });
			}else{
				showToastMsg(0, "Could not fetch FileName!");
				$('#'+currentForm+'_sect').html('<a href="'+s3_bucket+'buyer/business/'+file+'" target="_blank"><i class="fa fa-search"></i></a> '+originalfile+' <a href="javascript:void(0)" file="'+file+'" originalfile = "'+originalfile+'" currentForm="'+currentForm+'" class="removeUpload"><i class="fa fa-times redtext"></i></a>');
			}
			return false;
		});
	});

	$scope.updateBuyerRegDoc = function (){

		job_role = $scope.doc.jobRole;
		gov_reg_prof_doc = $scope.doc.govtRegProof.proofName;
		gov_reg_prof_filename = $scope.doc.govtRegProof.proofFileName;
		vat_tin_doc = $scope.doc.vatTinProof.proofName;
		vat_tin_prof_filename = $scope.doc.vatTinProof.proofFileName;
		csi_prof_filename = $scope.doc.csiProof.proofFileName;
		addr_prof_doc = $scope.doc.addressProof.proofName;
		addr_prof_filename = $scope.doc.addressProof.proofFileName;
		id_prof_doc = $scope.doc.IdProof.proofName;
		id_prof_filename = $scope.doc.IdProof.proofFileName;
		cert_prof_doc = $scope.doc.certProof.proofName;
		cert_prof_filename = $scope.doc.certProof.proofFileName;
		drug_prof_filename = $scope.doc.drugProof.proofFileName;
		
		var required_data = [job_role,gov_reg_prof_doc,gov_reg_prof_filename,vat_tin_doc,vat_tin_prof_filename,csi_prof_filename,addr_prof_doc,addr_prof_filename,id_prof_doc,id_prof_filename,cert_prof_doc,cert_prof_filename,drug_prof_filename];

		if(required_data.length > 0){
			
			for(var i = 0 ; i < required_data.length; i++){
				if(required_data[i] == undefined || required_data[i] == ""){
					showToastMsg(0, "Please fill in all the compulsary fields!");
					return false;
				}
			}

			data = $('#docSect').serialize()+'&'+$('#gov_reg_form').serialize()+'&'+$('#vat_form').serialize()+'&'+$('#csi_form').serialize()+'&'+$('#addr_form').serialize()+ '&'+$('#id_form').serialize()+ '&'+$('#cert_form').serialize()+'&'+$('#drug_form').serialize();
			$.ajax({
				type: "POST",
                url: "/buyer/UpdateCompany/"+$scope.buyerId,
                data: data, 
                dataType: 'json',
                beforeSend:function(){
            		$scope.isInProgress = true;
                   	$('#company_update_wait').html('<img src="/dist/img/loader.gif" title="loading"/> Please Wait!');
               	},
                success: function(data)
                {
                	$('#company_update_wait').html('');
                    if(data.error !== undefined){
                        if(data.error == 1){
                        	showToastMsg(0,data.msg);
                        }else{
                            showToastMsg(1,data.msg);
                        }
                    }else{
                    	showToastMsg(0,"Something Went Wrong, Please Try Again!");
                    }
                    $scope.isInProgress = false;
                }
			})
			return false;
		}else{
			showToastMsg(0, "Could not fetch all data!");
		}
	}

	$scope.updateBuyerManager = function (){
		rm_name = $scope.rmDetails.name;
		rm_email = $scope.rmDetails.email;
		rm_mobileNo = $scope.rmDetails.mobileNo;

		if(rm_name != "" && rm_email != "" && rm_mobileNo != ""){
			data = {
				name : rm_name,
				email : rm_email,
				mobileNo : rm_mobileNo
			}
			config = {};
			$http.post('/buyer/updateRelationManager/'+$scope.buyerId, data, config)
            .success(function (data, status, headers, config) {
                
                if(data.error == 1) {
                    showToastMsg(0, data.msg);
                    return false;
                }
                showToastMsg(1, data.msg);
            })
            .error(function (data, status, header, config) {
                showToastMsg(0, "Unable to update!");
            });
		}else{
			showToastMsg(0, "All fields should be filled!");	
		}
		return false;
	}
});