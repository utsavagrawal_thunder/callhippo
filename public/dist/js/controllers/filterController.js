// closure for this controller // private
(function() {

/*
 * Filter List Controller
 */
app.controller('FilterListController', function ($timeout, $scope, $http, $location) {
    // check for access
    noAccessRedirect($location);

     // csv file header & column order
    $scope.getCSVHeader = function () {
        return ['Filter Name', 'Description' , 'Filter Values' ,'Status'];
    };
    $scope.getCSVColumnOrder = function () {
        return ['name', 'description', 'possibleValuesStr', 'isActive'];
    };

    $http.get('/filter/getAll')
        .then(function (response) {
            $scope.headers = response.headers('Filter Ids');
            $scope.results = response.data;

            $scope.excelFormatData2 = response.data;
            for(var i=0; i<$scope.excelFormatData2.length; i++) {
                var tmpPossibleValuesArr = $scope.excelFormatData2[i].possibleValues;
                var possibleValuesStr = '';
                for(var j = 0; j < tmpPossibleValuesArr.length; j++) {
                   possibleValuesStr += (tmpPossibleValuesArr[j].value).toString()+', ';
                }
                $scope.excelFormatData2[i].possibleValuesStr = possibleValuesStr;
            }

            $timeout(function () {
                $('#filterListTable').DataTable();
            })
        });

    $scope.arrayToString = function (string) {
        return string.join(", ");
    };
    
    // Upload product excel
    $(document).ready(function(){
        $("#filterExcelUploadForm").ajaxForm({
            url: '/filter/uploadFiltersExcel',
            clearForm: true,
            beforeSubmit: function(arr, $form, options) {
                var excelFile = arr[0]['value'];
                if(excelFile == '') {
                    showToastMsg(0, 'Please select file');
                    return false;
                }
            },
            complete: function(response) {
                // error: Invalid file
                if(response.status == 422) {
                    showToastMsg(0, response.responseJSON.error);
                    return;
                }
                if(response.status == 500){
                    showToastMsg(0, response.responseJSON.msg);
                    return;
                }
                else {
                    showToastMsg(1, 'File uploaded on server. You will get confirmation mail once all Filters are uploaded to database.');
                }
            }
        });
    });
});


/*
 * Filter Add Controller
 */

app.controller('FilterAddController', function ($timeout, $scope, $http, $location) {
    // check for access
    noAccessRedirect($location);

    $scope.filterValues = [];
    $scope.addValue = function () {
        if ($scope.newValue && $scope.newValue.length > 0) {
            // $scope.filterValues.push($scope.newValue);
            for (var i = 0; i < $scope.filterValues.length; i++) {
                if($scope.filterValues[i].value.toLowerCase() == $scope.newValue.toLowerCase()) {
                    $scope.error = 'Value already exists';
                    return false;
                }
            }
            $scope.filterValues.push({value: $scope.newValue, isActive: $scope.valueStatus});
            $scope.newValue = "";
        }
    };
    
    $scope.removeItem = function (itemId) {
        var index = $scope.filterValues.indexOf(itemId);
        if (index > -1) {
            $scope.filterValues.splice(index, 1);
        }
    };
    
    $scope.submit = function () {
        submitInsertFilter('insert', $scope, $http, $timeout, $location);
    };
    $scope.changeFilterStatus = function(itemValue) {
        var itemStatus = $('#filterStatus_'+itemValue).val();
        var fvCnt = $scope.filterValues.length;
        for(var i = 0; i < fvCnt; i++) {
            if($scope.filterValues[i].value == itemValue) {
                $scope.filterValues[i].isActive = itemStatus;
                break;
            }
        }
    };
});

/*
 * Category Edit Controller
 */
app.controller('FilterEditController', function ($scope, $http, $routeParams, $timeout, $location) {
    // check for access
    noAccessRedirect($location);
    $scope.message = "Hello from filter edit controller";
    $http.get('/filter/getById?_id=' + $routeParams.id)
            .then(function (respObject) {
                console.log(respObject);
                $scope._id = respObject.data[0]._id;
                $scope.name = respObject.data[0].name;
                $scope.description = respObject.data[0].description;
                $scope.filterValues = respObject.data[0].possibleValues;
                $scope.status = respObject.data[0].isActive == false ? 'Inactive' : 'Active';
            });
    $scope.submit = function () {
        submitInsertFilter('update', $scope, $http, $timeout, $location);
    };
    $scope.addValue = function () {
        if ($scope.newValue && $scope.newValue.length > 0) {
            // $scope.filterValues.push($scope.newValue);
            for (var i = 0; i < $scope.filterValues.length; i++) {
                if($scope.filterValues[i].value.toLowerCase() == $scope.newValue.toLowerCase()) {
                    $scope.error = 'Value already exists';
                    return false;
                }
            }
            $scope.filterValues.push({value: $scope.newValue, isActive: $scope.valueStatus});
            $scope.newValue = "";
        }
    };
    $scope.removeItem = function (itemId) {
        var index = $scope.filterValues.indexOf(itemId);
        if (index > -1) {
            $scope.filterValues.splice(index, 1);
        }
    };

    $scope.changeFilterStatus = function(itemValue) {
        var itemStatus = $('#filterStatus_'+itemValue).val();
        var fvCnt = $scope.filterValues.length;
        for(var i = 0; i < fvCnt; i++) {
            if($scope.filterValues[i].value == itemValue) {
                $scope.filterValues[i].isActive = itemStatus;
                break;
            }
        }
    };
});


/*
 * Functions
 */

function submitInsertFilter(caller, $scope, $http, $timeout, $location) {

    /*
    var possibleValues = [];
    $("[ng-model='allValues']").each(function (index) {
        var thisValue = $(this).attr('value');
        var splitValue = thisValue.split('_##_');
        console.log("thisValue");
        console.log(thisValue);
        possibleValues.push({
            value: splitValue[0],
            isActive: splitValue[1]
        });
        console.log("possibleValues");
        console.log(possibleValues);
    });
    */

    // check for name empty & special chars
    if($scope.name == '' || typeof($scope.name) == 'undefined') {
        $scope.error = 'Please enter Name';
        return false;
    }
    if(onlySpecialChars($scope.name) == false) {
        $scope.error = 'Name should not contain only special characters';
        return false;
    }
    var possibleValues = $scope.filterValues;
    if(possibleValues.length == 0) {
        $scope.error = 'Please add filter values';
        return false;
    }
    var serverObj = {
        name: $scope.name,
        description: $scope.description ? $scope.description : "",
        possibleValues: possibleValues,
        status: $scope.status
    };
    var url = '';
    if (caller == 'update') {
        url = '/filter/update';
        serverObj._id = $scope._id;
    } else if (caller == 'insert') {
        url = '/filter/insert';
    }

    $http.post(url, serverObj)
            .then(function (response) {
                if (!response.data.error) {
                    $scope.success = response.statusText;
                    $timeout(function(){
                        $location.path('/filter');
                        $location.replace();
                    }, 3000);
                } else if (response.data.error == 1) {
                    $scope.error = 'Errors: ';
                    for (var i = 0; i < response.data.data.length; i++) {
                        $scope.error += response.data.data[i].message + ". ";
                    }
                    $scope.error += " "+response.data.message;
                }
            });
}

})(); // End of closure