// closure for this controller // private
(function () {

//Relations create controller
    app.controller('RelationsCreateController', function ($scope, $http, $timeout, $location) {

        // check for access
        noAccessRedirect($location);

        //object containing all relations
        var Relations = {};
        //selected column to show appropriate options while selecting parent.
        $scope.selectedColumnId = 0;
        // get Other category list
        $scope.otherCategoryList = new Array();

        //init for showing active class on item in li>tab-pane which is active
        $scope.selected = 0;

        $scope.select = function (index) {
            $scope.selected = index;
        };

        //object containing default parents for each having multiple parents
        $scope.defaultParentsForItems = {};

        //Remove item from Relations
        $scope.removeItem = function (itemId) {
            //hierarchyArray -> Location of item in the Relations object
            var hierarchyArray = itemId.split("__");

            switch (hierarchyArray.length) {
                case 2:
                    {
                        for (var i = 0; i < Relations[hierarchyArray[0]].children.length; i++) {
                            //match with slug
                            if (Relations[hierarchyArray[0]].children[i].slug == hierarchyArray[1]) {
                                Relations[hierarchyArray[0]].children.splice(i, 1);
                            }
                        }

                    }
                    break;
                case 3:
                    {
                        for (var i = 0; i < Relations[hierarchyArray[0]].children.length; i++) {
                            //match with slug
                            if (Relations[hierarchyArray[0]].children[i].slug == hierarchyArray[1]) {
                                for (var j = 0; j < Relations[hierarchyArray[0]].children[i].children.length; j++) {
                                    //match with slug
                                    if (Relations[hierarchyArray[0]].children[i].children[j].slug == hierarchyArray[2]) {
                                        Relations[hierarchyArray[0]].children[i].children.splice(j, 1);
                                    }
                                }

                            }
                        }
                    }
                    break;
                case 4:
                    {
                        for (var i = 0; i < Relations[hierarchyArray[0]].children.length; i++) {
                            //match with slug
                            if (Relations[hierarchyArray[0]].children[i].slug == hierarchyArray[1]) {
                                for (var j = 0; j < Relations[hierarchyArray[0]].children[i].children.length; j++) {
                                    //match with slug
                                    if (Relations[hierarchyArray[0]].children[i].children[j].slug == hierarchyArray[2]) {
                                        for (var k = 0; k < Relations[hierarchyArray[0]].children[i].children[j].children.length; k++) {
                                            //match with slug
                                            if (Relations[hierarchyArray[0]].children[i].children[j].children[k].slug == hierarchyArray[3]) {
                                                Relations[hierarchyArray[0]].children[i].children[j].children.splice(k, 1);
                                            }
                                        }

                                    }
                                }

                            }
                        }
                    }
                    break;
            }
            //re-initialise multiple parent modal
            $scope.defaultParentsForItems = {};
            identifyMultipleParents();

            //convert inputs to looko better
            // $('input').iCheck({
            //     checkboxClass: 'icheckbox_flat-green',
            //     radioClass: 'iradio_flat-blue'
            // });
            $scope.relations = Relations;
            $timeout(function(){
                $scope.$apply();
            });
        };

        // get other category list         
        $http.get('/category/getAllOtherCategories')
                .then(function (response) {
                    if (response.data.error == 0 && response.data.data != '') {
                        $scope.headers = response.headers('Relation');
                        $scope.otherCategoryList = response.data.data;
                    }
                });

        // get all product types
        $scope.productTypesList = new Array();
        $http.get('/category/getAllProductTypes')
                .then(function (response) {
                    if (response.data.error == 0 && response.data.data != '') {
                        $scope.headers = response.headers('Relation');
                        $scope.productTypesList = response.data.data;
                    }
                });
        // get Main category list
        $scope.mainCategories = new Array();
        $http.get('/category/getAllMainCategories')
                .then(function (response) {
                    if (response.data.error == 0 && response.data.data != '') {
                        $scope.headers = response.headers('Relation');
                        $scope.mainCategories = response.data.data;
                        $scope.parentValues = response.data.data;
                        //execute this at the end of this get request
                        $timeout(function () {
                            $('.chosen-select.parent').chosen({width: "inherit"});
                            // $('input').iCheck({
                            //     checkboxClass: 'icheckbox_flat-green',
                            //     radioClass: 'iradio_flat-blue'
                            // });

                        });
                    }
                });


        //get defined relations
        $http.get('/relations/get')
                .then(function (response) {
                    if (response.data.length > 0) {
                        var Rels = {};
                        for (var i = 0; i < response.data.length; i++) {
                            Rels[response.data[i].main_category_id] = {
                                name: response.data[i].main_category_name,
                                slug: response.data[i].main_category_slug,
                                children: response.data[i].children,
                            };
                        }
                        $scope.relations = Rels;
                        Relations = Rels;

                        //initialiseMultiParents modal
                        identifyMultipleParents();

                        // init external libs
                       
                        $('.chosen-select').trigger("chosen:updated");
                        // $('input').iCheck({
                        //     checkboxClass: 'icheckbox_flat-green',
                        //     radioClass: 'iradio_flat-blue'
                        // });
                    }
                    else {
                        if ($scope.relations && Object.keys($scope.relations).length == 0) {
                            for (var i = 0; i < $scope.mainCategories.length; i++) {
                                Relations[$scope.mainCategories[i]._id] = {
                                    name: $scope.mainCategories[i].name,
                                    slug: $scope.mainCategories[i].slug,
                                    children: []
                                };
                            }
                            $scope.relations = Relations;
                        }

                        console.log("Relations");
                        console.log(Relations);
                    }
                });

        //Save all relations & Push to db.
        $scope.saveRelations = function () {

            //check if for each, parents is defined.
            var parentNotDefined = [];
            $('[multiParentChild]').each(function (index) {
                if ($(this).find('[checkbox-name="multiParent"]:checked').length == 0) {
                    parentNotDefined.push($(this).find('div').attr('cat-name'));
                }
            });
            //show error
            if (parentNotDefined.length > 0) {
                $scope.relationsError = "Following categories have mulitple parents. Please define default parent for each to proceed\n\nHello/n/nWorld";
                $(".error-modal-lg").modal();
            } else {
                //save relations post call
                $http.post('/relations/save', $scope.relations)
                        .then(function (response) {
                            console.log("Response from server");
                            console.log(response);
                            //BUG HERE===> Always code comes here
                            if (response.status == 200)
                            {
                                console.log("No error");
                                $scope.relationsError = "Successfully saved";
                                $(".error-modal-lg").modal();
                            }
                            ;
                        });
            }
        };

        //on select column repopoulate the parents in dropdown at both locations. 
        //Other-category, Product Type
        $scope.selectColumn = function (mainCatId, colTwoId) {
            //first column.
            if (mainCatId == 1) {
                $scope.selectedColumnId = 0;
                $scope.parentValues = $scope.mainCategories;
                //second column
            } else if (!colTwoId) {
                $scope.selectedColumnId = 1;
                console.log("Before apply parent values" + mainCatId);
                $scope.parentValues = Relations[mainCatId].children;
                //third column
            } else {
                $scope.selectedColumnId = 2;
                console.log("Before apply parent values");
                for (var i = 0; i < Relations[mainCatId].children.length; i++) {
                    if (Relations[mainCatId].children[i]._id == colTwoId) {
                        $scope.parentValues = Relations[mainCatId].children[i].children;
                    }
                }
            }
            $('.chosen-select').trigger("chosen:updated");
            $timeout(function () {
                $scope.$apply();
            });
            console.log("After update parent Values");

            ;
        }
        $scope.addToMenu = function (sectionFrom) {
            console.log("sectionFrom");
            console.log(sectionFrom);
            var showDefaultParentPopup = false;
            if ($('#parentValues1 option:selected').length > 1) {
                $(".multiple-parent-modal-lg").modal();
            }
            if (sectionFrom == 1) {
                $('#parentValues1 option:selected').each(function (index) {
                    var parentId = $(this).val();
                    $('[name="otherCategory"]input[type="checkbox"]:checked').each(function (index) {
                        var thisDataId = $(this).attr('data-id');
                        if (parentId == thisDataId) {
                            $scope.relationsError = "Category " + $(this).attr('cat-name') + " is the parent selected";
                            $(".error-modal-lg").modal();
                        } else {
                            var addedItem = {
                                _id: $(this).attr('data-id'),
                                name: $(this).attr('cat-name'),
                                slug: $(this).attr('data-slug'),
                                isProductType: false,
                                defaultParent: '',
                                children: []
                            };
                            pushDataToColumns(parentId, addedItem);
                            $scope.relations = Relations;
                            console.log("$scope.relations");
                            console.log(JSON.stringify($scope.relations));
                        }

                        $scope.relations = Relations;
                    });
                });
            } else if (sectionFrom == 2) {
                $('#parentValues2 option:selected').each(function (index) {
                    var parentId = $(this).val();
                    console.log("parentId");
                    console.log(parentId);
                    $('[name="productType"]input[type="checkbox"]:checked').each(function (index) {
                        var thisDataId = $(this).attr('data-id');
                        console.log("thisDataId");
                        console.log(thisDataId);
                        if (parentId == thisDataId) {
                            $scope.relationsError = "Category " + $(this).attr('cat-name') + " is the parent selected";
                            $(".error-modal-lg").modal();
                        } else {
                            var addedItem = {
                                _id: $(this).attr('data-id'),
                                name: $(this).attr('cat-name'),
                                slug: $(this).attr('data-slug'),
                                isProductType: true,
                                defaultParent: '',
                                children: []
                            };
                            console.log("addedItem");
                            console.log(addedItem);
                            pushDataToColumns(parentId, addedItem);
                            $scope.relations = Relations;
//                        console.log(JSON.stringify(Relations));
                        }
                    });
                });
            }
        };

        $scope.defineParents = function () {
            $(".multiple-parent-modal-lg").modal();
        };

        $scope.saveDefaultParents = function () {
            //save parents for each item which has multiple parents
            $('[multiParentChild]').each(function (index) {
                var definedParent = $(this).find('[checkbox-name="multiParent"]:checked').attr('path');
                var path = definedParent.split('__');
                var parentObject = {};
                switch (path.length) {
                    case 2:
                        {
                            parentObject = {
                                _id: path[0],
                                name: Relations[path[0]].name,
                                slug: Relations[path[0]].slug
                            };
                        }
                        break;
                    case 3:
                        {
                            parentObject = {
                                _id: Relations[path[0]].children[path[1]]._id,
                                name: Relations[path[0]].children[path[1]].name,
                                slug: Relations[path[0]].children[path[1]].slug
                            };
                        }
                        break;
                    case 4:
                        {
                            parentObject = {
                                _id: Relations[path[0]].children[path[1]].children[path[2]]._id,
                                name: Relations[path[0]].children[path[1]].children[path[2]].name,
                                slug: Relations[path[0]].children[path[1]].children[path[2]].slug
                            };
                        }
                        break;
                }
                $(this).find('[checkbox-name="multiParent"]').each(function (index) {
                    var thisPath = $(this).attr('path').split('__');
                    switch (thisPath.length) {
                        case 2:
                            {
                                Relations[thisPath[0]].children[thisPath[1]].defaultParent = parentObject;
                            }
                            break;
                        case 3:
                            {
                                Relations[thisPath[0]].children[thisPath[1]].children[thisPath[2]].defaultParent = parentObject;
                            }
                            break;
                        case 4:
                            {
                                Relations[thisPath[0]].children[thisPath[1]].children[thisPath[2]].children[thisPath[3]].defaultParent = parentObject;
                            }
                            break;
                    }
                    $scope.relations = Relations;
                    $(".multiple-parent-modal-lg").modal('hide');
                });
            });
        };

        function pushDataToColumns(parentId, addedItem) {
            //if parent is defined i.e is a valid main category & column 1 is selected
            if (Relations[parentId] && $scope.selectedColumnId == 0) {
                console.log("Inside if");
                //******First level items can never be product types.*********
//            if (Relations[parentId].isProductType == true) {
//                $scope.relationsError = "Cannot1 attach under product type " + Relations[parentId].name;
//                $(".error-modal-lg").modal();
//            } else 
                if (checkifCatInSameLevel()) {
                    $scope.relationsError = "Category already added on this level.";
                    $(".error-modal-lg").modal();

                } else {
                    console.log("Before pushing");
                    Relations[parentId].children.push(addedItem);
                    console.log("Relations");
                    console.log(Relations);
                    loopOverAllRelations(addedItem);
                }

                function checkifCatInSameLevel() {
                    for (var i = 0; i < Relations[parentId].children.length; i++) {
                        if (Relations[parentId].children[i].slug == addedItem.slug) {
                            return true;
                        }
                    }
                    return false;
                }
                $scope.relations = Relations;
            }
            //push in second column
            else if ($scope.selectedColumnId == 1) {
                for (var i = 0; i < Object.keys(Relations).length; i++) {
                    var thisKey = Object.keys(Relations)[i];
                    if ($("[ng-model='mainCatList'].active").attr('id') == thisKey) {
                        for (var j = 0; j < Relations[thisKey].children.length; j++) {
                            if (Relations[thisKey].children[j]._id == parentId) {
                                if (Relations[thisKey].children[j].isProductType == true) {
                                    $scope.relationsError = "Cannot attach under product type " + Relations[thisKey].children[j].name;
                                    $(".error-modal-lg").modal();
                                } else if (checkifCatInSameLevel2(j)) {
                                    $scope.relationsError = "You have already attached this in this tree";
                                    $(".error-modal-lg").modal();
                                } else {
                                    Relations[thisKey].children[j].children.push(addedItem);
                                    loopOverAllRelations(addedItem);
                                    console.log("relations updated");
                                    $scope.relations = Relations;
                                }
                            }
                        }
                    }

                }

                function checkifCatInSameLevel2(j) {
                    console.log(Relations[thisKey].children[j]);
                    for (var i = 0; i < Relations[thisKey].children[j].children.length; i++) {
                        if (Relations[thisKey].children[j].children[i].slug == addedItem.slug) {
                            return true;
                        }
                    }
                    return false;
                }
                $scope.relations = Relations;
                //push in third column
            } else if ($scope.selectedColumnId == 2) {
                for (var i = 0; i < Object.keys(Relations).length; i++) {
                    var thisKey = Object.keys(Relations)[i];
                    if ($("[ng-model='mainCatList'].active").attr('id') == thisKey) {
                        for (var j = 0; j < Relations[thisKey].children.length; j++) {
                            if (Relations[thisKey].children[j]._id == addedItem._id) {
                                $scope.relationsError = "You have already attached this in this tree";
                                $(".error-modal-lg").modal();
                            } else {
                                for (var k = 0; k < Relations[thisKey].children[j].children.length; k++) {
                                    if (Relations[thisKey].children[j].children[k]._id == parentId) {
                                        if (Relations[thisKey].children[j].children[k].isProductType == true) {
                                            $scope.relationsError = "Cannot attach under product type " + Relations[thisKey].children[j].children[k].name;
                                            $(".error-modal-lg").modal();
                                        } else if (checkifCatInSameLevel3(j, k)) {
                                            $scope.relationsError = "You have already attached this in this tree";
                                            $(".error-modal-lg").modal();
                                        } else {
                                            Relations[thisKey].children[j].children[k].children.push(addedItem);
                                            loopOverAllRelations(addedItem);
                                            $scope.relations = Relations;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                function checkifCatInSameLevel3(j, k) {
                    for (var i = 0; i < Relations[thisKey].children[j].children[k].children.length; i++) {
                        if (Relations[thisKey].children[j].children[k].children[i].slug == addedItem.slug) {
                            return true;
                        }
                    }
                    return false;
                }
                $scope.relations = Relations;
            }
            return;
        }
        function loopOverAllRelations(addedItem) {
            /**
             * Loop Begin
             */
            for (var i = 0; i < Object.keys(Relations).length; i++) {
                var thisKey = Object.keys(Relations)[i];
                for (var j = 0; j < Relations[thisKey].children.length; j++) {
                    if (Relations[thisKey].children[j]._id == addedItem._id) {
                        pushToDefaultParentObject({
                            _id: thisKey,
                            name: Relations[thisKey].name,
                            slug: Relations[thisKey].slug,
                            path: thisKey + "__" + j
                        }, addedItem);
                    }
                    for (var k = 0; k < Relations[thisKey].children[j].children.length; k++) {
                        if (Relations[thisKey].children[j].children[k]._id == addedItem._id) {
                            pushToDefaultParentObject({
                                _id: Relations[thisKey].children[j]._id,
                                name: Relations[thisKey].children[j].name,
                                slug: Relations[thisKey].children[j].slug,
                                path: thisKey + "__" + j + "__" + k
                            }, addedItem);
                        }
                        for (var l = 0; l < Relations[thisKey].children[j].children[k].children.length; l++) {
                            if (Relations[thisKey].children[j].children[k].children[l]._id == addedItem._id) {
                                pushToDefaultParentObject({
                                    _id: Relations[thisKey].children[j].children[k]._id,
                                    name: Relations[thisKey].children[j].children[k].name,
                                    slug: Relations[thisKey].children[j].children[k].slug,
                                    path: thisKey + "__" + j + "__" + k + "__" + l
                                }, addedItem);
                            }
                        }
                    }
                }
            }
            //remove all items which has only one parent. Required Code optimisation
            for (var i = 0; i < Object.keys($scope.defaultParentsForItems).length; i++) {
                var thisKey = Object.keys($scope.defaultParentsForItems)[i];
                if ($scope.defaultParentsForItems[thisKey].parents.length == 1) {
                    delete $scope.defaultParentsForItems[thisKey];
                }
            }
//        if (!$scope.$$phase) {
//            $scope.$apply();
//        }
            return;
        }

        function pushToDefaultParentObject(parentObject, addedItem) {
            if ($scope.defaultParentsForItems[addedItem._id]) {
                var flag = false;
                //check for duplicate already existing
                $.grep($scope.defaultParentsForItems[addedItem._id].parents, function (e) {
                    if (e._id == parentObject._id) {
                        flag = true;
                    }
                });
                //if no duplicates, push to parents
                if (!flag) {
                    $scope.defaultParentsForItems[addedItem._id].parents.push(parentObject);
                }
                //insert first parent in object for addedItem
            } else {
                $scope.defaultParentsForItems[addedItem._id] = {
                    name: addedItem.name,
                    slug: addedItem.slug,
                    defaultParent: addedItem.defaultParent,
                    parents: [parentObject]
                };
            }
        }
        ;

        function identifyMultipleParents() {
            for (var i = 0; i < Object.keys(Relations).length; i++) {
                var thisKey = Object.keys(Relations)[i];
                for (var j = 0; j < Relations[thisKey].children.length; j++) {
                    for (var k = 0; k < Relations[thisKey].children[j].children.length; k++) {
                        for (var l = 0; j < Relations[thisKey].children[j].children[k].length; j++) {
                            loopOverAllRelations(Relations[thisKey].children[j].children[k].children[l]);
                        }
                        loopOverAllRelations(Relations[thisKey].children[j].children[k]);
                    }
                    loopOverAllRelations(Relations[thisKey].children[j]);
                }
            }

        }

    });


    /*
     * Default parent structure. 
     * 2)Research on nested $.grep
     * 
     */

})(); // End of closure