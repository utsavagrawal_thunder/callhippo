// closure for this controller // private
(function () {

//Local store for product types vs categories
    var categoryData = {};

//Department Add controller
    app.controller('DepartmentAddController', function ($scope, $http, $timeout, $location) {

        //contains all relations
        var Relations = {};

        //store all product types w.r.t department name. i.e. Object to be submitted to db for saving
        //contains array of objects, each containing info about 1 category and associated  product types
        $scope.categoryList = [];

        // get Main category list
        $scope.mainCategories = new Array();

        getAllMainCategories($scope, $http, $timeout);

        departmentAddController($scope, $http, $location, $timeout);

        $timeout(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-blue'
            });
        });
    });


//Department Edit controller

    app.controller('DepartmentEditController', function ($scope, $http, $timeout, $location, $routeParams) {

        //store all product types w.r.t department name. i.e. Object to be submitted to db for saving
        //contains array of objects, each containing info about 1 category and associated  product types
        $scope.categoryList = [];

        $http.get('/department/getById?_id=' + $routeParams.id)
                .then(function (response) {
                    console.log('Got the response');
                    console.log(response);
                    $scope._id = $routeParams.id;
                    $scope.name = response.data[0].name;
                    $scope.description = response.data[0].description;
                    $scope.status = response.data[0].isActive;
                    $scope.categoryList = response.data[0].categoryList;

                });
        getAllMainCategories($scope, $http, $timeout);
        departmentAddController($scope, $http, $location, $timeout);
    });


//Department List controller

    app.controller('DepartmentListController', function ($scope, $http, $timeout, $location) {

        $scope.Departments = [];

        $scope.selected = 0;

        $scope.select = function (index) {
            $scope.selected = index;
        };

        //function to display Popup
        $scope.div_show = function (id) {
            $http.get('/category/getById?_id=' + id.split('__')[3])
                    .then(function (response) {
                        console.log(response);
                        $scope.filters = response.data[0].productFilters.concat(response.data[0].variantFilters);
                        document.getElementById(id).style.display = "block";
                    });
        };

        //function to hide Popup
        $scope.div_hide = function (id) {
            document.getElementById(id).style.display = "none";
        };
        
        $http.get('/department/getAll')
                .then(function (response) {
                    $scope.Departments = response.data;
                    $scope.newDepartments = [];
                    
                    for (var i = 0; i < response.data.length; i++) {
                        for (var j = 0; j < response.data[i].categoryList.length; j++) {
//                            console.log("response.data[i].categoryList[j].productTypes.length");
//                            console.log(response.data[i].categoryList[j].productTypes.length);
                            var groupByParentsDepartmentData = {};
                            for (var k = 0; k < response.data[i].categoryList[j].productTypes.length; k++) {
//                                console.log("response.data[i].categoryList[j].productTypes[k]");
//                                console.log(response.data[i].categoryList[j].productTypes[k]);
                                if (!response.data[i].categoryList[j].productTypes[k].parent) {
                                        console.log("if no parent");
                                    if (!groupByParentsDepartmentData.hasOwnProperty("no-parent")) {
                                        groupByParentsDepartmentData["no-parent"] = [];
                                    }
                                    groupByParentsDepartmentData["no-parent"].push(response.data[i].categoryList[j].productTypes[k]);
                                } else {
                                    console.log("parent is there");
                                    var parentName = response.data[i].categoryList[j].productTypes[k].parent.name;
                                    if (!groupByParentsDepartmentData.hasOwnProperty(parentName)) {
                                        groupByParentsDepartmentData[parentName] = [];
                                    }
                                    groupByParentsDepartmentData[parentName].push(response.data[i].categoryList[j].productTypes[k]);
                                }
//                                var newCatListOfI = 
//                                $scope.newDepartments.push();
                            }
                            $scope.Departments[i].categoryList[j].productTypes = groupByParentsDepartmentData;
                        }
                    }
                    console.log("$scope.Departments");
                    console.log($scope.Departments);
                    
                    console.log("response.data");
                    console.log(response.data);
                    
                });


    });

    /*
     * ###################### FUNCTIONS ###########################
     */

    function loopOverAllRelations(Relations) {
        for (var i = 0; i < Relations.children.length; i++) {
            if (Relations.children[i].isProductType == true) {
                categoryData[Relations.main_category_id].push({
                    prodType: Relations.children[i],
                    checked: false,
                    parent: null
                });
            }
            for (var j = 0; j < Relations.children[i].children.length; j++) {
                if (Relations.children[i].children[j].isProductType == true) {
                    categoryData[Relations.main_category_id].push({
                        prodType: Relations.children[i].children[j],
                        checked: false,
                        parent: {
                            _id: Relations.children[i]._id,
                            name: Relations.children[i].name,
                            slug: Relations.children[i].slug
                        }
                    });
                }
                for (var k = 0; k < Relations.children[i].children[j].children; k++) {
                    if (Relations.children[i].children[j].children[k].isProductType == true) {
                        categoryData[Relations.main_category_id].push({
                            prodType: Relations.children[i].children[j].children[k],
                            checked: false,
                            parent: {
                                _id: Relations.children[i].children[j]._id,
                                name: Relations.children[i].children[j].name,
                                slug: Relations.children[i].children[j].slug
                            }
                        });
                    }
                }
            }
        }
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-blue'
        });
        return categoryData;
    }

    function departmentAddController($scope, $http, $location, $timeout) {
        $scope.addCategory = function () {
            var hasError = false;
            if ($('[name="mainCategory"] option:selected').val() == '?') {
                $scope.error = "Select a category to add. ";
                hasError = true;
            }
            if ($('[name="prodTypes"]:checked').length == 0) {
                $scope.error = "Check atleast one product type.";
                hasError = true;
            }

            if (!hasError) {
                var x = {
                    "cat_id": $('[name="mainCategory"] option:selected').val(),
                    "cat_name": $('[name="mainCategory"] option:selected').html(),
                    "cat_slug": $('[name="mainCategory"] option:selected').attr('data-slug'),
                    "productTypes": []
                };
                var prodTypeObject = {};
                var index;
                $('[name="prodTypes"]:checked').each(function (key) {
                    prodTypeObject = {
                        _id: $(this).attr('data-id'),
                        name: $(this).attr('cat-name'),
                        slug: $(this).attr('data-slug'),
                        parent: {
                            _id: $(this).attr('parent-id'),
                            name: $(this).attr('parent-name'),
                            slug: $(this).attr('parent-slug')
                        }
                    };
                    x.productTypes.push(prodTypeObject);
                });
                var newlyAdded = true;
                for (var i = 0; i < $scope.categoryList.length; i++) {
                    if ($scope.categoryList[i].cat_id === x.cat_id) {
                        newlyAdded = false;
                        $scope.categoryList[i].productTypes = x.productTypes;
                    }
                }
                if (newlyAdded === true) {
                    $scope.categoryList.push(x);
                }
            }
        };

        /*
         * @param {Object} item The product type to be removed
         * @param {String} catId The category id to which the  product type belongs to
         */
        $scope.removeItem = function (item, catId) {
            for (var i = 0; i < $scope.categoryList.length; i++) {
                if ($scope.categoryList[i].cat_id == catId) {
//                if id matches the one sent on click delete item from product type array
                    $scope.categoryList[i].productTypes.splice($scope.categoryList[i].productTypes.indexOf(item), 1);
                    //Remove category if all product types from this category removed
                    if ($scope.categoryList[i].productTypes.length === 0) {
                        $scope.categoryList.splice(i, 1);
                    }
                }
            }

        };

        $scope.saveDepartmentData = function () {
            console.log($scope.name);
            var hasError = false;
            if (!$scope.name || $scope.name == "") {
                $scope.error = "Name is compulsory.";
                hasError = true;

            }
            if ($scope.categoryList.length == 0) {
                $scope.error += "\n\nPlease add atleast one category to this department.";
                hasError = true;
            }
            if (!hasError) {
                var finalObj = {
                    name: $scope.name,
                    description: $scope.description,
                    categoryList: $scope.categoryList,
                    isActive: $scope.status
                };
                $http.post('/department/insert', finalObj)
                        .then(function (response) {
                            if (response.status == 200) {
                                $scope.success = "Successfully inserted";
                                $timeout(function () {
                                    if (!$scope.error) {
                                        $location.path('/department/');
                                        $location.replace();
                                    }
                                }, 2000);
                            }
                        });
            }
        };

        $scope.updateDepartmentData = function () {
            var hasError = false;
            if (!$scope.name || $scope.name == "") {
                $scope.error = "Name is compulsory.";
                hasError = true;

            }
            if ($scope.categoryList.length == 0) {
                $scope.error += "\n\nPlease add atleast one category to this department.";
                hasError = true;
            }
            if (!hasError) {

                var finalObj = {
                    _id: $scope._id,
                    name: $scope.name,
                    description: $scope.description,
                    categoryList: $scope.categoryList,
                    isActive: $scope.status
                };
                $http.post('/department/update', finalObj)
                        .then(function (response) {
                            if (response.status == 200) {
                                $timeout(function () {
                                    if (response.data.error == 0) {
                                        $scope.success = "Successfully inserted";
                                        $location.path('/department/');
                                        $location.replace();
                                    } else {
                                        $scope.error = response.data.message;
                                    }
                                }, 2000);
                            }
                        });
            }
        };
    }
    ;

    function getAllMainCategories($scope, $http, $timeout) {
        $http.get('/category/getAllMainCategories')
                .then(function (response) {
                    if (response.data.error == 0 && response.data.data != '') {
                        $scope.headers = response.headers('Department');
                        $scope.mainCategories = response.data.data;

                        //execute this at the end of this get request
                        $timeout(function () {
                            $('.chosen-select').chosen({width: "inherit"}).change(function (event, params) {
                                $scope.selectedCategory = params.selected;
                                $http.get('/relations/getById?catId=' + params.selected)
                                        .then(function (response) {
                                            Relations = response.data[0];
                                            categoryData[params.selected] = [];
                                            $scope.categoryData = categoryData;
                                            $scope.categoryData = loopOverAllRelations(Relations);

                                            for (var i = 0; i < $scope.categoryList.length; i++) {
                                                if ($scope.categoryList[i].cat_id === params.selected) {
                                                    for (var j = 0; j < $scope.categoryData[params.selected].length; j++) {
                                                        for (var k = 0; k < $scope.categoryList[i].productTypes.length; k++) {
                                                            if ($scope.categoryData[params.selected][j].prodType._id == $scope.categoryList[i].productTypes[k]._id) {
                                                                $scope.categoryData[params.selected][j].checked = true;
                                                                k = $scope.categoryList[i].productTypes.length;
                                                            } else {
                                                                $scope.categoryData[params.selected][j].checked = false;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        });
                            });

                        });
                    }
                });
    }

})(); // End of closure