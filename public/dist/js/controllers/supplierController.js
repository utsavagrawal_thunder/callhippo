// supplier get controller
app.controller('SupplierListController', function ($timeout, $scope, $http, $location) {
    // no access check
    noAccessRedirect($location);

    $http.get('/supplier/get')
            .then(function (response) {
                $scope.results = response.data.data;
                for (var i = 0; i < $scope.results.length; i++) {
                    for (var j = 0; j < $scope.results[i].addressInfo.length; j++) {
                        //show city name from billing address
                        if ($scope.results[i].addressInfo[j].addressType == 0) {
                            $scope.results[i].city = $scope.results[i].addressInfo[j].city;
                        }
                    }
                }
                $timeout(function () {
                    $('#supplierListTable').DataTable();
                });
            });
});

app.controller('SupplierAddController', function ($timeout, $scope, $http, $window, $location) {
    // no access check
    noAccessRedirect($location);

    $http.get('/supplier/get')
            .then(function (response) {
                var supplierList;
                $scope.supplierList = response.data.data;
            });

    // submit form
    $scope.submitForm = function () {
        $scope.error = '';
        $scope.success = '';
        // get data
        var companyName = $scope.companyName;
        var email = $scope.email;
        var password = $scope.password;
        var mobileNo = $scope.mobileNo;
        var address = $scope.address;
        var panNumber = $scope.panNumber;
        var status = $scope.status;

        // validations
        if (companyName == '' || typeof (companyName) == 'undefined') {
            $scope.error = 'Please enter companyName';
            return false;
        }
        if (email == '' || typeof (email) == 'undefined') {
            $scope.error = 'Please enter Email';
            return false;
        }
        if (password == '' || typeof (password) == 'undefined') {
            $scope.error = 'Please enter password';
            return false;
        }
        if (mobileNo == '' || typeof (mobileNo) == 'undefined') {
            $scope.error = 'Please enter MobileNo';
            return false;
        }
        if (address == '' || typeof (address) == 'undefined') {
            $scope.error = 'Please enter address';
            return false;
        }
        if (panNumber == '' || typeof (panNumber) == 'undefined') {
            $scope.error = 'Please enter panNumber.............';
            return false;
        }
        if (status == '' || typeof (status) == 'undefined') {
            $scope.error = 'Please select status';
            return false;
        }

        // data 
        var data = {
            companyName: companyName,
            email: email,
            password: password,
            mobileNo: mobileNo,
            address: address,
            panNumber: panNumber,
            status: status,
        };
        var config = {};

        $http.post('/supplier/insert', data, config)
                .success(function (data, status, headers, config) {
                    console.log('success add - ', data);

                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

                    $timeout(function () {
                        $window.location.href = '/supplier/#/supplier';
                    }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error add - ', data);
                    $scope.error = 'Unable to add Supplier';
                });
    };
});


app.controller('SupplierAddNewController', function ($timeout, $scope, $http, $window, $location) {
    // no access check
    noAccessRedirect($location);

    // $scope.supplierId = $routeParams.supplierId;
    $http.get('/supplier/get')
            .then(function (response) {

                console.log("supplier add Response is........");
                console.log(response);
                var supplierList;
                $scope.supplierList = response.data.data;
            });

    // submit form
    $scope.submitForm = function () {
        $scope.error = '';
        $scope.success = '';
        // get data
        var companyName = $scope.companyName;
        var email1 = $scope.email1;
        var password1 = $scope.password1;
        var mobileNo = $scope.mobileNo;
        var city = $scope.city;
        var pincode = $scope.pincode;
        var panNumber = $scope.panNumber;
        var status = $scope.status;

        // validations
        if (companyName == '' || typeof (companyName) == 'undefined') {
            $scope.error = 'Please enter companyName';
            return false;
        }
        if (email1 == '' || typeof (email1) == 'undefined') {
            $scope.error = 'Please enter Email';
            return false;
        }
        if (password1 == '' || typeof (password1) == 'undefined') {
            $scope.error = 'Please enter password';
            return false;
        }
        if (mobileNo == '' || typeof (mobileNo) == 'undefined') {
            $scope.error = 'Please enter MobileNo';
            return false;
        }
        if (city == '' || typeof (city) == 'undefined') {
            $scope.error = 'Please enter City';
            return false;
        }
        if (pincode == '' || typeof (pincode) == 'undefined') {
            $scope.error = 'Please enter Pincode';
            return false;
        }
        if (panNumber == '' || typeof (panNumber) == 'undefined') {
            $scope.error = 'Please enter panNumber......';
            return false;
        }
        if (status == '' || typeof (status) == 'undefined') {
            $scope.error = 'Please select status';
            return false;
        }

        // data 
        var data = {
            companyName: companyName,
            email1: email1,
            password1: password1,
            mobileNo: mobileNo,
            city: city,
            pincode: pincode,
            panNumber: panNumber,
            status: status,
        };
        var config = {};

        $http.post('/supplier/register', data, config)
                .success(function (data, status, headers, config) {
                    console.log('success add - ', data);

                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

                    $timeout(function () {
                        $window.location.href = '/admin/#/supplier/basicinfo/{{supplierId}}';
                    }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error add - ', data);
                    $scope.error = 'Unable to add Supplier';
                });
    };
});


app.controller('SupplierEditController', function ($timeout, $scope, $http, $window, $routeParams, $location) {
    // no access check
    noAccessRedirect($location);

    $scope.supplierId = $routeParams.supplierId;

    $('#tabHead1').addClass('active');

    $http.get('/supplier/get/' + $scope.supplierId)
            .then(function (response) {

                console.log("Supplier details is.....");
                console.log(response);

                var supplierDetails = response.data.data[0];
                $scope.supplierCode = supplierDetails.supplierCode;
                $scope.name = supplierDetails.name;
                $scope.email1 = supplierDetails.email1;
                $scope.mobileNo = supplierDetails.mobileNo;
                $scope.status = (supplierDetails.isActive).toString();
                $scope.initialStatus = (supplierDetails.isActive).toString();
                $scope.rejectReason = (supplierDetails.rejectReason);
            });
    // submit form
    $scope.updateBasicInfo = function () {
        $scope.error = '';
        $scope.success = '';

        // get data
        var name = $scope.name;
        var email1 = $scope.email1;
        var mobileNo = $scope.mobileNo;
        var status = $('#status').val();
        var rejectReason = $("#rejectReason").val();

        // validations
        if (name == '' || typeof (name) == 'undefined') {
            $scope.error = 'Please enter Name';
            return false;
        }
        if (email1 == '' || typeof (email1) == 'undefined') {
            $scope.error = 'Please enter Email';
            return false;
        }
        if (mobileNo == '' || typeof (mobileNo) == 'undefined') {
            $scope.error = 'Please enter Mobile no,,,,,,';
            return false;
        }
        if (status == '' || typeof (status) == 'undefined') {
        }
        if ((rejectReason == '' || typeof (rejectReason) == 'undefined') && (status == '2')) {
            $scope.error = 'Please select reject reason';
            return false;
        }
        // data 
        var data = {
            name: name,
            email1: email1,
            mobileNo: mobileNo,
            status: status,
            initialStatus: $scope.initialStatus,
            rejectReason: rejectReason
        };
        var config = {};

        $http.put('/supplier/updateBasicInfo/' + $scope.supplierId, data, config)
                .success(function (data, status, headers, config) {
                    console.log('success edit - ', data);
                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

                    $timeout(function () {
                        $window.location.href = '/admin/#/supplier';
                    }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error edit - ', data);
                    $scope.error = 'Unable to add supplier';
                });
    };
});


app.controller('SupplierBusinessRegController', function ($timeout, $scope, $http, $window, $routeParams, $location) {
    // no access check
    noAccessRedirect($location);


    $scope.supplierId = $routeParams.supplierId;

// for file upload
    $(document).ready(function () {
        $("#licenceid").ajaxForm({
            complete: function (response) {
                if (response.status == 422) {
                    alert(response.responseJSON.error);
                    return;
                }
                $scope.licenceFile = response.responseJSON.originalname;
                // save into hidden field
                $('#licenceurl').val(response.responseJSON.key.split('/')[2]);
                //apply changes to UI
                $timeout(function () {
                    $scope.$apply();
                });
            }
        });
        $("#salestaxid").ajaxForm({
            complete: function (response) {
                if (response.status == 422) {
                    alert(response.responseJSON.error);
                    return;
                }
                $scope.salestaxnoFile = response.responseJSON.originalname;
                // save into hidden field
                $('#salestaxnourl').val(response.responseJSON.key.split('/')[2]);
                //apply changes to UI
                $timeout(function () {
                    $scope.$apply();
                });
            }
        });
        $("#tinnoid").ajaxForm({
            complete: function (response) {
                if (response.status == 422) {
                    alert(response.responseJSON.error);
                    return;
                }
                $scope.tinnoFile = response.responseJSON.originalname;
                $('#tinnourl').val(response.responseJSON.key.split('/')[2]);
                $timeout(function () {
                    $scope.$apply();
                });
            }
        });
        $("#cstnoid").ajaxForm({
            complete: function (response) {
                if (response.status == 422) {
                    alert(response.responseJSON.error);
                    return;
                }
                $scope.cstnoFile = response.responseJSON.originalname;
                $('#cstnourl').val(response.responseJSON.key.split('/')[2]);
                $timeout(function () {
                    $scope.$apply();
                });
            }
        });
        $("#panid").ajaxForm({
            complete: function (response) {
                if (response.status == 422) {
                    alert(response.responseJSON.error);
                    return;
                }
                $scope.pannoFile = response.responseJSON.originalname;
                $('#pannourl').val(response.responseJSON.key.split('/')[2]);
                $timeout(function () {
                    $scope.$apply();
                });
            }
        });
        $("#servicetaxid").ajaxForm({
            complete: function (response) {
                if (response.status == 422) {
                    alert(response.responseJSON.error);
                    return;
                }
                $scope.servicetaxnoFile = response.responseJSON.originalname;
                $('#servicetaxnourl').val(response.responseJSON.key.split('/')[2]);
                $timeout(function () {
                    $scope.$apply();
                });
            }
        });
        $("#esicnoid").ajaxForm({
            complete: function (response) {
                if (response.status == 422) {
                    alert(response.responseJSON.error);
                    return;
                }
                $scope.esicnoFile = response.responseJSON.originalname;
                $('#esicnourl').val(response.responseJSON.key.split('/')[2]);
                $timeout(function () {
                    $scope.$apply();
                });
            }
        });
        $("#tdsid").ajaxForm({
            complete: function (response) {
                if (response.status == 422) {
                    alert(response.responseJSON.error);
                    return;
                }
                $scope.tdsexemptionFile = response.responseJSON.originalname;
                $('#tdsexemptionurl').val(response.responseJSON.key.split('/')[2]);
                $timeout(function () {
                    $scope.$apply();
                });
            }
        });
        $("#smallscaleid").ajaxForm({
            complete: function (response) {
                if (response.status == 422) {
                    alert(response.responseJSON.error);
                    return;
                }
                $scope.smallscaleFile = response.responseJSON.originalname;
                $('#smallscaleurl').val(response.responseJSON.key.split('/')[2]);
                $timeout(function () {
                    $scope.$apply();
                });
            }
        });
    });

    $('#tabHead2').addClass('active');

// hide success & error message
    $scope.show = function () {
        if ($scope.tdsexemptionName == 'no') {
            console.log('hide');
            $('#tdfile, #tdSubmitBtn').hide();
        } else {
            console.log('show');
            $('#tdfile, #tdSubmitBtn').show();
        }
    }

    $scope.show1 = function () {
        if ($scope.smallscaleName == 'no') {
            console.log('hide');
            $('#smallfile, #smallSubmitBtn').hide();
        } else {
            console.log('show');
            $('#smallfile, #smallSubmitBtn').show();
        }
    }

    $http.get('/supplier/get/' + $scope.supplierId)
            .then(function (response) {

                var supplierDetails = response.data.data[0];
                $scope.supplierCode = supplierDetails.supplierCode;
                if (supplierDetails.businessreg) {
                    $scope.licenceName = supplierDetails.businessreg.licence.name;
                    $scope.licenceFile = supplierDetails.businessreg.licence.file;
                    $scope.salestaxnoName = supplierDetails.businessreg.salestaxno.name;
                    $scope.salestaxnoFile = supplierDetails.businessreg.salestaxno.file;
                    $scope.tinnoName = supplierDetails.businessreg.tinno.name;
                    $scope.tinnoFile = supplierDetails.businessreg.tinno.file;
                    $scope.cstnoName = supplierDetails.businessreg.cstno.name;
                    $scope.cstnoFile = supplierDetails.businessreg.cstno.file;
                    $scope.pannoName = supplierDetails.businessreg.panno.name;
                    $scope.pannoFile = supplierDetails.businessreg.panno.file;
                    $scope.servicetaxnoName = supplierDetails.businessreg.servicetaxno.name;
                    $scope.servicetaxnoFile = supplierDetails.businessreg.servicetaxno.file;
                    $scope.esicnoName = supplierDetails.businessreg.esicno.name;
                    $scope.esicnoFile = supplierDetails.businessreg.esicno.file;
                    $scope.tdsexemptionName = supplierDetails.businessreg.tdsexemption.name;
                    $scope.tdsexemptionFile = supplierDetails.businessreg.tdsexemption.file;
                    $scope.smallscaleName = supplierDetails.businessreg.smallscale.name;
                    $scope.smallscaleFile = supplierDetails.businessreg.smallscale.file;
                }

            });

// submit form
    $scope.updateRegistration1 = function () {

        $scope.error = '';
        $scope.success = '';

        console.log($('#licenceurl').val());
        console.log($('#salestaxnourl').val());
        console.log('scope - ' + $scope.licenceurl);

        // get data
        var licence = {
            name: $scope.licenceName,
            //  file: $scope.licenceurl,
            file: ($('#licenceurl').val()),
        };
        var salestaxno = {
            name: $scope.salestaxnoName,
            file: ($('#salestaxnourl').val()),
        };
        var tinno = {
            name: $scope.tinnoName,
            file: ($('#tinnourl').val()),
        };
        var cstno = {
            name: $scope.cstnoName,
            file: ($('#cstnourl').val()),
        };
        var panno = {
            name: $scope.pannoName,
            file: ($('#pannourl').val()),
        };
        var servicetaxno = {
            name: $scope.servicetaxnoName,
            file: ($('#servicetaxnourl').val()),
        };
        var esicno = {
            name: $scope.esicnoName,
            file: ($('#esicnourl').val()),
        };
        var tdsexemption = {
            name: $scope.tdsexemptionName,
            file: ($('#tdsexemptionurl').val()),
        };
        var smallscale = {
            name: $scope.smallscaleName,
            file: ($('#smallscaleurl').val()),
        };


        // validations
        if (licence == '' || typeof (licence) == 'undefined') {
            $scope.error = 'Please enter licence';
            return false;
        }
        if (salestaxno == '' || typeof (salestaxno) == 'undefined') {
            $scope.error = 'Please enter salestaxno';
            return false;
        }
        if (tinno == '' || typeof (tinno) == 'undefined') {
            $scope.error = 'Please enter tinno';
            return false;
        }
        if (cstno == '' || typeof (cstno) == 'undefined') {
            $scope.error = 'Please enter cstno';
            return false;
        }
        if (panno == '' || typeof (panno) == 'undefined') {
            $scope.error = 'Please enter panno';
            return false;
        }
        if (servicetaxno == '' || typeof (servicetaxno) == 'undefined') {
            $scope.error = 'Please enter servicetaxno';
            return false;
        }
        if (esicno == '' || typeof (esicno) == 'undefined') {
            $scope.error = 'Please enter esicno';
            return false;
        }
        if (tdsexemption == '' || typeof (tdsexemption) == 'undefined') {
            $scope.error = 'please enter tdsexemption';
            return false;
        }
        if (smallscale == '' || typeof (smallscale) == 'undefined') {
            $scope.error = 'please enter smallscale';
            return false;
        }

        // data 
        var data = {
            licence: licence,
            salestaxno: salestaxno,
            tinno: tinno,
            cstno: cstno,
            panno: panno,
            servicetaxno: servicetaxno,
            esicno: esicno,
            tdsexemption: tdsexemption,
            smallscale: smallscale,
        };
        var config = {};

        $http.put('/supplier/updateRegistration/' + $scope.supplierId, data, config)
                .success(function (data, status, headers, config) {
                    console.log('success edit - ', data);

                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

                    $timeout(function () {
                        $window.location.href = '/admin/#/supplier';
                    }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error edit - ', data);
                    $scope.error = 'Unable to add supplier';
                });
    };
});



//Update Company Details
app.controller('SupplierCompanyDetailsController', function ($timeout, $scope, $http, $window, $routeParams, $location) {
    // no access check
    noAccessRedirect($location);

    $scope.supplierId = $routeParams.supplierId;
    $('#tabHead3').addClass('active');

    $http.get('/supplier/get/' + $scope.supplierId)
            .then(function (response) {

                var supplierDetails = response.data.data[0];
                $scope.supplierCode = supplierDetails.supplierCode;
                $scope.companyname = supplierDetails.companydetail.companyname;
                $scope.natureof = supplierDetails.companydetail.natureof;
                $scope.addressfor = supplierDetails.companydetail.addressfor;
                $scope.telephone = supplierDetails.companydetail.telephone;
                $scope.faxno = supplierDetails.companydetail.faxno;
                $scope.emailid = supplierDetails.companydetail.emailid;
                $scope.alternative = supplierDetails.companydetail.alternative;
                $scope.nameinfo = supplierDetails.companydetail.nameinfo;
                $scope.designation = supplierDetails.companydetail.designation;
                $scope.directno = supplierDetails.companydetail.directno;
                $scope.favourof = supplierDetails.companydetail.favourof;

            });

// submit form
    $scope.updateCompanyInfo = function () {
        $scope.error = '';
        $scope.success = '';

        // get data
        var companyname = $scope.companyname;
        var natureof = $scope.natureof;
        var addressfor = $scope.addressfor;
        var telephone = $scope.telephone;
        var faxno = $scope.faxno;
        var emailid = $scope.emailid;
        var alternative = $scope.alternative;
        var nameinfo = $scope.nameinfo;
        var designation = $scope.designation;
        var directno = $scope.directno;
        var favourof = $scope.favourof;

        // validations
        if (companyname == '' || typeof (companyname) == 'undefined') {
            $scope.error = 'Please enter companyname';
            return false;
        }
        if (natureof == '' || typeof (natureof) == 'undefined') {
            $scope.error = 'Please enter nature of business';
            return false;
        }
        if (addressfor == '' || typeof (addressfor) == 'undefined') {
            $scope.error = 'Please enter addressfor';
            return false;
        }
        if (telephone == '' || typeof (telephone) == 'undefined') {
            $scope.error = 'Please enter telephone';
            return false;
        }
        if (faxno == '' || typeof (faxno) == 'undefined') {
            $scope.error = 'Please enter faxno';
            return false;
        }
        if (emailid == '' || typeof (emailid) == 'undefined') {
            $scope.error = 'Please enter emailid';
            return false;
        }
        if (alternative == '' || typeof (alternative) == 'undefined') {
            $scope.error = 'Please enter alternative';
            return false;
        }
        if (nameinfo == '' || typeof (nameinfo) == 'undefined') {
            $scope.error = 'Please enter nameinfo';
            return false;
        }
        if (designation == '' || typeof (designation) == 'undefined') {
            $scope.error = 'Please select designation';
            return false;
        }
        if (directno == '' || typeof (directno) == 'undefined') {
            $scope.error = 'Please select directno';
            return false;
        }
        if (favourof == '' || typeof (favourof) == 'undefined') {
            $scope.error = 'Please select favourof';
            return false;
        }

        // data 
        var data = {
            companyname: companyname,
            natureof: natureof,
            addressfor: addressfor,
            telephone: telephone,
            faxno: faxno,
            emailid: emailid,
            alternative: alternative,
            nameinfo: nameinfo,
            designation: designation,
            directno: directno,
            favourof: favourof,
        };
        var config = {};

        $http.put('/supplier/updateCompanyInfo/' + $scope.supplierId, data, config)
                .success(function (data, status, headers, config) {
                    console.log('success edit - ', data);

                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

                    $timeout(function () {
                        $window.location.href = '/admin/#/supplier';
                    }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error edit - ', data);
                    $scope.error = 'Unable to add supplier';
                });
    };
});

//update Bank details
app.controller('SupplierBankDetailsController', function ($timeout, $scope, $http, $window, $routeParams, $location) {
    // no access check
    noAccessRedirect($location);

    $scope.supplierId = $routeParams.supplierId;
    $('#tabHead4').addClass('active');

    $http.get('/supplier/get/' + $scope.supplierId)
            .then(function (response) {

                var supplierDetails = response.data.data[0];
                $scope.supplierCode = supplierDetails.supplierCode;
                $scope.bankname = supplierDetails.bankdetail.bankname;
                $scope.bankaddress = supplierDetails.bankdetail.bankaddress;
                $scope.accountno = supplierDetails.bankdetail.accountno;
                $scope.ifsccode = supplierDetails.bankdetail.ifsccode;
                $scope.accounttype = (supplierDetails.bankdetail.accounttype).toString();
            });

// submit form
    $scope.updateBankInfo = function () {
        $scope.error = '';
        $scope.success = '';

        // get data
        var bankname = $scope.bankname;
        var bankaddress = $scope.bankaddress;
        var accountno = $scope.accountno;
        var ifsccode = $scope.ifsccode;
        var accounttype = $('#accounttype').val();

        // validations
        if (bankname == '' || typeof (bankname) == 'undefined') {
            $scope.error = 'Please enter bankname';
            return false;
        }
        if (bankaddress == '' || typeof (bankaddress) == 'undefined') {
            $scope.error = 'Please enter bankaddress';
            return false;
        }
        if (accountno == '' || typeof (accountno) == 'undefined') {
            $scope.error = 'Please enter accountno';
            return false;
        }
        if (ifsccode == '' || typeof (ifsccode) == 'undefined') {
            $scope.error = 'Please enter ifsccode';
            return false;
        }
        if (accounttype == '' || typeof (accounttype) == 'undefined') {
            $scope.error = 'Please select accounttype';
            return false;
        }
        // data 
        var data = {
            bankname: bankname,
            bankaddress: bankaddress,
            accountno: accountno,
            ifsccode: ifsccode,
            accounttype: accounttype,
        };
        var config = {};

        $http.put('/supplier/updateBankInfo/' + $scope.supplierId, data, config)
                .success(function (data, status, headers, config) {
                    console.log('success edit - ', data);

                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

                    $timeout(function () {
                        $window.location.href = '/admin/#/supplier';
                    }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error edit - ', data);
                    $scope.error = 'Unable to add supplier';
                });
    };
});


//Business Ref Update.
app.controller('SupplierBusinessRefController', function ($timeout, $scope, $http, $window, $routeParams, $location) {
    console.log("I am called222");
    // no access check
    noAccessRedirect($location);
    $scope.supplierId = $routeParams.supplierId;

    $('#tabHead5').addClass('active');

    $http.get('/supplier/get/' + $scope.supplierId)
            .then(function (response) {

                var supplierDetails = response.data.data[0];
                $scope.supplierCode = supplierDetails.supplierCode;
                $scope.businessRefList = supplierDetails.businessref;

            });
    //add multiple form
    $scope.choices = [{id: 'choice1', srNo: 1}];
    $scope.addNewChoice = function () {
        var newItemNo = $scope.choices.length + 1;
        $scope.choices.push({'id': 'choice' + newItemNo, srNo: newItemNo});
    };

    $scope.fillBusinessRefDetils = function (businessRefId) {
        // find details
        for (var i = 0; i < $scope.businessRefList.length; i++) {
            if (businessRefId == $scope.businessRefList[i]._id) {
                $('#id').val(businessRefId);
                $('#name').val($scope.businessRefList[i].nameref);
                $('#designation').val($scope.businessRefList[i].designation);
                $('#telephone').val($scope.businessRefList[i].telephone);
            }
        }
    };

// submit form
    $scope.addBusinessRef = function (srNo) {
        $scope.error = '';
        $scope.success = '';
        console.log("I am called");
        // get data
        var nameref = $('#nameref_' + srNo).val();
        var designation = $('#designation_' + srNo).val();
        var telephone = $('#telephone_' + srNo).val();

        // validations
        if (nameref == '' || typeof (nameref) == 'undefined') {
            $scope.error = 'Please enter Name';
            return false;
        }
        if (designation == '' || typeof (designation) == 'undefined') {
            $scope.error = 'Please enter Designation';
            return false;
        }
        if (telephone == '' || typeof (telephone) == 'undefined') {
            $scope.error = 'Please enter telephone';
            return false;
        }
        // data 
        var data = {
            nameref: nameref,
            designation: designation,
            telephone: telephone,
        };
        var config = {};

        $http.put('/supplier/updateBusinessRF/' + $scope.supplierId, data, config)
                .success(function (data, status, headers, config) {
                    console.log('success edit - ', data);

                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;
                    $timeout(function () {
                        $window.location.href = '/admin/#/supplier';
                    }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error edit - ', data);
                    $scope.error = 'Unable to add supplier';
                });
    };
});


//Business Ref Update. After Edit
app.controller('SupplierAllBusinessRefController', function ($timeout, $scope, $http, $window, $routeParams, $location) {
    // no access check
    noAccessRedirect($location);

    $scope.supplierId = $routeParams.supplierId;

    $('#tabHead5').addClass('active');

    $http.get('/supplier/get/' + $scope.supplierId)
            .then(function (response) {

                var supplierDetails = response.data.data[0];
                $scope.supplierCode = supplierDetails.supplierCode;
                $scope.businessRefList = supplierDetails.businessref;
            });

    //add multiple form
    $scope.choices = [{id: 'choice1', srNo: 1}];
    $scope.addNewChoice = function () {
        var newItemNo = $scope.choices.length + 1;
        $scope.choices.push({'id': 'choice' + newItemNo, srNo: newItemNo});
    };

    $scope.fillBusinessRefDetils = function (businessRefId) {
        console.log("In businessref demo......");

        $('#addform').hide();
        $('#updateform').show();

        // find details
        for (var i = 0; i < $scope.businessRefList.length; i++) {
            if (businessRefId == $scope.businessRefList[i]._id) {
                $('#id').val(businessRefId);
                $('#name').val($scope.businessRefList[i].nameref);
                $('#designation').val($scope.businessRefList[i].designation);
                $('#telephone').val($scope.businessRefList[i].telephone);
            }
        }
    }
    // submit form
    $scope.UpdateBusinessRF = function () {
        $scope.error = '';
        $scope.success = '';

        // get data
        var id = $('#id').val();
        var name = $('#name').val();
        var designation = $('#designation').val();
        var telephone = $('#telephone').val();

        // validations
        if (name == '' || typeof (name) == 'undefined') {
            $scope.error = 'Please enter Name';
            return false;
        }
        if (designation == '' || typeof (designation) == 'undefined') {
            $scope.error = 'Please enter Designation';
            return false;
        }
        if (telephone == '' || typeof (telephone) == 'undefined') {
            $scope.error = 'Please enter telephone';
            return false;
        }
        // data 
        var data = {
            id: id,
            nameref: name,
            designation: designation,
            telephone: telephone,
        };
        var config = {};

        $http.put('/supplier/updateAllBusinessRF/' + $scope.supplierId, data, config)
                .success(function (data, status, headers, config) {
                    console.log('success edit - ', data);

                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;
                    $timeout(function () {
                        $window.location.href = '/admin/#/supplier';
                    }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error edit - ', data);
                    $scope.error = 'Unable to add supplier';
                });
    };
});


//update  RM details
app.controller('SupplierRMDetailsController', function ($timeout, $scope, $http, $window, $routeParams, $location) {
    // no access check
    noAccessRedirect($location);

    $scope.supplierId = $routeParams.supplierId;

    $('#tabHead6').addClass('active');

    $http.get('/supplier/get/' + $scope.supplierId)
            .then(function (response) {

                var supplierDetails;
                $scope.supplierCode = response.data.data[0].supplierCode;
                if (response.data.data.length > 0 && response.data.data[0].rmdetail) {
                    var supplierDetails = response.data.data[0];
                    $scope.rmname = supplierDetails.rmdetail.rmname;
                    $scope.mobileno = supplierDetails.rmdetail.mobileno;
                }
            });

// submit form
    $scope.updatermdetails = function () {
        $scope.error = '';
        $scope.success = '';

        // get data
        var rmname = $scope.rmname;
        var mobileno = $scope.mobileno;

        // validations
        if (rmname == '' || typeof (rmname) == 'undefined') {
            $scope.error = 'Please enter Name';
            return false;
        }
        if (mobileno == '' || typeof (mobileno) == 'undefined') {
            $scope.error = 'Please enter mobileno';
            return false;
        }
        // data 
        var data = {
            rmname: rmname,
            mobileno: mobileno,
        };
        var config = {};

        $http.put('/supplier/updatermdetails/' + $scope.supplierId, data, config)
                .success(function (data, status, headers, config) {
                    console.log('success edit - ', data);

                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

                    $timeout(function () {
                        $window.location.href = '/admin/#/supplier';
                    }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error edit - ', data);
                    $scope.error = 'Unable to add supplier';
                });
    };
});

//Addresses
app.controller('SupplierAddressesController', function ($timeout, $scope, $http, $window, $routeParams, $location) {
    // buyer Profile
    $scope.loginError = '';
    $scope.loginSuccess = '';

    $scope.supplierId = $routeParams.supplierId;
    //make tab active
    $('#tabHead7').addClass('active');

    // get india states
    $scope.indiaStates = getIndiaStates();

    //get existing supplier details
    $http.get('/supplier/get/' + $scope.supplierId)
            .then(function (response) {

                $scope.supplierCode = response.data.data[0].supplierCode;
                if (response.data.data.length > 0 && response.data.data[0].addressInfo) {
                    $scope.buyerAddressList = response.data.data[0].addressInfo;
                    if ($scope.buyerAddressList.length >= 2) {
                        $('.submit-btn').attr('disabled', 'disabled');
//                        $('.submit-btn').after('Cannot add more than 2 addresses');
                    }
                }
            });

    $scope.fillBuyerDetils = function (buyerRefId) {
        // find details
        console.log($scope.buyerAddressList);
        for (var i = 0; i < $scope.buyerAddressList.length; i++) {
            if (buyerRefId == $scope.buyerAddressList[i]._id) {
                $('#addressId').val(buyerRefId);
                $('#fullName').val($scope.buyerAddressList[i].fullName);
                $('#addressHeading').val($scope.buyerAddressList[i].addressHeading);
                console.log("$scope.buyerAddressList[i].addressType");
                console.log($scope.buyerAddressList[i].addressType);
                $('#addressType').val($scope.buyerAddressList[i].addressType);
//                $scope.addressType = $scope.buyerAddressList[i].addressType;
                $('#addressDetail').val($scope.buyerAddressList[i].addressDetail);
                $('#landmark').val($scope.buyerAddressList[i].landmark);
                $('#city').val($scope.buyerAddressList[i].city);
                $('#pincode').val($scope.buyerAddressList[i].pincode);
                $('#state').val($scope.buyerAddressList[i].state);
                $('#country').val($scope.buyerAddressList[i].country);
                $('#phone').val($scope.buyerAddressList[i].phone);
                $('.submit-btn').removeAttr('disabled');
            }
        }
    }
    // submit form
    $scope.addressBuyer = function () {
        $scope.error = '';
        $scope.success = '';

        if ($scope.buyerAddressList.length == 2 && $('#addressId').val().length == 0) {
            $scope.error = "You can add only 2 addresses";
            return;
        }

        // get data
        var addressId = $('#addressId').val();
        var fullName = $('#fullName').val();
        var addressHeading = $('#addressHeading').val();
        var addressType = $('#addressType').val();
        var addressDetail = $('#addressDetail').val();
        var landmark = $('#landmark').val();
        var city = $('#city').val();
        var pincode = $('#pincode').val();
        var state = $('#state').val();
        var country = $('#country').val();
        var phone = $('#phone').val();

        // validations
        if (fullName == '' || typeof (fullName) == 'undefined') {
            $scope.error = 'Please enter fullName';
            return false;
        }
        if (addressHeading == '' || typeof (addressHeading) == 'undefined') {
            $scope.error = 'Please enter addressHeading';
            return false;
        }
        if (addressType == '' || typeof (addressType) == 'undefined') {
            $scope.error = 'Please enter addressType';
            return false;
        }
        for (var i = 0; i < $scope.buyerAddressList.length; i++) {
            if ($scope.buyerAddressList[i].addressType == addressType && $scope.buyerAddressList[i]._id != addressId) {
                $scope.error = "You have already added " + (addressType == 0 ? "Billing" : "Pickup ") + " type of address. Please change type";
                return;
            }
        }
        if (addressDetail == '' || typeof (addressDetail) == 'undefined') {
            $scope.error = 'Please enter address';
            return false;
        }
        if (landmark == '' || typeof (landmark) == 'undefined') {
            $scope.error = 'Please enter landmark';
            return false;
        }
        if (city == '' || typeof (city) == 'undefined') {
            $scope.error = 'Please enter city';
            return false;
        }
        if (pincode == '' || typeof (pincode) == 'undefined' || pincode.length > 6 || pincode.length < 6)
        {
            $scope.error = 'Please enter valid pincode';
            return false;
        }
        if (state == '' || typeof (state) == 'undefined') {
            $scope.error = 'Please enter state';
            return false;
        }
        if (country == '' || typeof (country) == 'undefined') {
            $scope.error = 'Please enter country';
            return false;
        }
        if (phone == '' || typeof (phone) == 'undefined') {
            $scope.error = 'Please enter phone';
            return false;
        }
        // data 
        var data = {
            fullName: fullName,
            addressHeading: addressHeading,
            addressType: addressType,
            addressDetail: addressDetail,
            landmark: landmark,
            city: city,
            pincode: pincode,
            state: state,
            country: country,
            phone: phone
        };
        var config = {};

        // check for id
        if (addressId != '') {
            data._id = addressId;
        }
        $http.put('/supplier/updateAddress/' + $scope.supplierId, data, config)
                .success(function (dataResponse, status, headers, config) {
                    console.log('success edit - ', dataResponse);

                    if (dataResponse.error == 1) {
                        $scope.error = dataResponse.msg;
                        return false;
                    }
                    $scope.success = dataResponse.msg;
                    var updated = false;
                    for (var i = 0; i < $scope.buyerAddressList.length; i++) {
                        if (data._id && $scope.buyerAddressList[i]._id == data._id) {
                            $scope.buyerAddressList[i] = data;
                            updated = true;
                        }
                    }
                    if (updated == false) {
                        $scope.buyerAddressList.push(data);
                    }
                    $('.submit-btn').attr('disabled', 'disabled');
                })
                .error(function (dataError, status, header, config) {
                    console.log('error edit - ', dataError);
                    $scope.error = 'Unable to add supplier address';
                });
    };
    
    //autocomplete
    $( "#pincode" ).autocomplete({
      source: function(request, response){
            console.log("request");
            console.log(request);
            $http.get('/supplier/pincodeInfo?pincode='+request.term)
                    .success(function(data, status){
                        $scope.pincodeInfo = data.data;
                        var autoCompleteArray = [];
                        for (var i = 0; i < data.data.length; i++) {
                            autoCompleteArray.push(data.data[i].pincode);
                        }
                        response(autoCompleteArray);
                    })
                    .error(function(data, status){
                        //catches error like 404
                        console.log(data);
                        console.log(status);
                    });
      },
      select: function( event, ui ) {
            var selectedElement;
            for (var i = 0; i < $scope.pincodeInfo.length; i++) {
                if(ui.item.value == $scope.pincodeInfo[i].pincode){
                    selectedElement = $scope.pincodeInfo[i];
                    break;
                }
            }
            $scope.state = selectedElement.state;
            $scope.city = selectedElement.city;
            $timeout(function(){
                $scope.$apply();
            });
      }
    });
});
