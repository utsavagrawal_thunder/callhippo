// closure for this controller // private
(function() {
    // initialize date picker for search
    function initDatepickerForSearch() {
        $(function() {
            $("#createdDateFrom, #createdDateTo").datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                clearBtn: true
            });
        });
    }

    // get sales summary count
    function getSalesSummaryCnt($scope, $http) {
        $scope.totalSales = 0;
        $scope.totalProductCharges = 0;
        $scope.netCommissionPercent = 0;
        $http.get('/sales/getSalesSummaryCnt')
            .success(function (data, status, headers, config) {
                $scope.totalSales = data.totalSales;
                $scope.totalProductCharges = data.totalProductCharges;
                $scope.netCommissionPercent = data.netCommissionPercent;
            })
            .error(function (data, status, header, config) {
                
            });
    }

    // All Buyer sales list
    app.controller('AllSalesController', function ($timeout, $scope, $http, $location, NgTableParams) {
        // check for access
        noAccessRedirect($location);

        // get data
        $scope.getAllSalesList = function() {
            getAllSalesList($scope, $http, $timeout, NgTableParams, {});
        };
        $scope.getAllSalesList();

        // Reset search
        $scope.resetSearch = function() {
            $scope.getAllSalesList();
        };

        // initialize datepicker
        initDatepickerForSearch();

        // show search box panel
        $scope.showSearchBox = function () {
            var searchType = $scope.searchType;

            // hide all
            $('#orderIdBox, #createdDateBox').hide();

            // show specific
            if(searchType == 'orderId') {
                $('#orderIdBox').show();
            }
            else if(searchType == 'createdDate') {
                $('#createdDateBox').show();
            }
        };

        // reset search form
        $scope.resetSearchForm = function() {
            // hide all
            $('#orderIdBox, #createdDateBox').hide();

            // get page 1 results 
            getAllSalesList($scope, $http, $timeout, NgTableParams, {});
        };

        // search sales list
        $scope.searchSales = function() {
            var searchType = $scope.searchType;
            var searchFields = {
                searchType: searchType
            };

            // for coupon code
            if(searchType == 'orderId') {
                var orderId = (typeof($scope.orderId) != 'undefined') ? $scope.orderId : '';

                // validation
                if(typeof(orderId) == 'undefined' || orderId == '') {
                    showToastMsg(0, 'Please enter order id');
                    $('#orderId').focus();
                    return false;
                }

                searchFields.orderId = orderId;
            }

            // for created date
            else if(searchType == 'createdDate') {
                var createdDateFrom = $('#createdDateFrom').val();
                var createdDateTo = $('#createdDateTo').val();

                // validations
                if(createdDateFrom == '' && createdDateTo == '') {
                    showToastMsg(0, 'Please select from or to date placed');
                    $('#createdDateFrom').focus();
                    return false;
                }
                if((createdDateFrom != '' && createdDateTo != '') && (createdDateTo < createdDateFrom)) {
                    showToastMsg(0, 'Please select end date greater than start date');
                    $('#createdDateTo').focus();
                    return false;
                }

                searchFields.createdDateFrom = createdDateFrom;
                searchFields.createdDateTo = createdDateTo;
            }

            // Now call API with ng-table
            getAllSalesList($scope, $http, $timeout, NgTableParams, searchFields);
        };

        // Get sales counts
        getSalesSummaryCnt($scope, $http);
    });

    // get all sales list
    function getAllSalesList($scope, $http, $timeout, NgTableParams, searchFields) {
        $scope.tableParams = new NgTableParams(
            {count: 25, page: 1, sorting: { createdDate: 'desc' }},
            {
                counts: [25, 50, 100],
                total: 0,
                getData: function (params) {
                    var count = params.count();
                    var page = params.page();
                    // for sorting
                    var sorting = params.sorting();
                    var sortParam = 'createdDate';
                    var sortOrder = 'DESC';
                    if(!isObjectEmpty(sorting)) {
                        sortParam = Object.keys(sorting)[0];
                        sortOrder = sorting[sortParam];                                        
                    }

                    var dataObj = {
                        searchTerm: '',
                        perPage: count,
                        pageNum: page,
                        sortParam: sortParam,
                        sortOrder: sortOrder,
                        searchFields: searchFields
                    };
                    return new Promise(function (resolve, reject) {
                        $http.post('/sales/getAllSales', dataObj)
                            .success(function (data, status, headers, config) {
                                $scope.tableParams.total(data.totalCnt);
                                resolve(data.data);
                            })
                            .error(function (data, status, header, config) {
                                reject([]);
                            });
                    });
                }
            });
    }

    // Supplier sales list
    app.controller('SupplierSalesController', function ($timeout, $scope, $http, $location, NgTableParams) {
        // check for access
        noAccessRedirect($location);

        // get data
        $scope.getSupplierSalesList = function() {
            getSupplierSalesList($scope, $http, $timeout, NgTableParams, {});
        };
        $scope.getSupplierSalesList();

        // Reset search
        $scope.resetSearch = function() {
            $scope.getSupplierSalesList();
        };

        // initialize datepicker
        initDatepickerForSearch();

        // show search box panel
        $scope.showSearchBox = function () {
            var searchType = $scope.searchType;

            // hide all
            $('#supplierCodeBox').hide();

            // show specific
            if(searchType == 'supplierCode') {
                $('#supplierCodeBox').show();
            }
        };

        // reset search form
        $scope.resetSearchForm = function() {
            // hide all
            $('#supplierCodeBox').hide();

            // get page 1 results 
            getSupplierSalesList($scope, $http, $timeout, NgTableParams, {});
        };

        // search sales list
        $scope.searchSales = function() {
            var searchType = $scope.searchType;
            var searchFields = {
                searchType: searchType
            };

            // for supplier code
            if(searchType == 'supplierCode') {
                var supplierCode = (typeof($scope.supplierCode) != 'undefined') ? $scope.supplierCode : '';

                // validation
                if(typeof(supplierCode) == 'undefined' || supplierCode == '') {
                    showToastMsg(0, 'Please enter supplier code');
                    $('#supplierCode').focus();
                    return false;
                }

                searchFields.supplierCode = supplierCode;
            }

            // Now call API with ng-table
            getSupplierSalesList($scope, $http, $timeout, NgTableParams, searchFields);
        };

        // Get sales counts
        getSalesSummaryCnt($scope, $http);
    });

    // get supplier sales list
    function getSupplierSalesList($scope, $http, $timeout, NgTableParams, searchFields) {
        $scope.tableParams = new NgTableParams(
            {count: 25, page: 1},
            {
                counts: [25, 50, 100],
                total: 0,
                getData: function (params) {
                    var count = params.count();
                    var page = params.page();

                    var dataObj = {
                        perPage: count,
                        pageNum: page,
                        searchFields: searchFields
                    };
                    return new Promise(function (resolve, reject) {
                        $http.post('/sales/getSupplierSales', dataObj)
                            .success(function (data, status, headers, config) {
                                $scope.tableParams.total(data.totalCnt);
                                resolve(data.data);
                            })
                            .error(function (data, status, header, config) {
                                reject([]);
                            });
                    });
                }
            });
    }

})(); // End of closure