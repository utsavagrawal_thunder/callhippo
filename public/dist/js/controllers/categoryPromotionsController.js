app.controller('CategoryPromotionsTab1Controller', function ($scope, $http, $timeout, $location) {
    $scope.isVisible = true;

    $scope.selectedCategorySlug = (window.location.href.split('#')[1]).split('/')[3];
    $http.get('/promotions/category/get?cat_slug=' + $scope.selectedCategorySlug)
            .then(function (response) {
                if (response.data.data.length == 0) {
                    $scope.promotionsError = "No active  main categories found";
                    $(".error-modal-lg").modal();
                    return;
                } else {
                    $scope.category = response.data.data[0].category;
                    $scope.mainCategories = response.data.data;
                    $scope.categoryName = response.data.data[0].category.name;
                    $scope.isActive = (response.data.data[0].isActive).toString();
                    console.log("$scope.isActive");
                    console.log($scope.isActive);
                }
            });

    $scope.submitDivider = function (dividerNumber) {
        var serverObj = {
            isHomePage: false,
            "parentCategory.slug" : $scope.selectedCategorySlug,
            isActive: $scope.isActive
        };
        $http.post('/promotions/category/insert', serverObj)
                .then(function (response) {
                    console.log(response);
                    if (response.data.error == 0) {
                        $scope.success = "Saved successfully.";
                        $timeout(function () {
                            $location.path('/categoryPromotions/divider2/' + $scope.category.slug);
                        }, 3000);
                    } else {
                        $scope.error = response.data.msg;
                    }
                });
    };
});
app.controller('CategoryPromotionsTab2Controller', function ($scope, $http, $timeout, $location) {
    $scope.dividerNumber = 2;
    $scope.dividerName = 'Top Products';

    $scope.selectedProducts = [];

    //toggle divider visiblity
    $scope.toggleVisiblity = function () {
        $scope.isVisible = !$scope.isVisible;
    };
    $scope.selectedCategorySlug = (window.location.href.split('#')[1]).split('/')[3];
    $http.get('/promotions/category/get?cat_slug=' + $scope.selectedCategorySlug)
            .then(function (response) {
                if (response.data.data.length == 0) {
                    $scope.promotionsError = "No active  main categories found";
                    $(".error-modal-lg").modal();
                    return;
                } else {
                    var promotionalData = response.data.data[0];
                    $scope.category = response.data.data[0].category;
                    for (var i = 0; i < promotionalData.dividers.length; i++) {
                        if (promotionalData.dividers[i].dividerNumber == 2) {
                            $scope.currentDividerInfo = promotionalData.dividers[i];
                            if ($scope.currentDividerInfo.dividerName) {
                                $scope.dividerName = $scope.currentDividerInfo.dividerName;
                            }
                            if (promotionalData.dividers[i].products && promotionalData.dividers[i].products.length > 0) {
                                $scope.selectedProducts = promotionalData.dividers[i].products;
                            }
                            $scope.isVisible = $scope.currentDividerInfo.isVisible;
                        }
                    }
                    $scope.mainCategories = response.data.data;
                    $scope.categoryName = response.data.data[0].category.name;
                }
            });

    $http.get('/relations/getProductTypesByMainCategory?cat_slug=' + $scope.selectedCategorySlug)
            .then(function (response) {
                if (response.data.error == 0) {
                    $scope.productTypes = response.data.data;
                } else {
                    $scope.error = "No product types for the selected category";
                }
                $timeout(function () {
                    //$scope.$apply();
                    $('.chosen-select.catId').chosen({width: "100%"});
                    $('.chosen-select.supId').chosen({width: "100%"}).trigger("chosen:updated");
                    $('.chosen-select.prodType').chosen({width: "100%"}).change(function (event, params) {
                        loadCatalogueIds($scope, params.selected, $http);
                    });
                });
            });

    $scope.submitDivider = function () {
        var supplier_id = $(".chosen-select.supId option:selected").val();
        var catalogue_id = $(".chosen-select.catId option:selected").val();
        var product_type_id = $(".chosen-select.prodType option:selected").val();
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            if ($scope.selectedProducts[i].supplier_id == supplier_id && $scope.selectedProducts[i].catalogue_id == catalogue_id) {
                $scope.error = "Same product already exists. Please select another";
                return;
            }
        }
        $scope.selectedProducts.push({
            product_type_id: product_type_id,
            catalogue_id: catalogue_id,
            supplier_id: supplier_id
        });
        var data = {
            "parentCategory.slug": $scope.category.slug,
            dividerNumber: 2,
            dividerName: $scope.dividerName,
            products: $scope.selectedProducts,
            isVisible: $scope.isVisible
        };
        $http.post('/promotions/category/insert', data, {})
                .success(function (data, status, headers, config) {
                    console.log('success add - ', data);

                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    console.log("data.msg");
                    console.log(data.msg);
                    $scope.success = data.msg;

//                $timeout(function () {
//                    $location.path('/cate');
//                }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error add - ', data);
                    $scope.error = 'Unable to add product';
                });
    };

    function loadCatalogueIds($scope, selectedProductType, $http) {
        var variantData;
        $scope.catalogueIds = [];
        $scope.supplierInfo = [];
        $http.get('/product/getCatIdsByProductType?prodTypeId=' + selectedProductType)
                .then(function (responseCatIds) {
                    $scope.catalogueIds = [];
                    if (responseCatIds.data.error == 0) {
                        variantData = responseCatIds.data.data;
                        for (var i = 0; i < responseCatIds.data.data.length; i++) {
                            for (var j = 0; j < responseCatIds.data.data[i].variants.length; j++) {
                                $scope.catalogueIds.push({
                                    _id: responseCatIds.data.data[i].variants[j].catalogueId
                                });
                            }
                        }
                    } else {
                        $scope.error = "No catalogue ids under this product type.";
                    }
                    $timeout(function () {
                        $scope.$apply();
                        $('.chosen-select.supId').trigger("chosen:updated");
                        $('.chosen-select.catId').trigger('chosen:updated');
                        $('.chosen-select.catId').change(function (event, params) {
                            loadSupplierIds($scope, params.selected, $http, variantData);
                        });
                    });
                });
    }

    //add supplier SKU to supplier dropdown
    function loadSupplierIds($scope, selectedCatalogueId, $http, variantData) {
        $scope.supplierInfo = [];
        console.log("variantData");
        console.log(variantData);
        console.log(selectedCatalogueId);
        for (var i = 0; i < variantData.length; i++) {
            for (var j = 0; j < variantData[i].variants.length; j++) {
                if (variantData[i].variants[j].catalogueId == selectedCatalogueId) {
                    console.log("inside if else");
                    for (var k = 0; k < variantData[i].variants[j].suppliers.length; k++) {
                        $scope.supplierInfo.push({
                            sku: variantData[i].variants[j].suppliers[k].sku
                        });
                    }
                }
            }
        }
        $timeout(function () {
            $scope.$apply();
            $('.chosen-select.supId').chosen({width: "100%"}).trigger("chosen:updated");
        });
    }

});
app.controller('CategoryPromotionsTab3Controller', function ($scope, $http, $timeout, $location) {

    $scope.selectedCategorySlug = (window.location.href.split('#')[1]).split('/')[3];
    $scope.selectedProducts = [];
    //Image initialisation
    var configObject = {
        initialPreviewAsData: true,
        uploadUrl: "/promotions/category/uploadImage/",
        deleteUrl: "/promotions/deleteImage/",
        uploadAsync: false,
        previewSettings: {image: {width: "120px", height: "40px"}},
        overwriteInitial: false,
        allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],
        maxFileSize: 3000,
        fileActionSettings: {
            showUpload: false
        }
    };

    //toggle divider visiblity
    $scope.toggleVisiblity = function () {
        $scope.isVisible = !$scope.isVisible;
    };

    $scope.reInitThisChosen = function (item) {
        var $selectedOption = $("li[identity='" + item.label + "'] select option:selected");
        $("li[identity='" + item.label + "'] select option[value='" + $selectedOption.val() + "']").attr('selected', 'selected');
        $("li[identity='" + item.label + "'] .chosen-select").chosen({width: "90%"});
    };

    //get existing data for selected category
    $http.get('/promotions/category/get?cat_slug=' + $scope.selectedCategorySlug)
            .then(function (response) {
                if (response.data.data.length == 0) {
                    $scope.promotionsError = "No active  main categories found";
                    $(".error-modal-lg").modal();
                    return;
                } else {
                    $scope["parentCategory"]= response.data.data[0].category;

                    /* Code for page edit i.e viewing page after saving it earlier */
                    for (var i = 0; i < response.data.data[0].dividers.length; i++) {
                        if (response.data.data[0].dividers[i].dividerNumber == 3) {
                            $scope.isVisible = response.data.data[0].dividers[i].isVisible;
                            $scope.selectedProducts = response.data.data[0].dividers[i].products;
                            $scope.subCategories = response.data.data[0].dividers[i].sub_categories;
                            //selected sub category for this divider section
                            $scope.category = response.data.data[0].dividers[i].category;
                            //category whose promotion page is being edited/created
                            $scope.parentCategory = response.data.data[0].category;
                            if (!configObject.initialPreviewConfig) {
                                configObject.initialPreviewConfig = [];
                            }
                            if (!configObject.initialPreview) {
                                configObject.initialPreview = [];
                            }
                            if (!$scope.sortedImages) {
                                $scope.sortedImages = [];
                            }

                            /* Show banner images if already selected */
                            for (var j = 0; j < response.data.data[0].dividers[i].bannerImages.length; j++) {
                                configObject.initialPreviewConfig.push({
                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                    width: "120px",
                                    key: "promotions/category/" + response.data.data[0].dividers[i].bannerImages[j].name
                                });
                                var imageUrl = s3_bucket + "promotions/category/" + response.data.data[0].dividers[i].bannerImages[j].name;
                                configObject.initialPreview.push(imageUrl);
                                $scope.sortedImages.push({
                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                    url: response.data.data[0].dividers[i].bannerImages[j].url
                                });
                            }

                            /* For selected products */
                            if (!$scope.catalogueIds) {
                                $scope.catalogueIds = [];
                            }
                            if (!$scope.supplierInfo) {
                                $scope.supplierInfo = [];
                            }
                            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                                loadCatalogueIds($scope, $scope.selectedProducts[i].product_type_id, $http, i+1, false);
                            }
                        }
                    }
                    $http.get('/relations/getById?catId=' + $scope.parentCategory._id)
                            .then(function (response) {
                                var relationsObject = response.data[0];
                                $scope.allChildren = extractAllChildren(response.data[0]);

                                //check if category is already selected previously i.e page is being edited and not created
                                if ($scope.category) {
                                    var noChildren = false;
                                    for (var i = 0; i < $scope.allChildren.length; i++) {
                                        if ($scope.allChildren[i]._id == $scope.category._id && $scope.allChildren[i].isProductType == true) {
                                            $(".child-categories .error").html("Selected category is a product type, hence no child categories");
                                            $(".child-categories .error").css('display', 'block');
                                            noChildren = true;
                                            return;
                                        }
                                    }
                                    if (noChildren === false) {
                                        $(".child-categories .error").css('display', 'none');
                                        $scope.thisCategoryChildren = extractAllChildrenWithCondition(relationsObject, $scope.category._id);
                                        $scope.models = {
                                            selected: null,
                                            lists: {"A": []}
                                        };
                                        var max = $scope.thisCategoryChildren.length < 9 ? $scope.thisCategoryChildren.length : 9;
                                        for (var i = 1; i <= max; i++) {
                                            $scope.models.lists["A"].push({
                                                label: i
                                            });
                                        }
                                    }

                                }
                                //on change current sectional category
                                $(".chosen-select.categoryDivider3").chosen({width: "100%"}).change(function (event, params) {
                                    var noChildren = false;
                                    for (var i = 0; i < $scope.allChildren.length; i++) {
                                        if ($scope.allChildren[i]._id == params.selected && $scope.allChildren[i].isProductType == true) {
                                            $(".child-categories .error").html("Selected category is a product type, hence no child categories");
                                            $(".child-categories .error").css('display', 'block');
                                            noChildren = true;
                                            return;
                                        }
                                    }
                                    if (noChildren === false) {
                                        $(".child-categories .error").css('display', 'none');
                                        $scope.thisCategoryChildren = extractAllChildrenWithCondition(relationsObject, params.selected);
                                        $scope.models = {
                                            selected: null,
                                            lists: {"A": []}
                                        };
                                        var max;
                                        max = $scope.thisCategoryChildren.length < 9 ? $scope.thisCategoryChildren.length : 9;                                        
                                        for (var i = 1; i <= max; i++) {
                                            $scope.models.lists["A"].push({
                                                label: i
                                            });
                                        }
                                        $timeout(function () {
                                            //apply scope value to view
                                            $scope.$apply();
                                            $('.chosen-select.childCategory').chosen({"width": "90%"});
                                        });
                                    }
                                });
                                $timeout(function ()
                                {
                                    if ($scope.category) {
                                        $(".chosen-select.categoryDivider3 option[value='" + $scope.category._id + "']").attr('selected', 'selected');
                                    }
                                    $(".chosen-select.categoryDivider3").trigger("chosen:updated");

                                    //chosen init all subcategories which are labelled
                                    if ($scope.models && $scope.models.lists) {
                                        for (var i = 1; i <= $scope.models.lists["A"].length; i++) {
                                            $("li[identity='" + i + "'] .chosen-select").chosen({width: "90%"});
                                            if ($scope.subCategories && $scope.subCategories[i - 1]) {
                                                //check for order and show selected subcategories if exists
//                                                for (var j = 0; j < $scope.subCategories.length; j++) {
                                                $("li[identity='" + i + "'] .chosen-select option[value='" + $scope.subCategories[i - 1]._id + "']").attr('selected', 'selected');
                                                $("li[identity='" + i + "'] .chosen-select").trigger("chosen:updated");
//                                                }
                                            }
                                        }
                                    }

                                });
                                imageUploaderInit(configObject, '#bannerImages3');
                            });
                }
            });

    //load product types for each product
    $http.get('/relations/getProductTypesByMainCategory?cat_slug=' + $scope.selectedCategorySlug)
            .then(function (response) {
                if (response.data.error == 0) {
                    $scope.productTypes = response.data.data;
                } else {
                    $scope.error = "No product types for the selected category";
                }
                $timeout(function () {
                    //$scope.$apply();

                    /* Initialise all chosen selects*/

                    $('.chosen-select.catId__1').chosen({width: "100%"});
                    $('.chosen-select.catId__2').chosen({width: "100%"});
                    $('.chosen-select.catId__3').chosen({width: "100%"});
                    $('.chosen-select.catId__4').chosen({width: "100%"});

                    $('.chosen-select.supId__1').chosen({width: "100%"});
                    $('.chosen-select.supId__2').chosen({width: "100%"});
                    $('.chosen-select.supId__3').chosen({width: "100%"});
                    $('.chosen-select.supId__4').chosen({width: "100%"});


                    $('.chosen-select.prodType__1').chosen({width: "100%"}).change(function (event, params) {
                        loadCatalogueIds($scope, params.selected, $http, 1);
                    });
                    $('.chosen-select.prodType__2').chosen({width: "100%"}).change(function (event, params) {
                        loadCatalogueIds($scope, params.selected, $http, 2);
                    });
                    $('.chosen-select.prodType__3').chosen({width: "100%"}).change(function (event, params) {
                        loadCatalogueIds($scope, params.selected, $http, 3);
                    });
                    $('.chosen-select.prodType__4').chosen({width: "100%"}).change(function (event, params) {
                        loadCatalogueIds($scope, params.selected, $http, 4);
                    });
                });
            });

    //save data
    $scope.submitDivider = function () {
        var data = {
            "parentCategory.slug": $scope.parentCategory.slug,
            dividerNumber: 3,
            isVisible: $scope.isVisible
        };
        /* Validations */
        //perform validations only if visiblity set to true
        if ($scope.isVisible == true) {

            //check if sectional category selected
            if ($(".chosen-select.categoryDivider3 option:selected").val() == "Select") {
                $scope.error = "Please select the category for this divider section";
                return;
            }

            //check if banner images are uploaded. Atleast 1
            if (!$scope.sortedImages || $scope.sortedImages.length == 0) {
                $scope.error = "No images uploaded for banner";
                return;
            }
            //check if URL's are valid
            $('[data-name="urlForImage"]').each(function (index) {
                if ($(this).val() == "" || isURL($(this).val()) == false) {
                    $scope.error = "Please enter valid URLs for all images";
                    return;
                }
            });

            //check if all 4 products are selected. 
            if ($scope.selectedProducts.length != 4) {
                $scope.error = "Please select all 4 products for this promotional page";
                return;
            }

        }
        /* No errors. Fomulate data object to be inserted */
        data["products"] = $scope.selectedProducts;
        console.log("$scope.sortedImages");
        console.log($scope.sortedImages);
        data["bannerImages"] = [];
        for (var i = 0; i < $scope.sortedImages.length; i++) {
            data["bannerImages"].push({
                name: $scope.sortedImages[i].caption,
                order: i,
                url: $scope.sortedImages[i].url
            });
        }
//        data["bannerImages"] = $scope.sortedImages;
        var order = [];
        $('ul[identifier="childrenSubcategoryList"] li[eachPromotionalSubcatLi]').each(function (index) {
            order.push($(this).attr('identity'));
        });

        data["subCategories"] = [];
        for (var i = 1; i <= $scope.models.lists["A"].length; i++) {
            data.subCategories.push({
                _id: $('#subCategories__' + i + ' option:selected').val(),
                name: $('#subCategories__' + i + ' option:selected').attr('data-name'),
                slug: $('#subCategories__' + i + ' option:selected').attr('data-slug'),
                order: order.indexOf($('#subCategories__' + i).parents('li').attr('identity'))
            });
        }
        data["category"] = {
            _id: $(".chosen-select.categoryDivider3 option:selected").val(),
            name: $(".chosen-select.categoryDivider3 option:selected").attr('data-name'),
            slug: $(".chosen-select.categoryDivider3 option:selected").attr('data-slug')
        };
        //ajax post submit
        $http.post('/promotions/category/insert', data, {})
                .success(function (data, status, headers, config) {
                    console.log('success add - ', data);
                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

//                $timeout(function () {
//                    $location.path('/cate');
//                }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error add - ', data);
                    $scope.error = 'Unable to add brand';
                });

    };

    //image uploader init
    function imageUploaderInit(configObject, imageDivId) {
        $(imageDivId).fileinput(configObject)
                .on('fileuploaded', function (event, data, previewId, index) {
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    $scope.serverImages = data.response[0];
                    configObject.initialPreviewConfig.push({
                        caption: data.response[0].originalname,
                        size: data.response[0].size,
                        width: "120px",
//                        key: data.response[i].key
                        key: data.response[i].filename
                    });
                    configObject.initialPreview.push(data.response[0].location);
                    $scope.sortedImages.push({
//                        caption: data.response[0].key.split('/')[2]
                        caption: data.response[0].filename
                    });
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filebatchuploadsuccess', function (event, data) {
                    //data.response is response from server
                    $scope.serverImages = data.response;
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    for (var i = 0; i < data.response.length; i++) {
                        configObject.initialPreviewConfig.push({
                            caption: data.response[i].originalname,
                            size: data.response[i].size,
                            width: "120px",
                            key: data.response[i].key
//                            key: data.response[i].filename
                        });
                        configObject.initialPreview.push(data.response[i].location);
//                        configObject.initialPreview.push(data.response[i].path);
                        $scope.sortedImages.push({
                            caption: data.response[i].key.split('/')[2]
//                            caption: data.response[i].filename
                        });
                    }
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filesorted', function (event, params) {
                    var oldImages = [];
                    for (var j = 0; j < $scope.sortedImages.length; j++) {
                        oldImages.push($scope.sortedImages[j]);
                    }
                    $scope.sortedImages = [];
                    for (var i = 0; i < params.stack.length; i++) {
                        $.each(oldImages, function (index) {
                            //use regex to see if uploaded image(without timestamp) matches the sorted current image
                            var regex = new RegExp("^" + params.stack[i].caption.split('.')[0] + "-[0-9]{12,20}\.[a-z]{2,4}$")
                            if (params.stack[i].caption == oldImages[index].caption || regex.test(oldImages[index].caption)) {
                                $scope.sortedImages.push({
                                    caption: oldImages[index].caption,
                                    url: oldImages[index].url
                                });
                            }
                        });
                    }
                    //update url set order  
                    $timeout(function () {
                        $scope.$apply();
                    });
                })
                .on('filedeleted', function (event, key) {
                    $scope.sortedImages.splice(key - 1, 1);
                    for (var i = 0; i < configObject.initialPreviewConfig.length; i++) {
                        if (configObject.initialPreviewConfig[i].key == key) {
                            configObject.initialPreviewConfig.splice(i, 1);
                            configObject.initialPreview.splice(i, 1);
                        }
                    }
                    $scope.$apply();
                });
    }

    //extract all children
    function extractAllChildren(relationsObject) {
        var allChildren = [];
        for (var i = 0; i < relationsObject.children.length; i++) {
            allChildren.push({
                _id: relationsObject.children[i]._id,
                name: relationsObject.children[i].name,
                slug: relationsObject.children[i].slug,
                isProductType: relationsObject.children[i].isProductType
            });
            if (relationsObject.children[i].children) {
                for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                    allChildren.push({
                        _id: relationsObject.children[i].children[j]._id,
                        name: relationsObject.children[i].children[j].name,
                        slug: relationsObject.children[i].children[j].slug,
                        isProductType: relationsObject.children[i].children[j].isProductType
                    });
                    if (relationsObject.children[i].children[j].children) {
                        for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                            allChildren.push({
                                _id: relationsObject.children[i].children[j].children[k]._id,
                                name: relationsObject.children[i].children[j].children[k].name,
                                slug: relationsObject.children[i].children[j].children[k].slug,
                                isProductType: relationsObject.children[i].children[j].children[k].isProductType
                            });
                        }
                    }
                }
            }
        }
        return allChildren;
    }
    ;


    //add product(catalogue) ids into dropdowns
    function loadCatalogueIds($scope, selectedProductType, $http, index, isParamsSelected = true) {
        var variantData;
//        $scope.catalogueIds[index] = [];
        if (!$scope.catalogueIds) {
            $scope.catalogueIds = [];
        }
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = []
        }
        $scope.supplierInfo[index] = [];
        $http.get('/product/getCatIdsByProductType?prodTypeId=' + selectedProductType)
                .then(function (responseCatIds) {
                    $scope.catalogueIds[index] = [];
                    if (responseCatIds.data.error == 0) {
                        variantData = responseCatIds.data.data;
                        for (var i = 0; i < responseCatIds.data.data.length; i++) {
                            for (var j = 0; j < responseCatIds.data.data[i].variants.length; j++) {
                                $scope.catalogueIds[index].push({
                                    _id: responseCatIds.data.data[i].variants[j].catalogueId
                                });
                            }
                        }
                    } else {
                        $scope.error = "No catalogue ids under this product type.";
                    }
                    $timeout(function () {
                        $scope.$apply();
                        //update supplier sku list of current indexed selected product
                        if($scope.selectedProducts[index-1] && isParamsSelected == false){
                            //show selected product type ids
                            $('.chosen-select.prodType__' + index + ' option[value="' + $scope.selectedProducts[index-1].product_type_id + '"]').attr('selected', 'selected');
                            $('.chosen-select.prodType__' + index).trigger("chosen:updated");
                            //show selected catalogue ids
                            $('.chosen-select.catId__' + index +' option[value="'+$scope.selectedProducts[index-1].catalogue_id+'"]').attr('selected', 'selected');
                            $('.chosen-select.catId__' + index).trigger('chosen:updated');
                            loadSupplierIds($scope, $scope.selectedProducts[index-1].catalogue_id, variantData, index, false);
                        }
                        //update supplier sku list of current indexed selected product
                        $('.chosen-select.supId__' + index).trigger("chosen:updated");
                        $('.chosen-select.catId__' + index).trigger('chosen:updated');
                        $('.chosen-select.catId__' + index).chosen({width: "100%"}).change(function (event, params) {
                            loadSupplierIds($scope, params.selected, variantData, index);
                        });
                    });
                });
    }

    //add supplier SKU to supplier dropdown
    function loadSupplierIds($scope, selectedCatalogueId, variantData, index, isParamsSelected = true) {
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = [];
        }
        $scope.supplierInfo[index] = [];
        for (var i = 0; i < variantData.length; i++) {
            for (var j = 0; j < variantData[i].variants.length; j++) {
                if (variantData[i].variants[j].catalogueId == selectedCatalogueId) {
                    for (var k = 0; k < variantData[i].variants[j].suppliers.length; k++) {
                        $scope.supplierInfo[index].push({
                            sku: variantData[i].variants[j].suppliers[k].sku
                        });
                    }
                }
            }
        }
        $timeout(function () {
            $scope.$apply();
            $('.chosen-select.supId__' + index).chosen({width: "100%"}).trigger("chosen:updated").change(function (event, params) {
//                if(!$scope.selectedProducts[index]) {
//                $scope.selectedProducts[index - 1] = {}
//                }
                $scope.selectedProducts[index - 1] = {
                    product_type_id: $('.chosen-select.prodType__' + index + ' option:selected').val(),
                    catalogue_id: $('.chosen-select.catId__' + index + ' option:selected').val(),
                    supplier_id: params.selected
                };
            });
            if($scope.selectedProducts[index-1] && $scope.selectedProducts[index-1].supplier_id && isParamsSelected == false){
                $('.chosen-select.supId__' + index+' option[value="'+$scope.selectedProducts[index-1].supplier_id+'"]').attr('selected', 'selected');
                $('.chosen-select.supId__' + index).trigger("chosen:updated");
            }
        });
    }

    //extract children for selected category id
    function extractAllChildrenWithCondition(relationsObject, category_id) {
        var returnArray = [];
        for (var i = 0; i < relationsObject.children.length; i++) {
            //found in first column
            if (relationsObject.children[i]._id == category_id) {
                //add all child categories from second column
                for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                    returnArray.push({
                        _id: relationsObject.children[i].children[j]._id,
                        name: relationsObject.children[i].children[j].name,
                        slug: relationsObject.children[i].children[j].slug
                    });
                    //add all child categories from second column
                    for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                        returnArray.push({
                            _id: relationsObject.children[i].children[j].children[k]._id,
                            name: relationsObject.children[i].children[j].children[k].name,
                            slug: relationsObject.children[i].children[j].children[k].slug
                        });
                    }
                }
            }
            /*also look in second column for matches of selected category 
             since category maybe attached at multiple places */
//            else {
            for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                //if match found in second column
                if (relationsObject.children[i].children[j]._id == category_id) {
                    //add all child categories from column 3
                    for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                        returnArray.push({
                            _id: relationsObject.children[i].children[j].children[k]._id,
                            name: relationsObject.children[i].children[j].children[k].name,
                            slug: relationsObject.children[i].children[j].children[k].slug
                        });
                    }
                }
            }
//            }
        }
        return returnArray;
    }

});
app.controller('CategoryPromotionsTab4Controller', function ($scope, $http, $timeout, $location) {

    $scope.selectedCategorySlug = (window.location.href.split('#')[1]).split('/')[3];
    $scope.selectedProducts = [];
    //Image initialisation
    var configObject = {
        initialPreviewAsData: true,
        uploadUrl: "/promotions/category/uploadImage/",
        deleteUrl: "/promotions/deleteImage/",
        uploadAsync: false,
        previewSettings: {image: {width: "120px", height: "40px"}},
        overwriteInitial: false,
        allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],
        maxFileSize: 3000,
        fileActionSettings: {
            showUpload: false
        }
    };

    //toggle divider visiblity
    $scope.toggleVisiblity = function () {
        $scope.isVisible = !$scope.isVisible;
    };

    $scope.reInitThisChosen = function (item) {
        var $selectedOption = $("li[identity='" + item.label + "'] select option:selected");
        $("li[identity='" + item.label + "'] select option[value='" + $selectedOption.val() + "']").attr('selected', 'selected');
        $("li[identity='" + item.label + "'] .chosen-select").chosen({width: "90%"});
    };

    //get existing data for selected category
    $http.get('/promotions/category/get?cat_slug=' + $scope.selectedCategorySlug)
            .then(function (response) {
                if (response.data.data.length == 0) {
                    $scope.promotionsError = "No active  main categories found";
                    $(".error-modal-lg").modal();
                    return;
                } else {
//                    $scope.category = response.data.data[0].category;
                    $scope.parentCategory = response.data.data[0].category;

                    /* Code for page edit i.e viewing page after saving it earlier */
                    for (var i = 0; i < response.data.data[0].dividers.length; i++) {
                        if (response.data.data[0].dividers[i].dividerNumber == 4) {
                            $scope.isVisible = response.data.data[0].dividers[i].isVisible;
                            $scope.selectedProducts = response.data.data[0].dividers[i].products;
                            $scope.subCategories = response.data.data[0].dividers[i].sub_categories;
                            //selected sub category for this divider section
                            $scope.category = response.data.data[0].dividers[i].category;
                            //category whose promotion page is being edited/created
                            if (!configObject.initialPreviewConfig) {
                                configObject.initialPreviewConfig = [];
                            }
                            if (!configObject.initialPreview) {
                                configObject.initialPreview = [];
                            }
                            if (!$scope.sortedImages) {
                                $scope.sortedImages = [];
                            }

                            /* Show banner images if already selected */
                            for (var j = 0; j < response.data.data[0].dividers[i].bannerImages.length; j++) {
                                configObject.initialPreviewConfig.push({
                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                    width: "120px",
                                    key: "promotions/category/" + response.data.data[0].dividers[i].bannerImages[j].name
                                });
                                var imageUrl = s3_bucket + "promotions/category/" + response.data.data[0].dividers[i].bannerImages[j].name;
                                configObject.initialPreview.push(imageUrl);
                                $scope.sortedImages.push({
                                    caption: response.data.data[0].dividers[i].bannerImages[j].name,
                                    url: response.data.data[0].dividers[i].bannerImages[j].url
                                });
                            }

                            /* For selected products */
                            if (!$scope.catalogueIds) {
                                $scope.catalogueIds = [];
                            }
                            if (!$scope.supplierInfo) {
                                $scope.supplierInfo = [];
                            }
                            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                                loadCatalogueIds($scope, $scope.selectedProducts[i].product_type_id, $http, i+1, false);
                            }

                            /* for each product initialise*/
                            //Product 1
//                            $http.get('/product/getCatIdsByProductType?prodTypeId=' + $scope.selectedProducts[0].product_type_id)
//                                    .then(function (responseCatIds) {
//                                        $scope.catalogueIds[1] = [];
//                                        if (responseCatIds.data.error == 0) {
//                                            var variantData = responseCatIds.data.data;
//                                            for (var l = 0; l < responseCatIds.data.data.length; l++) {
//                                                for (var j = 0; j < responseCatIds.data.data[l].variants.length; j++) {
//                                                    $scope.catalogueIds[1].push({
//                                                        _id: responseCatIds.data.data[l].variants[j].catalogueId
//                                                    });
//                                                }
//                                            }
//                                        } else {
//                                            $scope.error = responseCatIds.data.msg;
//                                        }
//                                        $('.chosen-select.prodType__' + 1 + ' option[value="' + $scope.selectedProducts[0].product_type_id + '"]').attr('selected', 'selected');
//                                        $('.chosen-select.prodType__' + 1).trigger("chosen:updated");
//                                        //update supplier sku list of current indexed selected product
//                                        $('.chosen-select.catId__' + 1).trigger('chosen:updated');
//                                        $('.chosen-select.supId__' + 1).trigger("chosen:updated");
//                                        loadSupplierIds($scope, $scope.selectedProducts[0].catalogue_id, variantData, 1);
//                                    });
//
//                            //Product 2
//                            $http.get('/product/getCatIdsByProductType?prodTypeId=' + $scope.selectedProducts[1].product_type_id)
//                                    .then(function (responseCatIds) {
//                                        $scope.catalogueIds[2] = [];
//                                        if (responseCatIds.data.error == 0) {
//                                            var variantData = responseCatIds.data.data;
//                                            for (var l = 0; l < responseCatIds.data.data.length; l++) {
//                                                for (var j = 0; j < responseCatIds.data.data[l].variants.length; j++) {
//                                                    $scope.catalogueIds[2].push({
//                                                        _id: responseCatIds.data.data[l].variants[j].catalogueId
//                                                    });
//                                                }
//                                            }
//                                        } else {
//                                            $scope.error = responseCatIds.data.msg;
//                                        }
//                                        $('.chosen-select.prodType__' + 2 + ' option[value="' + $scope.selectedProducts[1].product_type_id + '"]').attr('selected', 'selected');
//                                        $('.chosen-select.prodType__' + 2).trigger("chosen:updated");
//                                        //update supplier sku list of current indexed selected product
//                                        $('.chosen-select.supId__' + 2).trigger("chosen:updated");
//                                        $('.chosen-select.catId__' + 2).trigger('chosen:updated');
//                                        loadSupplierIds($scope, $scope.selectedProducts[1].catalogue_id, variantData, 2);
//                                    });
//                            //Product 3
//
//                            $http.get('/product/getCatIdsByProductType?prodTypeId=' + $scope.selectedProducts[2].product_type_id)
//                                    .then(function (responseCatIds) {
//                                        $scope.catalogueIds[3] = [];
//                                        if (responseCatIds.data.error == 0) {
//                                            var variantData = responseCatIds.data.data;
//                                            for (var l = 0; l < responseCatIds.data.data.length; l++) {
//                                                for (var j = 0; j < responseCatIds.data.data[l].variants.length; j++) {
//                                                    $scope.catalogueIds[3].push({
//                                                        _id: responseCatIds.data.data[l].variants[j].catalogueId
//                                                    });
//                                                }
//                                            }
//                                        } else {
//                                            $scope.error = responseCatIds.data.msg;
//                                        }
//                                        $('.chosen-select.prodType__' + 3 + ' option[value="' + $scope.selectedProducts[2].product_type_id + '"]').attr('selected', 'selected');
//                                        $('.chosen-select.prodType__' + 3).trigger("chosen:updated");
//                                        //update supplier sku list of current indexed selected product
//                                        $('.chosen-select.supId__' + 3).trigger("chosen:updated");
//                                        $('.chosen-select.catId__' + 3).trigger('chosen:updated');
//                                        loadSupplierIds($scope, $scope.selectedProducts[2].catalogue_id, variantData, 3);
//                                    });
//
//                            //Product 4
//                            $http.get('/product/getCatIdsByProductType?prodTypeId=' + $scope.selectedProducts[3].product_type_id)
//                                    .then(function (responseCatIds) {
//                                        $scope.catalogueIds[4] = [];
//                                        if (responseCatIds.data.error == 0) {
//                                            var variantData = responseCatIds.data.data;
//                                            for (var l = 0; l < responseCatIds.data.data.length; l++) {
//                                                for (var j = 0; j < responseCatIds.data.data[l].variants.length; j++) {
//                                                    $scope.catalogueIds[4].push({
//                                                        _id: responseCatIds.data.data[l].variants[j].catalogueId
//                                                    });
//                                                }
//                                            }
//                                        } else {
//                                            $scope.error = responseCatIds.data.msg;
//                                        }
//                                        $('.chosen-select.prodType__' + 4 + ' option[value="' + $scope.selectedProducts[3].product_type_id + '"]').attr('selected', 'selected');
//                                        $('.chosen-select.prodType__' + 4).trigger("chosen:updated");
//                                        //update supplier sku list of current indexed selected product
//                                        $('.chosen-select.supId__' + 4).trigger("chosen:updated");
//                                        $('.chosen-select.catId__' + 4).trigger('chosen:updated');
//                                        loadSupplierIds($scope, $scope.selectedProducts[3].catalogue_id, variantData, 4);
//                                    });
                        }
                    }
                    $http.get('/relations/getById?catId=' + $scope.parentCategory._id)
                            .then(function (response) {
                                var relationsObject = response.data[0];
                                $scope.allChildren = extractAllChildren(response.data[0]);

                                //check if category is already selected previously i.e page is being edited and not created
                                if ($scope.category) {
                                    var noChildren = false;
                                    for (var i = 0; i < $scope.allChildren.length; i++) {
                                        if ($scope.allChildren[i]._id == $scope.category._id && $scope.allChildren[i].isProductType == true) {
                                            $(".child-categories .error").html("Selected category is a product type, hence no child categories");
                                            $(".child-categories .error").css('display', 'block');
                                            noChildren = true;
                                            return;
                                        }
                                    }
                                    if (noChildren === false) {
                                        $(".child-categories .error").css('display', 'none');
                                        $scope.thisCategoryChildren = extractAllChildrenWithCondition(relationsObject, $scope.category._id);
                                        $scope.models = {
                                            selected: null,
                                            lists: {"A": []}
                                        };
                                        for (var i = 1; i <= $scope.thisCategoryChildren.length; i++) {
                                            $scope.models.lists["A"].push({
                                                label: i
                                            });
                                        }
                                    }

                                }
                                //on change current sectional category
                                $(".chosen-select.categoryDivider3").chosen({width: "100%"}).change(function (event, params) {
                                    var noChildren = false;
                                    for (var i = 0; i < $scope.allChildren.length; i++) {
                                        if ($scope.allChildren[i]._id == params.selected && $scope.allChildren[i].isProductType == true) {
                                            $(".child-categories .error").html("Selected category is a product type, hence no child categories");
                                            $(".child-categories .error").css('display', 'block');
                                            noChildren = true;
                                            return;
                                        }
                                    }
                                    if (noChildren === false) {
                                        $(".child-categories .error").css('display', 'none');
                                        $scope.thisCategoryChildren = extractAllChildrenWithCondition(relationsObject, params.selected);
                                        $scope.models = {
                                            selected: null,
                                            lists: {"A": []}
                                        };
                                        var max;
                                        max = $scope.thisCategoryChildren.length < 9 ? $scope.thisCategoryChildren.length : 9;                                        
                                        for (var i = 1; i <= max; i++) {
                                            $scope.models.lists["A"].push({
                                                label: i
                                            });
                                        }
                                        $timeout(function () {
                                            //apply scope value to view
                                            $scope.$apply();
                                            $('.chosen-select.childCategory').chosen({"width": "90%"});
                                        });
                                    }
                                });
                                $timeout(function ()
                                {
                                    if ($scope.category) {
                                        $(".chosen-select.categoryDivider3 option[value='" + $scope.category._id + "']").attr('selected', 'selected');
                                    }
                                    $(".chosen-select.categoryDivider3").trigger("chosen:updated");

                                    //chosen init all subcategories which are labelled
                                    if ($scope.models && $scope.models.lists) {
                                        for (var i = 1; i <= $scope.models.lists["A"].length; i++) {
                                            $("li[identity='" + i + "'] .chosen-select").chosen({width: "90%"});
                                            if ($scope.subCategories) {
                                                //check for order and show selected subcategories if exists
//                                                for (var j = 0; j < $scope.subCategories.length; j++) {
                                                $("li[identity='" + i + "'] .chosen-select option[value='" + $scope.subCategories[i - 1]._id + "']").attr('selected', 'selected');
                                                $("li[identity='" + i + "'] .chosen-select").trigger("chosen:updated");
//                                                }
                                            }
                                        }
                                    }

                                });
                                imageUploaderInit(configObject, '#bannerImages3');
                            });
                }
            });

    //load product types for each product
    $http.get('/relations/getProductTypesByMainCategory?cat_slug=' + $scope.selectedCategorySlug)
            .then(function (response) {
                if (response.data.error == 0) {
                    $scope.productTypes = response.data.data;
                } else {
                    $scope.error = "No product types for the selected category";
                }
                $timeout(function () {
                    //$scope.$apply();

                    /* Initialise all chosen selects*/

                    $('.chosen-select.catId__1').chosen({width: "100%"});
                    $('.chosen-select.catId__2').chosen({width: "100%"});
                    $('.chosen-select.catId__3').chosen({width: "100%"});
                    $('.chosen-select.catId__4').chosen({width: "100%"});

                    $('.chosen-select.supId__1').chosen({width: "100%"});
                    $('.chosen-select.supId__2').chosen({width: "100%"});
                    $('.chosen-select.supId__3').chosen({width: "100%"});
                    $('.chosen-select.supId__4').chosen({width: "100%"});


                    $('.chosen-select.prodType__1').chosen({width: "100%"}).change(function (event, params) {
                        loadCatalogueIds($scope, params.selected, $http, 1);
                    });
                    $('.chosen-select.prodType__2').chosen({width: "100%"}).change(function (event, params) {
                        loadCatalogueIds($scope, params.selected, $http, 2);
                    });
                    $('.chosen-select.prodType__3').chosen({width: "100%"}).change(function (event, params) {
                        loadCatalogueIds($scope, params.selected, $http, 3);
                    });
                    $('.chosen-select.prodType__4').chosen({width: "100%"}).change(function (event, params) {
                        loadCatalogueIds($scope, params.selected, $http, 4);
                    });
                });
            });

    //save data
    $scope.submitDivider = function () {
        $scope.error = "";
        var data = {
            "parentCategory.slug": $scope.parentCategory.slug,
            dividerNumber: 4,
            isVisible: $scope.isVisible
        };
        /* Validations */
        //perform validations only if visiblity set to true
        if ($scope.isVisible == true) {

            //check if sectional category selected
            if ($(".chosen-select.categoryDivider3 option:selected").val() == "Select") {
                $scope.error = "Please select the category for this divider section";
                return;
            }

            //check if banner images are uploaded. Atleast 1
            if (!$scope.sortedImages || $scope.sortedImages.length == 0) {
                $scope.error = "No images uploaded for banner";
                return;
            }
            //check if URL's are valid
            $('[data-name="urlForImage"]').each(function (index) {
                if ($(this).val() == "" || isURL($(this).val()) == false) {
                    $scope.error = "Please enter valid URLs for all images";
                    return;
                }
            });

            //check if all 4 products are selected. 
            if ($scope.selectedProducts.length != 4) {
                $scope.error = "Please select all 4 products for this promotional page";
                return;
            }

        }
        /* No errors. Fomulate data object to be inserted */
        data["products"] = $scope.selectedProducts;
        data["bannerImages"] = [];
        for (var i = 0; i < $scope.sortedImages.length; i++) {
            data["bannerImages"].push({
                name: $scope.sortedImages[i].caption,
                order: i,
                url: $scope.sortedImages[i].url
            });
        }
        var order = [];
        $('ul[identifier="childrenSubcategoryList"] li[eachPromotionalSubcatLi]').each(function (index) {
            order.push($(this).attr('identity'));
        });

        data["subCategories"] = [];
        if($scope.models){
            for (var i = 1; i <= $scope.models.lists["A"].length; i++) {
                data.subCategories.push({
                    _id: $('#subCategories__' + i + ' option:selected').val(),
                    name: $('#subCategories__' + i + ' option:selected').attr('data-name'),
                    slug: $('#subCategories__' + i + ' option:selected').attr('data-slug'),
                    order: order.indexOf($('#subCategories__' + i).parents('li').attr('identity'))
                });
            }
        }
        
        data["category"] = {
            _id: $(".chosen-select.categoryDivider3 option:selected").val(),
            name: $(".chosen-select.categoryDivider3 option:selected").attr('data-name'),
            slug: $(".chosen-select.categoryDivider3 option:selected").attr('data-slug')
        };
        //ajax post submit
        if($scope.error == ""){
            $http.post('/promotions/category/insert', data, {})
                .success(function (data, status, headers, config) {
                    console.log('success add - ', data);
                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    $scope.success = data.msg;

//                $timeout(function () {
//                    $location.path('/cate');
//                }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error add - ', data);
                    $scope.error = 'Unable to add brand';
                });
        }

    };

    //image uploader init
    function imageUploaderInit(configObject, imageDivId) {
        $(imageDivId).fileinput(configObject)
                .on('fileuploaded', function (event, data, previewId, index) {
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    $scope.serverImages = data.response[0];
                    configObject.initialPreviewConfig.push({
                        caption: data.response[0].originalname,
                        size: data.response[0].size,
                        width: "120px",
                        key: data.response[i].key
//                        key: data.response[i].filename
                    });
                    configObject.initialPreview.push(data.response[0].location);
                    $scope.sortedImages.push({
                        caption: data.response[0].key.split('/')[2]
//                        caption: data.response[0].filename
                    });
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filebatchuploadsuccess', function (event, data) {
                    //data.response is response from server
                    $scope.serverImages = data.response;
                    if (!$scope.sortedImages) {
                        $scope.sortedImages = [];
                    }
                    if (!configObject.initialPreviewConfig) {
                        configObject.initialPreviewConfig = [];
                    }
                    if (!configObject.initialPreview) {
                        configObject.initialPreview = [];
                    }
                    for (var i = 0; i < data.response.length; i++) {
                        configObject.initialPreviewConfig.push({
                            caption: data.response[i].originalname,
                            size: data.response[i].size,
                            width: "120px",
                            key: data.response[i].key
//                            key: data.response[i].filename
                        });
                        configObject.initialPreview.push(data.response[i].location);
//                        configObject.initialPreview.push(data.response[i].path);
                        $scope.sortedImages.push({
                            caption: data.response[i].key.split('/')[2]
//                            caption: data.response[i].filename
                        });
                    }
                    $timeout(function () {
                        $(imageDivId).fileinput('refresh', configObject);
                    });
                })
                .on('filesorted', function (event, params) {
                    var oldImages = [];
                    for (var j = 0; j < $scope.sortedImages.length; j++) {
                        oldImages.push($scope.sortedImages[j]);
                    }
                    $scope.sortedImages = [];
                    for (var i = 0; i < params.stack.length; i++) {
                        $.each(oldImages, function (index) {
                            //use regex to see if uploaded image(without timestamp) matches the sorted current image
                            var regex = new RegExp("^" + params.stack[i].caption.split('.')[0] + "-[0-9]{12,20}\.[a-z]{2,4}$")
                            if (params.stack[i].caption == oldImages[index].caption || regex.test(oldImages[index].caption)) {
                                $scope.sortedImages.push({
                                    caption: oldImages[index].caption,
                                    url: oldImages[index].url
                                });
                            }
                        });
                    }
                    //update url set order  
                    $timeout(function () {
                        $scope.$apply();
                    });
                })
                .on('filedeleted', function (event, key) {
                    $scope.sortedImages.splice(key - 1, 1);
                    for (var i = 0; i < configObject.initialPreviewConfig.length; i++) {
                        if (configObject.initialPreviewConfig[i].key == key) {
                            configObject.initialPreviewConfig.splice(i, 1);
                            configObject.initialPreview.splice(i, 1);
                        }
                    }
                    $scope.$apply();
                });
    }

    //extract all children
    function extractAllChildren(relationsObject) {
        var allChildren = [];
        for (var i = 0; i < relationsObject.children.length; i++) {
            allChildren.push({
                _id: relationsObject.children[i]._id,
                name: relationsObject.children[i].name,
                slug: relationsObject.children[i].slug,
                isProductType: relationsObject.children[i].isProductType
            });
            if (relationsObject.children[i].children) {
                for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                    allChildren.push({
                        _id: relationsObject.children[i].children[j]._id,
                        name: relationsObject.children[i].children[j].name,
                        slug: relationsObject.children[i].children[j].slug,
                        isProductType: relationsObject.children[i].children[j].isProductType
                    });
                    if (relationsObject.children[i].children[j].children) {
                        for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                            allChildren.push({
                                _id: relationsObject.children[i].children[j].children[k]._id,
                                name: relationsObject.children[i].children[j].children[k].name,
                                slug: relationsObject.children[i].children[j].children[k].slug,
                                isProductType: relationsObject.children[i].children[j].children[k].isProductType
                            });
                        }
                    }
                }
            }
        }
        return allChildren;
    }
    ;


    //add product(catalogue) ids into dropdowns
    function loadCatalogueIds($scope, selectedProductType, $http, index, isParamsSelected = true) {
        var variantData;
//        $scope.catalogueIds[index] = [];
        if (!$scope.catalogueIds) {
            $scope.catalogueIds = [];
        }
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = []
        }
        $scope.supplierInfo[index] = [];
        $http.get('/product/getCatIdsByProductType?prodTypeId=' + selectedProductType)
                .then(function (responseCatIds) {
                    $scope.catalogueIds[index] = [];
                    if (responseCatIds.data.error == 0) {
                        variantData = responseCatIds.data.data;
                        for (var i = 0; i < responseCatIds.data.data.length; i++) {
                            for (var j = 0; j < responseCatIds.data.data[i].variants.length; j++) {
                                $scope.catalogueIds[index].push({
                                    _id: responseCatIds.data.data[i].variants[j].catalogueId
                                });
                            }
                        }
                    } else {
                        $scope.error = "No catalogue ids under this product type.";
                    }
                    $timeout(function () {
                        $scope.$apply();
                        //update supplier sku list of current indexed selected product
                        if($scope.selectedProducts[index-1] && isParamsSelected == false){
                            //show selected product type ids
                            $('.chosen-select.prodType__' + index + ' option[value="' + $scope.selectedProducts[index-1].product_type_id + '"]').attr('selected', 'selected');
                            $('.chosen-select.prodType__' + index).trigger("chosen:updated");
                            //show selected catalogue ids
                            $('.chosen-select.catId__' + index +' option[value="'+$scope.selectedProducts[index-1].catalogue_id+'"]').attr('selected', 'selected');
                            $('.chosen-select.catId__' + index).trigger('chosen:updated');
                            loadSupplierIds($scope, $scope.selectedProducts[index-1].catalogue_id, variantData, index, false);
                        }
                        //update supplier sku list of current indexed selected product
                        $('.chosen-select.supId__' + index).trigger("chosen:updated");
                        $('.chosen-select.catId__' + index).trigger('chosen:updated');
                        $('.chosen-select.catId__' + index).chosen({width: "100%"}).change(function (event, params) {
                            loadSupplierIds($scope, params.selected, variantData, index);
                        });
                    });
                });
    }

    //add supplier SKU to supplier dropdown
    function loadSupplierIds($scope, selectedCatalogueId, variantData, index, isParamsSelected = true) {
        if (!$scope.supplierInfo) {
            $scope.supplierInfo = [];
        }
        $scope.supplierInfo[index] = [];
        for (var i = 0; i < variantData.length; i++) {
            for (var j = 0; j < variantData[i].variants.length; j++) {
                if (variantData[i].variants[j].catalogueId == selectedCatalogueId) {
                    for (var k = 0; k < variantData[i].variants[j].suppliers.length; k++) {
                        $scope.supplierInfo[index].push({
                            sku: variantData[i].variants[j].suppliers[k].sku
                        });
                    }
                }
            }
        }
        $timeout(function () {
            $scope.$apply();
            $('.chosen-select.supId__' + index).chosen({width: "100%"}).trigger("chosen:updated").change(function (event, params) {
//                if(!$scope.selectedProducts[index]) {
//                $scope.selectedProducts[index - 1] = {}
//                }
                $scope.selectedProducts[index - 1] = {
                    product_type_id: $('.chosen-select.prodType__' + index + ' option:selected').val(),
                    catalogue_id: $('.chosen-select.catId__' + index + ' option:selected').val(),
                    supplier_id: params.selected
                };
            });
            if($scope.selectedProducts[index-1] && isParamsSelected == false){
                $('.chosen-select.supId__' + index+' option[value="'+$scope.selectedProducts[index-1].supplier_id+'"]').attr('selected', 'selected');
                $('.chosen-select.supId__' + index).trigger("chosen:updated");
            }
        });
    }

    //extract children for selected category id
    function extractAllChildrenWithCondition(relationsObject, category_id) {
        var returnArray = [];
        for (var i = 0; i < relationsObject.children.length; i++) {
            //found in first column
            if (relationsObject.children[i]._id == category_id) {
                //add all child categories from second column
                for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                    returnArray.push({
                        _id: relationsObject.children[i].children[j]._id,
                        name: relationsObject.children[i].children[j].name,
                        slug: relationsObject.children[i].children[j].slug
                    });
                    //add all child categories from second column
                    for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                        returnArray.push({
                            _id: relationsObject.children[i].children[j].children[k]._id,
                            name: relationsObject.children[i].children[j].children[k].name,
                            slug: relationsObject.children[i].children[j].children[k].slug
                        });
                    }
                }
            }
            /*also look in second column for matches of selected category 
             since category maybe attached at multiple places */
//            else {
            for (var j = 0; j < relationsObject.children[i].children.length; j++) {
                //if match found in second column
                if (relationsObject.children[i].children[j]._id == category_id) {
                    //add all child categories from column 3
                    for (var k = 0; k < relationsObject.children[i].children[j].children.length; k++) {
                        returnArray.push({
                            _id: relationsObject.children[i].children[j].children[k]._id,
                            name: relationsObject.children[i].children[j].children[k].name,
                            slug: relationsObject.children[i].children[j].children[k].slug
                        });
                    }
                }
            }
//            }
        }
        return returnArray;
    }

}

);
app.controller('CategoryPromotionsTab5Controller', function ($scope, $http, $timeout, $location) {
    $scope.dividerNumber = 5;
    $scope.dividerName = 'New Launches';

    $scope.selectedProducts = [];

    //toggle divider visiblity
    $scope.toggleVisiblity = function () {
        $scope.isVisible = !$scope.isVisible;
    };
    $scope.selectedCategorySlug = (window.location.href.split('#')[1]).split('/')[3];
    $http.get('/promotions/category/get?cat_slug=' + $scope.selectedCategorySlug)
            .then(function (response) {
                if (response.data.data.length == 0) {
                    $scope.promotionsError = "No active  main categories found";
                    $(".error-modal-lg").modal();
                    return;
                } else {
                    var promotionalData = response.data.data[0];
                    $scope.parentCategory = response.data.data[0].category;
                    for (var i = 0; i < promotionalData.dividers.length; i++) {
                        if (promotionalData.dividers[i].dividerNumber == 5) {
                            $scope.currentDividerInfo = promotionalData.dividers[i];
                            if ($scope.currentDividerInfo.dividerName) {
                                $scope.dividerName = $scope.currentDividerInfo.dividerName;
                            }
                            if (promotionalData.dividers[i].products && promotionalData.dividers[i].products.length > 0) {
                                $scope.selectedProducts = promotionalData.dividers[i].products;
                            }
                            $scope.isVisible = $scope.currentDividerInfo.isVisible;
                        }
                    }
                    $scope.mainCategories = response.data.data;
                    $scope.categoryName = response.data.data[0].category.name;
//                $scope.isVisible = (response.data.data[0].dividers).toString();
                }
            });

    $http.get('/relations/getProductTypesByMainCategory?cat_slug=' + $scope.selectedCategorySlug)
            .then(function (response) {
                if (response.data.error == 0) {
                    $scope.productTypes = response.data.data;
                } else {
                    $scope.error = "No product types for the selected category";
                }
                $timeout(function () {
                    //$scope.$apply();
                    $('.chosen-select.prodType').chosen({width: "100%"}).change(function (event, params) {
                        loadCatalogueIds($scope, params.selected, $http);
                    });
                });
            });

    //remove existing product
    $scope.removeExistingProduct = function (eachProduct) {
        console.log("eachProduct");
        console.log(eachProduct);
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            if ($scope.selectedProducts[i]._id == eachProduct._id)
            {
                $scope.selectedProducts.splice(i, 1);
//                $('.chosen-select.featuredBrands option[value="' + eachBrand._id + '"]').removeAttr("selected").trigger("chosen:updated");
            }
        }
        $timeout(function () {
            $scope.$apply();
        });
    }


    $scope.submitDivider = function () {
        var supplier_id = $(".chosen-select.supId option:selected").val();
        var catalogue_id = $(".chosen-select.catId option:selected").val();
        var product_type_id = $(".chosen-select.prodType option:selected").val();
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            if ($scope.selectedProducts[i].supplier_id == supplier_id && $scope.selectedProducts[i].catalogue_id == catalogue_id) {
                $scope.error = "Same product already exists. Please select another";
                return;
            }
        }
        if (supplier_id !== 'Select' && catalogue_id !== 'Select' && product_type_id !== 'Select') {
            $scope.selectedProducts.push({
                product_type_id: product_type_id,
                catalogue_id: catalogue_id,
                supplier_id: supplier_id
            });
        }
        var data = {
            "parentCategory.slug": $scope.parentCategory.slug,
            dividerNumber: 5,
            dividerName: $scope.dividerName,
            products: $scope.selectedProducts,
            isVisible: $scope.isVisible
        };
        $http.post('/promotions/category/insert', data, {})
                .success(function (data, status, headers, config) {
                    console.log('success add - ', data);
                    if (data.error == 1) {
                        $scope.error = data.msg;
                        return false;
                    }
                    console.log("data.msg");
                    console.log(data.msg);
                    $scope.success = data.msg;

//                $timeout(function () {
//                    $location.path('/cate');
//                }, 2000);
                })
                .error(function (data, status, header, config) {
                    console.log('error add - ', data);
                    $scope.error = 'Unable to add brand';
                });
    };

    function loadCatalogueIds($scope, selectedProductType, $http) {
        var variantData;
        $scope.catalogueIds = [];
        $scope.supplierInfo = [];
        $http.get('/product/getCatIdsByProductType?prodTypeId=' + selectedProductType)
                .then(function (responseCatIds) {
                    $scope.catalogueIds = [];
                    if (responseCatIds.data.error == 0) {
                        variantData = responseCatIds.data.data;
                        for (var i = 0; i < responseCatIds.data.data.length; i++) {
                            for (var j = 0; j < responseCatIds.data.data[i].variants.length; j++) {
                                $scope.catalogueIds.push({
                                    _id: responseCatIds.data.data[i].variants[j].catalogueId
                                });
                            }
                        }
                    } else {
                        $scope.error = "No catalogue ids under this product type.";
                    }
                    $timeout(function () {
                        $scope.$apply();
                        $('.chosen-select.supId').trigger("chosen:updated");
                        $('.chosen-select.catId').trigger('chosen:updated');
                        $('.chosen-select.catId').chosen({width: "100%"}).change(function (event, params) {
                            loadSupplierIds($scope, params.selected, $http, variantData);
                        });
                    });
                });
    }

    //add supplier SKU to supplier dropdown
    function loadSupplierIds($scope, selectedCatalogueId, $http, variantData) {
        $scope.supplierInfo = [];
        console.log("variantData");
        console.log(variantData);
        console.log(selectedCatalogueId);
        for (var i = 0; i < variantData.length; i++) {
            for (var j = 0; j < variantData[i].variants.length; j++) {
                if (variantData[i].variants[j].catalogueId == selectedCatalogueId) {
                    console.log("inside if else");
                    for (var k = 0; k < variantData[i].variants[j].suppliers.length; k++) {
                        $scope.supplierInfo.push({
                            sku: variantData[i].variants[j].suppliers[k].sku
                        });
                    }
                }
            }
        }
        $timeout(function () {
            $scope.$apply();
            $('.chosen-select.supId').chosen({width: "100%"}).trigger("chosen:updated");
        });
    }
});
app.controller('CategoryPromotionsTab6Controller', function ($scope, $http, $timeout, $location) {
    
    $scope.isVisible = false;
//    $scope.dividerNumber = 6;
    $scope.dividerName = "Shop by brands";
    
    //toggle divider visiblity
    $scope.toggleVisiblity = function(dividerNumber){
        $scope.isVisible = !$scope.isVisible;
    }
    
    $scope.selectedCategorySlug = (window.location.href.split('#')[1]).split('/')[3];
    //get all promotions
    $http.get('/promotions/category/get?cat_slug=' + $scope.selectedCategorySlug)
            .then(function (response) {
                if (response.data.data.length == 0) {
                    $scope.promotionsError = "No active  main categories found";
                    $(".error-modal-lg").modal();
                    return;
                } else {
                    var promotionalData = response.data.data[0];
                    $scope.parentCategory = response.data.data[0].category;
                    for (var i = 0; i < promotionalData.dividers.length; i++) {
                        if (promotionalData.dividers[i].dividerNumber == 6) {
//                            $scope.currentDividerInfo = promotionalData.dividers[i];
                            if (promotionalData.dividers[i].dividerName && promotionalData.dividers[i].dividerName.length > 0) {
                                $scope.dividerName = promotionalData.dividers[i].dividerName == "" ? "Shop by brands" : promotionalData.dividers[i].dividerName;
                            }
                            if (promotionalData.dividers[i].brands && promotionalData.dividers[i].brands.length > 0) {
                                $scope.brands = promotionalData.dividers[i].brands;
                            }
                            $scope.isVisible = promotionalData.dividers[i].isVisible;
                        }
                    }
                }
            });
    //get all brands        
    $http.get('/brand/getForCategory?cat_slug='+$scope.selectedCategorySlug)
        .then(function (responseBrands) {
            console.log("responseBrands");
            console.log(responseBrands);
            if (responseBrands.data.data.length > 0) {
                $scope.allBrands = responseBrands.data.data;
            }
            $timeout(function () {
                if (!$scope.brands) {
                    $scope.brands = [];
                }
                $('.chosen-select.featuredBrands').chosen({width: "100%"}).change(function (event, params) {
                    if (params.selected) {
                        for (var i = 0; i < $scope.brands.length; i++) {
                            if($scope.brands[i]._id == params.selected){
                                $scope.promotionsError = "Brand already exists";
                                $('.error-modal-lg').modal();
                                return;
                            }
                        }
                        $scope.brands.push({
                            _id: params.selected,
                            name: $('.chosen-select.featuredBrands option[value="' + params.selected + '"]').attr('data-name'),
                            slug: $('.chosen-select.featuredBrands option[value="' + params.selected + '"]').attr('data-slug')
                        });
                    } else if (params.deselected && $scope.brands.length > 0) {
                        for (var i = 0; i < $scope.brands.length; i++) {
                            if ($scope.brands[i]._id == params.deselected) {
                                $scope.brands.splice(i, 1);
                            }
                        }
                    }
                    $scope.$apply();

                });

            });
                    
        });
    
    //delete brand
    $scope.removeExistingBrand = function (eachBrand) {
        console.log("removeExistingBrand");
        console.log(eachBrand);
        for (var i = 0; i < $scope.brands.length; i++) {
            if ($scope.brands[i]._id == eachBrand._id)
            {
                $scope.brands.splice(i, 1);
                $('.chosen-select.featuredBrands option[value="' + eachBrand._id + '"]').removeAttr("selected").trigger("chosen:updated");
            }
        }
        $timeout(function () {
            $scope.$apply();
        });
    };
    
    //save divider
    $scope.submitDivider = function(){
        var serverObject = {};
        if ($scope.selectedCategorySlug) {
            serverObject["parentCategory.slug"] = $scope.parentCategory.slug;
        }
        serverObject.dividerNumber = 6;
        
        if ($scope.brands.length <= 0) {
            $scope.error = "Please select brands";
            return;
        } 
        else {
            serverObject.brands = $scope.brands;
            serverObject.isVisible = $scope.isVisible
            $http.post('/promotions/category/insert', serverObject)
                    .then(function (response) {
                        console.log(response);
                        if(response.data.error == 0){
                            $scope.success = "Successfully saved";
                        }
                        else {
                            $scope.error = response.data.msg;
                        }
                    });
        }
    }
    
});


/*************************************Category Promotions List Controller********************************************/
app.controller('CategoryPromotionsListController', function ($scope, $http, $timeout, $location) {
    $http.get('/promotions/category/get')
            .then(function (response) {
                console.log("response");
                console.log(response);
                if (response.data.data.length > 0) {
                    $scope.promotionalCategories = response.data.data;
                    $timeout(function () {
                        $("#promotionalCategoryListTable").DataTable();
                    });
                }
            });
});