// Buyer get controller
app.controller('BuyerListController', function ($timeout, $scope, $http, $location) {
    // check for access
    noAccessRedirect($location);

    $http.get('/buyer/get')
        .then(function (response) {

            $scope.results = response.data.data;
            $timeout(function () {
                $('#buyerListTable').DataTable();
            });
         });   
    $scope.message = 'Hello from Buyer List Controller';
});


app.controller('BuyerEditController', function ($timeout, $scope, $http, $window, $routeParams, $location) {
    // check for access
    noAccessRedirect($location);

    $scope.buyerId = $routeParams.buyerId;

    // get user details
    $http.get('/buyer/get/'+$scope.buyerId)
        .then(function (response) {
            
                var buyerDetails = response.data.data[0];
                $scope.name = buyerDetails.name;
                $scope.buyerCode = buyerDetails.buyerCode;
                $scope.mobileNo = buyerDetails.mobileNo;
                $scope.email = buyerDetails.email;
                $scope.user_type = (buyerDetails.buyerType).toString();
                $scope.user_type_val = (buyerDetails.buyerType).toString();
              //  $scope.status = (buyerDetails.isActive == true) ? 1 : 0;
                $scope.status = (buyerDetails.isActive).toString();
                $scope.buyerSelDepartmentId = (buyerDetails.department != undefined) ? buyerDetails.department._id : "";
                $scope.documents = buyerDetails.documents;
                $scope.doc = $scope.documents;
                $scope.rmDetails = buyerDetails.rmDetails;
                $scope.documentStatus = buyerDetails.documentStatus;
                $scope.documentRemarks = buyerDetails.documentRemarks;
                $scope.upgradePointsAdded = (buyerDetails.upgradePointsAdded != undefined) ? buyerDetails.upgradePointsAdded : false;


                $http.get('/department/getAll')
                .then(function (department_resp){
                    $scope.department = department_resp.data;
                });
                $timeout(function() {
                    $scope.$apply();
                });
            });

    // submit form
    $scope.submitForm = function () {
        $scope.error = '';
        $scope.success = '';

        // get data
        var name = $scope.name;
        var buyerCode = $scope.buyerCode;
        var mobileNo = $scope.mobileNo;
        var email = $scope.email;
        var user_type = $scope.user_type_val;
        var buyerSelDepartmentId = $scope.buyerSelDepartmentId;
        var buyerSelDepartmentSlug = $('#department option:selected').attr('data-slug');
        var buyerSelDepartmentName = $('#department option:selected').attr('data-name');
        var status = $('#status').val();
        var documentStatus = $scope.documentStatus;
        var documentRemarks = $scope.documentRemarks;
        var upgradePointsAdded = $scope.upgradePointsAdded;
            
        // validations
        if(name == '' || typeof(name) == 'undefined'){
            $scope.error = 'Please enter Name';
            return false;
        }
        if(mobileNo == '' || typeof(mobileNo) == 'undefined') {
            $scope.error = 'Please enter MobileNo';
            return false;
        }
        if(email == '' || typeof(email) == 'undefined') {
            $scope.error = 'Please enter Email';
            return false;
        }
        if(status == '' || typeof(status) == 'undefined') {
                $scope.error = 'Please select status';
                return false;
        }
        if(user_type == undefined || user_type == ""){
            $scope.error = 'Please select a User Type : General/Upgraded';
            return false;
        }
        if(user_type == 2 || user_type == '2'){
            //upgraded
            if(buyerSelDepartmentId == undefined || buyerSelDepartmentId == ""){
                $scope.error = 'For Upgraded user, please select a department!';
                return false;
            }
        }
        if(documentStatus != undefined && documentStatus == "rejected"){
            if(documentRemarks == undefined || documentRemarks == ""){
                $scope.error = 'If the Document Status is rejected, Please mention a reason!';
                return false;
            }
        }
        // data 
        var data = {
            name: name,
            buyerCode : buyerCode,
            mobileNo: mobileNo,
            email: email,
            status: status,
            user_type  :user_type,
            department : {
                id : buyerSelDepartmentId,
                name : buyerSelDepartmentName,
                slug : buyerSelDepartmentSlug
            },
            documentStatus : documentStatus,
            documentRemarks : documentRemarks,
            upgradePointsAdded : upgradePointsAdded
           // role: role,
          };
        var config = {};

        $http.put('/buyer/update/'+$scope.buyerId, data, config)
        .success(function (data, status, headers, config) {
            
            if(data.error == 1) {
                $scope.error = data.msg;
                return false;
            }
            $scope.upgradePointsAdded = true;
            $scope.success = data.msg;
        })
        .error(function (data, status, header, config) {
            console.log('error edit - ', data);
            $scope.error = 'Unable to add buyer';
        });
    };
});


app.controller('BuyerAddNewController', function ($timeout, $scope, $http, $window, $location) {
    // check for access
    noAccessRedirect($location);

   // $scope.supplierId = $routeParams.supplierId;
    $http.get('/buyer/get')
        .then(function (response) {
            var buyerList;
            $scope.buyerList = response.data.data;  
       });

        // submit form
        $scope.submitForm = function () {
            $scope.error = '';
            $scope.success = '';
            // get data
            var name = $scope.name;
            var email =   $scope.email;
            var password = $scope.password;
            var mobileNo = $scope.mobileNo;
           // var status = $scope.status;

            // validations
            if(name == '' || typeof(name) == 'undefined') {
                $scope.error = 'Please enter Name';
                return false;
            }
            if(email == '' || typeof(email) == 'undefined') {
                $scope.error = 'Please enter Email';
                return false;
            }
            if(password == '' || typeof(password) == 'undefined') {
                $scope.error = 'Please enter password';
                return false;
            }
            if(mobileNo == '' || typeof(mobileNo) == 'undefined') {
                $scope.error = 'Please enter MobileNo';
                return false;
            }
            // if(status == '' || typeof(status) == 'undefined') {
            //     $scope.error = 'Please select status';
            //     return false;
            // }

           // data 
            var data = {
                name: name,
                email: email,
                password: password,
                mobileNo: mobileNo,
            };
            var config = {};

            $http.post('/buyer/register', data, config)
            .success(function (data, status, headers, config) {
                
                if(data.error == 1) {
                    $scope.error = data.msg;
                    return false;
                }
                $scope.success = data.msg;

                $timeout(function () {
                   // $window.location.href = '/admin/#/supplier/basicinfo/{{supplierId}}';
                    $window.location.href = '/admin/#/buyer';
                }, 2000);            
            })
            .error(function (data, status, header, config) {
                console.log('error add - ', data);
                $scope.error = 'Unable to add Buyer';
            });
        };
});
