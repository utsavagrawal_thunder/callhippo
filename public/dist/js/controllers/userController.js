// closure for this controller // private
(function() {

// User get controller
app.controller('UserListController', function ($timeout, $scope, $http, $location) {
    // check for access
    noAccessRedirect($location);    
   
    // csv file header & column order
    $scope.getCSVHeader = function () {
        return ['User Name', 'Email', 'First Name', 'Last Name', 'Mobile No', 'Status', 'Roles', 'createdDate', 'updatedDate'];
    };
    $scope.getCSVColumnOrder = function () {
        return ['username', 'email', 'firstname', 'lastname', 'mobileno', 'isActive', 'role', 'createdDate', 'updatedDate'];
    };

    $http.get('/user/get')
        .then(function (response) {

            console.log("User Response is");
            console.log(response);
            $scope.results = response.data.data;
            $timeout(function () {
                $('#userListTable').DataTable();
            });


            var userCnt = $scope.results.length;

            $scope.totalSaleSuUser = 0;
            $scope.totalSalesBuyer = 0;
            $scope.totalClientsBuyer = 0;
            $scope.totalClientSupplier = 0; 
            $scope.totalMISExcutive = 0;

            for(var i = 0; i < userCnt; i++) 
            {
              
               if($scope.results[i].role == 'SALE-SU') 
                {
                    $scope.totalSaleSuUser++;
                }
                else if($scope.results[i].role == 'SALE-BU') 
                {
                    $scope.totalSalesBuyer++;
                }
                else if($scope.results[i].role == 'CLIENT-BU') 
                {
                    $scope.totalClientsBuyer++;
                }
                else if($scope.results[i].role == 'CLIENT-SU') 
                {
                    $scope.totalClientSupplier++;
                }
                else if($scope.results[i].role == 'MIS-EXEC') 
                {
                    $scope.totalMISExcutive++;
                } 

            }
               
        });
});

// Add 
app.controller('UserAddController', function ($timeout, $scope, $http, $window, $location) {
//Add a comment to this line
    // check for access
    noAccessRedirect($location);


    $http.get('/role/get')
        .then(function (response) {
            $scope.rolesList = response.data.data;            
        });

    // submit form
    $scope.submitForm = function () {
        $scope.error = '';
        $scope.success = '';

        // get data
        var username = $scope.username;
        var email =   $scope.email;
        var firstname = $scope.firstname;
        var lastname = $scope.lastname;
        var mobileno = $scope.mobileno;
        var password = $scope.password;
        var status = $scope.status;
        var role = $scope.role;


        // validations
        if(username == '' || typeof(username) == 'undefined') {
            $scope.error = 'Please enter UserName';
            return false;
        }
        if(email == '' || typeof(email) == 'undefined') {
            $scope.error = 'Please enter Email';
            return false;
        }
        if(firstname == '' || typeof(firstname) == 'undefined') {
            $scope.error = 'Please enter FirstName';
            return false;
        }
        if(lastname == '' || typeof(lastname) == 'undefined') {
            $scope.error = 'Please enter LastName';
            return false;
        }
        if(mobileno == '' || typeof(mobileno) == 'undefined') {
            $scope.error = 'Please enter MobileNo';
            return false;
        }
        if(password == '' || typeof(password) == 'undefined') {
            $scope.error = 'Please enter password';
            return false;
        }
        if(status == '' || typeof(status) == 'undefined') {
            $scope.error = 'Please select status';
            return false;
        }
        if(role == '' || typeof(role) == 'undefined') {
            $scope.error = 'Please select Roles';
            return false;
        }

       // data 
        var data = {
            username: username,
            email: email,
            firstname: firstname,
            lastname: lastname,
            mobileno: mobileno,
            password: password,
            status: status,
            role: role,
        };
        var config = {};

        $http.post('/user/insert', data, config)
        .success(function (data, status, headers, config) {
            console.log('success add - ', data);

            if(data.error == 1) {
                $scope.error = data.msg;
                return false;
            }
            $scope.success = data.msg;

            $timeout(function () {
                $window.location.href = '/admin/#/user';
            }, 2000);            
        })
        .error(function (data, status, header, config) {
            console.log('error add - ', data);
            $scope.error = 'Unable to add User';
        });
    };
});


app.controller('UserEditController', function ($timeout, $scope, $http, $window, $routeParams, $location) {
    // check for access
    noAccessRedirect($location);

    //get roles details
    $http.get('/role/get')
    .then(function (response) {
            $scope.rolesList = response.data.data;            
    });


    // get user details
    $http.get('/user/get/'+$routeParams.id)
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.headers = response.headers('User');
                var userDetails = response.data.data[0];
                $scope.username = userDetails.username;
                $scope.email = userDetails.email;
                $scope.firstname = userDetails.firstname;
                $scope.lastname = userDetails.lastname;
                $scope.mobileno = userDetails.mobileno;
                $scope.password = userDetails.password;
                $scope.status = (userDetails.isActive == true) ? 1 : 0;
             //   $scope.role = userDetails.role;
                    
            }
        });

    // submit form
    $scope.submitForm = function () {
        $scope.error = '';
        $scope.success = '';

        // get data
        var username = $scope.username;
        var email = $scope.email;
        var firstname = $scope.firstname;
        var lastname = $scope.lastname;
        var mobileno = $scope.mobileno;
        var password = $scope.password;
        var status = $('#status').val();
     //   var role = $scope.role;
       
        // validations
        if(username == '' || typeof(username) == 'undefined'){
            $scope.error = 'Please enter UserName';
            return false;
        }
        if(email == '' || typeof(email) == 'undefined') {
            $scope.error = 'Please enter Email';
            return false;
        }
        if(firstname == '' || typeof(firstname) == 'undefined'){
            $scope.error = 'Please enter FirstName';
            return false;
        }
        if(lastname == '' || typeof(lastname) == 'undefined') {
            $scope.error = 'Please enter LastName';
            return false;
        }
        if(mobileno == '' || typeof(mobileno) == 'undefined') {
            $scope.error = 'Please enter Mobileno';
            return false;
        }
        if(password == '' || typeof(password) == 'undefined') {
            $scope.error = 'Please enter Password';
            return false;
        }
        if(status == '' || typeof(status) == 'undefined'){
            $scope.error = 'Please select status';
            return false;
        }
 /*       if(role == '' || typeof(role) == 'undefined') {
            $scope.error = 'Please select role';
            return false;
        }
*/
        // data 
        var data = {
            username: username,
            email: email,
            firstname: firstname,
            lastname: firstname,
            mobileno: mobileno,
            password: password,
            status: status,
           // role: role,
          };
        var config = {};

        $http.put('/user/update/'+$routeParams.id, data, config)
        .success(function (data, status, headers, config) {
            console.log('success edit - ', data);
            
            if(data.error == 1) {
                $scope.error = data.msg;
                return false;
            }
            $scope.success = data.msg;
            $timeout(function () {
                $window.location.href = '/admin/#/user';
            }, 2000);
        })
        .error(function (data, status, header, config) {
            console.log('error edit - ', data);
            $scope.error = 'Unable to add user';
        });
    };
});

})(); // End of closure
