// closure for this controller // private
(function() {

// Product images url
var productImgAwsPath = s3_bucket+'product/images/';

// All Buyer order List
app.controller('AllBuyersOrdersController', function ($timeout, $scope, $http, $location, NgTableParams) {

    //all buyers status radio buttons
    $scope.showDetails = false;
    
     $scope.$watch('showDetails', function(){
        $scope.toggleText = $scope.showDetails ? 'View less' : 'View more';
    })


    // check for access
    noAccessRedirect($location);

    // get data
    $scope.searchOrder = '';
    $scope.getAllBuyersOrderList = function() {
        getAllBuyersOrderList($scope, $http, $timeout, NgTableParams);
    };
    $scope.getAllBuyersOrderList();

    // Reset search
    $scope.resetSearch = function() {
        $scope.searchOrder = '';
        $scope.getAllBuyersOrderList();
    };
});

// get buyer all orders list
function getAllBuyersOrderList($scope, $http, $timeout, NgTableParams) {
    $scope.tableParams = new NgTableParams(
        {count: 25, page: 1, sorting: { createdDate: 'desc' }},
        {
            counts: [25, 50, 100],
            total: 0,
            getData: function (params) {
                var count = params.count();
                var page = params.page();
                // for sorting
                var sorting = params.sorting();
                var sortParam = 'createdDate';
                var sortOrder = 'DESC';
                if(!isObjectEmpty(sorting)) {
                    sortParam = Object.keys(sorting)[0];
                    sortOrder = sorting[sortParam];                                        
                }

                var dataObj = {
                    searchTerm: $.trim($scope.searchOrder),
                    status: $('input[name=listingStatus]:checked').val(),
                    perPage: count,
                    pageNum: page,
                    sortParam: sortParam,
                    sortOrder: sortOrder
                };
                return new Promise(function (resolve, reject) {
                    $http.post('/order/getAllBuyersOrders', dataObj)
                        .success(function (data, status, headers, config) {
                            $scope.tableParams.total(data.totalCnt);
                            resolve(data.data);
                        })
                        .error(function (data, status, header, config) {
                            reject([]);
                        });
                });
            }
        });
}

// Get Particluar Buyer order List
app.controller('BuyerOrdersController', function ($timeout, $scope, $http, $location, $routeParams, NgTableParams) {


    //all suppliyers status radio buttons
    $scope.showDetails = false;
    
     $scope.$watch('showDetails', function(){
        $scope.toggleText = $scope.showDetails ? 'View less' : 'View more';
    });

    // check for access
    noAccessRedirect($location);

    // get route params
    $scope.buyerId = $routeParams.buyerId;
    $scope.buyerCode = $routeParams.buyerCode;

    // get data
    $scope.searchOrder = '';
    $scope.getBuyerOrderList = function() {
        getBuyerOrderList($scope, $http, $timeout, NgTableParams);
    };
    $scope.getBuyerOrderList();

    // Reset search
    $scope.resetSearch = function() {
        $scope.searchOrder = '';
        $scope.getBuyerOrderList();
    };

    // Get buyer orders counts
    $scope.totalOrders = 0;
    $scope.successOrdersCnt = 0;
    $scope.unSuccessOrdersCnt = 0;
    $http.get('/order/getBuyerCounts/'+$scope.buyerId)
        .then(function (response) {
            var allData = response.data.data;
            var allDataCnt = allData.length;
            for(var i = 0; i < allDataCnt; i++) {
                var tmpStatusCnt = allData[i].count;
                $scope.totalOrders += tmpStatusCnt;

                // delivered
                if(allData[i].status == 4) {
                    $scope.successOrdersCnt = tmpStatusCnt;
                }
                // unsuccess status
                else {
                    $scope.unSuccessOrdersCnt += tmpStatusCnt;
                }
            }            
        });
});

// get particular buyer orders list
function getBuyerOrderList($scope, $http, $timeout, NgTableParams) {
    $scope.tableParams = new NgTableParams(
        {count: 25, page: 1, sorting: { createdDate: 'desc' }},
        {
            counts: [25, 50, 100],
            total: 0,
            getData: function (params) {
                var count = params.count();
                var page = params.page();
                // for sorting
                var sorting = params.sorting();
                var sortParam = 'createdDate';
                var sortOrder = 'DESC';
                if(!isObjectEmpty(sorting)) {
                    sortParam = Object.keys(sorting)[0];
                    sortOrder = sorting[sortParam];                                        
                }

                var dataObj = {
                    buyerId: $scope.buyerId,
                    searchTerm: $.trim($scope.searchOrder),
                    status: $('input[name=listingStatus]:checked').val(),
                    perPage: count,
                    pageNum: page,
                    sortParam: sortParam,
                    sortOrder: sortOrder
                };
                return new Promise(function (resolve, reject) {
                    $http.post('/order/getBuyerOrders', dataObj)
                        .success(function (data, status, headers, config) {
                            $scope.tableParams.total(data.totalCnt);
                            resolve(data.data);
                        })
                        .error(function (data, status, header, config) {
                            reject([]);
                        });
                });
            }
        });
}

// Order details controller 
app.controller('OrderDetailsController', function ($timeout, $scope, $http, $window, $routeParams, $location) {
    // check for access
    noAccessRedirect($location);

    // get order id & supplier id
    $scope.orderId = $routeParams.orderId;
    var supplierId = (typeof($routeParams.supplierId) != 'undefined' && $routeParams.supplierId != '') ? $routeParams.supplierId : '';
    $scope.supplierId = supplierId;

    // get order details
    $http.get('/order/get/'+$scope.orderId+'?supplierId='+supplierId)
        .success(function (data, status, headers, config) {
            // no details
            if(data.data.length == 0) {
                $location.path('/orders/buyers/all');
                return;
            }

            // success
            $scope.orderDetails = data.data[0];
            $scope.parcelList = $scope.orderDetails.parcels ? $scope.orderDetails.parcels : [];
            var totalParcelList = ($scope.parcelList).length;

            // Remove other suuplier product orders if supplierId is passed            
            if(supplierId != '' && isMongoId(supplierId)) {
                for(var i = 0; i < ($scope.orderDetails.products).length; i++) {
                    var prdSupId = ($scope.orderDetails.products[i]['supplier']['supplierId']).toString();
                    if(supplierId != prdSupId) {
                        ($scope.orderDetails.products).splice(i, 1);
                        i--;
                    }
                }
            }

            // Generate show image url
            var products = $scope.orderDetails.products;
            var productsCnt = products.length;
            for(var i = 0; i < productsCnt; i++) {
                // get image
                $scope.orderDetails.products[i]['showImageUrl'] = productImgAwsPath+getResizedProductImage(products[i]['image'], 210, 210);                
            }

            // Show selected parcels
            $timeout(function() {
                for(var i = 0; i < productsCnt; i++) {
                    // for parcel
                    var shippingAddress = products[i]['shippingAddress'];
                    var shippingAddressCnt = shippingAddress.length;
                    for(var j = 0; j < shippingAddressCnt; j++) {
                        var tmpShipItemId = (shippingAddress[j]['_id']).toString();
                        var tmpProdParcels = shippingAddress[j]['parcels'] ? shippingAddress[j]['parcels'] : [];
                        var tmpProdParcelsCnt = tmpProdParcels.length;
                        var tmpParcelNames = '';
                        for(var k = 0; k < tmpProdParcelsCnt; k++) {
                            // get parcel name
                            for(var l = 0; l < totalParcelList; l++) {
                                if(tmpProdParcels[k] == $scope.parcelList[l]['_id']) {
                                    tmpParcelNames += $scope.parcelList[l]['parcelName'];

                                    // last
                                    if(k != (tmpProdParcelsCnt - 1)) {
                                        tmpParcelNames += ', ';
                                    }
                                    break;
                                }
                            }
                        }
                        $("#parcelsList_"+tmpShipItemId).html(tmpParcelNames);
                    }
                }
            }, 200);
        })
        .error(function (data, status, header, config) {
            $location.path('/orders/buyers/all');
        });

    // Chnage order status html
    $scope.getProductOrderStatusStr = function(orderStatus) {
        var orderStatusStr = '';
        if(orderStatus == 3) {
            orderStatusStr = 'Shipping'
        }
        else if(orderStatus == 4) {
            orderStatusStr = 'Delivered'    
        }

        return orderStatusStr;
    };

    // update order status
    $scope.updateOrderStatus = function(orderId, productItemId, subOrderId) {
        // get status
        var orderStatus = $.trim($('#orderStatus_'+subOrderId).val()); 

        // make request
        var dataToPost = {
            orderId: orderId,
            subOrderId: subOrderId,
            productItemId: productItemId,
            orderStatus: orderStatus
        };
        $http.post('/order/updateOrderStatus', dataToPost)
            .success(function (data, status, headers, config) {
                // error
                if(data.error == 1) {
                    showToastMsg(0, data.msg);
                    return false;
                }

                // Update status html
                var orderStatusStr = $scope.getProductOrderStatusStr(orderStatus);
                $('#buyerDelStatusDisp_'+subOrderId+', #supDelStatusDisp_'+subOrderId).html('<strong>'+orderStatusStr+'</strong>');

                // success
                showToastMsg(1, data.msg);

                // Hide panel
                $('#orderStatusPanel_'+subOrderId).hide();
            })
            .error(function (data, status, header, config) {
                showToastMsg(0, 'Unable to change order status');
            });
    };

    // get couriers list
    $scope.getCouriersList = function(parcelId) {
        // reset courier pg form
        $('#courierPgForm').attr('action', '');
        $('#pg').val('');
        $('#orderCode').val('');

        // show loader
        $('#loader-wrapper').show();

        // make request
        var dataToPost = {
            parcelId: parcelId,
            orderId: $scope.orderId
        };
        $http.post('/order/couriers/get', dataToPost)
            .success(function (data, status, headers, config) {
                // hide loader
                $('#loader-wrapper').hide();

                // error
                if(data.error == 1) {
                    showToastMsg(0, data.msg);                    
                    return false;
                }

                // success
                // showToastMsg(1, data.msg);

                // couriers list
                $scope.couriersList = data.data;

                // put order ids
                $('#c_parcelId').val(parcelId);

                // open popup
                $('#couriersListPopup').modal();
            })
            .error(function (data, status, header, config) {
                // hide loader
                $('#loader-wrapper').hide();
                showToastMsg(0, 'Unable to get couriers list');
            });
    };

    // create new abandoned shipment
    $scope.createAbandonedShipment = function(courierId, serviceId, serviceName) {
        // show loader
        $('#loader-wrapper').show();

        // get order ids
        var parcelId = $.trim($('#c_parcelId').val());

        // make request
        var dataToPost = {
            courierId: courierId,
            serviceId: serviceId,
            serviceName: serviceName,
            parcelId: parcelId,
            orderId: $scope.orderId
        };
        $http.post('/order/abandoned-shipment/create', dataToPost)
            .success(function (data, status, headers, config) {
                // hide loader
                $('#loader-wrapper').hide();

                // error
                if(data.error == 1) {
                    showToastMsg(0, data.msg);
                    return false;
                }

                // success
                // showToastMsg(1, data.msg);

                // close popup
                $('#packagingPopupCloseBtn').click();

                // data
                var pgLink = data.data.pgLink; 
                var pg = data.data.pg;
                var courierOrderCode = data.data.courierOrderCode;

                // submit form
                $('#courierPgForm').attr('action', pgLink);
                $('#pg').val(pg);
                $('#orderCode').val(courierOrderCode);
                $('#courierPgForm').submit();
            })
            .error(function (data, status, header, config) {
                // hide loader
                $('#loader-wrapper').hide();
                showToastMsg(0, 'Unable to create shipment');
            });
    };
});

// All Suppliers order List
app.controller('AllSuppliersOrdersController', function ($timeout, $scope, $http, $location, NgTableParams) {

    //suppliers status radio buttons
    $scope.showDetails = false;
    
     $scope.$watch('showDetails', function(){
        $scope.toggleText = $scope.showDetails ? 'View less' : 'View more';
    })


    // check for access
    noAccessRedirect($location);

    // get data
    $scope.searchOrder = '';
    $scope.getAllSuppliersOrderList = function() {
        getAllSuppliersOrderList($scope, $http, $timeout, NgTableParams);
    };
    $scope.getAllSuppliersOrderList();

    // Reset search
    $scope.resetSearch = function() {
        $scope.searchOrder = '';
        $scope.getAllSuppliersOrderList();
    };
});

// get supplier all orders list
function getAllSuppliersOrderList($scope, $http, $timeout, NgTableParams) {
    $scope.tableParams = new NgTableParams(
        {count: 25, page: 1, sorting: { createdDate: 'desc' }},
        {
            counts: [25, 50, 100],
            total: 0,
            getData: function (params) {
                var count = params.count();
                var page = params.page();
                // for sorting
                var sorting = params.sorting();
                var sortParam = 'createdDate';
                var sortOrder = 'DESC';
                if(!isObjectEmpty(sorting)) {
                    sortParam = Object.keys(sorting)[0];
                    sortOrder = sorting[sortParam];                                        
                }

                var dataObj = {
                    searchTerm: $.trim($scope.searchOrder),
                    status: $('input[name=listingStatus]:checked').val(),
                    perPage: count,
                    pageNum: page,
                    sortParam: sortParam,
                    sortOrder: sortOrder
                };
                return new Promise(function (resolve, reject) {
                    $http.post('/order/getAllSuppliersOrders', dataObj)
                        .success(function (data, status, headers, config) {
                            $scope.tableParams.total(data.totalCnt);
                            resolve(data.data);
                        })
                        .error(function (data, status, header, config) {
                            reject([]);
                        });
                });
            }
        });
}

// Get Particluar Supplier order List
app.controller('SupplierOrdersController', function ($timeout, $scope, $http, $location, $routeParams, NgTableParams) {

    //suppliers status radio buttons
    $scope.showDetails = false;
    
     $scope.$watch('showDetails', function(){
        $scope.toggleText = $scope.showDetails ? 'View less' : 'View more';
    })

    // check for access
    noAccessRedirect($location);

    // get route params
    $scope.supplierId = $routeParams.supplierId;
    $scope.supplierCode = $routeParams.supplierCode;

    // get data
    $scope.searchOrder = '';
    $scope.getSupplierOrderList = function() {
        getSupplierOrderList($scope, $http, $timeout, NgTableParams);
    };
    $scope.getSupplierOrderList();

    // Reset search
    $scope.resetSearch = function() {
        $scope.searchOrder = '';
        $scope.getSupplierOrderList();
    };

    // Get supplier orders counts
    $scope.totalOrders = 0;
    $scope.totalOrdersServed = 0;
    $scope.totalOrdersRejected = 0;
    $scope.totalOrdersPending = 0;
    $http.get('/order/getSupplierCounts/'+$scope.supplierId)
        .then(function (response) {
            var allData = response.data.data;
            var allDataCnt = allData.length;
            for(var i = 0; i < allDataCnt; i++) {
                var tmpStatusCnt = allData[i].count;
                $scope.totalOrders += tmpStatusCnt;

                // delivered
                if(allData[i].status == 4 || allData[i].status == 7) {
                    $scope.totalOrdersServed = tmpStatusCnt;
                }
                else if(allData[i].status == 1) {
                    $scope.totalOrdersPending = tmpStatusCnt;
                }
                // unsuccess status
                else if([8,9,10,11].indexOf(allData[i].status) != -1) {
                    $scope.totalOrdersRejected += tmpStatusCnt;
                }
            }            
        });
});

// get particular supplier orders list
function getSupplierOrderList($scope, $http, $timeout, NgTableParams) {
    $scope.tableParams = new NgTableParams(
        {count: 25, page: 1, sorting: { createdDate: 'desc' }},
        {
            counts: [25, 50, 100],
            total: 0,
            getData: function (params) {
                var count = params.count();
                var page = params.page();
                // for sorting
                var sorting = params.sorting();
                var sortParam = 'createdDate';
                var sortOrder = 'DESC';
                if(!isObjectEmpty(sorting)) {
                    sortParam = Object.keys(sorting)[0];
                    sortOrder = sorting[sortParam];                                        
                }

                var dataObj = {
                    supplierId: $scope.supplierId,
                    searchTerm: $.trim($scope.searchOrder),
                    status: $('input[name=listingStatus]:checked').val(),
                    perPage: count,
                    pageNum: page,
                    sortParam: sortParam,
                    sortOrder: sortOrder
                };
                return new Promise(function (resolve, reject) {
                    $http.post('/order/getSupplierOrders', dataObj)
                        .success(function (data, status, headers, config) {
                            $scope.tableParams.total(data.totalCnt);
                            resolve(data.data);
                        })
                        .error(function (data, status, header, config) {
                            reject([]);
                        });
                });
            }
        });
}

})(); // End of closure