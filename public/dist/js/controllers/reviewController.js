// closure for this controller // private
(function() {

	//review controller
	app.controller('ReviewController', function ($timeout, $scope, $http, $location) {
		// check for access
    	noAccessRedirect($location);

    	$scope.allreviews = [];
        
        $scope.getAllReviews = function(){
		
            //get all reviews of user
            $http.get('/user/get-all-reviews')
            .success(function (data, status, headers, config) {
                //error
                if(data.error == 1){
                    //error found
                    showToastMsg(0, (data.msg != undefined && data.msg != "")? data.msg: 'Some Error Occured!' );
                    return;
                }

                // no details
                if(data.data == undefined) {
                    showToastMsg(0, (data.msg != undefined && data.msg != "")? data.msg: 'No Reviews Found!' );
                    return;
                }

                $scope.allReviews = (data.data.reviews != undefined)?data.data.reviews : [];


            })
            .error(function (data, status, header, config) {
                showToastMsg(0, (data.msg != undefined && data.msg != "")? data.msg: 'Something went Wrong!' );
            });
            
        }
		
        //get Reviews
        $scope.getAllReviews();

        $scope.intialStars = [1,2,3,4,5];
        //function to return array of elements
        $scope.getTotalRating = function (num){
        	return new Array(num);
        }

        $scope.updateRating  = function (rating_id, status, field){
        	
        	if(rating_id != undefined && rating_id != 0 && rating_id != "" && status != undefined){

        		if(status == 0 || status == 1){

        			$('#'+field).attr('disabled','disabled');
	        		// data 
			        var postdata = {
			            rating: rating_id,
			            status : status
			        };
			        var config = {};
			        console.log(postdata);
	        		$http.post('/user/update-rating-status', postdata, config)
	                .success(function (data, status, headers, config) {
	                    //error
	                    if(data.error == 1){
	                        //error found
	                        showToastMsg(0, (data.msg != undefined && data.msg != "")? data.msg: 'Some Error Occured!' );
	                        return;
	                    }

	                    // no details
	                    if(data.data == undefined) {
	                        showToastMsg(0, (data.msg != undefined && data.msg != "")? data.msg: 'Not able to update rating!' );
	                        return;
	                    }
	                    $('#'+field).removeAttr('disabled');
	                    $('.rat_status_sect'+rating_id).html('<b>'+data.data.review+'</b>');
	                    showToastMsg(1, data.msg);
	                })
	                .error(function (data, status, header, config) {
	                	$('#'+field).removeAttr('disabled');
	                    showToastMsg(0, (data.msg != undefined && data.msg != "")? data.msg: 'Something went Wrong!' );
	                });

        		}else{
        			showToastMsg(0, "Only Approved and Rejected Status is Allowed!" );	
        		}
        	}else{
        		showToastMsg(0, "Could not find rating id attached!" );
        	}
        }

	});
})();