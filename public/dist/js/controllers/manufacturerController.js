// closure for this controller // private
(function() {

// manufacturer get controller
app.controller('ManufacturerListController', function ($timeout, $scope, $http, $location) {
    // check for access
    noAccessRedirect($location);

     // csv file header & column order
    $scope.getCSVHeader = function () {
        return ['Manufacturers Name', 'Description' , 'Category' , 'SubCategory',  'Status'];
    };
    $scope.getCSVColumnOrder = function () {
        return ['name', 'description', 'featuredCategories', 'featuredSubCategories',  'isActive'];
    };

    $http.get('/manufacturer/get')
        .then(function (response) {
            $scope.headers = response.headers('Manufacturer list');
            $scope.results = response.data.data;

            $scope.excelFormatData = $scope.results;
            for(var i=0; i<$scope.excelFormatData.length; i++) {

                var tmpFeaturedCategoriesArr = $scope.excelFormatData[i].featuredCategories;
                var featuredCategoryStr = '';
                for(var j = 0; j < tmpFeaturedCategoriesArr.length; j++) {
                  //  featuredCategoryStr = tmpFeaturedCategoriesArr[j].name+', ';
                    featuredCategoryStr += tmpFeaturedCategoriesArr[j].name.toString()+' , ';
                }
                $scope.excelFormatData[i].featuredCategories = featuredCategoryStr;

                var tmpFeaturedSubCategoriesArr = $scope.excelFormatData[i].featuredSubCategories;
                var featuredSubCategoryStr = '';
                for(var j=0; j<tmpFeaturedSubCategoriesArr.length; j++){
                        featuredSubCategoryStr = tmpFeaturedSubCategoriesArr[j].name.toString()+' , ';
                }
                $scope.excelFormatData[i].featuredSubCategories = featuredSubCategoryStr;
            }

            $timeout(function () {
                $('#manufacturerListTable').DataTable();
            });
        });
});

// Add manufacturer
app.controller('ManufacturerAddController', function ($timeout, $scope, $http, $location) {
    // check for access
    noAccessRedirect($location);

    $scope.logoImg = '';
    // for dropzone file uploader
    $scope.dropzoneConfig = {
        'options': { // passed into the Dropzone constructor
            url: '/manufacturer/upload',
            maxFilesize: 8, // MB
            maxFiles: 1,
            dictDefaultMessage: 'Drag an image here to upload, or click to select one',
            acceptedFiles: 'image/*',
            addRemoveLinks: true,
            removedfile: function(file) {
                // empty img
//                $scope.logoImg = '';
                var name = file.name;

                // remove file from server
                $http.delete('/manufacturer/delFile/'+$scope.logoImg)
                .then(function (response) {});

                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
            },
            init: function() {
                this.on('success', function( file, resp ){
                    $scope.logoImg = resp.key.split('/')[1];
                });
            },
        },
        'eventHandlers': {
            'sending': function (file, xhr, formData) {
            },
            'success': function (file, response) {
            }
        }
    };

    // get Main category list
    $scope.mainCategoryList = new Array();
    $http.get('/category/getAllMainCategories')
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.headers = response.headers('Manufacturer');
                $scope.mainCategoryList = response.data.data;
            }
        });

    // get other category list
    $scope.otherCategoryList = new Array();
    $http.get('/category/getAllOtherCategories')
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.headers = response.headers('Manufacturer');
                $scope.otherCategoryList = response.data.data;
            }
        });

    // submit form
    $scope.submitForm = function () {
        $scope.error = '';
        $scope.success = '';

        // get data
        var name = $scope.name;
        var description = $scope.description;
        var status = $scope.status;
        // var logoImg = 'image/test.jpg';
        var logoImg = $scope.logoImg;
        var selectedCategories = $('input:checkbox[name=category]:checked').map(function() {
            var value = this.value;
            var splitData = value.split('_##_');
            var tmpObj = {id: splitData[0], name: splitData[1]};
            return tmpObj;
        }).get();
        var selectedSubCategories = $('input:checkbox[name=subCategory]:checked').map(function() {
            var value = this.value;
            var splitData = value.split('_##_');
            var tmpObj = {id: splitData[0], name: splitData[1]};
            return tmpObj;
        }).get();

        // validations
        if(name == '' || typeof(name) == 'undefined') {
            $scope.error = 'Please enter Name';
            return false;
        }
        if(onlySpecialChars(name) == false) {
            $scope.error = 'Name should not contain only special characters';
            return false;
        }
        if(logoImg == '' || typeof(logoImg) == 'undefined') {
            $scope.error = 'Please select image';
            return false;
        }
        /*
        if(selectedCategories.length == 0) {
            $scope.error = 'Please select categories';
            return false;
        }
        if(selectedSubCategories.length == 0) {
            $scope.error = 'Please select sub categories';
            return false;
        }
        */
        if(status == '' || typeof(status) == 'undefined') {
            $scope.error = 'Please select status';
            return false;
        }

        // data 
        var data = {
            name: name,
            description: description,
            status: status,
            logoImg: logoImg,
            categories: JSON.stringify(selectedCategories),
            subCategories: JSON.stringify(selectedSubCategories)
        };
        var config = {};

        $http.post('/manufacturer/insert', data, config)
        .success(function (data, status, headers, config) {
            console.log('success add - ', data);

            if(data.error == 1) {
                $scope.error = data.msg;
                return false;
            }
            $scope.success = data.msg;

            $timeout(function () {
                $location.path('/manufacturer');
            }, 2000);            
        })
        .error(function (data, status, header, config) {
            console.log('error add - ', data);
            $scope.error = 'Unable to add manufacturer';
        });
    };
});

// Edit manufacturer
app.controller('ManufacturerEditController', function ($timeout, $scope, $http, $location, $routeParams) {
    // check for access
    noAccessRedirect($location);

    // for dropzone file uploader
    $scope.dropzoneConfig = {
        'options': { // passed into the Dropzone constructor
            url: '/manufacturer/upload',
            maxFilesize: 8, // MB
            maxFiles: 1,
            dictDefaultMessage: 'Drag an image here to upload, or click to select one',
            acceptedFiles: 'image/*',
            addRemoveLinks: true,
            removedfile: function(file) {
                // empty img & set max limit to 1
//                $scope.logoImg = '';
                this.options.maxFiles = 1;

                var name = file.name;

                // remove file from server
                $http.delete('/manufacturer/delFile/'+$scope.logoImg)
                .then(function (response) {});

                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
            },
            init: function() {
                this.on('success', function( file, resp ){
                    $scope.logoImg = resp.key.split('/')[1];
                });

                // keep dropzone refernce
                $scope.dropzone = this;
            },
        },
        'eventHandlers': {
            'sending': function (file, xhr, formData) {
            },
            'success': function (file, response) {
            }
        }
    };

    // get manufacturer details
    $http.get('/manufacturer/get/'+$routeParams.id)
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.headers = response.headers('Manufacturer');
                var manufacturerDetails = response.data.data[0];
                $scope.name = manufacturerDetails.name;
                $scope.description = manufacturerDetails.description;
                $scope.status = (manufacturerDetails.isActive == true) ? 1 : 0;
                $scope.logoImg = manufacturerDetails.logoImg;
                $scope.featuredCategories = manufacturerDetails.featuredCategories;
                $scope.featuredSubCategories = manufacturerDetails.featuredSubCategories;

                // For preloading DB images to dropzone
                if($scope.logoImg != '' && typeof($scope.logoImg) != 'undefined') {
                    var imgFile = { name: $scope.logoImg, size: 0 };
                    $scope.dropzone.options.addedfile.call($scope.dropzone, imgFile);
                    $scope.dropzone.options.thumbnail.call($scope.dropzone, imgFile, s3_bucket+'manufacturer/'+$scope.logoImg);
                    // reduce max files option
                    $scope.dropzone.options.maxFiles = 0;

                    // hide image sizes
                    $('.dz-size, .dz-progress').hide();
                }                

                // get Main category list
                $scope.mainCategoryList = new Array();
                $http.get('/category/getAllMainCategories')
                    .then(function (response) {
                        if(response.data.error == 0 && response.data.data != '') {
                            $scope.headers = response.headers('Manufacturer');
                            $scope.mainCategoryList = response.data.data;

                            // set checkbox ticked
                            $timeout(function () {
                                angular.forEach($scope.featuredCategories, function(value, key) {
                                    $("#mainCategory_"+value._id).prop('checked', true);
                                });
                            }, 200);
                        }
                    });

                // get other category list
                $scope.otherCategoryList = new Array();
                $http.get('/category/getAllOtherCategories')
                    .then(function (response) {
                        if(response.data.error == 0 && response.data.data != '') {
                            $scope.headers = response.headers('Manufacturer');
                            $scope.otherCategoryList = response.data.data;

                            // set checkbox ticked
                            $timeout(function () {
                                angular.forEach($scope.featuredSubCategories, function(value, key) {
                                    $("#otherCategory_"+value._id).prop('checked', true);
                                });
                            }, 200);
                        }
                    });
            }
        });

    // submit form
    $scope.submitForm = function () {
        $scope.error = '';
        $scope.success = '';

        // get data
        var name = $scope.name;
        var description = $scope.description;
        var status = $('#status').val();
        var logoImg = $scope.logoImg;
        var selectedCategories = $('input:checkbox[name=category]:checked').map(function() {
            var value = this.value;
            var splitData = value.split('_##_');
            var tmpObj = {id: splitData[0], name: splitData[1]};
            return tmpObj;
        }).get();
        var selectedSubCategories = $('input:checkbox[name=subCategory]:checked').map(function() {
            var value = this.value;
            var splitData = value.split('_##_');
            var tmpObj = {id: splitData[0], name: splitData[1]};
            return tmpObj;
        }).get();

        // validations
        if(name == '' || typeof(name) == 'undefined') {
            $scope.error = 'Please enter Name';
            return false;
        }
        if(onlySpecialChars(name) == false) {
            $scope.error = 'Name should not contain only special characters';
            return false;
        }
        if(logoImg == '' || typeof(logoImg) == 'undefined') {
            $scope.error = 'Please select image';
            return false;
        }
        /*
        if(selectedCategories.length == 0) {
            $scope.error = 'Please select categories';
            return false;
        }
        if(selectedSubCategories.length == 0) {
            $scope.error = 'Please select sub categories';
            return false;
        }
        */
        if(status == '' || typeof(status) == 'undefined') {
            $scope.error = 'Please select status';
            return false;
        }

        // data 
        var data = {
            name: name,
            description: description,
            status: status,
            logoImg: logoImg,
            categories: JSON.stringify(selectedCategories),
            subCategories: JSON.stringify(selectedSubCategories)
        };
        var config = {};

        $http.put('/manufacturer/update/'+$routeParams.id, data, config)
        .success(function (data, status, headers, config) {
            console.log('success edit - ', data);
            
            if(data.error == 1) {
                $scope.error = data.msg;
                return false;
            }
            $scope.success = data.msg;

            $timeout(function () {
                $location.path('/manufacturer');
            }, 2000);
        })
        .error(function (data, status, header, config) {
            console.log('error edit - ', data);
            $scope.error = 'Unable to add manufacturer';
        });
    };
});

})(); // End of closure