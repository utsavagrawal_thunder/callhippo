// closure for this controller // private
(function() {

// offer get controller
app.controller('OfferListController', function ($timeout, $scope, $http, $location) {
    // check for access
    noAccessRedirect($location);

    // csv file header & column order
    $scope.getCSVHeader = function () {
        return ['Coupon code', 'Valid From', 'Valid Till', 'Type', 'Type Item', 'Discount Type', 'Discount Value', 'Cart Value', 'Max discount', 'Valid for', 'No. of times used', 'Status'];
    };
    $scope.getCSVColumnOrder = function () {
        return ['couponCode', 'validFromFormatted', 'validTillFormatted', 'typeStr', 'typeItemNameStr', 'discountType', 'discountValue', 'minCartValue', 'maxDiscountAmount', 'validForUserStr', 'usageLimit', 'isActiveStr'];
    };

    // format data for excel
    $scope.formatDataForExcel = function() {
        var resultsCnt = ($scope.results).length;

        // update some details for excel
        for(var i = 0; i < resultsCnt; i++) {
            // date
            $scope.results[i]['validFromFormatted'] = getDateYMD($scope.results[i].validFrom);
            $scope.results[i]['validTillFormatted'] = getDateYMD($scope.results[i].validTill);

            // valid for
            var validForUserStr = 'General';
            if($scope.results[i].validForUser == 2) {
                validForUserStr = 'Featured';
            }
            $scope.results[i]['validForUserStr'] = validForUserStr;

            // status
            $scope.results[i]['isActiveStr'] = ($scope.results[i].isActive == 1) ? 'Active' : 'Inactive';

            // for type
            var tmpType = $scope.results[i].type;
            // product type
            if(tmpType == 1) {
                $scope.results[i]['typeStr'] = 'Product Type';
            }
            // main category
            else if(tmpType == 2) {
                $scope.results[i]['typeStr'] = 'Main Category';
            }
            // sub category
            else if(tmpType == 3) {
                $scope.results[i]['typeStr'] = 'Sub Category';
            }
            // product variant
            else if(tmpType == 4) {
                $scope.results[i]['typeStr'] = 'Product Variant';
            }
            // department
            else if(tmpType == 5) {
                $scope.results[i]['typeStr'] = 'Department';
            }
            // brand
            else if(tmpType == 6) {
                $scope.results[i]['typeStr'] = 'Brand';
            }

            // for type item            
            var tmpTypeItemDetails = $scope.results[i]['typeItemDetails'];
            var tmpTypeItemDetailsCnt = tmpTypeItemDetails.length;
            var typeItemNameStr = '';
            for(var t = 0; t < tmpTypeItemDetailsCnt; t++) {
                typeItemNameStr += tmpTypeItemDetails[t].name;
                
                if(t != (tmpTypeItemDetailsCnt - 1)) {
                    typeItemNameStr += ', ';   
                }
            }
            $scope.results[i]['typeItemNameStr'] = typeItemNameStr;
        }
    }

    // get all offers
    $http.get('/offer/get')
        .then(function (response) {
            $scope.results = response.data.data;
            
            // format data for excel
            $scope.formatDataForExcel();

            $timeout(function () {
                $scope.offerDataTable = $('#offerListTable').DataTable({
                    "lengthChange": false,
                    "searching": false,
                    "pageLength": 10
                });
            });
        });

    // Get coupon types counts
    $scope.productTypeCoupons = 0;
    $scope.mainCategoryCoupons = 0;
    $scope.subCategoryCoupons = 0;
    $scope.productVariantCoupons = 0;
    $scope.departmentCoupons = 0;
    $scope.brandCoupons = 0;
    $scope.manufacturerCoupons = 0;
    $scope.totalCoupons = 0;
    $http.get('/offer/getCouponTypeCounts')
        .then(function (response) {
            var allData = response.data.data;
            var allDataCnt = allData.length;
            for(var i = 0; i < allDataCnt; i++) {
                var tmpType = allData[i].type;
                var tmpCount = allData[i].count;
                // total coupons
                $scope.totalCoupons += tmpCount;
                // product type
                if(tmpType == 1) {
                    $scope.productTypeCoupons = tmpCount;
                }
                // main category
                else if(tmpType == 2) {
                    $scope.mainCategoryCoupons = tmpCount;
                }
                // sub category
                else if(tmpType == 3) {
                    $scope.subCategoryCoupons = tmpCount;
                }
                // product variant
                else if(tmpType == 4) {
                    $scope.productVariantCoupons = tmpCount;
                }
                // department
                else if(tmpType == 5) {
                    $scope.departmentCoupons = tmpCount;
                }
                // brand
                else if(tmpType == 6) {
                    $scope.brandCoupons = tmpCount;
                }
                // manufacturer
                else if(tmpType == 7) {
                    $scope.manufacturerCoupons = tmpCount;
                }
            }            
        });

    // initialize datepicker
    initDatepickerForSearch();

    // show search box panel
    $scope.showSearchBox = function () {
        var searchType = $scope.searchType;

        // hide all
        $('#couponCodeBox, #validDateBox, #typeBox, #discountTypeBox, #discountValueBox, #cartValueBox, #maxDiscountBox, #validForUserBox, #statusBox').hide();

        // show specific
        if(searchType == 'couponCode') {
            $('#couponCodeBox').show();
        }
        else if(searchType == 'validDate') {
            $('#validDateBox').show();
        }
        else if(searchType == 'type') {
            $('#typeBox').show();
        }
        else if(searchType == 'discountType') {
            $('#discountTypeBox').show();
        }
        else if(searchType == 'discountValue') {
            $('#discountValueBox').show();
        }
        else if(searchType == 'cartValue') {
            $('#cartValueBox').show();
        }
        else if(searchType == 'maxDiscount') {
            $('#maxDiscountBox').show();
        }
        else if(searchType == 'validForUser') {
            $('#validForUserBox').show();
        }
        else if(searchType == 'status') {
            $('#statusBox').show();
        }
    }

    // search offer
    $scope.searchOffer = function() {
        var searchType = $scope.searchType;
        var data = {
            searchType: searchType
        };

        // for coupon code
        if(searchType == 'couponCode') {
            var couponCode = (typeof($scope.couponCode) != 'undefined') ? $scope.couponCode : '';

            // validation
            if(typeof(couponCode) == 'undefined' || couponCode == '') {
                showToastMsg(0, 'Please enter coupon code');
                $('#couponCode').focus();
                return false;
            }

            data.couponCode = couponCode;
        }

        // for valid date
        else if(searchType == 'validDate') {
            var validFrom = $('#validFrom').val();
            var validTill = $('#validTill').val();

            // validations
            if(validFrom == '' && validTill == '') {
                showToastMsg(0, 'Please select start or end date');
                $('#validFrom').focus();
                return false;
            }
            if((validFrom != '' && validTill != '') && (validTill < validFrom)) {
                showToastMsg(0, 'Please select end date greater than start date');
                $('#validTill').focus();
                return false;
            }

            data.validFrom = validFrom;
            data.validTill = validTill;
        }

        // for type
        else if(searchType == 'type') {
            var type = (typeof($scope.type) != 'undefined') ? $scope.type : '';

            // validations
            if(typeof(type) == 'undefined' || type == '') {
                showToastMsg(0, 'Please select type');
                $('#type').focus();
                return false;
            }

            data.type = type;
        }

        // for discount type
        else if(searchType == 'discountType') {
            var discountType = (typeof($scope.discountType) != 'undefined') ? $scope.discountType : '';

            // validations
            if(typeof(discountType) == 'undefined' || discountType == '') {
                showToastMsg(0, 'Please select discount type');
                $('#discountType').focus();
                return false;
            }

            data.discountType = discountType;
        }

        // for discount value
        else if(searchType == 'discountValue') {
            var discountValueFrom = (typeof($scope.discountValueFrom) != 'undefined') ? parseInt($scope.discountValueFrom, 10) : 0;
            discountValueFrom = isNaN(discountValueFrom) ? 0 : discountValueFrom;
            var discountValueTill = (typeof($scope.discountValueTill) != 'undefined') ? parseInt($scope.discountValueTill, 10) : 0;
            discountValueTill = isNaN(discountValueTill) ? 0 : discountValueTill;

            // validations
            if(discountValueFrom == 0 && discountValueTill == 0) {
                showToastMsg(0, 'Please enter start or end discount value');
                $('#discountValueFrom').focus();
                return false;
            }
            if((discountValueFrom != 0 && discountValueTill != 0) && (discountValueTill < discountValueFrom)) {
                showToastMsg(0, 'Please enter end discount value greater than start discount value');
                $('#discountValueTill').focus();
                return false;
            }

            data.discountValueFrom = discountValueFrom;
            data.discountValueTill = discountValueTill;
        }    

        // for cart value
        else if(searchType == 'cartValue') {
            var cartValueFrom = (typeof($scope.cartValueFrom) != 'undefined') ? parseInt($scope.cartValueFrom, 10) : 0;
            cartValueFrom = isNaN(cartValueFrom) ? 0 : cartValueFrom;
            var cartValueTill = (typeof($scope.cartValueTill) != 'undefined') ? parseInt($scope.cartValueTill, 10) : 0;
            cartValueTill = isNaN(cartValueTill) ? 0 : cartValueTill;

            // validations
            if(cartValueFrom == 0 && cartValueTill == 0) {
                showToastMsg(0, 'Please enter start or end cart value');
                $('#discountValueFrom').focus();
                return false;
            }
            if((cartValueFrom != 0 && cartValueTill != 0) && (cartValueTill < cartValueFrom)) {
                showToastMsg(0, 'Please enter end cart value greater than start cart value');
                $('#discountValueFrom').focus();
                return false;
            }

            data.cartValueFrom = cartValueFrom;
            data.cartValueTill = cartValueTill;
        }

        // for max discount
        else if(searchType == 'maxDiscount') {
            var maxDiscountFrom = (typeof($scope.maxDiscountFrom) != 'undefined') ? parseInt($scope.maxDiscountFrom, 10) : 0;
            maxDiscountFrom = isNaN(maxDiscountFrom) ? 0 : maxDiscountFrom;
            var maxDiscountTill = (typeof($scope.maxDiscountTill) != 'undefined') ? parseInt($scope.maxDiscountTill, 10) : 0;
            maxDiscountTill = isNaN(maxDiscountTill) ? 0 : maxDiscountTill;

            // validations
            if(maxDiscountFrom == 0 && maxDiscountTill == 0) {
                showToastMsg(0, 'Please enter start or end max discount');
                $('#discountValueFrom').focus();
                return false;
            }
            if((maxDiscountFrom != 0 && maxDiscountTill != 0) && (maxDiscountTill < maxDiscountFrom)) {
                showToastMsg(0, 'Please enter end max discount greater than start max discount');
                $('#discountValueFrom').focus();
                return false;
            }

            data.maxDiscountFrom = maxDiscountFrom;
            data.maxDiscountTill = maxDiscountTill;
        }

        // for valid for
        else if(searchType == 'validForUser') {
            var validForUser = (typeof($scope.validForUser) != 'undefined') ? $scope.validForUser : '';

            // validations
            if(typeof(validForUser) == 'undefined' || validForUser == '') {
                showToastMsg(0, 'Please select valid for');
                $('#validForUser').focus();
                return false;
            }

            data.validForUser = validForUser;
        }

        // for status
        else if(searchType == 'status') {
            var status = (typeof($scope.status) != 'undefined') ? $scope.status : '';

            // validations
            if(typeof(status) == 'undefined' || status == '') {
                showToastMsg(0, 'Please select status');
                $('#status').focus();
                return false;
            }

            data.status = status;   
        }

        // get data
        $http.post('/offer/getSearchedOffer', data)
        .success(function (data, status, headers, config) {
            // error on add
            if(data.error == 1) {
                showToastMsg(0, data.msg);
                return false;
            }
            // showToastMsg(1, data.msg);

            // destroy datatable
            $scope.offerDataTable.destroy();

            $scope.results = data.data;            

            // reload datatable
            $timeout(function () {
                $scope.offerDataTable = $('#offerListTable').DataTable({
                    "lengthChange": false,
                    "searching": false,
                    "pageLength": 10
                });
            });

            // format data for excel
            $scope.formatDataForExcel();
        })
        .error(function (data, status, header, config) {
            $scope.results = [];
        });
    };
});

// validate form
function validateForm(command, $scope) {
    // get data
    var couponCode = $scope.couponCode;
    var validFrom = $('#validFrom').val();
    var validTill = $('#validTill').val();
    var type = $scope.type;
    var discountType = $scope.discountType;
    var discountValue = $scope.discountValue;
    var minCartValue = $scope.minCartValue;
    var maxDiscountAmount = $scope.maxDiscountAmount;
    var validForUser = $scope.validForUser;
    var usageLimit = $scope.usageLimit;
    var status = $scope.status;
    
    // validations
    if(typeof(couponCode) == 'undefined' || couponCode == '') {
        showToastMsg(0, 'Please enter coupon code');
        $('#couponCode').focus();
        return false;
    }
    if(typeof(validFrom) == 'undefined' || validFrom == '') {
        showToastMsg(0, 'Please select start date');
        $('#validFrom').focus();
        return false;
    }
    if(typeof(validTill) == 'undefined' || validTill == '') {
        showToastMsg(0, 'Please select end date');
        $('#validTill').focus();
        return false;
    }
    if(validTill < validFrom) {
        showToastMsg(0, 'Please select end date greater than start date');
        $('#validTill').focus();
        return false;
    }
    if(typeof(type) == 'undefined' || type == '') {
        showToastMsg(0, 'Please select type');
        $('#type').focus();
        return false;
    }

    // for item details
    var typeItemDetails = getTypeItemDetails($scope);
    if(typeItemDetails.length == 0) {
        showToastMsg(0, 'Please select type item');
        $('#type').focus();
        return false;
    }

    if(typeof(discountType) == 'undefined' || discountType == '') {
        showToastMsg(0, 'Please select discount type');
        $('#discountType').focus();
        return false;
    }
    if(typeof(discountValue) == 'undefined' || discountValue == '') {
        showToastMsg(0, 'Please enter discount value');
        $('#discountValue').focus();
        return false;
    }
    if(typeof(minCartValue) == 'undefined' || minCartValue == '') {
        showToastMsg(0, 'Please enter minimum cart value');
        $('#minCartValue').focus();
        return false;
    }
    if(typeof(maxDiscountAmount) == 'undefined' || maxDiscountAmount == '') {
        showToastMsg(0, 'Please enter max discount amount');
        $('#maxDiscountAmount').focus();
        return false;
    }
    if(typeof(validForUser) == 'undefined' || validForUser == '') {
        showToastMsg(0, 'Please select user type');
        $('#validForUser').focus();
        return false;
    }
    if(typeof(usageLimit) == 'undefined' || usageLimit == '') {
        showToastMsg(0, 'Please enter usage limit');
        $('#usageLimit').focus();
        return false;
    }
    if(typeof(status) == 'undefined' || status == '') {
        showToastMsg(0, 'Please select status');
        $('#status').focus();
        return false;
    }

    return true;
}

// initialize date picker
function initDatepicker(command) {
    $(function() {
        var options = {
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true
        };
        if(command == 'ADD') {
            options['startDate'] = '0d';
        }

        $("#validFrom, #validTill").datepicker(options);
    });
}

// initialize date picker for search
function initDatepickerForSearch() {
    $(function() {
        $("#validFrom, #validTill").datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            clearBtn: true
        });
    });
}

// get all product types
function getAllProductTypes($scope, $http) {
    // list fetched once
    if($scope.productTypeListFetched) {
        return;
    }

    $scope.productTypeListFetched = true;
    $http.get('/category/getAllProductTypes')
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.productTypeList = response.data.data;
                var productTypeListCnt = ($scope.productTypeList).length;

                for(var i = 0; i < productTypeListCnt; i++) {
                    $scope.productTypeList[i]['id'] = $scope.productTypeList[i]['_id'];
                }

                // autocomplete list
                $(document).ready(function() {
                    $("#productType").tokenInput($scope.productTypeList, {
                        minChars: 2,
                        propertyToSearch: 'name',
                        tokenValue: 'id',
                        resultsLimit: 10,
                        hintText: 'Type product type',
                        preventDuplicates: true,
                        theme: 'facebook'
                    });
                });
            }
        });
}

// get all main categories
function getAllMainCategories($scope, $http) {
    // list fetched once
    if($scope.mainCategoryListFetched) {
        return;
    }

    $scope.mainCategoryListFetched = true;
    $http.get('/category/getAllMainCategories')
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.mainCategoryList = response.data.data;
                var mainCategoryListCnt = ($scope.mainCategoryList).length;

                for(var i = 0; i < mainCategoryListCnt; i++) {
                    $scope.mainCategoryList[i]['id'] = $scope.mainCategoryList[i]['_id'];
                }

                // autocomplete list
                $(document).ready(function() {
                    $("#mainCategory").tokenInput($scope.mainCategoryList, {
                        minChars: 2,
                        propertyToSearch: 'name',
                        tokenValue: 'id',
                        resultsLimit: 10,
                        hintText: 'Type main category',
                        preventDuplicates: true,
                        theme: 'facebook'
                    });
                });
            }
        });
}

// get all sub categories
function getAllSubCategories($scope, $http) {
    // list fetched once
    if($scope.subCategoryListFetched) {
        return;
    }

    $scope.subCategoryListFetched = true;
    $http.get('/category/getAllOtherCategories')
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.subCategoryList = response.data.data;
                var subCategoryListCnt = ($scope.subCategoryList).length;

                for(var i = 0; i < subCategoryListCnt; i++) {
                    $scope.subCategoryList[i]['id'] = $scope.subCategoryList[i]['_id'];
                }

                // autocomplete list
                $(document).ready(function() {
                    $("#subCategory").tokenInput($scope.subCategoryList, {
                        minChars: 2,
                        propertyToSearch: 'name',
                        tokenValue: 'id',
                        resultsLimit: 10,
                        hintText: 'Type sub category',
                        preventDuplicates: true,
                        theme: 'facebook'
                    });
                });
            }
        });
}

// get all brands
function getAllBrands($scope, $http) {
    // list fetched once
    if($scope.brandListFetched) {
        return;
    }

    $scope.brandListFetched = true;
    $http.get('/brand/getAllActiveBrands')
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.brandList = response.data.data;
                var brandListCnt = ($scope.brandList).length;

                for(var i = 0; i < brandListCnt; i++) {
                    $scope.brandList[i]['id'] = $scope.brandList[i]['_id'];
                }

                // autocomplete list
                $(document).ready(function() {
                    $("#brand").tokenInput($scope.brandList, {
                        minChars: 2,
                        propertyToSearch: 'name',
                        tokenValue: 'id',
                        resultsLimit: 10,
                        hintText: 'Type brand',
                        preventDuplicates: true,
                        theme: 'facebook'
                    });
                });
            }
        });
}

// get all manufacturer
function getAllManufacturers($scope, $http) {
    // list fetched once
    if($scope.manufacturerListFetched) {
        return;
    }

    $scope.manufacturerListFetched = true;
    $http.get('/manufacturer/getAllActiveManufacturers')
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.manufacturerList = response.data.data;
                var manufacturerListCnt = ($scope.manufacturerList).length;

                for(var i = 0; i < manufacturerListCnt; i++) {
                    $scope.manufacturerList[i]['id'] = $scope.manufacturerList[i]['_id'];
                }

                // autocomplete list
                $(document).ready(function() {
                    $("#manufacturer").tokenInput($scope.manufacturerList, {
                        minChars: 2,
                        propertyToSearch: 'name',
                        tokenValue: 'id',
                        resultsLimit: 10,
                        hintText: 'Type manufacturer',
                        preventDuplicates: true,
                        theme: 'facebook'
                    });
                });
            }
        });
}

// get all departments
function getAllDepartments($scope, $http) {
    // list fetched once
    if($scope.departmentListFetched) {
        return;
    }

    $scope.departmentListFetched = true;
    $http.get('/department/getAllActiveDepartments')
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                $scope.departmentList = response.data.data;
                var departmentListCnt = ($scope.departmentList).length;

                for(var i = 0; i < departmentListCnt; i++) {
                    $scope.departmentList[i]['id'] = $scope.departmentList[i]['_id'];
                }

                // autocomplete list
                $(document).ready(function() {
                    $("#department").tokenInput($scope.departmentList, {
                        minChars: 2,
                        propertyToSearch: 'name',
                        tokenValue: 'id',
                        resultsLimit: 10,
                        hintText: 'Type department',
                        preventDuplicates: true,
                        theme: 'facebook'
                    });
                });
            }
        });
}

// get matched variants 
function getSearchedProductVariants() {
    $(document).ready(function() {
        $("#productVariant").tokenInput('/product/getSearchedProductVariantName', {
            method: 'POST',
            queryParam: 'searchTerm',
            minChars: 2,
            propertyToSearch: 'name',
            resultsLimit: 10,
            preventDuplicates: true,
            hintText: 'Type variant name',
            theme: 'facebook'
        });
    });
}

// show particluar search box on type select
function showTypeItemSearchBox($scope, $http) {
    var tmpType = $scope.type;
    $('#productTypeSearch, #mainCategorySearch, #subCategorySearch, #productVariantSearch, #departmentSearch, #brandSearch').hide();

    // product type
    if(tmpType == 1) {
        $('#productTypeSearch').show();
        getAllProductTypes($scope, $http);
    }
    // main category
    else if(tmpType == 2) {
        $('#mainCategorySearch').show();
        getAllMainCategories($scope, $http);
    }
    // sub category
    else if(tmpType == 3) {
        $('#subCategorySearch').show();
        getAllSubCategories($scope, $http);
    }
    // product variant
    else if(tmpType == 4) {
        $('#productVariantSearch').show();
        getSearchedProductVariants();
    }
    // department
    else if(tmpType == 5) {
        $('#departmentSearch').show();
        getAllDepartments($scope, $http);
    }
    // brand
    else if(tmpType == 6) {
        $('#brandSearch').show();
        getAllBrands($scope, $http);
    }
    // manufacturer
    else if(tmpType == 7) {
        $('#manufacturerSearch').show();
        getAllManufacturers($scope, $http);
    }
}

// add type item value in search box
function addTypeItemValue($scope, tmpType, typeItemDetails) {
    $('#productTypeSearch, #mainCategorySearch, #subCategorySearch, #productVariantSearch, #departmentSearch, #brandSearch').hide();
    var searchBoxId = '';

    // product type
    if(tmpType == 1) {
        searchBoxId = 'productType';
        $('#productTypeSearch').show();
    }
    // main category
    else if(tmpType == 2) {
        searchBoxId = 'mainCategory';
        $('#mainCategorySearch').show();
    }
    // sub category
    else if(tmpType == 3) {
        searchBoxId = 'subCategory';
        $('#subCategorySearch').show();
    }
    // product variant
    else if(tmpType == 4) {
        searchBoxId = 'productVariant';
        $('#productVariantSearch').show();
    }
    // department
    else if(tmpType == 5) {
        searchBoxId = 'department';
        $('#departmentSearch').show();
    }
    // brand
    else if(tmpType == 6) {
        searchBoxId = 'brand';
        $('#brandSearch').show();
    }
    // manufacturer
    else if(tmpType == 7) {
        searchBoxId = 'manufacturer';
        $('#manufacturerSearch').show();
    }

    // add
    $('#'+searchBoxId).tokenInput("clear");
    var typeItemDetailsCnt = typeItemDetails.length;
    for(var i = 0; i < typeItemDetailsCnt; i++) {
        $('#'+searchBoxId).tokenInput("add", {id: typeItemDetails[i]._id, name: typeItemDetails[i].name});
    }
}

// get selected type item value
function getTypeItemDetails($scope) {
    var tmpType = $scope.type;
    var tmpTypeItemDetails;

    // product type
    if(tmpType == 1) {
        tmpTypeItemDetails = $('#productType').tokenInput("get");
    }
    // main category
    else if(tmpType == 2) {
        tmpTypeItemDetails = $('#mainCategory').tokenInput("get");
    }
    // sub category
    else if(tmpType == 3) {
        tmpTypeItemDetails = $('#subCategory').tokenInput("get");
    }
    // product variant
    else if(tmpType == 4) {
        tmpTypeItemDetails = $('#productVariant').tokenInput("get");
    }
    // department
    else if(tmpType == 5) {
        tmpTypeItemDetails = $('#department').tokenInput("get");
    }
    // brand
    else if(tmpType == 6) {
        tmpTypeItemDetails = $('#brand').tokenInput("get");
    }
    // manufacturer
    else if(tmpType == 7) {
        tmpTypeItemDetails = $('#manufacturer').tokenInput("get");
    }

    // build object
    var typeItemDetails = new Array();
    var tmpTypeItemDetailsCnt = tmpTypeItemDetails.length;
    for(var i = 0; i < tmpTypeItemDetailsCnt; i++) {
        var tmpObj = new Object();
        tmpObj._id = tmpTypeItemDetails[i].id;
        tmpObj.name = tmpTypeItemDetails[i].name;
        typeItemDetails.push(tmpObj);
    }

    return typeItemDetails;
}

// Add offer
app.controller('OfferAddController', function ($timeout, $scope, $http, $location) {
    // check for access
    noAccessRedirect($location);

    // initialize datepickers
    initDatepicker('ADD');

    // init fetchs
    $scope.productTypeListFetched = false;
    $scope.mainCategoryListFetched = false;
    $scope.subCategoryListFetched = false;
    $scope.brandListFetched = false;
    $scope.departmentListFetched = false;
    $scope.manufacturerListFetched = false;

    // get product types    
    getAllProductTypes($scope, $http);

    // after type select
    $scope.showTypeItemSearchBox = function() {
        showTypeItemSearchBox($scope, $http);
    };

    // submit form
    $scope.submitForm = function () {
        // validations
        if(!validateForm('ADD', $scope)) {
            return false;
        }

        // get data
        var couponCode = $scope.couponCode;
        var validFrom = $('#validFrom').val();
        var validTill = $('#validTill').val();
        var type = $scope.type;        
        var discountType = $scope.discountType;
        var discountValue = $scope.discountValue;
        var minCartValue = $scope.minCartValue;
        var maxDiscountAmount = $scope.maxDiscountAmount;
        var validForUser = $scope.validForUser;
        var usageLimit = $scope.usageLimit;
        var status = $scope.status;
        var typeItemDetails = getTypeItemDetails($scope);
        var description = $scope.description;
        var termsConditions = $scope.termsConditions;

        // data 
        var data = {
            couponCode: couponCode,
            validFrom: validFrom,
            validTill: validTill,
            type: type,
            typeItemDetails: JSON.stringify(typeItemDetails),
            discountType: discountType,
            discountValue: discountValue,
            minCartValue: minCartValue,
            maxDiscountAmount: maxDiscountAmount,
            validForUser: validForUser,
            usageLimit: usageLimit,
            status: status,
            description: description,
            termsConditions: termsConditions
        };

        $http.post('/offer/insert', data)
        .success(function (data, status, headers, config) {
            // error on add
            if(data.error == 1) {
                showToastMsg(0, data.msg);
                return false;
            }
            showToastMsg(1, data.msg);

            $timeout(function () {
                $location.path('/offer');
            }, 2000);
        })
        .error(function (data, status, header, config) {
            showToastMsg(0, 'Unable to add coupon');
        });
    };
});

// Edit offer
app.controller('OfferEditController', function ($timeout, $scope, $http, $location, $routeParams) {
    // check for access
    noAccessRedirect($location);

    // check if offer id present in url
    if(typeof($routeParams.id) == 'undefined' || $routeParams.id == '') {
        $location.path('/offer');
        return;
    }

    // initialize datepickers
    initDatepicker('EDIT');

    // init fetchs
    $scope.productTypeListFetched = false;
    $scope.mainCategoryListFetched = false;
    $scope.subCategoryListFetched = false;
    $scope.brandListFetched = false;
    $scope.departmentListFetched = false;
    $scope.manufacturerListFetched = false;

    // after type select
    $scope.showTypeItemSearchBox = function() {
        showTypeItemSearchBox($scope, $http);
    };

    // get coupon details
    $http.get('/offer/get/'+$routeParams.id)
        .then(function (response) {
            if(response.data.error == 0 && response.data.data != '') {
                var offerDetails = response.data.data[0];
                $scope.couponCode = offerDetails.couponCode;
                $scope.type = (offerDetails.type).toString();
                $scope.discountType = offerDetails.discountType;
                $scope.discountValue = offerDetails.discountValue;
                $scope.minCartValue = offerDetails.minCartValue;
                $scope.maxDiscountAmount = offerDetails.maxDiscountAmount;
                $scope.validForUser = (offerDetails.validForUser).toString();
                $scope.usageLimit = offerDetails.usageLimit;
                $scope.status = (offerDetails.isActive).toString();
                $('#validFrom').datepicker('setDate', getDateYMD(offerDetails.validFrom));
                $('#validTill').datepicker('setDate', getDateYMD(offerDetails.validTill));
                $scope.description = offerDetails.description;
                $scope.termsConditions = offerDetails.termsConditions;
                
                // initiate search boxes
                showTypeItemSearchBox($scope, $http);

                // add type item
                var typeItemDetails = offerDetails.typeItemDetails;
                $timeout(function () {
                    addTypeItemValue($scope, $scope.type, typeItemDetails);
                }, 500);                
            }
        });

    // submit form
    $scope.submitForm = function () {
        // validations
        if(!validateForm('EDIT', $scope)) {
            return false;
        }

        // get data
        var couponCode = $scope.couponCode;
        var validFrom = $('#validFrom').val();
        var validTill = $('#validTill').val();
        var type = $scope.type;
        var discountType = $scope.discountType;
        var discountValue = $scope.discountValue;
        var minCartValue = $scope.minCartValue;
        var maxDiscountAmount = $scope.maxDiscountAmount;
        var validForUser = $scope.validForUser;
        var usageLimit = $scope.usageLimit;
        var status = $scope.status;
        var typeItemDetails = getTypeItemDetails($scope);
        var description = $scope.description;
        var termsConditions = $scope.termsConditions;

        // data 
        var data = {
            couponCode: couponCode,
            validFrom: validFrom,
            validTill: validTill,
            type: type,
            typeItemDetails: JSON.stringify(typeItemDetails),
            discountType: discountType,
            discountValue: discountValue,
            minCartValue: minCartValue,
            maxDiscountAmount: maxDiscountAmount,
            validForUser: validForUser,
            usageLimit: usageLimit,
            status: status,
            description: description,
            termsConditions: termsConditions
        };

        $http.put('/offer/update/'+$routeParams.id, data)
        .success(function (data, status, headers, config) {
            // error on add
            if(data.error == 1) {
                showToastMsg(0, data.msg);
                return false;
            }
            showToastMsg(1, data.msg);

            $timeout(function () {
                $location.path('/offer');
            }, 2000);
        })
        .error(function (data, status, header, config) {
            showToastMsg(0, 'Unable to update coupon');
        });
    };
});

})(); // End of closure