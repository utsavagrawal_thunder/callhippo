/*
 * Angular app initialise
 */

angular.module('medibridge-admin', ['app_dep']);
var app = angular.module('app_dep', ['ngRoute', 'ngValidate', 'dropzone', 'ngSanitize', 'ngCsv', 'ngTable', 'dndLists']);

// Dropzone directive
angular.module('dropzone', []).directive('dropzone', function () {
    return function (scope, element, attrs) {
        var config, dropzone;

        config = scope[attrs.dropzone];

        // create a Dropzone for the element with the given options
        dropzone = new Dropzone(element[0], config.options);

        // bind the given event handlers
        angular.forEach(config.eventHandlers, function (handler, event) {
            dropzone.on(event, handler);
        });
    };
});

// template cache
app.factory('$templateCache', function ($cacheFactory, $http, $injector) {
    var cache = $cacheFactory('templates');
    var allTplPromise;

    return {
        get: function (url) {
            var fromCache = cache.get(url);

            // already have required template in the cache
            if (fromCache) {
                return fromCache;
            }

            // first template request ever - get the all tpl file
            if (!allTplPromise) {
                allTplPromise = $http.get('pages/all-templates.ejs').then(function (response) {
                    // compile the response, which will put stuff into the cache
                    $injector.get('$compile')(response.data);
                    return response;
                });
            }

            // return the all-tpl promise to all template requests
            return allTplPromise.then(function (response) {
                return {
                    status: response.status,
                    data: cache.get(url)
                };
            });
        },
        put: function (key, value) {
            cache.put(key, value);
        }
    };
});

app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider

                .when('/admin', {
                    templateUrl: 'layout.ejs',
                    controller: 'HomeController'
                })

                .when('/about', {
                    templateUrl: 'pages/about.ejs',
                    controller: 'CategoryController'
                })

                .when('/category/add/', {
                    templateUrl: 'pages/category/add.ejs',
                    controller: 'CategoryAddController'
                })

                .when('/category/', {
                    templateUrl: 'pages/category/list.ejs',
                    controller: 'CategoryListController'
                })
                
                .when('/category/edit/:id/', {
                    templateUrl: 'pages/category/edit.ejs',
                    controller: 'CategoryEditController'
                })

                .when('/filter/add/', {
                    templateUrl: 'pages/filter/add.ejs',
                    controller: 'FilterAddController'
                })

                .when('/filter', {
                    templateUrl: 'pages/filter/list.ejs',
                    controller: 'FilterListController'
                })

                .when('/filter/edit/:id', {
                    templateUrl: 'pages/filter/edit.ejs',
                    controller: 'FilterEditController'
                })

                .when('/brand/add/', {
                    templateUrl: 'pages/brand/add.ejs',
                    controller: 'BrandAddController'
                })

                .when('/brand', {
                    templateUrl: 'pages/brand/list.ejs',
                    controller: 'BrandListController'
                })

                .when('/brand/edit/:id', {
                    templateUrl: 'pages/brand/edit.ejs',
                    controller: 'BrandEditController'
                })

                .when('/product/add/', {
                    templateUrl: 'pages/product/add.ejs',
                    controller: 'ProductAddController'
                })

                .when('/product/add/:id', {
                    templateUrl: 'pages/product/add.ejs',
                    controller: 'ProductAddController'
                })

                .when('/product/', {
                    templateUrl: 'pages/product/list.ejs',
                    controller: 'ProductListController'
                })

                .when('/product/edit/:productId/:variantId', {
                    templateUrl: 'pages/product/edit.ejs',
                    controller: 'ProductEditController'
                })

                .when('/product/edit-supplier/:productId/:variantId/:supplierId', {
                    templateUrl: 'pages/product/edit-supplier.ejs',
                    controller: 'ProductEditSupplierController'
                })

                .when('/product/supplier/', {
                    templateUrl: 'pages/product/list-supplier.ejs',
                    controller: 'ProductSupplierListController'
                })

                .when('/manufacturer/add/', {
                    templateUrl: 'pages/manufacturer/add.ejs',
                    controller: 'ManufacturerAddController'
                })

                .when('/manufacturer/', {
                    templateUrl: 'pages/manufacturer/list.ejs',
                    controller: 'ManufacturerListController'
                })

                .when('/manufacturer/edit/:id', {
                    templateUrl: 'pages/manufacturer/edit.ejs',
                    controller: 'ManufacturerEditController'
                })

                 .when('/user/add/', {
                    templateUrl: 'pages/user/add.ejs',
                    controller: 'UserAddController'
                })
                
                .when('/user/', {
                    templateUrl: 'pages/user/list.ejs',
                    controller: 'UserListController'
                })

                .when('/user/edit/:id', {
                    templateUrl: 'pages/user/edit.ejs',
                    controller: 'UserEditController'
                })

                .when('/relations/create', {
                    templateUrl: 'pages/relations/create.ejs',
                    controller: 'RelationsCreateController'
                })

                .when('/department/add/', {
                    templateUrl: 'pages/department/add.ejs',
                    controller: 'DepartmentAddController'
                })

                .when('/department/edit/:id', {
                    templateUrl: 'pages/department/edit.ejs',
                    controller: 'DepartmentEditController'
                })
                
                .when('/department/', {
                    templateUrl: 'pages/department/list.ejs',
                    controller: 'DepartmentListController'
                })

                .when('/offer/add/', {
                    templateUrl: 'pages/offer/add.ejs',
                    controller: 'OfferAddController'
                })

                .when('/offer', {
                    templateUrl: 'pages/offer/list.ejs',
                    controller: 'OfferListController'
                })

                .when('/offer/edit/:id', {
                    templateUrl: 'pages/offer/edit.ejs',
                    controller: 'OfferEditController'
                })

                .when('/supplier/basicinfo/:supplierId', {
                    templateUrl: 'pages/supplier/basicinfo.ejs',
                    controller: 'SupplierEditController'
                })
                
                .when('/supplier/businessreg/:supplierId',{
                    templateUrl: 'pages/supplier/businessreg.ejs',
                    controller: 'SupplierBusinessRegController'                    
                })
                
                .when('/supplier/companydetails/:supplierId',{
                  templateUrl: 'pages/supplier/companydetails.ejs',
                  controller: 'SupplierCompanyDetailsController'  
                })

                .when('/supplier/bankdetails/:supplierId',{
                  templateUrl: 'pages/supplier/bankdetails.ejs',
                  controller: 'SupplierBankDetailsController'  
                })
                
                .when('/supplier/businessref/:supplierId', {
                    templateUrl: 'pages/supplier/businessref.ejs',
                    controller: 'SupplierBusinessRefController'
                })

                .when('/supplier/addresses/:supplierId', {
                    templateUrl: 'pages/supplier/addresses.ejs',
                    controller: 'SupplierAddressesController'
                })

                .when('/supplier/rmdetails/:supplierId',{
                  templateUrl: 'pages/supplier/rmdetails.ejs',
                  controller: 'SupplierRMDetailsController'  
                })
                                                
                .when('/supplier/', {
                    templateUrl: 'pages/supplier/list.ejs',
                    controller: 'SupplierListController'
                })

                .when('/supplier/addsupplier/', {
                    templateUrl: 'pages/supplier/addsupplier.ejs',
                    controller: 'SupplierAddNewController'
                })

                .when('/buyers/', {
                    templateUrl: 'pages/buyer/list.ejs',
                    controller: 'BuyerListController'
                })

                .when('/buyer/edit/:buyerId', {
                    templateUrl: 'pages/buyer/edit.ejs',
                    controller: 'BuyerEditController'
                })

                .when('/buyer/addbuyer/', {
                    templateUrl: 'pages/buyer/addbuyer.ejs',
                    controller: 'BuyerAddNewController'
                }) 

                .when('/orders/buyers/all', {
                    templateUrl: 'pages/orders/allBuyers.ejs',
                    controller: 'AllBuyersOrdersController'
                })

                .when('/orders/buyer/:buyerId/:buyerCode', {
                    templateUrl: 'pages/orders/buyer.ejs',
                    controller: 'BuyerOrdersController'
                })

                .when('/orders/detail/:orderId/:supplierId?', {
                    templateUrl: 'pages/orders/orderDetails.ejs',
                    controller: 'OrderDetailsController'
                })

                .when('/orders/suppliers/all', {
                    templateUrl: 'pages/orders/allSuppliers.ejs',
                    controller: 'AllSuppliersOrdersController'
                })

                .when('/orders/supplier/:supplierId/:supplierCode', {
                    templateUrl: 'pages/orders/supplier.ejs',
                    controller: 'SupplierOrdersController'
                })

                .when('/reviews', {
                    templateUrl: 'pages/review/allReviews.ejs',
                    controller: 'ReviewController'
                })

                .when('/no-access', {
                    templateUrl: 'pages/no-access.ejs',
                    controller: ''
                })
                
//***************************Promotions(Home) page ************************************
                
                .when('/promotions/', {
                    templateUrl: 'pages/home/create.ejs',
                    controller: 'PromotionsAddController'
                })
                
                .when('/promotions/tab1', {
                    templateUrl: 'pages/home/tab1.ejs',
                    controller: 'PromotionsTab1Controller'
                })
                
                .when('/promotions/tab2', {
                    templateUrl: 'pages/home/tab2.ejs',
                    controller: 'PromotionsTab2Controller'
                })
                
                .when('/promotions/tab3', {
                    templateUrl: 'pages/home/tab3.ejs',
                    controller: 'PromotionsTab3Controller'
                })
                
                .when('/promotions/tab4', {
                    templateUrl: 'pages/home/tab4.ejs',
                    controller: 'PromotionsTab4Controller'
                })
                
                .when('/promotions/tab5', {
                    templateUrl: 'pages/home/tab5.ejs',
                    controller: 'PromotionsTab5Controller'
                })
                
                .when('/promotions/tab6', {
                    templateUrl: 'pages/home/tab6.ejs',
                    controller: 'PromotionsTab6Controller'
                })
                
                .when('/promotions/tab7', {
                    templateUrl: 'pages/home/tab7.ejs',
                    controller: 'PromotionsAdd7Controller'
                })

//***************************Promotions(Home) page END************************************

                .when('/shopping-list/', {
                    templateUrl: 'pages/shopping-list/list.ejs',
                    controller: 'ShoppingListController'
                })

                .when('/shopping-list/suppliers/:orderId', {
                    templateUrl: 'pages/shopping-list/suppliers.ejs',
                    controller: 'ShoppingListSupplierController'
                })

                .when('/shopping-list/suppliers/products/:orderId/:supplierCode', {
                    templateUrl: 'pages/shopping-list/products.ejs',
                    controller: 'ShoppingListSupplierProductsController'
                })

                .when('/sales/', {
                    templateUrl: 'pages/sales/all.ejs',
                    controller: 'AllSalesController'
                })

                .when('/sales/supplier/', {
                    templateUrl: 'pages/sales/supplier.ejs',
                    controller: 'SupplierSalesController'
                })
                
//                ***************************category promotions page ************************************
                
                .when('/categoryPromotions/', {
                    templateUrl: 'pages/categoryPromotions/list.ejs',
                    controller: 'CategoryPromotionsListController'
                })
                
                .when('/categoryPromotions/divider1/:id/', {
                    templateUrl: 'pages/categoryPromotions/divider1.ejs',
                    controller: 'CategoryPromotionsTab1Controller'
                })
                
                .when('/categoryPromotions/divider2/:id/', {
                    templateUrl: 'pages/categoryPromotions/divider2.ejs',
                    controller: 'CategoryPromotionsTab2Controller'
                })
                .when('/categoryPromotions/divider3/:id/', {
                    templateUrl: 'pages/categoryPromotions/divider3.ejs',
                    controller: 'CategoryPromotionsTab3Controller'
                })
                .when('/categoryPromotions/divider4/:id/', {
                    templateUrl: 'pages/categoryPromotions/divider4.ejs',
                    controller: 'CategoryPromotionsTab4Controller'
                })
                .when('/categoryPromotions/divider5/:id/', {
                    templateUrl: 'pages/categoryPromotions/divider5.ejs',
                    controller: 'CategoryPromotionsTab5Controller'
                })
                .when('/categoryPromotions/divider6/:id/', {
                    templateUrl: 'pages/categoryPromotions/divider6.ejs',
                    controller: 'CategoryPromotionsTab6Controller'
                })
                .when('/categoryPromotions/divider7/:id/', {
                    templateUrl: 'pages/categoryPromotions/divider7.ejs',
                    controller: 'CategoryPromotionsTab7Controller'
                })

//                ***************************category promotions page END************************************

                .otherwise({redirectTo: '/admin/'});


        // enable html5Mode for pushstate ('#'-less URLs)
        // $locationProvider.html5Mode(true);
        // $locationProvider.hashPrefix('!');

    }]);
//angular.module('medibridge-admin').run(function ($templateCache){
//  $templateCache.put('pages/about.ejs', '');
//});
app.controller('HomeController', function ($scope) {
    console.log("Home controller called");
    $scope.message = 'Hello from Home Controller';
});

app.controller('CategoryController', function ($scope) {
    console.log("category controller called");
    $scope.message = 'Hello from Category Controller';
});


// no access check
function noAccessRedirect($location) {
    $(document).ready(function() {
        var noAccessRedirect = $('#noAccessRedirect').val();
        if(noAccessRedirect == -1) {
            // $window.location.href = '/admin/';
            $location.path('/no-access');
            return;
        }
    });
}
